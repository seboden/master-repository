the_dacke_feud = {
	potential = {
		tag = SWE
		normal_or_historical_nations = yes
		is_subject = no
		uses_doom = no
		NOT = { has_country_flag = had_the_dacke_feud }
		any_owned_province = {
			region = swedish_region
		}
	}


	can_start = {
		has_any_disaster = no
		religion = protestant
		has_country_modifier = vasa_centralization_reforms
	}
	
	
	can_stop = {
		OR = {
			has_any_disaster = yes
			NOT = { religion = protestant }
		}
	}

	
	
	progress = {
		modifier = {
			factor = 1
			NOT = { stability = 1 }
		}
		modifier = {
			factor = 1
			war_exhaustion = 4
		}
	}




	
	can_end = {
		NOT = {
			has_spawned_rebels = particularist_rebels
		}
		stability = 1
	}
	



	modifier = {
		global_unrest = 5
	}	
	
	
	on_start = the_dacke_feud.1
	on_end = the_dacke_feud.100
	
	on_monthly = {
		events = {
		}
		random_events = { 
			1000 = 0
			100 = the_dacke_feud.2
			100 = the_dacke_feud.3
			10 = centralisation.99
		}
	}
}

