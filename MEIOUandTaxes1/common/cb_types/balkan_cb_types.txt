# unite Wallachia
cb_de_jure_wallachia = {
	ai_peace_desire = -10
	prerequisites = {
		tag = WAL
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				OR = {
					area = oltenia_area
					area = muntenia_area
					province_id = 159 
					province_id = 3782 
					province_id = 2380
				}
			}
			
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_form_personal_union
		po_change_government
		po_change_religion
		po_become_vassal
		po_become_protectorate
	}
	
	war_goal = wg_de_jure_wallachia
}

# unite Moldavia
cb_de_jure_moldavia = {
	ai_peace_desire = -10
	prerequisites = {
		tag = MOL
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				area = moldavia_area
			}
			
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_form_personal_union
		po_change_government
		po_change_religion
		po_become_vassal
		po_become_protectorate
	}
	
	war_goal = wg_de_jure_moldavia
}

# unite Hungary
cb_de_jure_hungary = {
	ai_peace_desire = -10
	prerequisites = {
		OR = {
			tag = HUN
			culture_group = magyar
			AND = {
				tag = TRA
				government_rank = 5
			}
			has_country_flag = king_of_hungary_outside
		}
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				province_group = kingdom_of_hungary_group
			}
			
		}
		is_neighbor_of = FROM
	}
	
	attacker_disabled_po = {
		po_change_government
		po_change_religion
	}
	
	war_goal = wg_de_jure_hungary
}
# unite Bulgaria
cb_de_jure_bulgaria = {
	ai_peace_desire = -10
	prerequisites = {
		tag = BUL
		adm_tech = 12
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				OR = {
					area = bulgaria_area
					area = dobrogea_area
					area = vidin_area 
					area = macedonia_area 					
					province_id = 2501 
					province_id = 147
				}
			}
			
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_form_personal_union
		po_change_government
		po_change_religion
		po_become_vassal
		po_become_protectorate
	}
	
	war_goal = wg_de_jure_bulgaria
}
# unite Serbia
cb_de_jure_serbia = {
	ai_peace_desire = -10
	prerequisites = {
		tag = SER
		adm_tech = 12
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				OR = {
					area = macedonia_area
					area = serbia_area	
					province_id = 147
					province_id = 3776
				}
			}
			
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_form_personal_union
		po_change_government
		po_change_religion
		po_become_vassal
		po_become_protectorate
	}
	
	war_goal = wg_de_jure_serbia
}
# unite Croatia
cb_de_jure_croatia = {
	ai_peace_desire = -10
	prerequisites = {
		tag = CRO
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				OR = {
					area = croatia_area
					area = east_adriatic_coast_area	
					province_id = 2389
				}
			}
			
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_form_personal_union
		po_change_government
		po_change_religion
		po_become_vassal
		po_become_protectorate
	}
	
	war_goal = wg_de_jure_croatia
}

# unite Bosnia
cb_de_jure_bosnia = {
	ai_peace_desire = -10
	prerequisites = {
		tag = BOS
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				area = bosnia_area
			}
			
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_form_personal_union
		po_change_government
		po_change_religion
		po_become_vassal
		po_become_protectorate
	}
	
	war_goal = wg_de_jure_bosnia
}

# unite Albania
cb_de_jure_albania = {
	ai_peace_desire = -10
	prerequisites = {
		OR = {
			tag = ALB
			tag = ALC
		}
		OR = { singleplayer_trigger = yes ai = yes }
		FROM = {
			NOT = { is_subject_of = ROOT }
			NOT = { overlord_of = ROOT }
			any_owned_province = {
				area = albania_area
			}
			
		}
		is_neighbor_of = FROM
	}
	attacker_disabled_po = {
		po_form_personal_union
		po_change_government
		po_change_religion
		po_become_vassal
		po_become_protectorate
	}
	
	war_goal = wg_de_jure_albania
}