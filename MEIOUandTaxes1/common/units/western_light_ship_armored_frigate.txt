#56 - Armored Frigate (HMS Warrior, 1860)
type = light_ship

hull_size = 64 #6109 tonnage
base_cannons = 75 
sail_speed = 14 #steam
trade_power = 10

sailors = 400

sprite_level = 5 #