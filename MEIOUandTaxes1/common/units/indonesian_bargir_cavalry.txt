# Bargir Raiders (27)

unit_type = austranesian
type = cavalry
maneuver = 2

offensive_morale = 7
defensive_morale = 3
offensive_fire = 3
defensive_fire = 2
offensive_shock = 4
defensive_shock = 1

