#45 - Three Masted Xebec
type = galley

hull_size = 21
base_cannons = 19 
sail_speed = 8

sailors = 310

trigger = {
	NOT = { technology_group = western }
	NOT = { technology_group = eastern }
	}
	