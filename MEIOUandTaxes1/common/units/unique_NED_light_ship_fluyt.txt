#15 - Fluyt
type = light_ship

hull_size = 22 #60 tonnage
base_cannons = 25 #2-8 cannons per caravel
sail_speed = 8 #top speed 8 knots
trade_power = 4.2 #BONUS

sailors = 72

sprite_level = 3

trigger = { 
	primary_culture = dutch
	}
