# steppestech Medium Hussar (40)

type = cavalry
unit_type = steppestech
maneuver = 2

offensive_morale = 4
defensive_morale = 5
offensive_fire = 4
defensive_fire = 3
offensive_shock = 6
defensive_shock = 4
