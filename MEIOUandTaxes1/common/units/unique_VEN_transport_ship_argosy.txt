#40 - East India Man (Doddington, 1748)
type = transport

hull_size = 16 #600-1500 tons burthen (/30)
base_cannons = 9 #26 guns
sail_speed = 5 #4-5 knots
trade_power = 1.0 #BONUS

sailors = 36

sprite_level = 5

trigger = { 
	OR = {
		primary_culture = venetian
		culture_group = balkan_slavic
		}
	is_colonial_nation = no
	}