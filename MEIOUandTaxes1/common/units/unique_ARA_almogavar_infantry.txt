#0 - Western Halberd Infantry - Almogavar

type = infantry
unit_type = western
maneuver = 1

offensive_morale = 2 #BONUS
defensive_morale = 1
offensive_fire = 0
defensive_fire = 0
offensive_shock = 2
defensive_shock = 2

trigger = {
	culture_group = catalan_group
	}