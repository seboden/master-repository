#4 - Cog - Baghlah
type = transport

hull_size = 7 #140 tonnage
base_cannons = 3 #4 guns
sail_speed = 4 #4-5 knots in a good wind

sailors = 16

sprite_level = 1

trigger = { 
	OR = {
		technology_group = muslim
		technology_group = turkishtech
		technology_group = high_turkishtech
		technology_group = nomad_group
		technology_group = steppestech
		technology_group = soudantech
		technology_group = sub_saharan
		technology_group = indian
		technology_group = hawaii_tech
		}
	is_colonial_nation = no
	}
	