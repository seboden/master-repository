#27 - Heavy Galleon (Sao Martinho, 1580)
type = heavy_ship

hull_size = 58 #600 tons (/15)
base_cannons = 56
sail_speed = 8 #8 knots

sailors = 310

sprite_level = 3

trigger = { 
	OR = { 
		technology_group = indian  # DLC use separate indian unit
		technology_group = chinese # DLC use separate indian unit
		technology_group = austranesian # DLC use separate indian unit
		technology_group = hawaii_tech # DLC use separate indian unit
		technology_group = mongol_tech  # DLC use separate indian unit
		technology_group = south_american # DLC use separate indian unit
		technology_group = mesoamerican # DLC use separate indian unit
		technology_group = north_american  # DLC use separate indian unit
	}
	OR = { 
		has_idea_group = grand_fleet_ideas
		AND = { 
			has_idea_group = naval_ideas
			has_country_modifier = western_arms_trade
		}
	}
	NOT = { tag = SPA }
	NOT = { technology_group = muslim } # DLC use separate muslim unit
	NOT = { technology_group = turkishtech } # DLC use separate muslim unit
	NOT = { technology_group = high_turkishtech } # DLC use separate muslim unit
	NOT = { technology_group = nomad_group } # DLC use separate muslim unit
	NOT = { technology_group = steppestech } # DLC use separate muslim unit
	NOT = { technology_group = soudantech } # DLC use separate muslim unit
	NOT = { technology_group = sub_saharan } # DLC use separate muslim unit
	NOT = { technology_group = western } # DLC use separate standard unit
	NOT = { technology_group = eastern } # DLC use separate standard unit
}
	
	
