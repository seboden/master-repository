# Western Gallop Cavalry - Hakkapeliitta (30)

type = cavalry
unit_type = western
maneuver = 2

offensive_morale = 6
defensive_morale = 3
offensive_fire = 2
defensive_fire = 1
offensive_shock = 6 #BONUS
defensive_shock = 4

trigger = { 
	OR = { 
		culture_group = swedish_group 
		tag = KAL
		}
	}