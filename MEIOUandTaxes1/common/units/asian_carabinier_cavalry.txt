#Carabinier Cavalry (40)

type = cavalry
unit_type = chinese
maneuver = 2

offensive_morale = 6
defensive_morale = 4
offensive_fire = 5
defensive_fire = 3
offensive_shock = 4
defensive_shock = 4
