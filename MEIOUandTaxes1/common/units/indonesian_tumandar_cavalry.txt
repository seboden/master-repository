# Tumandar Cavalry (15)

unit_type = austranesian
type = cavalry
maneuver = 2

offensive_morale = 3
defensive_morale = 4
offensive_fire = 0
defensive_fire = 0
offensive_shock = 3
defensive_shock = 4

trigger = {
	OR = {
		religion_group = muslim
		religion = sikhism
	}
}
