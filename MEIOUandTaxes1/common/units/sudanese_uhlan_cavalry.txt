#45 - Uhlans

type = cavalry
unit_type = soudantech
maneuver = 2

offensive_morale = 7
defensive_morale = 4
offensive_fire = 4
defensive_fire = 3
offensive_shock = 7
defensive_shock = 3

trigger = {
	any_owned_province = { #Tsetse flies and jungle
		NOT = { region = central_africa_region }
		NOT = { region = kongo_region }
		NOT = { region = south_africa_region }
		NOT = { region = east_africa_region }
		}
	}
