# Carbine Cavalry (28)

unit_type = north_american
type = cavalry

maneuver = 2
offensive_morale = 5
defensive_morale = 2
offensive_fire =   4
defensive_fire =   2
offensive_shock =  5
defensive_shock =  2
