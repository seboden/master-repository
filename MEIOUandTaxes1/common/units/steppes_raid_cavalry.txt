#Raid Cavalry (24)

type = cavalry
unit_type = steppestech
maneuver = 2

offensive_morale = 2
defensive_morale = 4
offensive_fire = 2
defensive_fire = 4
offensive_shock = 2
defensive_shock = 4
