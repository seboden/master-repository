
kingdom_of_sevilla = {
	local_manpower_modifier = -0.3
	local_tax_modifier = -0.3
	local_production_efficiency = -0.3
	province_trade_power_modifier = -0.15
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_missionary_strength = -0.03
	local_unrest = -6
	
	picture = "coa_sevilla_reino"
}

kingdom_of_toledo = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_missionary_strength = -0.03
	local_unrest = -4.5
	
	picture = "coa_toledo_reino"
}

kingdom_of_valencia = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_missionary_strength = -0.03
	local_unrest = -4.5
	
	picture = "coa_valencia_reino"
}

kingdom_of_catalonia = {
	local_manpower_modifier = -0.15
	local_tax_modifier = -0.15
	local_production_efficiency = -0.15
	province_trade_power_modifier = -0.05
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_missionary_strength = -0.03
	local_unrest = -4.5
	
	picture = "coa_catalonia_reino"
}


kingdom_of_cordoba = {
	local_manpower_modifier = -0.3
	local_tax_modifier = -0.3
	local_production_efficiency = -0.3
	province_trade_power_modifier = -0.15
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_missionary_strength = -0.03
	local_unrest = -6
	
	picture = "coa_cordoba_reino"
}

kingdom_of_jaen = {
	local_manpower_modifier = -0.3
	local_tax_modifier = -0.3
	local_production_efficiency = -0.3
	province_trade_power_modifier = -0.15
	regiment_recruit_speed = 1.0
	ship_recruit_speed = 1.0
	local_missionary_strength = -0.03
	local_unrest = -6
	
	picture = "coa_jaen_reino"
}
