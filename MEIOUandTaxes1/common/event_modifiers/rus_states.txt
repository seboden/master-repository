#minor Russian princes in the south were more under the control of the horde

# Kiev
principality_of_kiev = {
	local_tax_modifier = -0.80
	local_manpower_modifier = -0.80

	picture = "coa_kievan_estate"
}
# Chernigov
principality_of_chernigov = {
	local_tax_modifier = -0.80
	local_manpower_modifier = -0.80
	
	picture = "coa_chernigov_estate"
}