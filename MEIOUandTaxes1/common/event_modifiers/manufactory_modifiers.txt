university_1 = {
	technology_cost = -0.01
	idea_cost = -0.005
	army_tradition_decay = -0.001
	navy_tradition_decay = -0.001
}

university_2 = {
	technology_cost = -0.02
	idea_cost = -0.01
	army_tradition_decay = -0.002
	navy_tradition_decay = -0.002
}

university_3 = {
	technology_cost = -0.03
	idea_cost = -0.015
	army_tradition_decay = -0.003
	navy_tradition_decay = -0.003
}

university_4 = {
	technology_cost = -0.04
	idea_cost = -0.02
	army_tradition_decay = -0.004
	navy_tradition_decay = -0.004
}

university_5 = {
	technology_cost = -0.05
	idea_cost = -0.025
	army_tradition_decay = -0.005
	navy_tradition_decay = -0.005
}

university_6 = {
	technology_cost = -0.06
	idea_cost = -0.03
	army_tradition_decay = -0.006
	navy_tradition_decay = -0.006
}

university_7 = {
	technology_cost = -0.07
	idea_cost = -0.035
	army_tradition_decay = -0.007
	navy_tradition_decay = -0.007
}

university_8 = {
	technology_cost = -0.08
	idea_cost = -0.04
	army_tradition_decay = -0.008
	navy_tradition_decay = -0.008
}

university_9 = {
	technology_cost = -0.09
	idea_cost = -0.045
	army_tradition_decay = -0.009
	navy_tradition_decay = -0.009
}

university_10 = {
	technology_cost = -0.1
	idea_cost = -0.05
	army_tradition_decay = -0.01
	navy_tradition_decay = -0.01
}

university_11 = {
	technology_cost = -0.105
	idea_cost = -0.0525
	army_tradition_decay = -0.0105
	navy_tradition_decay = -0.0105
}

university_12 = {
	technology_cost = -0.11
	idea_cost = -0.055
	army_tradition_decay = -0.011
	navy_tradition_decay = -0.011
}

university_13 = {
	technology_cost = -0.115
	idea_cost = -0.0575
	army_tradition_decay = -0.0115
	navy_tradition_decay = -0.0115
}

university_14 = {
	technology_cost = -0.12
	idea_cost = -0.06
	army_tradition_decay = -0.012
	navy_tradition_decay = -0.012
}

university_15 = {
	technology_cost = -0.125
	idea_cost = -0.0625
	army_tradition_decay = -0.0125
	navy_tradition_decay = -0.0125
}

university_16 = {
	technology_cost = -0.13
	idea_cost = -0.065
	army_tradition_decay = -0.0130
	navy_tradition_decay = -0.0130
}

university_17 = {
	technology_cost = -0.135
	idea_cost = -0.0675
	army_tradition_decay = -0.0135
	navy_tradition_decay = -0.0135
}

university_18 = {
	technology_cost = -0.14
	idea_cost = -0.07
	army_tradition_decay = -0.014
	navy_tradition_decay = -0.014
}

university_19 = {
	technology_cost = -0.145
	idea_cost = -0.0725
	army_tradition_decay = -0.0145
	navy_tradition_decay = -0.0145
}

university_20 = {
	technology_cost = -0.15
	idea_cost = -0.075
	army_tradition_decay = -0.015
	navy_tradition_decay = -0.015
}

wharf_1 = {
	dip_tech_cost_modifier = -0.01
	heavy_ship_cost = -0.03
	light_ship_cost = -0.03
	transport_cost = -0.03
}

wharf_2 = {
	dip_tech_cost_modifier = -0.02
	heavy_ship_cost = -0.05
	light_ship_cost = -0.05
	transport_cost = -0.05
}

wharf_3 = {
	dip_tech_cost_modifier = -0.03
	heavy_ship_cost = -0.0633
	light_ship_cost = -0.0633
	transport_cost = -0.0633
}

wharf_4 = {
	dip_tech_cost_modifier = -0.04
	heavy_ship_cost = -0.0722
	light_ship_cost = -0.0722
	transport_cost = -0.0722
}

wharf_5 = {
	dip_tech_cost_modifier = -0.05
	heavy_ship_cost = -0.0781
	light_ship_cost = -0.0781
	transport_cost = -0.0781
}

wharf_6 = {
	dip_tech_cost_modifier = -0.06
	heavy_ship_cost = -0.0821
	light_ship_cost = -0.0821
	transport_cost = -0.0821
}

wharf_7 = {
	dip_tech_cost_modifier = -0.07
	heavy_ship_cost = -0.0847
	light_ship_cost = -0.0847
	transport_cost = -0.0847
}

wharf_8 = {
	dip_tech_cost_modifier = -0.08
	heavy_ship_cost = -0.0865
	light_ship_cost = -0.0865
	transport_cost = -0.0865
}

wharf_9 = {
	dip_tech_cost_modifier = -0.09
	heavy_ship_cost = -0.0877
	light_ship_cost = -0.0877
	transport_cost = -0.0877
}

wharf_10 = {
	dip_tech_cost_modifier = -0.1
	heavy_ship_cost = -0.0884
	light_ship_cost = -0.0884
	transport_cost = -0.0884
}

wharf_11 = {
	dip_tech_cost_modifier = -0.105
	heavy_ship_cost = -0.089
	light_ship_cost = -0.089
	transport_cost = -0.089
}

wharf_12 = {
	dip_tech_cost_modifier = -0.11
	heavy_ship_cost = -0.0893
	light_ship_cost = -0.0893
	transport_cost = -0.0893
}

wharf_13 = {
	dip_tech_cost_modifier = -0.115
	heavy_ship_cost = -0.0895
	light_ship_cost = -0.0895
	transport_cost = -0.0895
}

wharf_14 = {
	dip_tech_cost_modifier = -0.12
	heavy_ship_cost = -0.0897
	light_ship_cost = -0.0897
	transport_cost = -0.0897
}

wharf_15 = {
	dip_tech_cost_modifier = -0.125
	heavy_ship_cost = -0.0898
	light_ship_cost = -0.0898
	transport_cost = -0.0898
}

wharf_16 = {
	dip_tech_cost_modifier = -0.13
	heavy_ship_cost = -0.0899
	light_ship_cost = -0.0899
	transport_cost = -0.0899
}

wharf_17 = {
	dip_tech_cost_modifier = -0.135
	heavy_ship_cost = -0.0899
	light_ship_cost = -0.0899
	transport_cost = -0.0899
}

wharf_18 = {
	dip_tech_cost_modifier = -0.14
	heavy_ship_cost = -0.0899
	light_ship_cost = -0.0899
	transport_cost = -0.0899
}

wharf_19 = {
	dip_tech_cost_modifier = -0.145
	heavy_ship_cost = -0.09
	light_ship_cost = -0.09
	transport_cost = -0.09
}

wharf_20 = {
	dip_tech_cost_modifier = -0.15
	heavy_ship_cost = -0.09
	light_ship_cost = -0.09
	transport_cost = -0.09
}

textile_1 = {
	adm_tech_cost_modifier = -0.01
}

textile_2 = {
	adm_tech_cost_modifier = -0.02
}

textile_3 = {
	adm_tech_cost_modifier = -0.03
}

textile_4 = {
	adm_tech_cost_modifier = -0.04
}

textile_5 = {
	adm_tech_cost_modifier = -0.05
}

textile_6 = {
	adm_tech_cost_modifier = -0.06
}

textile_7 = {
	adm_tech_cost_modifier = -0.07
}

textile_8 = {
	adm_tech_cost_modifier = -0.08
}

textile_9 = {
	adm_tech_cost_modifier = -0.09
}

textile_10 = {
	adm_tech_cost_modifier = -0.1
}

textile_11 = {
	adm_tech_cost_modifier = -0.105
}

textile_12 = {
	adm_tech_cost_modifier = -0.11
}

textile_13 = {
	adm_tech_cost_modifier = -0.115
}

textile_14 = {
	adm_tech_cost_modifier = -0.120
}

textile_15 = {
	adm_tech_cost_modifier = -0.125
}

textile_16 = {
	adm_tech_cost_modifier = -0.13
}

textile_17 = {
	adm_tech_cost_modifier = -0.135
}

textile_18 = {
	adm_tech_cost_modifier = -0.14
}

textile_19 = {
	adm_tech_cost_modifier = -0.145
}

textile_20 = {
	adm_tech_cost_modifier = -0.15
}

refinery_1 = {
	dip_tech_cost_modifier = -0.01
}

refinery_2 = {
	dip_tech_cost_modifier = -0.02
}

refinery_3 = {
	dip_tech_cost_modifier = -0.03
}

refinery_4 = {
	dip_tech_cost_modifier = -0.04
}

refinery_5 = {
	dip_tech_cost_modifier = -0.05
}

refinery_6 = {
	dip_tech_cost_modifier = -0.06
}

refinery_7 = {
	dip_tech_cost_modifier = -0.07
}

refinery_8 = {
	dip_tech_cost_modifier = -0.08
}

refinery_9 = {
	dip_tech_cost_modifier = -0.09
}

refinery_10 = {
	dip_tech_cost_modifier = -0.10
}

refinery_11 = {
	dip_tech_cost_modifier = -0.105
}

refinery_12 = {
	dip_tech_cost_modifier = -0.11
}

refinery_13 = {
	dip_tech_cost_modifier = -0.115
}

refinery_14 = {
	dip_tech_cost_modifier = -0.12
}

refinery_15 = {
	dip_tech_cost_modifier = -0.125
}

refinery_16 = {
	dip_tech_cost_modifier = -0.13
}

refinery_17 = {
	dip_tech_cost_modifier = -0.135
}

refinery_18 = {
	dip_tech_cost_modifier = -0.14
}

refinery_19 = {
	dip_tech_cost_modifier = -0.145
}

refinery_20 = {
	dip_tech_cost_modifier = -0.15
}

weapons_1 = {
	mil_tech_cost_modifier = -0.01
	artillery_cost = -0.02
	infantry_cost = -0.02
}

weapons_2 = {
	mil_tech_cost_modifier = -0.02
	artillery_cost = -0.0333
	infantry_cost = -0.0333
}

weapons_3 = {
	mil_tech_cost_modifier = -0.03
	artillery_cost = -0.0422
	infantry_cost = -0.0422
}

weapons_4 = {
	mil_tech_cost_modifier = -0.04
	artillery_cost = -0.0481
	infantry_cost = -0.0481
}

weapons_5 = {
	mil_tech_cost_modifier = -0.05
	artillery_cost = -0.0521
	infantry_cost = -0.0521
}

weapons_6 = {
	mil_tech_cost_modifier = -0.06
	artillery_cost = -0.0547
	infantry_cost = -0.0547
}

weapons_7 = {
	mil_tech_cost_modifier = -0.07
	artillery_cost = -0.0565
	infantry_cost = -0.0565
}

weapons_8 = {
	mil_tech_cost_modifier = -0.08
	artillery_cost = -0.0577
	infantry_cost = -0.0577
}

weapons_9 = {
	mil_tech_cost_modifier = -0.09
	artillery_cost = -0.0584
	infantry_cost = -0.0584
}

weapons_10 = {
	mil_tech_cost_modifier = -0.1
	artillery_cost = -0.059
	infantry_cost = -0.059
}

weapons_11 = {
	mil_tech_cost_modifier = -0.105
	artillery_cost = -0.0593
	infantry_cost = -0.0593
}

weapons_12 = {
	mil_tech_cost_modifier = -0.11
	artillery_cost = -0.0595
	infantry_cost = -0.0595
}

weapons_13 = {
	mil_tech_cost_modifier = -0.115
	artillery_cost = -0.0597
	infantry_cost = -0.0597
}

weapons_14 = {
	mil_tech_cost_modifier = -0.12
	artillery_cost = -0.0598
	infantry_cost = -0.0598
}

weapons_15 = {
	mil_tech_cost_modifier = -0.125
	artillery_cost = -0.0599
	infantry_cost = -0.0599
}

weapons_16 = {
	mil_tech_cost_modifier = -0.13
	artillery_cost = -0.0599
	infantry_cost = -0.0599
}

weapons_17 = {
	mil_tech_cost_modifier = -0.135
	artillery_cost = -0.0599
	infantry_cost = -0.0599
}

weapons_18 = {
	mil_tech_cost_modifier = -0.14
	artillery_cost = -0.06
	infantry_cost = -0.06
}

weapons_19 = {
	mil_tech_cost_modifier = -0.145
	artillery_cost = -0.06
	infantry_cost = -0.06
}

weapons_20 = {
	mil_tech_cost_modifier = -0.15
	artillery_cost = -0.06
	infantry_cost = -0.06
}

fine_arts_academy_1 = {
	stability_cost_modifier = -0.025
	#relations_decay_of_me = 0.05
	prestige_decay = -0.002
}

fine_arts_academy_2 = {
	stability_cost_modifier = -0.05
	#relations_decay_of_me = 0.0833
	prestige_decay = -0.0033
}

fine_arts_academy_3 = {
	stability_cost_modifier = -0.075
	#relations_decay_of_me = 0.1056
	prestige_decay = -0.0042
}

fine_arts_academy_4 = {
	stability_cost_modifier = -0.1
	#relations_decay_of_me = 0.1204
	prestige_decay = -0.0048
}

fine_arts_academy_5 = {
	stability_cost_modifier = -0.125
	#relations_decay_of_me = 0.1302
	prestige_decay = -0.0052
}

fine_arts_academy_6 = {
	stability_cost_modifier = -0.150
	#relations_decay_of_me = 0.1368
	prestige_decay = -0.0055
}

fine_arts_academy_7 = {
	stability_cost_modifier = -0.175
	#relations_decay_of_me = 0.1412
	prestige_decay = -0.0056
}

fine_arts_academy_8 = {
	stability_cost_modifier = -0.2
	#relations_decay_of_me = 0.1441
	prestige_decay = -0.0058
}

fine_arts_academy_9 = {
	stability_cost_modifier = -0.21
	#relations_decay_of_me = 0.1461
	prestige_decay = -0.0058
}

fine_arts_academy_10 = {
	stability_cost_modifier = -0.22
	#relations_decay_of_me = 0.1474
	prestige_decay = -0.0059
}

fine_arts_academy_11 = {
	stability_cost_modifier = -0.23
	#relations_decay_of_me = 0.1483
	prestige_decay = -0.0059
}

fine_arts_academy_12 = {
	stability_cost_modifier = -0.24
	#relations_decay_of_me = 0.1488
	prestige_decay = -0.006
}

fine_arts_academy_13 = {
	stability_cost_modifier = -0.25
	#relations_decay_of_me = 0.1492
	prestige_decay = -0.006
}

fine_arts_academy_14 = {
	stability_cost_modifier = -0.26
	#relations_decay_of_me = 0.1495
	prestige_decay = -0.006
}

fine_arts_academy_15 = {
	stability_cost_modifier = -0.27
	#relations_decay_of_me = 0.1497
	prestige_decay = -0.006
}

fine_arts_academy_16 = {
	stability_cost_modifier = -0.28
	#relations_decay_of_me = 0.1498
	prestige_decay = -0.006
}

fine_arts_academy_17 = {
	stability_cost_modifier = -0.29
	#relations_decay_of_me = 0.1498
	prestige_decay = -0.006
}

fine_arts_academy_18 = {
	stability_cost_modifier = -0.30
	#relations_decay_of_me = 0.1499
	prestige_decay = -0.006
}

fine_arts_academy_19 = {
	stability_cost_modifier = -0.325
	#relations_decay_of_me = 0.1499
	prestige_decay = -0.006
}

fine_arts_academy_20 = {
	prestige_decay = -0.006
	stability_cost_modifier = -0.35
	#relations_decay_of_me = 0.15
}

