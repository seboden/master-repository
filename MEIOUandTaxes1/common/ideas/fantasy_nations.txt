#######################################
#########                   ###########
####       MEIOU and Taxes        #####
####			Sun_Wu			   ####
#########                   ###########
#######################################

#British Isles
CEL_ideas = {

	start = {
		num_accepted_cultures = 2 # accepted_culture_threshold = -0.2
		core_creation = -0.1
			}

	bonus = {
		global_unrest = -1.5
	}

	trigger = {
		tag = CEL
	}
	free = yes

	slavers = {
		production_efficiency = 0.1
			}
	druids = {
		idea_cost = -0.1
		technology_cost = -0.1
	}
	chariot-riders = {
		cavalry_power = 0.10
	}
	pre-historic_trackways = {
		trade_steering = 0.10
	}
	what_are_you_looking_at = {
		infantry_power = 0.15
		land_morale = 0.10
	}
	skilled_metalworkers= {
		global_trade_goods_size = 0.2
		prestige = 1
	}
	head_hunters = {
		leader_land_shock = 1
	}
}

UEF_ideas = {
	start = {
		trade_steering = 0.10
		global_foreign_trade_power = 0.10
	}

	bonus = {
		discipline = 0.05
	}

	trigger = {
		tag = UEF
	}
	free = yes

	uef_wool_monopole = {
		production_efficiency = 0.1
	}
	uef_navigation_acts = {
		privateer_efficiency = 0.15
		embargo_efficiency = 0.25
	}
	uef_royal_navy = {
		naval_morale = 0.15
	}
	uef_vauban_fortifications = {
		defensiveness = 0.3
	}
	uef_the_philosophes = {
		technology_cost = -0.1
	}
	uef_reform_of_comission_buying = {
		leader_land_fire = 1
	}
	uef_court_of_versailles = {
		prestige_decay = -0.02
	}
}

#France

OCC_ideas = {
	start = {
		free_leader_pool = 1
		global_own_trade_power = 0.1
	}

	bonus = {
		global_manpower_modifier = 0.15
	}

	trigger = {
		tag = OCC
	}
	free = yes

	gascon_troops = {
		infantry_power = 0.15
	}
	languedocien_tolerance = {
		tolerance_heretic = 1
		tolerance_heathen = 1
	}
	provencal_ports = {
		trade_efficiency = 0.10
		naval_forcelimit_modifier = 0.10
	}
	auvergnat_folk = {
		prestige = 1.5
	}
	limousin_production = {
		production_efficiency = 0.15
	}
	vivaro_alpine_borders = {
		defensiveness = 0.2
	}
	lenga_d_oc = {
		improve_relation_modifier = 0.15
	}
}

ARL_ideas = {
	start = {
		diplomatic_upkeep = 1
		diplomatic_reputation = 2
	}

	bonus = {
		global_manpower_modifier = 0.15
	}

	trigger = {
		tag = ARL
	}
	free = yes

	memories_of_angevin = {
		num_accepted_cultures = 1 # fabricate_claims_time = -0.2
	}
	formation_of_the_lances = {
		discipline = 0.05
		land_morale = 0.1
	}
	provencal_port = {
		trade_efficiency = 0.15
	}
	toulon_shipyard = {
		naval_forcelimit_modifier = 0.15
		naval_maintenance_modifier = -0.10
	}
	king_of_arles = {
		prestige = 1
		legitimacy = 1
	}
	tarascons_castle = {
		defensiveness = 0.2
	}
	provencal_quilting = {
		production_efficiency = 0.1
	}
}

#Iberia

#Low Countries

#Germany

ASE_ideas = {
	start = {
		light_ship_power = 0.2
	}

	bonus = {
		core_creation = -0.10
	}

	trigger = {
		tag = ASE
	}
	free = yes

	looters = {
		land_attrition = -0.10
		naval_attrition = -0.10
	}
	raiders = {
		land_morale = 0.10
		leader_land_shock = 1
	}
	abandoning_our_pagan_traditions = {
		tolerance_own = 1
	}
	our_home_is_the_sea = {
		naval_morale = 0.1
	}
	a_lasting_identity = {
		prestige = 1
	}
	wandering_hearts = {
		global_colonial_growth = 10
		range = 0.15
	}
	the_domesday_book = {
		global_tax_modifier = 0.20
	}
}

#Italy

#Scandinavia

#Baltics
ILL_ideas = {
	start = {
		navy_tradition = 1
		navy_tradition_decay = -0.01
			}

	bonus = {
		naval_forcelimit_modifier = 0.33
	}

	trigger = {
		tag = ILL
	}
	free = yes

	illyria_rules_the_sea = {
		global_ship_cost = -0.15
		global_ship_recruit_speed = -0.10
	}
	sea_guerillas = {
		galley_power = 0.2
		naval_morale = 0.15
	}
	total_control_of_the_adriatic  = {
		global_prov_trade_power_modifier = 0.10
		trade_steering = 0.10
	}
	hellenic_influences = {
		technology_cost = -0.05
	}
	sica  = {
		infantry_power = 0.10
	}
	defiers_of_rome = {
		enemy_core_creation = 0.5
		
	}
	koine = {
		global_unrest = -1
	}
}

#Hellas

#Anatolia

#Africa

#India

GUP_ideas = {
	start = {
		prestige = 1
		technology_cost = -0.10
	}

	bonus = {
		num_accepted_cultures = 3 # accepted_culture_threshold = -0.33
	}

	trigger = {
		tag = GUP
	}
	free = yes

	ports_of_the_silk_road = {
		global_trade_power = 0.15
	}
	the_indian_steel_longbow = {
		infantry_power = 0.10
		leader_land_fire = 1
	}
	navaratna = {
		advisor_cost = -0.20
	}
	holding_back_the_huns = {
		discipline = 0.05
	}
	support_for_buddhist_and_jain_cultures = {
		tolerance_heathen = 1
		tolerance_heretic = 1
	}
	vishayapati = {
		global_tax_modifier = 0.10
	}
	the_world_trembles = {
		land_forcelimit_modifier = 0.15
		naval_forcelimit_modifier = 0.15
	}
}

#East-Asia

#Central Asia

#Persia

ILK_ideas = {
	start = {
		years_of_nationalism = -5
		land_maintenance_modifier = -0.15
			}

	bonus = {
		global_unrest = -1
	}

	trigger = {
		tag = ILK
	}
	free = yes

	we_do_not_sow = {
		core_creation = -0.25
			}
	the_horde_is_here = {
		cavalry_power = 0.15
		discipline = 0.025
	}
	silk_route_empire = {
		caravan_power = 0.15
		trade_efficiency = 0.1
			}
	the_blood_of_genghis_khan = {
		advisor_pool = 2
	}
	steppe_leaders = {
		leader_land_shock	= 1
	}
	alliance_with_the_west= {
		diplomatic_upkeep = 2
		technology_cost = -0.05
	}
	conversion_to_islam = {
		tolerance_own = 2
	}
}

#Indochina

#Indonesia
