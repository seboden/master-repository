#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 118  193  177 }

historical_idea_groups = {
	leadership_ideas
	popular_religion_ideas
	quantity_ideas
	trade_ideas
	aristocracy_ideas
	economic_ideas
	spy_ideas
	logistic_ideas
}

historical_units = {
	#Steppes Group
	steppes_spear_infantry
	steppes_swarm_cavalry
	steppes_archer_infantry
	steppes_heavy_swarm_cavalry
	steppes_divani_swarm_cavalry
	steppes_heavy_infantry
	steppes_tumandar_cavalry
	steppes_handgunner_infantry
	steppes_yikitlar_cavalry
	steppes_firelock_cavalry
	steppes_banner_infantry
	steppes_raid_cavalry
	steppes_green_standard_infantry
	steppes_shock_cavalry
	steppes_field_army_infantry
	steppes_armeblanche_cavalry
	steppes_hunter_cavalry
	steppes_drill_infantry
	steppes_impulse_infantry
	steppes_lancer_cavalry
	steppes_breech_infantry
}

monarch_names = {
	"Abdul-Karim #0" = 20
	"Abdul-Latif #0" = 20
	"Abdul-Rash�d #0" = 20
	"Abdullah #0" = 20
	"Ahmad #0" = 20
	"Alghu #0" = 20
	"Ali #0" = 20
	"Baraq #0" = 20
	"Bayan Quli #0" = 20
	"Buqa Tem�r #0" = 20
	"Buzan #0" = 20
	"Chagatay #0" = 10
	"Changshi #0" = 20
	"Danishmendji #0" = 20
	"Dost Muhammad #0" = 20
	"Dughlat #0" = 20
	"Duwa #0" = 20
	"Duwa Tem�r #0" = 20
	"Eljigidey #0" = 20
	"Esen Buqa #0" = 20
	"Ilbars #0" = 20
	"Ilyas #1" = 20
	"Ismail #0" = 20
	"Kebek #0" = 20
	"Khabul #0" = 20
	"Khizr #1" = 20
	"K�nchek #0" = 20
	"Mahmud #0" = 20
	"Mansur #0" = 20
	"Mubarak #0" = 20
	"Muhammad #1" = 20
	"Muhammad Amin #0" = 20
	"Muhammad Mumin #0" = 20
	"Naqsh-i-Jahan #0" = 20
	"Neg�bei #0" = 20
	"Qara Hulagu #0" = 10
	"Qazaghan #0" = 20
	"Qazan #0" = 20
	"Quraysh #0" = 10
	"Sa'id #0" = 20
	"Satuq #0" = 20
	"Shams-i-Jahan #1" = 20
	"Sher Muhammad #0" = 20
	"Soyurghatmish #0" = 20
	"Taliqu #0" = 20
	"Tarmashirin #0" = 20
	"Tem�r #0" = 20
	"Tughlugh Tem�r #1" = 20
	"Uwais #0" = 20
	"Yasa'ur #0" = 20
	"Yesu Mongke #0" = 10
	"Yesun Tem�r #0" = 20
	"Yunus #0" = 20

	"Heying" = -1
	"Chunmei" = -1
	"Tianhui" = -1
	"Kon" = -1
	"Mao" = -1
	"Aigiarm" = -1
	"Borte" = -1
	"Ho'elun" = -1
	"Orqina" = -1
	"Toregene" = -1
}

leader_names = { 
	Altanqai Boketei Tegusteni Qadancher Tolui-gene 
	Bayardai Yekedai "Suren Chinua" "Altan Unegen" 
	"Boke Qorchi" "Arigh Erdene" "Bayan Vachir" "Delger Dash"
}

ship_names = {
	"Alghu Khan"
	"Baraq Khan"
	Chagatai
	Orghina 
	"Genghis Khan"
	"Mubarak Shah"
	"Yes�nto'a"
	Qipchaq
	"Jagatai Khan" "Amo Darya"
	"Syr Darya" "�gedai Khan"
	G�y�k "M�ngke Khan" "Qubilai Khan"
	Duwa "Qazan Khan" "Tughlugh Timur"
	Irtysh "Amir Husayn" "Timur i Leng"
	Samarqand
}
