#Country Name: Tamatave
#Tag: TAM
#MEIOU-FB - African/Sudan tech group split Nov 08

graphical_culture = africangfx

color = { 150 150 250 }

historical_idea_groups = {
	logistic_ideas
	trade_ideas
	quality_ideas
	naval_ideas
	spy_ideas
	economic_ideas
	aristocracy_ideas
	administrative_ideas
}

historical_units = { #Madagascar group
	sub_saharan_mobata_infantry
	sub_saharan_soba_infantry
	sub_saharan_professional_infantry
	sub_saharan_gunze_infantry
	sub_saharan_foreign_infantry
	sub_saharan_native_infantry
	sub_saharan_musketeer_infantry
	sub_saharan_disciplined_infantry
	western_line_infantry
	western_drill_infantry
	western_columnar_infantry
	western_jominian_infantry
}

monarch_names = {
	"Andriamandisoarivo #0" = 100
	"Andriamahatindriarivo #0" = 100
	"Andrianahilitsy #0" = 100
	"Andrianiveniarivo #0" = 100
	"Andrianihoatra #0" = 100
	"Andrianikeniarivo #0" = 100
	"Tombola #0" = 100
	"Tsimalomo #0" = 100
	"Maka #0" = 100
	"Andriantsoly #0" = 100
	"Andriandahifotsy #0" = 100
	"Ramitraho #0" = 100
	"Radama #0" = 100
	
	"Ranavalona #0" = -10
	"Rangita #0" = -10
	"Tsiomeko #0" = -10
	"Rafohy #0" = -10
	"Ravahiny #0" = -10
	"Oantitsy #0" = -10
	"Andrianaginarivo #0" = -10
}

leader_names = {
	Addy
	Afrifa
	Sadami
	Sabah
	Zakari
	Offei
	Donkor
	Akoto
	Sribor
	Moyoyo
}

ship_names = {
	Antankarana Antanarivo
	Betsileo Betsmisaraka Boina
	Ihorombe Morondava Sakalava
	Sihanaka Taomasina Tsimiheki
}
