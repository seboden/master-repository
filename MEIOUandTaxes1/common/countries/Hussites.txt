#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 10  10  40 }

historical_idea_groups = {
	popular_religion_ideas
	leadership_ideas
	administrative_ideas
	quality_ideas
	spy_ideas
	trade_ideas
	aristocracy_ideas
	economic_ideas
}

historical_units = { #South German group
	western_halberd_infantry
	western_knight_cavalry
	western_manatarms_infantry
	western_freecompany_infantry
	western_runner_cavalry
	western_pike_infantry
	western_gendarme_cavalry
	western_wheellock_infantry
	unique_GER_landsknecht_infantry
	western_earlytercio_infantry
	western_cuirassier_cavalry
	western_tercio_infantry
	western_musketeersquare_infantry
	western_dragoon_cavalry
	western_countermarch_infantry
	western_volley_infantry
	western_regular_infantry
	western_skirmisher_infantry
	western_mediumhussar_cavalry
	western_mass_infantry
	western_uhlan_cavalry
	western_columnar_infantry
	western_lancer_cavalry
	western_jominian_infantry
}

monarch_names = {
	"Vladislav #2" = 20
	"Ferdinand #0" = 20
	"Fridrich #0 Falck�" = 20
	"Jiri #0" = 20
	"Ladislav #0" = 20
	"Ludv�k #0" = 20
	"Jindrich #1 Fridrich" = 10
	"Zikmund #0" = 10
	"Viktorin #0" = 10
	"Karel #4 Ludv�k" = 5
	"Jan #1" = 5
	"Jindrich #1" = 5
	"Boczek #0" = 5
	"Eduard #0" = 5
	"Fridrich #0" = 5
	"Ruprecht #0" = 5
	"Karel #4" = 1
	"V�clav #4" = 1
	"Premysl #2" = 1
	"Alexandr #0" = 1
	"Alojz #0" = 1
	"Bronislav #0" = 1
	"Ctibor #0" = 1
	"David #0" = 1
	"Drahoslav #0" = 1
	"Ign�c #0" = 1
	"Jakub #0" = 1
	"Jozef #0" = 1
	"Kliment #0" = 1
	"Leos #0" = 1
	"Lubomir #0" = 1
	"Luk�s #0" = 1
	"Marek #0" = 1
	"Mikol�s #0" = 1
	"Miroslav #0" = 1
	"Ondrej #0" = 1
	"Otokar #0" = 1
	"Pavol #0" = 1
	"Vilem #0" = 1
	"Zdislav #0" = 1
	
	"Aleida #0" = -1
	"Cordula #0" = -1
	"Gundula #0" = -1
	"Selma #0" = -1
	"Ulrike #0" = -1
	"Barbara #0" = -1
	"Alexandra #0" = -1
	"Christiane #0" = -1
	"Emma #0" = -1
	"Corinna #0" = -1
	"Else #0" = -1
	"Hedwig #0" = -1
	"Sylvia #0" = -1
	"Wilhelmine #0" = -1
	"Alexis #0" = -1
	"Evelyn #0" = -1
	"Hannelore #0" = -1
	"Irma #0" = -1
	"Heinrike #0" = -1
	"Ilona #0" = -1
	"Andrea #0" = -1
}

leader_names = {
	Boleslav Brno
	Kladno
	Lablonec Liberec
	Pardubice Plzen Praha
	Trutnov
	Zatec
}

ship_names = {
	"Svat� Vaclav"
	"Svat� Mikul�s"
	"Svat� Jir�"
	"Svat� Anezk�"
	"Nejsvatejs� Trojice"
	"Kr�l Karel"
	"Kr�l Boleslav"
	"Kr�l Premysl"
	"Panna Maria Vitezn�"
	"Jan Hus"
	S�rka
	Ctirad
	Cech�
	Moravy
	Budvar
	Libuse
	Odv�zn�
	Orel
	Sokol
	Slezko
	Sipk�
	Hvezd�
}
