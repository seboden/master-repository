# ZIN - Kel Gres

graphical_culture = muslimgfx

color = { 213  124  146 }

historical_idea_groups = {
	trade_ideas
	popular_religion_ideas
	leadership_ideas
	naval_ideas
	quantity_ideas
	administrative_ideas
	economic_ideas
	spy_ideas
}

historical_units = {
	#North African group
	muslim_mashain_walker_infantry
	muslim_arab_horseman
	muslim_crossbow_infantry
	muslim_jinete_cavalry
	muslim_rammaha_lancer_cavalry
	muslim_elchee_infantry
	muslim_arquebusier
	muslim_qoorchilar_cavalry
	muslim_reformed_qizilbash_cavalry
	muslim_tofangchi_musketeer_infantry
	muslim_jazayerchi_musketeer_infantry
	muslim_zanbourak_camel_cavalry
	muslim_piyadegan_infantry
	muslim_savaran_saltani_cavalry
	muslim_taifat_al_rusa_infantry
	muslim_hunter_cavalry
	muslim_drill_infantry
	muslim_columnar_infantry
	muslim_lancer_cavalry
	muslim_breech_infantry
}

monarch_names = {
	"Muhammad #0" = 200
	"Ahmad #0" = 80
	"'Abdall�h #0" = 60
	"'Abd al-Malik #0" = 40
	"'Al� #0" = 40
	"Isma'�l #1" = 20
	"Yahy� #1" = 20
	"'al-Wal�d #0" = 20
	"al-Mustad� #0" = 20
	"al-Rash�d #0" = 20
	"Hish�m #0" = 20
	"Idr�sid #0" = 20
	"Sulaym�n #0" = 20
	"Zayd�n #0" = 20
	"Zayn #0" = 20
	"'Abd ar-Rahm�n #0" = 10
	"al-Yaz�d #0" = 10
	"Mansur #0" = 10
	"Muhraz #0" = 10
	"Nasir #0" = 10
	"Ab� Faris #0" = 5
	"Abbas #0" = 5
	"Ab� Merwan #0" = 5
	"al-Talib #0" = 5
	"Hammada #0" = 5
	"Hasam #0" = 5
	"Hashim #0" = 5
	"Ibrahim #0" = 5
	"Ja'far #0" = 5
	"Maimun #0" = 5
	"Maslama #0" = 5
	"Said #0" = 5
	"Umar #0" = 5
	"Y�suf #0" = 5
	"Ataullah #0" = 1
	"Habib #0" = 1
	"Jabril #0" = 1
	"Lutfi #0" = 1
	"Rasul #0" = 1
	"Zulfiqar #0" = 1

	"Aida #0" = -1
	"Zia #0" = -1
	"Saleh #0" = -1
	"Fatima #0" = -1
	"Samira #0" = -1
	"Aicha #0" = -1
	"Chrifa #0" = -1
	"Djihane #0" = -1
	"Esra #0" = -1
	"Leila #0" = -1
	"Safia #1" = -1
}

leader_names = {
	Sahiri
	Haida
	Sellami
	Arazi
	Berui
	Lahsini
	Skah
	Barek
	Alami
	Behar
}

ship_names = {
	Tawwab Muntaqim Afuww Fa'uf "Malik Mulk"
	"Jalal Ikram" Muqsit Jami Ghani Mughni
	Mani Darr Nafi Nur Hadi Badi Baqi Warith
	Rashid Sabur "Fazl Azim"
}
