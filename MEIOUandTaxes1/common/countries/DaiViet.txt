#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 136  117  90 }

historical_idea_groups = {
	logistic_ideas
	naval_ideas
	popular_religion_ideas
	trade_ideas
	administrative_ideas
	diplomatic_ideas
	aristocracy_ideas
	spy_ideas
}

#Vietnamese group
historical_units = {
	asian_mixed_crossbow_infantry
	asian_heavy_elephant_cavalry
	asian_war_elephant_cavalry
	asian_fire_lance_infantry
	unique_VIE_huochong_infantry
	asian_tower_elephant_cavalry
	asian_handgunner_infantry
	asian_chong_and_crossbow_infantry
	asian_armored_elephant_cavalry
	asian_chong_infantry
	asian_arquebusier_infantry
	asian_musketeer_infantry
	asian_war_elephant_artillery_cavalry
	asian_matchlock_musketeer_infantry
	asian_platoon_infantry
	asian_carabinier_cavalry
	asian_skirmisher_infantry
	asian_uhlan_cavalry
	asian_rifled_infantry
	asian_impulse_infantry
	asian_lancer_cavalry
	asian_breech_infantry
}

monarch_names = {
	"Nghe Tong #0" = 20
	"Due Tong #0" = 20
	"Thai Tong #0" = 20
	"Nhan Tong #0" = 20
	"Hien Tong #0" = 20
	"Tuc Tong #0" = 20
	"Uy Muc #0" = 20
	"Chieu Tong #0" = 20
	"Cung Hoang #0" = 20
	"Dang Doanh #0" = 20
	"Anh Tong #0" = 20
	"The Tong #0" = 20
	"Gia Tong #0" = 20
	"Hi Tong #0" = 20
	"Du Tong #0" = 20
	"Duy Phuong #0" = 20
	"Y Tong #0" = 20
	"De Hien #0" = 20
	"Tran Thuan Tong #0" = 20
	"Tran Thieu De #0" = 20
	"Kui Li #0" = 20
	"Han Thuong #0" = 20
	"Hau Tran #0" = 20
	"Vi Muc De #0" = 20
	"Kung Hoang #0" = 20
	"Dang Dung #0" = 20
	"De Duy-Phuong #0" = 20
	"Kinh-Tong #0" = 20
	
	"Ai Van #0" = -1
	"Anh Duong #0" = -1
	"Bach V�n #0" = -1
	"C�m V�n #0" = -1
	"Di�m #0" = -1
	"H� Giang #0" = -1
	"H�ng Loan #0" = -1
	"Lan D�i #0" = -1
	"My Hanh #0" = -1
	"Quynh #0" = -1
}

leader_names = {
	Cao 
	Dang Doan 
	Hoang Huynh 
	Lam Le Ly 
	Ng Nguyen Nguyen-Dinh Nguyen-Khoa 
	Pham 
	Tran Tran-Dinh Tran-Nhu Tran-Thanh Trinh Truong 
	Vu 
}

ship_names = {
	"Tran Canh" "Tran Hoang" "Tran Kh�m" "Tran Thuy�n"
	"Tran Manh" "Tran Vuong" "Tran Hao" "Duong Nhat Le"
	"Tran Phu" "Tran K�nh" "Tran Hien" "Tran Ngung"
	"Tran An" "Th�i T�ng" "Th�nh T�ng" "Nh�n T�ng"
	"Anh T�ng" "Minh T�ng" "Hien T�ng" "Du T�ng"
	"Nghe T�ng" "Due T�ng" "Thuan T�ng"
}
