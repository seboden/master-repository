#Country Name: Please see filename.

graphical_culture = asiangfx

color = { 206  130  61 }

historical_idea_groups = {
	logistic_ideas
	spy_ideas
	trade_ideas
	leadership_ideas
	economic_ideas
	diplomatic_ideas
	popular_religion_ideas
	quantity_ideas
}

historical_units = { #Chinese group
	asian_mixed_crossbow_infantry
	asian_fire_lance_cavalry
	asian_lancer_bow_cavalry
	asian_halberdier_infantry
	asian_light_cavalry
	asian_handgunner_infantry
	asian_rocketeer_infantry
	asian_northern_guard_cavalry
	asian_mandarin_infantry
	asian_wagon_infantry
	asian_dragoon_cavalry
	asian_charge_infantry
	asian_regular_infantry
	asian_mediumhussar_cavalry
	asian_skirmisher_infantry
	asian_hunter_cavalry
	asian_impulse_infantry
	asian_columnar_infantry
	asian_lancer_cavalry
	asian_breech_infantry
}

monarch_names = {
	"Qizhen #0" = 20
	"Qiyu #0" = 20
	"Jianjun #0" = 20
	"Youcheng #0" = 20
	"Houzhao #0" = 20
	"Houcong #0" = 20
	"Zaihou #0" = 20
	"Yijun #0" = 20
	"Changluo #0" = 20
	"Youjian #0" = 20
	"Yousong #0" = 20
	"Yujian #0" = 20
	"Youlang #0" = 20
	"Chenggong #0" = 100
	"Kehshuan #0" = 100
	"Biao #0" = 100
	"Shuang #0" = 100
	"Gang #0" = 100
	"Su #0" = 100
	"Zhen #0" = 100
	"Fu #0" = 100
	"Zi #0" = 100
	"Qi #0" = 100
	"Tan #0" = 100
	"Chun #0" = 100
	"Bai #0" = 100
	"Gui #0" = 100
	"Zhi #0" = 100
	"Quan #0" = 100
	"Pian #0" = 100
	"Hui #0" = 100
	"Song #0" = 100
	"Mo #0" = 100
	"Ying #0" = 100
	"Jing #0" = 100
	"Dang #0" = 100
	"Nan #0" = 100
	"Gaoxu #0" = 100
	"Caosui #0" = 100
	"Gauxi #0" = 100
	"Youyuan #0" = 100
	"Youlun #0" = 100
	"Youbin #0" = 100
	"Houwei #0" = 100
	"Youxiao #0" = 100
	"Youji #0" = 100
	"Youxue #0" = 100
	"Youyi #0" = 100
	"Youshan #0" = 100
	"Cixuan #0" = 100
	"Cijiang #0" = 100
	"Cizhao #0" = 100
	"Cihuan #0" = 100
	"Cican #0" = 100
	"He #0" = 10
	"Ji #0" = 10
	"Da #0" = 10
	"Yuchun #0" = 10

	"Yong'an" = -1
	"Yongping" = -1
	"Ancheng" = -1
	"Xianning" = -1
	"Changning" = -1
	"Zhaoxian" = -1
	"Zhaoyi" = -1
	"Jieyu" = -1
	"Gonghui" = -1
	"Jinghui" = -1
}

leader_names = {
	Li Wang Zhang Liu Zhao Chen Yang Wu Huang Zhou Xu Zhu Lin Sun Ma
	Gao Hu Zheng Guodao Xie He Xu Song Shen Luo Han Deng Liang Ye
	Peng Fang Wang Cui Cai Cheng Yuan  Pan Lu Cao Tang Feng Qian Du
}

ship_names = {
	Gaodi Huidi Wendi Zhaodi Zhangdi "Da Ming Guo"
	Beijing "Zeng He" Ji'nan Nanjing Hangzhou 
	Wuchang Fuzhou Nanchang Guilin Guangzhou
	Guiyang Yunnan Chendo Xi'an Taiyuan "Huang He"
	"Chang Jiang" "Xijiang" "Kung-fu tzu" Mencius
	"Xun Zi" "Yan Hui" "Min Sun" "Ran Geng" "Ran Yong"
	"Zi-you" "Zi-lu" "Zi-wo" "Zi-gong" "Yan Yan"
	"Zi-xia" "Zi-zhang" "Zeng Shen" "Dantai Mieming"
	"Fu Buji" "Yuan Xian" "Gungye Chang" "Nangong Guo"
	"Gongxi Ai" "Zeng Dian" "Yan Wuyao" "Shang Zhu"
	"Gao Chai" "Qidiao Kai" "Gongbo Liao" "Sima Geng"
	"Fan Xu" "You Ruo" "Gongxi Chi" "Wuma Shi" "Liang Zhan"
	"Yan Xing" "Ran Ru" "Cao Xu" "Bo Qian" "Gongsun Long"
}
