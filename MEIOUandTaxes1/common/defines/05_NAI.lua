NDefines.NAI.AI_USES_HISTORICAL_IDEA_GROUPS = 1								-- If set to 0, ai will use ai_will_do instead of historical ideagroups when picking ideagroups (ai never uses historical ideagroups in custom/random setup)

NDefines.NAI.AI_CONVERT_CULTURES = 0										-- If set to 0, AI will not convert cultures
NDefines.NAI.AGGRESSIVENESS = 1000											-- Base chance (out of 10000) of AI being willing to start a war each diplomatic tick (~1.5 times a month)
NDefines.NAI.AGGRESSIVENESS_BONUS_EASY_WAR = 2000							-- Added to aggressiveness if the war is against a weak or particularily hated enemy
NDefines.NAI.TRADE_INTEREST_THRESHOLD = 4 									-- Number of merchants required to be a nation with trade interest
NDefines.NAI.FORCE_COMPOSITION_CHANGE_TECH_LEVEL = 30						-- Tech level at which AI will double its artillery fraction

NDefines.NAI.OVER_FORCELIMIT_AVOIDANCE_FACTOR = 100							-- The higher this number is, the less willing the AI will be to exceed forcelimits
NDefines.NAI.COLONY_BUDGET_AMOUNT = 8.0										-- AI will reserve a maximum of this amount of monthly ducats for colonies (multiplied by number of colonists)
NDefines.NAI.DESIRED_SURPLUS = 0.2											-- AI will aim for having at least this fraction of their income as surplus when they don't have large savings
NDefines.NAI.DESIRED_DEFICIT = 0.02											-- AI will try to spend this fraction of their money above their target for long term savings.
NDefines.NAI.MAX_SAVINGS = 72												-- AI will keep a maximum of this * their monthly income in long-term savings
NDefines.NAI.ARMY_BUDGET_FRACTION = 0.6										-- AI will spend a maximum of this fraction of monthly income on army maintenance (based off wartime costs)
NDefines.NAI.NAVY_BUDGET_FRACTION = 0.4										-- AI will spend a maximum of this fraction of monthly income on navy maintenance (based off wartime costs)
NDefines.NAI.FORT_BUDGET_FRACTION = 0.3										-- AI will spend a maximum of this fraction of monthly income on forts
NDefines.NAI.ADVISOR_BUDGET_FRACTION = 0.2 									-- AI will spend a maximum of this fraction of monthly income on advisor maintenance

NDefines.NAI.PEACE_WAR_EXHAUSTION_FACTOR = 1.2
NDefines.NAI.PEACE_EXCESSIVE_DEMANDS_FACTOR = 0.01							-- AI unwillingness to peace based on demanding more stuff than you have warscore
NDefines.NAI.PEACE_EXCESSIVE_DEMANDS_THRESHOLD = 30							-- If you have less warscore than this, excessive demands will be factored in more highly
NDefines.NAI.PEACE_MILITARY_STRENGTH_FACTOR = 9								-- AI unwillingness to peace based on manpower & forcelimits
NDefines.NAI.PEACE_TIME_MONTHS = 36											-- Months of additional AI stubbornness in a war
NDefines.NAI.PEACE_TIME_MAX_MONTHS = 600									-- Max months applied to time factor in a war
NDefines.NAI.PEACE_TIME_EARLY_FACTOR = 0.5									-- During months of stubbornness the effect of time passed is multiplied by this
NDefines.NAI.PEACE_TIME_LATE_FACTOR = 0.75

NDefines.NAI.PEACE_ALLY_TIME_MULT = 0.5										-- Multiplies PEACE_TIME_FACTOR for allies in a war
NDefines.NAI.PEACE_WAR_DIRECTION_FACTOR = 0.8								-- AI willingness to peace based on who's making gains in the war
NDefines.NAI.PEACE_WAR_DIRECTION_WINNING_MULT = 5.0							-- Multiplies AI emphasis on war direction if it's the one making gains

NDefines.NAI.DIPLOMATIC_ACTION_SUBSIDIES_RELATIONS_FACTOR = 0.5				-- AI scoring for giving subsidies to a country based on opinion of the other country

NDefines.NAI.DIPLOMATIC_INTEREST_DISTANCE = 50 -- If border distance is greater than this, diplomatic AI will have less interest in the country
