
NDefines.NDiplomacy.UNCONDITIONAL_SURRENDER_MONTHS = 1					-- Months before unconditional surrender starts having an effect on Call for Peace. Set to negative values to disable feature.
NDefines.NDiplomacy.DISHONORABLE_PEACE_MONTHS = 36						-- See DISHONORABLE_PEACE_WARSCORE. Set to 0 to entirely disable the feature.
NDefines.NDiplomacy.DISHONORABLE_PEACE_WARSCORE = -20					-- If you have more than this amount of individual warscore, peacing out within DISHONORABLE_PEACE_MONTHS of war start counts as a dishonorable act and incurs a CALL_ALLY_DECLINE_PRESTIGE_PENALTY hit.
NDefines.NDiplomacy.PREPARE_FOR_WAR_COST = 5							-- Cost in favors to ask AI to prepare for war

NDefines.NDiplomacy.FAVOR_GAIN_FOR_HELP = 30						-- Amount of favors gained for helping allies in wars (based on war contribution relative to their power)
NDefines.NDiplomacy.TRUST_PENALTY_FOR_SEPARATE_PEACE = 20			-- Trust penalty for signing a separate peace


NDefines.NDiplomacy.ALLOW_LEADER_DEMAND_TOGGLE = 1						-- Whether or not player is allowed to set if warleader can negotiate for them
NDefines.NDiplomacy.VASSALIZE_BASE_DEVELOPMENT_CAP = 600				-- Countries with more total base tax than this cannot be vassalized
NDefines.NDiplomacy.PEACE_IMPACT_ADM_SCORE = 0.1
NDefines.NDiplomacy.PEACE_IMPACT_DIP_SCORE = 0.1
NDefines.NDiplomacy.PEACE_IMPACT_MIL_SCORE = 0.1
NDefines.NDiplomacy.AUTONOMY_WARSCORE_COST_MODIFIER = 0.40				-- How much autonomy reduces score by (at 1, 50% autonomy = 50% reduction)	
NDefines.NDiplomacy.MAX_PEACE_TREATY_COST = 500						-- in diplo power
NDefines.NDiplomacy.MAX_FREE_CITIES = 15
NDefines.NDiplomacy.HRE_PRINCE_AUTHORITY_THRESHOLD = 80					-- Threshold below which you lose IA, and above which you gain it
NDefines.NDiplomacy.END_OF_CRUSADES = 1870 								-- End of Crusade/Excommunicate actions. AI might also befriend old religious enemies.		# DEI GRATIA CHANGED
NDefines.NDiplomacy.WE_IMPACT_ON_ANNEX_INTEGRATE = -0.075				-- multiplied with current WE
NDefines.NDiplomacy.TRUCE_YEARS = 3 									-- _DDEF_TRUCE_YEARS_; Years of Truce
NDefines.NDiplomacy.SCALED_TRUCE_YEARS = 7								-- Additional years of truce based on % of warscore taken in war (100% warscore = full scaled truce years)
NDefines.NDiplomacy.MONARCH_GOV_CHANGE_LEGITIMACY_PENALTY = 0.25		-- Penalty(%) on the legitimacy when changing gov type to the monarchy

NDefines.NDiplomacy.DETECTED_SPY_NETWORK_DAMAGE_MIN = 20
NDefines.NDiplomacy.DETECTED_SPY_NETWORK_DAMAGE_MAX = 70
NDefines.NDiplomacy.SUPPORT_REBELS_EFFECT = 10
NDefines.NDiplomacy.SUPPORT_REBELS_MONEY_FACTOR = 0.5					
NDefines.NDiplomacy.FABRICATE_CLAIM_COST = 40
NDefines.NDiplomacy.CORRUPT_OFFICIALS_COST = 50
NDefines.NDiplomacy.OVEREXTENSION_THRESHOLD = 2.0 						-- at which threshold you can get events

NDefines.NDiplomacy.AE_OTHER_CONTINENT = 0
NDefines.NDiplomacy.AE_DIFFERENT_RELIGION = -0.6
NDefines.NDiplomacy.AE_ATTACKER_DEVELOPMENT = 0.06						-- +50% cap (at 1000 development)
NDefines.NDiplomacy.AE_DEFENDER_DEVELOPMENT = 0.04						-- -50% cap (at 1000 development)
NDefines.NDiplomacy.AE_DISTANCE_BASE = 1.20
NDefines.NDiplomacy.AE_PROVINCE_CAP = 40								-- Province development above this will not count for AE

NDefines.NDiplomacy.DIP_PORT_FEES = 0.2									-- DIP_PORT_FEES
NDefines.NDiplomacy.CLAIM_PEACE_COST_DIP_FRACTION = -0.25				-- Fraction of dipcost you pay for cores/claims
NDefines.NDiplomacy.CORE_PEACE_COST_DIP_FRACTION = -0.5					-- Fraction of dipcost you pay for cores
NDefines.NDiplomacy.MONTHS_BEFORE_TOTAL_OCCUPATION = 24					-- Before this many months have passed in the war you cannot gain 100% warscore by just occupying the warleader

NDefines.NDiplomacy.PO_DEMAND_PROVINCES_AE = 0.55						-- _DDEF_PO_DEMAND_PROVINCES_AE = 10, (Per development)
NDefines.NDiplomacy.PO_RETURN_CORES_AE = 0.3		 					-- (Per core, only applied if returning cores to vassals of winner)
NDefines.NDiplomacy.PO_FORM_PU_AE = 0.3									-- _DDEF_PO_FORM_PU_AE = 10, (Per development)
NDefines.NDiplomacy.PO_BECOME_VASSAL_AE = 0.3		 					-- _DDEF_PO_BECOME_VASSAL_AE = 10, (Per development)
NDefines.NDiplomacy.PO_BECOME_PROTECTORATE_AE = 0.25 					-- _DDEF_PO_BECOME_VASSAL_AE = 10, (Per development)

NDefines.NDiplomacy.PEACE_COST_BECOME_VASSAL = 0.7						-- Vassalize a country (scales by province wealth)
NDefines.NDiplomacy.PEACE_COST_CONVERSION = 0.3							-- scaled with countrysize for forced conversion in peace.
NDefines.NDiplomacy.PEACE_COST_CONCEDE = 5 								-- _DDEF_PEACE_COST_CONCEDE_ Base Peace cost for conceding defeat
NDefines.NDiplomacy.PEACE_COST_DEMAND_PROVINCE = 1.1					-- Demand a province (scales by province wealth, also used for annex)
NDefines.NDiplomacy.PEACE_COST_RETURN_CORE = 0.9						-- Return a core (scales by province wealth)
NDefines.NDiplomacy.PEACE_COST_REVOKE_CORE = 0.6						-- Revoke a core (scales by province wealth)
NDefines.NDiplomacy.PEACE_COST_RELEASE_ANNEXED = 0.8					-- Release annexed nation (scales by province wealth)
NDefines.NDiplomacy.PEACE_COST_RELEASE_VASSAL = 0.5						-- Release vassal (scales by province wealth)
NDefines.NDiplomacy.PEACE_COST_DEMAND_NON_OCCUPIED_PROVINCE_MULT = 1.25
NDefines.NDiplomacy.PEACE_COST_DEMAND_CAPITAL_MULT = 1.10
NDefines.NDiplomacy.INTEGRATE_UNION_MIN_YEARS = 10
NDefines.NDiplomacy.INTEGRATE_VASSAL_MIN_YEARS = 10
NDefines.NDiplomacy.AGITATE_FOR_LIBERTY_DESIRE = 15						-- Liberty Desire gained due to ongoing agitation.
NDefines.NDiplomacy.AGITATE_FOR_LIBERTY_RATE = 0.5						-- Monthly rate at which Liberty Desire rises towards the maximum during agitation, or otherwise falls towards zero.
NDefines.NDiplomacy.ANNEX_DIP_COST_PER_DEVELOPMENT = 6					-- per development

NDefines.NDiplomacy.MAX_PEACE_TREATY_AE = 100

NDefines.NAI.DIPLOMATIC_ACTION_OFFER_CONDOTTIERI_ONLY_NEIGHBORS = 0
