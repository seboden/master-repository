#buildings.txt
#
#Glossary
# cost = x #==> cost in $ to build (subject to other modifiers)
# time = x #==> number of months to build.
# modifier = m # a modifier on the province that the building gives
# trigger = t # an and trigger that needs to be fullfilled to build and keep the building
# one_per_country = yes/no # if yes, only one of these can exist in a country
# manufactory = { trade_good trade_good } # list of trade goods that get a production bonus
# onmap = yes/no # show as a sprite on the map

################################################
# Manufactories
################################################
#farm_estate
#fine_arts_academy
#plantations
#refinery
#textile
#tradecompany
#weapons
#wharf

farm_estate = {
	#available at tech level 9 (adm tech)
	cost = 500
	time = 60
	trigger = {
		owner = { NOT = { government = native_council } }
	}
	manufactory = {
		wheat
		rice
		millet
		maize
		livestock
		cheese
	}
	modifier = {
		trade_goods_size_modifier = 0.5
		supply_limit_modifier = 0.5
	}
	onmap = yes
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 0
			owner = { NOT = { monthly_income = 25 } }
		}
		modifier = {
			factor = 0.5
			owner = { NOT = { monthly_income = 50 } }
		}
		modifier = {
			factor = 1.25
			owner = { monthly_income = 75 }
		}		
		modifier = {
			factor = 1.25
			owner = { monthly_income = 100 }
		}	
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 0.25
			is_overseas = yes
		}	
	}
}
refinery = {
	#available at tech level 12 (dip tech)
	cost = 2000
	time = 60
	trigger = {
		owner = { NOT = { government = native_council } }
	}
	manufactory = {
		wine
		sugar
		beer
		rum
		olive
		palm
	}
	modifier = {
		trade_goods_size_modifier = 0.5
	}
	onmap = yes
	category = 7
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 0
			owner = { NOT = { monthly_income = 25 } }
		}
		modifier = {
			factor = 0.5
			owner = { NOT = { monthly_income = 50 } }
		}
		modifier = {
			factor = 1.25
			owner = { monthly_income = 75 }
		}		
		modifier = {
			factor = 1.25
			owner = { monthly_income = 100 }
		}			
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 0.25
			is_overseas = yes
		}			
	}
}

weapons = {
	#available at tech level 12 (mil tech)
	cost = 2000
	time = 60
	trigger = {
		owner = { NOT = { government = native_council } }
	}
	manufactory = {
		copper
		iron
		lead
		sulphur
		steel
		arms
		ammunitions
		hardware
		coal
		tin
	}
	modifier = {
		trade_goods_size_modifier = 0.5
	}
	onmap = yes
	category = 7
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 0
			owner = { NOT = { monthly_income = 25 } }
		}
		modifier = {
			factor = 0.5
			owner = { NOT = { monthly_income = 50 } }
		}
		modifier = {
			factor = 1.25
			owner = { monthly_income = 75 }
		}		
		modifier = {
			factor = 1.25
			owner = { monthly_income = 100 }
		}	
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 0.25
			is_overseas = yes
		}	
	}
}

wharf = {
	#available at tech level 12 (dip tech)
	cost = 2000
	time = 60
	trigger = {
		owner = { NOT = { government = native_council } }
	}
	manufactory = {
		naval_supplies
		hemp
		lumber
		lead
		arms
		ammunitions
		fish
	}
	modifier = {
		trade_goods_size_modifier = 0.5
	}
	onmap = yes
	category = 7
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 0
			owner = { NOT = { monthly_income = 25 } }
		}
		modifier = {
			factor = 0.5
			owner = { NOT = { monthly_income = 50 } }
		}
		modifier = {
			factor = 1.25
			owner = { monthly_income = 75 }
		}		
		modifier = {
			factor = 1.25
			owner = { monthly_income = 100 }
		}	
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 0.25
			is_overseas = yes
		}	
	}
}

textile = {
	#available at tech level 14 (adm tech)
	cost = 1500
	time = 60
	trigger = {
		owner = { NOT = { government = native_council } }
	}
	manufactory = {
		hemp
		wool
		cloth
		linen
		silk
		cotton
		carpet
	}
	modifier = {
		trade_goods_size_modifier = 0.5
	}
	onmap = yes
	category = 7
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 0
			owner = { NOT = { monthly_income = 25 } }
		}
		modifier = {
			factor = 0.5
			owner = { NOT = { monthly_income = 50 } }
		}
		modifier = {
			factor = 1.25
			owner = { monthly_income = 75 }
		}		
		modifier = {
			factor = 1.25
			owner = { monthly_income = 100 }
		}	
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 0.25
			is_overseas = yes
		}	
	}
}

fine_arts_academy = {
	#available at tech level 16 (adm tech)
	cost = 3000
	time = 72
	trigger = {
		owner = { NOT = { government = native_council } }
	}
	manufactory = {
		chinaware
		glassware
		jewelery
		marble
		gold
		silver
		gems
	}
	modifier = {
		trade_goods_size_modifier = 0.5
	}
	onmap = yes
	category = 7
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 0
			owner = { NOT = { monthly_income = 25 } }
		}
		modifier = {
			factor = 0.5
			owner = { NOT = { monthly_income = 50 } }
		}
		modifier = {
			factor = 1.25
			owner = { monthly_income = 75 }
		}		
		modifier = {
			factor = 1.25
			owner = { monthly_income = 100 }
		}			
		modifier = {
			factor = 0.25
			is_overseas = yes
		}	
		modifier = {
			factor = 0
			is_capital = yes
		}			
	}	
}

plantations = {
	#available at tech level 18 (adm tech)
	cost = 500
	time = 60
	trigger = {
		owner = { NOT = { government = native_council } }
	}
	manufactory = {
		coffee
		cacao
		cotton
		sugar
		tobacco
		indigo
		tea
		opium
		pepper
	}
	modifier = {
		trade_goods_size_modifier = 0.5
		province_trade_power_value = 5.0
		local_production_efficiency = 0.20
	}
	onmap = yes
	category = 7
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 0
			owner = { NOT = { monthly_income = 25 } }
		}
		modifier = {
			factor = 0.5
			owner = { NOT = { monthly_income = 50 } }
		}
		modifier = {
			factor = 1.25
			owner = { monthly_income = 75 }
		}		
		modifier = {
			factor = 1.25
			owner = { monthly_income = 100 }
		}	
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 0.25
			is_overseas = yes
		}	
	}
}

tradecompany = {
#available at tech level 12 (dip tech)
	cost = 500
	time = 60
	trigger = {
		owner = { NOT = { government = native_council } }
	}
	manufactory = {
		tea
		chinaware
		slaves
		pepper
		clove
		nutmeg
		cinnamon
		fur
		opium
	}
	modifier = {
		trade_goods_size_modifier = 0.5
		province_trade_power_value = 10
		local_production_efficiency = 0.10
	}
	onmap = yes
	category = 7
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			owner = { is_at_war = yes }
		}
		modifier = {
			factor = 0
			owner = { NOT = { monthly_income = 25 } }
		}
		modifier = {
			factor = 0.5
			owner = { NOT = { monthly_income = 50 } }
		}
		modifier = {
			factor = 1.25
			owner = { monthly_income = 75 }
		}		
		modifier = {
			factor = 1.25
			owner = { monthly_income = 100 }
		}	
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 0.25
			is_overseas = yes
		}	
	}
}
