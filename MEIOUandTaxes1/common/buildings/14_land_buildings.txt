### MODIFIED BY DO OVERHAUL ###

#Glossary
# cost = x #==> cost in $ to build (subject to other modifiers)
# time = x #==> number of months to build.
# modifier = m # a modifier on the province that the building gives
# trigger = t # an and trigger that needs to be fullfilled to build and keep the building
# one_per_country = yes/no # if yes, only one of these can exist in a country
# manufactory = { trade_good trade_good } # list of trade goods that get a production bonus
# onmap = yes/no # show as a sprite on the map

################################################
# Land Buildings
################################################
#Armory
#Arsenal

#Training Fields
#Barracks
#Regimental Camp
#Conscription Center

################################################
# Tier 1, 15th Century Buildings
################################################
armory = {
	cost = 100
	time = 12

	trigger = {
		NOT = {  has_building = arsenal }
	}
	
	modifier = {
		regiment_recruit_speed = -0.25
		local_defensiveness = 0.05
		garrison_growth = 0.15
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0.4
			always = yes
		}
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 0.6
			is_overseas = yes
		}
		modifier = {
			factor = 0.75
			NOT = { unrest = 1 }
		}
		modifier = {
			factor = 0.8
			NOT = {
				trade_goods = iron
				trade_goods = copper
				trade_goods = hardware
				trade_goods = arms
			}		
		}
		modifier = {
			factor = 1.5
			OR = {
				has_building = fort_14th
				has_building = fort_15th
				has_building = fort_16th
				has_building = fort_17th
				has_building = fort_18th
			}		
		}		
	}
}

training_fields = {
	cost = 100
	time = 12

	trigger = {
		NOT = {  has_building = barracks }
		NOT = {  has_building = regimental_camp }
		NOT = {  has_building = conscription_center }
	}
	
	modifier = {
		land_forcelimit = 0.25
		local_manpower = 0.100
		local_manpower_modifier = 0.10
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			NOT = { base_manpower = 2 }
		}
		modifier = {
			factor = 0.5
			NOT = { base_manpower = 4 }
		}
		modifier = {
			factor = 1.5
			base_manpower = 6
		}
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 0.5
			is_overseas = yes
		}
		modifier = {
			factor = 0.9
			NOT = {
				trade_goods = iron
				trade_goods = copper
				trade_goods = hardware
				trade_goods = arms
			}		
		}		
	}
}

################################################
# Tier 2, 16th Century Buildings
################################################
barracks = {
	cost = 200
	time = 12
	
	trigger = {
		NOT = {  has_building = regimental_camp }
		NOT = {  has_building = conscription_center }
	}
	
	make_obsolete = training_fields
	
	modifier = {
		land_forcelimit = 0.5
		local_manpower = 0.200
		local_manpower_modifier = 0.20
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			NOT = { base_manpower = 2 }
		}
		modifier = {
			factor = 0.5
			NOT = { base_manpower = 4 }
		}
		modifier = {
			factor = 1.5
			base_manpower = 6
		}
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 0.5
			is_overseas = yes
		}
		modifier = {
			factor = 0.9
			NOT = {
				trade_goods = iron
				trade_goods = copper
				trade_goods = hardware
				trade_goods = arms
			}		
		}		
	}
}

################################################
# Tier 3, 17th Century Buildings
################################################
arsenal = {
	cost = 300
	time = 24
	
	make_obsolete = armory
	
	modifier = {
		regiment_recruit_speed = -0.50
		local_defensiveness = 0.10
		garrison_growth = 0.30
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0.4
			always = yes
		}
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 0.6
			is_overseas = yes
		}
		modifier = {
			factor = 0.75
			NOT = { unrest = 1 }
		}
		modifier = {
			factor = 0.8
			NOT = {
				trade_goods = iron
				trade_goods = copper
				trade_goods = hardware
				trade_goods = arms
			}		
		}
		modifier = {
			factor = 1.5
			OR = {
				has_building = fort_14th
				has_building = fort_15th
				has_building = fort_16th
				has_building = fort_17th
				has_building = fort_18th
			}		
		}		
	}
}

regimental_camp = {
	cost = 300
	time = 24
	
	make_obsolete = barracks
	
	trigger = {
		NOT = {  has_building = conscription_center }
	}
	
	modifier = {
		land_forcelimit = 1
		local_manpower = 0.400
		local_manpower_modifier = 0.35
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			NOT = { base_manpower = 2 }
		}
		modifier = {
			factor = 0.5
			NOT = { base_manpower = 4 }
		}
		modifier = {
			factor = 1.5
			base_manpower = 8
		}
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 0.5
			is_overseas = yes
		}
		modifier = {
			factor = 0.8
			NOT = {
				trade_goods = iron
				trade_goods = copper
				trade_goods = hardware
				trade_goods = arms
			}		
		}		
	}
}

################################################
# Tier 4, 18th Century Buildings
################################################
conscription_center = {
	cost = 400
	time = 36
	
	make_obsolete = regimental_camp
	
	modifier = {
		land_forcelimit = 2
		local_manpower = 0.600
		local_manpower_modifier = 0.5
	}
	
	ai_will_do = {
		factor = 100
		modifier = {
			factor = 0
			NOT = { base_manpower = 2 }
		}
		modifier = {
			factor = 0.5
			NOT = { base_manpower = 4 }
		}
		modifier = {
			factor = 1.5
			base_manpower = 8
		}
		modifier = {
			factor = 2.0
			is_capital = yes
		}
		modifier = {
			factor = 0.5
			is_overseas = yes
		}
		modifier = {
			factor = 0.8
			NOT = {
				trade_goods = iron
				trade_goods = copper
				trade_goods = hardware
				trade_goods = arms
			}		
		}		
	}
}