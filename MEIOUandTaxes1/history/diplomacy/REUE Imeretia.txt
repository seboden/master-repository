
# Ottoman vassal
vassal = {
	first = TUR
	second = IME
	start_date = 1540.1.1
	end_date = 1810.1.1
}

# Armenia as vassal
vassal = {
	first = QAR
	second = ARM
	start_date = 1356.1.1
	end_date = 1360.1.1
}
vassal = {
	first = CHU
	second = SYU
	start_date = 1356.1.1
	end_date = 1357.1.1
}
vassal = {
	first = WHI
	second = SYU
	start_date = 1357.1.1
	end_date = 1360.1.1
}
