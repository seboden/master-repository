# MYR - Yeren

government = steppe_horde government_rank = 3
mercantilism = 0.0
technology_group = steppestech
primary_culture = daur
religion = tengri_pagan_reformed
capital = 1058	# Jaxa

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1356.1.1 = {
	monarch = {
		name = "Council of Elders"
		ADM = 2
		DIP = 2
		MIL = 2
		regent = yes
	}
}

1540.1.1 = {
	monarch = {
		name = "Boobei Baatur"
		dynasty = "Dular"
		adm = 2
		dip = 2
		mil = 2
	}
	set_government_rank = 4
}

1560.1.1 = {
	heir = {
		name = "Subui"
		monarch_name = "Subui"
		dynasty = "Dular"
		birth_date = 1560.1.1
		death_date = 1580.1.1
		claim = 95
		adm = 2
		dip = 3
		mil = 3
	}
} #True name unknown

1580.1.1 = {
	monarch = {
		name = "Subui"
		dynasty = "Dular"
		adm = 2
		dip = 3
		mil = 3
	}
	heir = {
		name = "Bombogor"
		monarch_name = "Bombogor"
		dynasty = "Dular"
		birth_date = 1580.1.1
		death_date = 1640.1.1
		claim = 95
		adm = 2
		dip = 3
		mil = 2
	}
}

1620.1.1 = {
	monarch = {
		name = "Bombogor"
		dynasty = "Dular"
		adm = 2
		dip = 3
		mil = 2
	}
}
1640.1.1 = {
	monarch = {
		name = "Arbasi"
		dynasty = "Dular"
		adm = 2
		dip = 3
		mil = 2
	}
}
1665.1.1 = {
	monarch = {
		name = "Nicefor"
		dynasty = "Czernichowski"
		adm = 4
		dip = 3
		mil = 5
	}
}
