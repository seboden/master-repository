# TKD - Takeda clan

government = early_medieval_japanese_gov
government_rank = 3
mercantilism = 0.0
primary_culture = kanto
religion = mahayana
technology_group = chinese
capital = 2289		# Kai

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1330.1.1 = { 
	monarch = {
		name = "Nobutake"
		dynasty = "Takeda"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	heir = {
		name = "Nobunari"
		monarch_name = "Nobunari"
		dynasty = "Toki"
		birth_date = 1330.1.1
		death_date = 1394.1.1
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1359.8.7 = { 
	monarch = {
		name = "Nobunari"
		dynasty = "Takeda"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	heir = {
		name = "Nobuharu"
		monarch_name = "Nobumitsu"
		dynasty = "Takeda"
		birth_date = 1355.1.1
		death_date = 1413.1.1
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1394.1.1 = { 
	monarch = {
		name = "Nobuharu"
		dynasty = "Takeda"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	heir = {
		name = "Nobumitsu"
		monarch_name = "Nobumitsu"
		dynasty = "Takeda"
		birth_date = 1380.1.1
		death_date = 1417.2.22
		claim = 90
		ADM = 2
		DIP = 2
		MIL = 1
	}
}
	
1413.1.1 = { 
	monarch = {
		name = "Nobumitsu"
		dynasty = "Takeda"
		ADM = 2
		DIP = 2
		MIL = 1
	}
	heir = {
		name = "Nobushige"
		monarch_name = "Nobushige"
		dynasty = "Takeda"
		birth_date = 1386.1.1
		death_date = 1450.12.28
		claim = 90
		ADM = 3
		DIP = 2
		MIL = 3
	}
}
	
1417.2.22 = { 
	monarch = {
		name = "Nobushige"
		dynasty = "Takeda"
		ADM = 3
		DIP = 2
		MIL = 3
	}
}
	
1430.1.1 = {
	heir = {
		name = "Nobumori"
		monarch_name = "Nobumori"
		dynasty = "Takeda"
		birth_date = 1430.1.1
		death_date = 1455.1.1
		claim = 90
		ADM = 1
		DIP = 2
		MIL = 1
	}
}
	
1450.12.28 = { 
	monarch = {
		name = "Nobumori"
		dynasty = "Takeda"
		ADM = 1
		DIP = 2
		MIL = 1
	}
	heir = {
		name = "Nobumasa"
		monarch_name = "Nobumasa"
		dynasty = "Takeda"
		birth_date = 1447.1.1
		death_date = 1505.10.13
		claim = 90
		ADM = 2
		DIP = 3
		MIL = 2
	}
}
	
1455.1.1 = { 
	monarch = {
		name = "Nobumasa"
		dynasty = "Takeda"
		ADM = 2
		DIP = 3
		MIL = 2
	}
}

1471.1.1 = {
	heir = {
		name = "Nobutsuna"
		monarch_name = "Nobutsuna"
		dynasty = "Takeda"
		birth_date = 1471.1.1
		death_date = 1507.3.27
		claim = 90
		ADM = 1
		DIP = 1
		MIL = 2
	}
}
	
1491.1.1 = { 
	monarch = {
		name = "Nobutsuna"
		dynasty = "Takeda"
		ADM = 1
		DIP = 1
		MIL = 2
		}
	}
	
1494.2.11 = {
	heir = {
		name = "Nobutora"
		monarch_name = "Nobutora"
		dynasty = "Takeda"
		birth_date = 1494.2.11
		death_date = 1574.3.27
		claim = 90
		ADM = 4
		DIP = 3
		MIL = 4
	}
}

	
1507.3.27 = { 
	monarch = {
		name = "Nobutora"
		dynasty = "Takeda"
		ADM = 4
		DIP = 3
		MIL = 4
	}
	}
	
1521.12.1 = {
	heir = {
		name = "Harunobu"
		monarch_name = "Harunobu"
		dynasty = "Takeda"
		birth_date = 1521.12.1
		death_date = 1573.5.13
		claim = 90
		ADM = 5
		DIP = 5
		MIL = 5
	}
}
	
1541.6.1 = { 
	monarch = {
		name = "Harunobu"
		dynasty = "Takeda"
		ADM = 5
		DIP = 5
		MIL = 5
	}
	heir = {
		name = "Yoshinobu"
		monarch_name = "Yoshinobu"
		dynasty = "Takeda"
		birth_date = 1538.1.1
		death_date = 1567.1.1
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1567.1.1 = {
	heir = {
		name = "Katsuyori"
		monarch_name = "Katsuyori"
		dynasty = "Takeda"
		birth_date = 1546.12.1
		death_date = 1582.4.3
		claim = 80
		ADM = 3
		DIP = 2
		MIL = 4
	}
}
	
1573.5.13 = { 
	monarch = {
		name = "Katsuyori"
		dynasty = "Takeda"
		ADM = 3
		DIP = 2
		MIL = 4
	}
	heir = {
		name = "Nobukatsu"
		monarch_name = "Nobukatsu"
		dynasty = "Takeda"
		birth_date = 1567.12.11
		death_date = 1582.4.3
		claim = 90
		ADM = 2
		DIP = 2
		MIL = 2
	}
}
