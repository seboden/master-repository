# WLB - Waldenburg

government = feudal_monarchy
government_rank = 1
mercantilism = 0.0
primary_culture = high_saxon
religion = catholic
technology_group = western
capital = 3741

1000.1.1   = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1328.1.1   = {
	monarch = {
		name = "Graf"
		dynasty = "von Waldenburg"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

# 1378 - into the county of Sch�nburg

1540.1.1   = {
	religion = protestant
}

