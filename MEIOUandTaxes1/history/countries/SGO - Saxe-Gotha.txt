# No previous file for Saxe-Gotha

government = feudal_monarchy government_rank = 4 #DUCHY
mercantilism = 0.0
primary_culture = high_saxon
religion = catholic
elector = yes
technology_group = western
capital = 71

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1524.1.1 = {
	religion = protestant
	set_country_flag = wettin_succession
}

1680.1.1 = {
	monarch = {
		name = "Friedrich I"
		dynasty = "von Wettin"
		DIP = 2
		ADM = 2
		MIL = 2
	}
}

1691.1.1 = {
	monarch = {
		name = "Friedrich II"
		dynasty = "von Wettin"
		DIP = 2
		ADM = 2
		MIL = 2
	}
}

1732.1.1 = {
	monarch = {
		name = "Friedrich III"
		dynasty = "von Wettin"
		DIP = 2
		ADM = 2
		MIL = 2
	}
}

1772.1.1 = {
	monarch = {
		name = "Ernst II"
		dynasty = "von Wettin"
		DIP = 2
		ADM = 2
		MIL = 2
	}
}

1804.1.1 = {
	monarch = {
		name = "August"
		dynasty = "von Wettin"
		DIP = 2
		ADM = 2
		MIL = 2
	}
}
