# NUR - Nurmberg

government = imperial_city
mercantilism = 5
primary_culture = eastfranconian
religion = catholic
technology_group = western
capital = 81
fixed_capital = 81	# N�rmberg

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1356.1.1 = {
	monarch = {
		name = "Stadtrat"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1525.1.1 = {
	religion = protestant
	}
