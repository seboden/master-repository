# AYG - Aymara kingdom of Caranga

government = tribal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = aimara
religion = inti
technology_group = south_american
capital = 3439

1356.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 5 }
	monarch = {
		name = "Tribal Council"
		dynasty = "Aymara"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

# Conquered by the Incas under Huayna Capac (reign 1483-1523), although the exact date of this takeover is unknown.
