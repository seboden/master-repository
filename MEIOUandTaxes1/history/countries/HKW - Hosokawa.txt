# HKW - Hosokawa clan

government = early_medieval_japanese_gov
government_rank = 3
mercantilism = 0.0
primary_culture = kansai
religion = mahayana
technology_group = chinese
capital = 3968		# Awa

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1352.4.5 = {
	monarch = {
		name = "Yoriyuki"
		dynasty = "Hosokawa"
		ADM = 5
		DIP = 4
		MIL = 3
		}
}

1352.4.5 = {
	heir = {
		name = "Yorimoto"
		monarch_name = "Yorimoto"
		dynasty = "Hosokawa"
		birth_date = 1343.1.1
		death_date = 1397.6.2
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1392.3.25 = {
	monarch = {
		name = "Yorimoto" 
		dynasty = "Hosokawa"
		ADM = 3
		DIP = 3
		MIL = 3
		}
}

1392.3.25 = {
	heir = {
		name = "Mitsumoto"
		monarch_name = "Mitsumoto"
		dynasty = "Hosokawa"
		birth_date = 1378.1.1
		death_date = 1426.11.15
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1397.6.2 = {
	monarch = {
		name = "Mitsumoto" 
		dynasty = "Hosokawa"
		ADM = 4
		DIP = 3
		MIL = 3
		}
}

1399.1.1 = {
	heir = {
		name = "Mochimoto"
		monarch_name = "Mochimoto"
		dynasty = "Hosokawa"
		birth_date = 1399.1.1
		death_date = 1429.8.14
		claim = 90
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1426.11.15 = {
	monarch = {
		name = "Mochimoto" 
		dynasty = "Hosokawa"
		ADM = 2
		DIP = 2
		MIL = 2
		}
}

1429.8.14 = {
	monarch = {
		name = "Mochiyuki" 
		dynasty = "Hosokawa"
		ADM = 3
		DIP = 3
		MIL = 2
		}
}

1430.1.1 = {
	heir = {
		name = "Katsumoto"
		monarch_name = "Katsumoto"
		dynasty = "Hosokawa"
		birth_date = 1430.1.1
		death_date = 1473.6.6
		claim = 90
		ADM = 3
		DIP = 4
		MIL = 3
	}
}

1442.9.8 = {
	monarch = {
		name = "Katsumoto" 
		dynasty = "Hosokawa"
		ADM = 3
		DIP = 4
		MIL = 3
		}
}

1466.1.1 = {
	heir = {
		name = "Masamoto"
		monarch_name = "Masamoto"
		dynasty = "Hosokawa"
		birth_date = 1466.1.1
		death_date = 1507.8.1
		claim = 90
		ADM = 2
		DIP = 3
		MIL = 2
	}
}

1473.11.15 = {
	monarch = {
		name = "Masamoto" 
		dynasty = "Hosokawa"
		ADM = 2
		DIP = 3
		MIL = 2
		}
}

1489.1.1 = {
	heir = {
		name = "Sumiyuki"
		monarch_name = "Sumiyuki"
		dynasty = "Hosokawa"
		birth_date = 1399.1.1
		death_date = 1507.9.7
		claim = 60
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1503.5.1 = {
	heir = {
		name = "Sumimoto"
		monarch_name = "Sumimoto"
		dynasty = "Hosokawa"
		birth_date = 1489.1.1
		death_date = 1520.6.24
		claim = 70
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1507.8.1 = {
	monarch = {
		name = "Sumiyuki" 
		dynasty = "Hosokawa"
		ADM = 2
		DIP = 2
		MIL = 2
		}
}

1507.9.7 = {
	monarch = {
		name = "Sumitomo" 
		dynasty = "Hosokawa"
		ADM = 2
		DIP = 2
		MIL = 2
		}
}

1514.1.1 = {
	heir = {
		name = "Harumoto"
		monarch_name = "Harumoto"
		dynasty = "Hosokawa"
		birth_date = 1514.1.1
		death_date = 1563.3.24
		claim = 80
		ADM = 4
		DIP = 4
		MIL = 3
	}
}

1520.6.24 = {
	monarch = {
		name = "Takakuni" 
		dynasty = "Hosokawa"
		ADM = 3
		DIP = 4
		MIL = 4
		}
}

1531.7.21 = {
	monarch = {
		name = "Harumoto" 
		dynasty = "Hosokawa"
		ADM = 4
		DIP = 4
		MIL = 3
		}
}

1548.1.1 = {
	heir = {
		name = "Akimoto"
		monarch_name = "Akimoto"
		dynasty = "Hosokawa"
		birth_date = 1548.1.1
		death_date = 1592.1.1
		claim = 70
		ADM = 1
		DIP = 2
		MIL = 1
	}
}