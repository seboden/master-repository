# QII - Qi
# LS - Chinese Civil War

government = chinese_monarchy_5 government_rank = 4
mercantilism = 0.0
technology_group = chinese
religion = confucianism
primary_culture = hanyu
capital = 699

historical_rival = KOR
historical_rival = JOS

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 3 }
}

1338.1.1 = {
	monarch = {
		name = "Gui"
		dynasty = "Mao"
		ADM = 5
		DIP = 3
		MIL = 5
	}
	set_country_flag = red_turban_reb
}
