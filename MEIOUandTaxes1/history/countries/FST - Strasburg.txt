# FST - Strasburg

government = imperial_city
mercantilism = 5
primary_culture = rhine_alemanisch
religion = catholic
technology_group = western
capital = 4006
fixed_capital = 4006

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1349.1.1 = {
	monarch = {
		name = "City Council"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
