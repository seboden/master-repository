# Country: Minangkabau
# file name: MKP - Minangkabau
# MEIOU-FB Indonesia mod v3 - for IN+JV
# for performance reasons MKP represents both Minangkabau + Padang + others
# FB-TODO rulers c1350 to 1789
# 2010-jan-21 - FB - HT3 changes

government = eastern_monarchy government_rank = 5
mercantilism = 0.0
primary_culture = minang
religion = vajrayana
technology_group = austranesian
capital = 619	# Minangkabau

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1347.1.1 = {
	monarch = {
		name = "Adityawarman"
		dynasty = "Adityawarman"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1550.1.1 = {
	religion = sunni
	government_rank = 4
}

1789.1.1 = {
	monarch = {
		name = "Khalifatullah Inayat Syah"
		dynasty = "Adityawarman"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1816.1.1 = {
	monarch = {
		name = "Alam Bagager"
		dynasty = "Adityawarman"
		ADM = 4
		DIP = 3
		MIL = 3
	}
}

1821.1.1 = {
	monarch = {
		name = "Yahsir Alam"
		dynasty = "Adityawarman"
		ADM = 2
		DIP = 4
		MIL = 1
	}
}
