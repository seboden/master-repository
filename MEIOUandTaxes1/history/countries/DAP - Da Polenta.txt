# DAP - Da Polenta Ravenna

government = signoria_monarchy 
mercantilism = 0.0
primary_culture = romagnol
religion = catholic
technology_group = western
capital = 3867

historical_rival = VEN
historical_rival = PAP

1000.1.1   = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1322.1.1   = {
	monarch = {
		name = "Ostasio I"
        dynasty = "Da Polenta"
		adm = 3
		dip = 3
		mil = 3
	}
}

1346.1.1   = {
	monarch = {
		name = "Bernardino"
        dynasty = "Da Polenta"
		adm = 3
		dip = 3
		mil = 3
	}
	heir = {
		name = "Guido"
		monarch_name ="Guido III"
		dynasty = "Da Polenta"
		birth_date = 1330.1.1
		death_date = 1389.1.1
		claim = 80
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1359.1.1   = {
	monarch = {
		name = "Guido III"
        dynasty = "Da Polenta"
		adm = 3
		dip = 3
		mil = 3
	}
}

1390.1.1   = {
	monarch = {
		name = "Ostasio II"
        dynasty = "Da Polenta"
		adm = 3
		dip = 3
		mil = 3
	}
}

1396.1.1   = {
	monarch = {
		name = "Bernardino II"
        dynasty = "Da Polenta"
		adm = 3
		dip = 3
		mil = 3
	}
}

1400.1.1   = {
	monarch = {
		name = "Obizzo"
        dynasty = "Da Polenta"
		adm = 3
		dip = 3
		mil = 3
	}
}

1404.1.1   = {
	monarch = {
		name = "Aldobrandino"
        dynasty = "Da Polenta"
		adm = 3
		dip = 3
		mil = 3
	}
}

1406.1.1   = {
	monarch = {
		name = "Ostasio III"
        dynasty = "Da Polenta"
		adm = 3
		dip = 3
		mil = 3
	}
}

# 1440 to Venice
