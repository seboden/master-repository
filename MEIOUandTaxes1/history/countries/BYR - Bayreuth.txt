# BYR - Bayreuth

government = despotic_monarchy government_rank = 2
mercantilism = 0.0
technology_group = western
primary_culture = eastfranconian
religion = catholic
capital = 70

1356.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1398.1.1 = {
	monarch = {
		name = "Friedrich V"
		dynasty = "von Hohenzollern"
		DIP = 4
		ADM = 4
		MIL = 1
	}
}

1522.3.8 = {
	monarch = {
		name = "Vormundschaftsregierung"
		regent = yes
		dip = 1
		adm = 1
		mil = 1
	}
	heir = {
		name = "Albert Alcibiades"
		monarch_name = "Albert Alcibiades"
		dynasty = "von Hohenzollern"
		birth_date = 1522.3.8
		death_date = 1557.1.8
		claim = 95
		ADM = 2
		DIP = 2
		MIL = 5
	}
}

1528.1.1   = { religion = protestant }
1530.1.1 = { set_country_flag = hohenzollern_succession }
1530.1.2 = {
	government = administrative_monarchy
}
