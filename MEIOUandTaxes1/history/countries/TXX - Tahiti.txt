# TXX - Tahiti


government = tribal_monarchy government_rank = 3
mercantilism = 0.0
primary_culture = polynesian
religion = polynesian_religion
technology_group = hawaii_tech
capital = 1244	# Tahiti

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 3 }
}

1350.1.1 = {
	monarch = {
		name = "Krang"
		dynasty = "Krang"
		ADM = 4
		DIP = 5
		MIL = 4
	}
}
