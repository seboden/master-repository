# MIX - Mixtecs

government = tribal_monarchy government_rank = 3
mercantilism = 0.0
primary_culture = mixtec
religion = nahuatl
technology_group = mesoamerican
capital = 2645

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1356.1.1 = {
	monarch = {
		name = "Atonal"
		dynasty = "Tilantongo"
		ADM = 5
		DIP = 4
		MIL = 3
	}
}#temp pending further research

1450.1.1 = {
	monarch = {
		name = "Atonal II"
		dynasty = "Tilantongo"
		ADM = 4
		DIP = 3
		MIL = 2
	}
}
