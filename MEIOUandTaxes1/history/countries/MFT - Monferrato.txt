# MFT - Monferrato
# 2010-jan-21 - FB - HT3 changes

government = despotic_monarchy government_rank = 2
mercantilism = 0.0
# offensive_defensive = 3		##1
# land_naval = -4			##1
# quality_quantity = 0		##-1
primary_culture = piedmontese
religion = catholic
technology_group = western
capital = 1377

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1306.1.1 = {
	monarch = {
		name = "Teodoro I"
		dynasty = "Palaiologos"
		ADM = 4
		DIP = 4
		MIL = 6
	}
}

1321.2.5 = {
	heir  = {
		name = "Gian"
		monarch_name = "Gian II"
		dynasty = "Palaiologos"
		birth_date = 1321.2.5
		death_date = 1372.3.19
		claim = 95
		ADM = 4
		DIP = 4
		MIL = 5
	}
}


1338.4.24 = {
	monarch = {
		name = "Gian II"
		dynasty = "Palaiologos"
		ADM = 4
		DIP = 4
		MIL = 5
	}
}

1360.1.1 = {
	heir  = {
		name = "Otone"
		monarch_name = "Otone III"
		dynasty = "Palaiologos"
		birth_date = 1360.1.1
		death_date = 1378.12.16
		claim = 95
		ADM = 2
		DIP = 1
		MIL = 1
	}
}

1372.3.19 = {
	monarch = {
		name = "Regency Council"
		regent = yes
		ADM = 2
		DIP = 2
		MIL = 2
	}
	heir  = {
		name = "Otone"
		monarch_name = "Otone III"
		dynasty = "Palaiologos"
		birth_date = 1360.1.1
		death_date = 1378.12.16
		claim = 95
		ADM = 2
		DIP = 1
		MIL = 1
	}
}

1375.1.1 = {
	monarch = {
		name = "Otone III"
		dynasty = "Palaiologos"
		ADM = 2
		DIP = 1
		MIL = 1
	}
	heir  = {
		name = "Gian"
		monarch_name = "Gian III"
		dynasty = "Palaiologos"
		birth_date = 1362.1.1
		death_date = 1381.8.25
		claim = 95
		ADM = 3
		DIP = 2
		MIL = 3
	}
}

1378.12.16 = {
	monarch = {
		name = "Gian III"
		dynasty = "Palaiologos"
		ADM = 3
		DIP = 2
		MIL = 3
	}
	heir  = {
		name = "Teodoro"
		monarch_name = "Teodoro II"
		dynasty = "Palaiologos"
		birth_date = 1364.1.1
		death_date = 1418.4.26
		claim = 95
		ADM = 3
		DIP = 4
		MIL = 5
	}
}

1381.8.25 = {
	monarch = {
		name = "Teodoro II"
		dynasty = "Palaiologos"
		ADM = 3
		DIP = 4
		MIL = 5
	}
}

1395.3.23 = {
	heir  = {
		name = "Gian Giacomo"
		monarch_name = "Gian Giacomo"
		dynasty = "Palaiologos"
		birth_date = 1395.3.23
		death_date = 1445.3.12
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 2
	}
}

1418.4.26 = {
	monarch = {
		name = "Gian Giacomo"
		dynasty = "Palaiologos"
		ADM = 3
		DIP = 3
		MIL = 2
	}
	heir  = {
		name = "Gian"
		monarch_name = "Gian IV"
		dynasty = "Palaiologos"
		birth_date = 1413.6.24
		death_date = 1464.1.19
		claim = 95
		ADM = 3
		DIP = 1
		MIL = 2
	}
}

1445.3.12 = {
	monarch = {
		name = "Gian IV"
		dynasty = "Palaiologos"
		ADM = 3
		DIP = 1
		MIL = 2
	}
	heir  = {
		name = "Guglielmo"
		monarch_name = "Guglielmo VIII"
		dynasty = "Palaiologos"
		birth_date = 1420.7.19
		death_date = 1483.2.27
		claim = 95
		ADM = 5
		DIP = 5
		MIL = 6
	}
}

1464.1.19 = {
	monarch = {
		name = "Guglielmo VIII"
		dynasty = "Palaiologos"
		ADM = 5
		DIP = 5
		MIL = 6
	}
	heir  = {
		name = "Bonifacio"
		monarch_name = "Bonifacio III"
		dynasty = "Palaiologos"
		birth_date = 1424.8.10
		death_date = 1494.3.4
		claim = 95
		ADM = 3
		DIP = 4
		MIL = 3
	}
}

1483.2.27 = {
	monarch = {
		name = "Bonifacio III"
		dynasty = "Palaiologos"
		ADM = 3
		DIP = 4
		MIL = 3
	}
}
	
1486.8.10 = {
	heir  = {
		name = "Guglielmo"
		monarch_name = "Guglielmo IX"
		dynasty = "Palaiologos"
		birth_date = 1486.8.10
		death_date = 1518.10.1
		claim = 95
		ADM = 2
		DIP = 2
		MIL = 4
	}
}

1494.3.4 = {
	monarch = {
		name = "Regency Council"
		regent = yes
		ADM = 2
		DIP = 2
		MIL = 2
	}
	heir  = {
		name = "Guglielmo"
		monarch_name = "Guglielmo IX"
		dynasty = "Palaiologos"
		birth_date = 1486.8.10
		death_date = 1518.10.1
		claim = 95
		ADM = 2
		DIP = 2
		MIL = 4
	}
}

1501.8.10 = {
	monarch = {
		name = "Guglielmo IX"
		dynasty = "Palaiologos"
		ADM = 2
		DIP = 2
		MIL = 4
	}
}

1512.12.21 = {
	heir  = {
		name = "Bonifacio"
		monarch_name = "Bonifacio IV"
		dynasty = "Palaiologos"
		birth_date = 1512.12.21
		death_date = 1530.6.6
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1518.10.1 = {
	monarch = {
		name = "Anna d'Alencon"
		regent = yes
		ADM = 4
		DIP = 3
		MIL = 3
	}
	heir  = {
		name = "Bonifacio"
		monarch_name = "Bonifacio IV"
		dynasty = "Palaiologos"
		birth_date = 1512.12.21
		death_date = 1530.6.6
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1527.12.21 = {
	monarch = {
		name = "Bonifacio IV"
		dynasty = "Palaiologos"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	heir  = {
		name = "Gian George"
		monarch_name = "Gian George"
		dynasty = "Palaiologos"
		birth_date = 1488.1.20
		death_date = 1533.4.30
		claim = 95
		ADM = 4
		DIP = 3
		MIL = 2
	}
}

1530.1.2 = {
	government = administrative_monarchy
}

1530.6.6 = {
	monarch = {
		name = "Gian George"
		dynasty = "Palaiologos"
		ADM = 4
		DIP = 3
		MIL = 2
	}
}

# 1536 : Passes to the Gonzagua dukes of Mantua; after three years of spanish occupation
