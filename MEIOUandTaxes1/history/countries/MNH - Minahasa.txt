# MNH - Minahasa

government = tribal_monarchy government_rank = 3
mercantilism = 0.0
# offensive_defensive = -4
primary_culture = sulawesi
religion = polynesian_religion
technology_group = austranesian
capital = 644 # Menade

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1608.1.1 = {
	religion = sunni
}

1820.1.1 = {
	religion = reformed
}
