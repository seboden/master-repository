# Nazca
# Tag : NZC

government = tribal_monarchy government_rank = 1
mercantilism = 0.0
primary_culture = nazca
religion = inti
technology_group = south_american
capital = 2066	# Nazca

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 3 }
}

1352.1.1 = {
	monarch = {
		name = "Huama"
		dynasty = "Nazca"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
