# Free City of Danzig

government = imperial_city
mercantilism = 20
primary_culture = prussian
religion = catholic
technology_group = western
fixed_capital = 2355	# Danzig

1000.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 1 }
}

1807.7.9 = {
	monarch = {
		name = "Franz Joseph I"
		ADM = 3
		DIP = 2
		MIL = 4
		leader = {	name = "Franz Joseph I"   	type = general	rank = 1	fire = 3	shock = 3	manuever = 3	siege = 2 }
	}
}
