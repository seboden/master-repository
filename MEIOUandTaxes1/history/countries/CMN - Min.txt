# MIN - Min
# LS - Chinese Civil War
# 2010-jan-21 - FB - HT3 changes

government = chinese_monarchy_5 government_rank = 4
# aristocracy_plutocracy = 4
mercantilism = 0.0
# secularism_theocracy = -4
technology_group = chinese
religion = confucianism
primary_culture = minyu
add_accepted_culture = wuhan
capital = 1053

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1319.1.1 = {
	monarch = {
		name = "Guozhen"
		dynasty = "Fang"
		ADM = 5
		DIP = 3
		MIL = 6
	}
}

1662.1.1 = {
	monarch = {
		name = "Jingzhong"
		dynasty = "Geng"
		ADM = 5
		DIP = 5
		MIL = 6
	}
}
