#SRK - Surakarta
#10.01.27 FB-HT3 - make HT3 changes

government = eastern_monarchy government_rank = 4 # oligarchical_monarchy
mercantilism = 0.0
primary_culture = sulawesi
technology_group = austranesian
religion = sunni
capital = 1386	# Surakarta

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 3 }
}

1749.12.11 = {
	monarch = {
		name = "Paku III"
		dynasty = "Buwono"
		ADM = 2
		DIP = 3
		MIL = 3
	}
	heir = {
		name = "Paku"
		monarch_name = "Paku IV"
		dynasty = "Buwono"
		birth_date = 1740.1.1
		death_date = 1820.8.1
		claim = 95
		ADM = 1
		DIP = 2
		MIL = 1
	}
}

1788.8.26 = {
	monarch = {
		name = "Paku IV"
		dynasty = "Buwono"
		ADM = 1
		DIP = 2
		MIL = 1
	}
	heir = {
		name = "Paku"
		monarch_name = "Paku V"
		dynasty = "Buwono"
		birth_date = 1770.1.1
		death_date = 1840.1.1
		claim = 95
		ADM = 1
		DIP = 2
		MIL = 1
	}
}

1820.8.1 = {
	monarch = {
		name = "Paku V"
		dynasty = "Buwono"
		ADM = 1
		DIP = 2
		MIL = 1
	}
}