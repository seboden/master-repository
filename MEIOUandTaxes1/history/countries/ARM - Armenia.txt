# ARM - Arm�nia

government = despotic_monarchy government_rank = 5 #Kingdom
mercantilism = 0.0
primary_culture = owm_armenian #Eastern Armenian
add_accepted_culture = ge_armenian
add_accepted_culture = cilician
religion = coptic
technology_group = eastern
capital = 419

1320.1.1 = {
	monarch = {
		name = "Zacharie IV"
		dynasty = "Zakarian"
		birth_date = 1300.1.1
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1356.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1358.1.1 = {
	monarch = {
		name = "Schansch� IV"
		dynasty = "Zakarian"
		birth_date = 1320.1.1
		ADM = 2
		DIP = 3
		MIL = 1
	}
}

# Conquered by the Qara Qoyunlu
