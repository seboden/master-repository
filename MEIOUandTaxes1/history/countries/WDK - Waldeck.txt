# WDK - Waldeck

government = feudal_monarchy
government_rank = 1
mercantilism = 0.0
primary_culture = hessian
religion = catholic
technology_group = western
capital = 3730

1000.1.1   = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}

1348.5.1   = {
	monarch = {
		name = "Otto II"
		dynasty = "von Waldeck"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1397.2.16  = {
	monarch = {
		name = "Heinrich VI"
		dynasty = "von Waldeck"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1442.1.1   = {
	monarch = {
		name = "Wolrad I"
		dynasty = "von Waldeck"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1475.2.1   = {
	monarch = {
		name = "Philipp II"
		dynasty = "von Waldeck"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1524.10.26 = {
	monarch = {
		name = "Philipp III"
		dynasty = "von Waldeck"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1525.1.1 = {
	religion = protestant
}
1530.1.2 = {
	government = administrative_monarchy
}

1530.8.1 = {
	heir = {
		name = "Daniel"
		monarch_name = "Daniel I"
		dynasty = "von Waldeck"
		birth_date = 1530.8.1
		death_date = 1577.6.7
		claim = 95
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1539.6.20  = {
	monarch = {
		name = "Wolrad II"
		dynasty = "von Waldeck"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1575.4.15  = {
	monarch = {
		name = "Josias I"
		dynasty = "von Waldeck"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1588.8.6   = {
	monarch = {
		name = "Christian"
		dynasty = "von Waldeck"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1637.12.31 = {
	monarch = {
		name = "Philipp VII"
		dynasty = "von Waldeck"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1645.2.24  = {
	monarch = {
		name = "Christian Ludwig"
		dynasty = "von Waldeck"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1706.12.12 = {
	monarch = {
		name = "Friedrich Anton"
		dynasty = "von Waldeck"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1728.1.1   = {
	monarch = {
		name = "Karl August"
		dynasty = "von Waldeck"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1763.8.29  = {
	monarch = {
		name = "Friedrich Karl"
		dynasty = "von Waldeck"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1812.9.24  = {
	monarch = {
		name = "Georg I"
		dynasty = "von Waldeck"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1813.9.9   = {
	monarch = {
		name = "Georg II"
		dynasty = "von Waldeck"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1845.5.15  = {
	monarch = {
		name = "Georg Viktor"
		dynasty = "von Waldeck"
		ADM = 2
		DIP = 2
		MIL = 2
	}
}
