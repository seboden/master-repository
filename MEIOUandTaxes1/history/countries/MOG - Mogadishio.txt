# City-state of Mogadishio
# Tag : MOG
# Actual rulers of the Ajuraan State, but invented dates

government = despotic_monarchy government_rank = 3
mercantilism = 0.0
primary_culture = somali
religion = sunni
technology_group = east_african unit_type = soudantech
capital = 1199 # Mareeg

historical_friend = ZAN

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 2 }
}

1356.1.1 = {
	monarch = {
		name = "Ajuran"
		dynasty = "Gareen"
		ADM = 4
		DIP = 3
		MIL = 3
	}
}

1426.1.1 = {
	monarch = {
		name = "Arliqo"
		dynasty = "Gareen"
		ADM = 4
		DIP = 3
		MIL = 3
	}
}

1506.1.1 = {
	monarch = {
		name = "Sarjelle"
		dynasty = "Gareen"
		ADM = 4
		DIP = 3
		MIL = 3
	}
}

1576.1.1 = {
	monarch = {
		name = "Fadumo"
		dynasty = "Gareen"
		ADM = 4
		DIP = 3
		MIL = 3
	}
}

1656.1.1 = {
	monarch = {
		name = "Umur"
		dynasty = "Gareen"
		ADM = 4
		DIP = 3
		MIL = 3
	}
}
