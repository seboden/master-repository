# KOJ - Crusader Jerusalem
# 2010-jan-16 - FB - HT3 changes
# 2013-sep-29 - GG - Tag changed from KJR

government = feudal_monarchy government_rank = 5
# innovative_narrowminded = -3
mercantilism = 0.0
primary_culture = frankish
religion = catholic
technology_group = eastern
unit_type = western
capital = 379	# Judea

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 3 }
}
