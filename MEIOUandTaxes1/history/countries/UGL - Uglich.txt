# UGL - Uglich

government = feudal_monarchy government_rank = 3
mercantilism = 0.0
primary_culture = russian
religion = orthodox
technology_group = eastern
capital = 2580

1000.1.1  = {
	set_variable = { which = "centralization_decentralization" value = 2 }
}
