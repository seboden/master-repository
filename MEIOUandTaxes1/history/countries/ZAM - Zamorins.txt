# ZAM - Zamorins

government = indian_monarchy government_rank = 4
mercantilism = 0.0
technology_group = indian
religion = hinduism
primary_culture = malayalam
capital = 534	# Calicut
fixed_capital = 534 # 
historical_rival = VIJ
# historical_friend = VIJ

1000.1.1  = {
	add_country_modifier = {
		name = "concubinage"
		duration = -1
	}
	set_variable = { which = "centralization_decentralization" value = 4 }
}

# Zamorins # Hard to find info on names, Mnavikraman alive at 1498 and thus hardly Raja in 1399...#
1356.1.1 = {
	monarch = {
		name = "Manavikraman"
		dynasty = "Saamoothiri"
		DIP = 4
		ADM = 6
		MIL = 3
	}
}
