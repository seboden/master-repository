# ETR - Etruria

government = enlightened_despotism government_rank = 5
mercantilism = 0.0
primary_culture = lombard
religion = catholic
technology_group = western
capital = 116	# Firenze


1000.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 4 }
}
1801.3.21 = {
	monarch = {
		name = "Lodovico I"
		dynasty = "de Bourbon"
		ADM = 3
		DIP = 4
		MIL = 3
	}
}

1801.3.21 = {
	heir = {
		name = "Lodovico"
		monarch_name = "Lodovico II"
		dynasty = "de Bourbon"
		birth_date = 1799.12.22
		death_date = 1883.4.16
		claim = 95
		ADM = 2
		DIP = 3
		MIL = 3
	}
}

1803.5.27 = {
	monarch = {
		name = "Lodovico II"
		dynasty = "de Bourbon"
		ADM = 2
		DIP = 3
		MIL = 3
	}
}
