name = "Red Turban Rebellion"
war_goal = {
	type = unify_china
	casus_belli = cb_chinese_civil_war
	province = 708 # Dadu
}

1356.1.1   = {
	add_attacker = ZOU
	add_defender = YUA
	add_defender = CYU
	add_defender = QIN
	add_defender = LNG
	add_defender = CEN
	add_defender = CSE
}
1375.1.1   = {
	rem_attacker = ZOU
	rem_defender = YUA
	rem_defender = CYU
	rem_defender = QIN
	rem_defender = LNG
	rem_defender = CEN
	rem_defender = CSE
}