# 708 - Zhili Dadu

owner = YUA
controller = YUA
culture = hanyu
religion = confucianism
capital = "Khanbalyk"
trade_goods = services
hre = no
base_tax = 15
base_production = 15
base_manpower = 10
is_city = yes
fort_14th = yes



discovered_by = chinese
discovered_by = steppestech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = major_city
		duration = -1
	}
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
	add_permanent_province_modifier = { 
		name = great_wall_ruins
		duration = -1
	}
}
#1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1200.1.1 = { paved_road_network = yes courthouse = yes }
1250.1.1 = { temple = yes }
1271.12.18  = {#Proclamation of Yuan dynasty
	add_core = YUA
}
#1356.1.1 = {	remove_core = YUA } # Red Turbans
1368.1.1  = {
	owner = MNG
	controller = MNG
	add_core = MNG
	rename_capital = "Beiping" 
	change_province_name = "Beiping"
}
1420.1.1  = {
	rename_capital = "Beijing" 
	change_province_name = "Beijing"
}
1420.1.1 = { royal_palace = yes }
1529.3.17 = { 
	naval_arsenal = yes
	marketplace = yes
	customs_house = yes
}
1530.1.1 = { fort_14th = no fort_15th = yes }
1551.1.1 = {
	remove_province_modifier = great_wall_ruins
	add_permanent_province_modifier = {
		name = great_wall_full
		duration = -1
	}
}
1640.1.1 = {
	controller = MCH
}
1644.3.19  = {
	owner = DSH
	controller = DSH
	add_core = DSH
}
#1644.6.6 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1644.4.29 = {
	controller = MCH
}
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = DSH
	rename_capital = "Gemun Hecen"
	change_province_name = "Gemun Hecen"
} # The Qing Dynasty
1662.1.1 = {
	remove_core = MNG
}
