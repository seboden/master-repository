# 1067 - Khoongorai

owner = YEN
controller = YEN
culture = kirgiz
religion = tengri_pagan_reformed
capital = "Kem-Kemchik"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
discovered_by = steppestech

1356.1.1  = {
	add_core = YEN
}
1604.1.1 = {
	discovered_by = RUS
   	owner = RUS
   	controller = RUS
   	religion = orthodox
   	culture = russian
	capital = "Krasnoyarsk"
}
1629.1.1 = {
	add_core = RUS
}
