# No previous file for Chowanoc

culture = powhatan
religion = totemism
capital = "Chowanoc"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 45
native_ferocity = 3
native_hostileness = 4

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1607.1.1  = { discovered_by = ENG } # John Smith
1615.1.1  = { discovered_by = FRA } # �tienne Br�l�
1634.3.24 = {	owner = ENG
		controller = ENG
		culture = english
	    	religion = protestant
	    	capital = "St. Mary's City"
		citysize = 358	
		trade_goods = tobacco
	    } # Founding of St. Mary's City
1707.5.12 = { owner = GBR controller = GBR discovered_by = GBR } 
1721.1.1  = { add_core = GBR }
1729.7.30 = { capital = "Baltimore" }
1750.1.1  = { citysize = 1522 }
1764.7.1  = {	culture = american
		unrest = 6
	    } # Growing unrest
1776.7.4  = {	owner = USA
		controller = USA
		add_core = USA
	    } # Declaration of independence
1782.11.1 = {	remove_core = GBR 
		unrest = 0
	    } # Preliminary articles of peace, the British recognized Amercian independence
1790.1.1  = { capital = "Washington" }
1800.1.1  = { citysize = 4180 }
