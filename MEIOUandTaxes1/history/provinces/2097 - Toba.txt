# 2097 - Toba

owner = ATJ
controller = ATJ
culture = batak			#the Bataks - fiercely independant
religion = hinduism			#this region remained hinduism/pagan until C19.
capital = "Sibolga"
trade_goods = lumber
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian

1356.1.1   = {
	add_core = ATJ
	add_core = MKP
#	add_core = PGY
}
1515.2.1 = { training_fields = yes }
1688.1.1   = {
	add_core = NED
}
1825.1.1   = {
	owner = NED
	controller = NED
	unrest = 2
} # The Dutch gradually gained control
