# 2940 - Zemmour

owner = MOR
controller = MOR
culture = fassi
religion = sunni
capital = "Tifelt"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = wheat
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech
hre = no

#1088.1.1 = { dock = yes shipyard = yes }
#1133.1.1 = { mill = yes }
1356.1.1 = {
	add_core = MOR
	owner = FEZ
	controller = FEZ
	add_core = FEZ
}
1523.8.16 = { mill = yes }
1530.1.1 = {
	add_core = MOR
}
1554.1.1   = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = FEZ
}
1603.1.1 = { unrest = 5 } # The death of the Saadita Ahmad I al-Mansur
1613.1.1 = { unrest = 0 }
1659.1.1 = { unrest = 7 } # The last ruler of Saadi is overthrown
1660.1.1 = { unrest = 3}
1672.1.1 = { unrest = 4 } # Oppositions against Ismail, & the idea of a unified state
1727.1.1 = { unrest = 0 }
