# 2658 - Ghardaiya

owner = MZB
controller = MZB
culture = berber
religion = ibadi
capital = "Ghardaiya"
trade_goods = wool
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan

1000.1.1 = {
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}
1356.1.1   = {
	add_core = MZB
}
1530.1.1 = {
	add_permanent_claim = ALG
}
