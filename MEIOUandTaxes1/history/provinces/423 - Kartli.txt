
owner = GEO
controller = GEO
culture = georgian
religion = orthodox
capital = "T'bilisi"
trade_goods = copper
hre = no
base_tax = 9
base_production = 9
base_manpower = 4
is_city = yes
discovered_by = muslim
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = inland_center_of_trade_modifier
		duration = -1
	}
}
1200.1.1 = { marketplace = yes }
1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = GEO
}
1444.1.1  = {
        add_core = KRT
}
1458.1.1  = { unrest = 5 } # Safavid campaign against Georgia
1460.1.1  = { unrest = 0 } # Defeated by Shirwan Shah
1466.1.1  = {
	owner = KRT
	controller = KRT
	add_core = KRT
	remove_core = GEO
	bailiff = yes
}
1600.1.1  = { fort_14th = yes }
1625.1.1  = { unrest = 8 } # Insurrection, headed by Giorgi Saakadze
1630.1.1  = { unrest = 0  }

1723.1.1  = { controller = TUR } # Ottoman-Persian War, Kartli is invaded by Turkish troops
1727.1.1  = {
	owner = GEO
	controller = GEO
	add_core = GEO
}
1730.1.1  = {  }
1810.2.20 = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = TUR
} # Annexed by Russia
