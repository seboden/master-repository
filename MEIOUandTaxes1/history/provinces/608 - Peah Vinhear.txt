# 608 - Tuol Basan  #### change localisation file
# TM-Smiles indochina-mod for meiou

owner = KHM
controller = KHM
add_core = KHM
culture = khmer
religion = buddhism
capital = "Tuol Basan"

base_tax = 3
base_production = 3
#base_manpower = 1.0
base_manpower = 2.0
citysize = 10000	  
trade_goods = rice ####


discovered_by = chinese
discovered_by = indian

hre = no

1450.1.1 = { citysize = 5000 }
1500.1.1 = { citysize = 5000 }
1550.1.1 = { citysize = 5000 }
1600.1.1 = { citysize = 6500 }
1650.1.1 = { citysize = 7540 }
1700.1.1 = { citysize = 8000 }
1750.1.1 = { citysize = 8400 }
1800.1.1 = { citysize = 8800 }
1867.1.1 = { 			
	owner = FRA
	controller = FRA
}
