# No previous file for Cill Chainnigh

owner = BTU
controller = BTU
culture = irish
religion = catholic
capital = "Cill Chainnigh"
trade_goods = wheat
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
discovered_by = western
discovered_by = muslim
discovered_by = eastern
hre = no

700.1.1 = {
	add_permanent_province_modifier = { name = clan_land duration = -1 }
}
1356.1.1  = {
	add_core = BTU
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1529.1.1  = { #The Butlers were loyal to Henry VIII and from here on served England
	owner = ENG
	controller = ENG
	add_core = ENG
}
1530.1.4  = {
	bailiff = yes	
}
1642.1.1  = { controller = REB } #Estimated
1642.6.7  = { owner = IRE controller = IRE } #Confederation of Kilkenny
1650.3.28 = { controller = ENG } #Capture of Kilkenny
1652.4.1  = { owner = ENG } #End of the Irish Confederates
1655.1.1  = { fort_14th = yes }
 #Estimated
1689.3.12 = { controller = REB } #James II Lands in Ireland
1690.8.1  = { controller = ENG } #Estimated
 #marketplace Estimated
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
