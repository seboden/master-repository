# 3721 - Nuurdfresklun

owner = SHL
controller = SHL
culture = frisian
religion = catholic
hre = no
base_tax = 6
base_production = 6
trade_goods = salt
base_manpower = 3
is_city = yes
capital = "H�sem"
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1   = {
	add_core = DEN
	add_core = SHL
}
1500.1.1 = { road_network = yes }
1523.6.21  = {
	remove_core = DEN
	#add_core = DAN
}
1530.1.4  = {
	bailiff = yes	
}
1536.1.1   = {
	religion = protestant
}
1644.1.12  = {
	controller = SWE
} #Torstenssons War-Captured by Lennart Torstensson
1645.8.13  = {
	controller = SHL
} #The Peace of Br�msebro
1657.10.23 = {
	controller = SWE
} #Karl X Gustavs First Danish War-Captured by Wrangel
1658.2.26  = {
	controller = SHL
} #The Peace of Roskilde - Duchy fully independent
1710.1.1   = {
	
}
1713.3.13  = {
	owner = DAN
	controller = DAN
} # With Siege of T�nning, Denmark takes back control over entire Slesvig
1720.7.3   = {
	remove_core = SHL
} # 
1814.5.17 = {
	owner = DEN
	controller = DEN
	add_core = DEN
	remove_core = DAN
}
