#1136 -Malozi

culture = bemba
religion = animism
capital = "Mengu"
trade_goods = millet
hre = no
native_size = 80
native_ferocity = 4.5
native_hostileness = 9
discovered_by = central_african

1000.1.1 = {
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}