# No previous file for Susquehannock

add_core = SSQ
owner = SSQ
controller = SSQ
is_city = yes
culture = susquehannock
religion = totemism
capital = "Susquehannok"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 1 
native_hostileness = 6

1650.1.1  = { trade_goods = fur } #Full extent of the Susquehannock at start of the Beaver Wars
1671.1.1  = {  } # Abraham Wood
1677.1.1  = {
 	owner = LEN
	controller = LEN
	culture = lenape
} #Lenape vassals of Iroquois resettle
1707.5.12 = {  }
1756.1.1  = {	owner = GBR
		controller = GBR
		culture = english
		religion = protestant
} #Shamokin Fort
1776.7.4  = {	owner = USA
		controller = USA
	    } # Declaration of independence
1782.11.1 = {	unrest = 0   } # Preliminary articles of peace, the British recognized Amercian independence
1801.7.4  = {	add_core = USA }
