# 2305 - Hoanya
# LS - Chinese Civil War

culture = taiwanese
religion = polynesian_religion
capital = "Hoanya"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 50
native_ferocity = 3
native_hostileness = 5
discovered_by = chinese

1000.1.1   = {
	set_province_flag = taiwanese_natives
}
1544.1.1 =  {
	discovered_by = POR
}
1616.1.1   = {
	owner = NED
	controller = NED
	citysize = 50
	trade_goods = fur
	base_tax = 3
base_production = 3
#	base_manpower = 1.0
	base_manpower = 2.0
	set_province_flag = trade_good_set
} # VoC's first tradepost
1637.1.1   = {
	citysize = 200
} # Local tribes surrender to VoC
1662.2.1   = {
	owner= TNG
	controller = TNG
	culture = hakka
	religion = confucianism
	add_core = TNG
	citysize = 10000
	} # Zheng Chenggong
1683.1.1   = {
	owner= QNG
	controller = QNG
	add_core = QNG
#	culture = wuhan
#	religion = confucianism
#	trade_goods = fur
#	citysize = 1000
} # The Qing conquer the last Ming stronghold
#1700.1.1   = {
#	citysize = 50000
#}
1700.1.1   = {
	add_core = QNG
}
1750.1.1   = {
	citysize = 20000
}
1800.1.1   = {
	citysize = 50000
}
