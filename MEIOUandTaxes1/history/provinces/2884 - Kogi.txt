# 2884 - Kogi

owner = NRI
controller = NRI
culture = igbo		
religion = west_african_pagan_reformed		 
capital = "Kogi"
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = cotton
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1   = {
	add_core = IGB
}
1807.1.1   = {
	owner = SOK
	controller = SOK
	add_core = SOK
}
