# No previous file for Narsinghpur

owner = GHR
controller = GHR
culture = gondi
religion = hinduism
capital = "Narsinghpur"
trade_goods = rice
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim 
discovered_by = steppestech


1120.1.1 = { farm_estate = yes }
1133.1.1 = { mill = yes }
1356.1.1 = {
	#add_core = BRR
	add_core = GHR
	add_core = DGA
}
#1410.1.1 = {
#	controller = BAH
#} # Invaded by Bahmanis
#1411.1.1 = {
#	owner = BAH
#} # Invaded by Bahmanis
#1417.1.1 = {
#	controller = MLW
#} # Invaded by Malwa
#1418.1.1 = {
#	owner = MLW
#} # Invaded by Malwa
1450.1.1 = { citysize = 11000 }
#1467.1.1 = {
#	owner = BAH
#	controller = BAH
#} # Invaded by Bahmanis
#1468.1.1 = {
#	owner = MLW
#	controller = MLW
#} # Invaded by Malwa
1498.1.1 = { discovered_by = POR }
1530.1.1 = { 
	add_permanent_claim = MUG
}
1564.5.1 = { controller = MUG }
1564.7.1 = { controller = DGA }
1580.1.1 = {
	owner = DGA
	controller = DGA
	capital = Deogarh
	fort_16th = yes
} # Independence and new dominant state
1724.1.1 = {
	owner = BHO
	controller = BHO
	add_core = BHO
}	#Forms kingdom of Nagpur
#1743.1.1 = {	   } # Marathas take control over Nagpur kingdom
#1743 Take over by the Maratha Bhonsle dynasty
1818.6.3 = {
	owner = GBR
	controller = GBR
}
