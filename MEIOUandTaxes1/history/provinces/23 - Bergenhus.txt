# Bergenhus
# MEIOU - Gigau

owner = NOR
controller = NOR
add_core = NOR
#add_core = VES
culture = norwegian
religion = catholic
hre = no
base_tax = 6
base_production = 6
trade_goods = fish
base_manpower = 2
is_city = yes
fort_14th = yes
capital = "Bergen"

discovered_by = western
discovered_by = eastern

1088.1.1 = { dock = yes }
1250.1.1 = { temple = yes }
1515.1.1 = { training_fields = yes }
1522.2.15 = { dock = yes shipyard = yes }
1522.3.20 = { dock = no naval_arsenal = yes }
1523.6.21  = {
	owner = DAN
	controller = DAN
	add_core = DAN
}
1526.1.1 = { religion = protestant } #preaching of Hans Tausen
1529.12.17 = { merchant_guild = yes }
1531.11.1 = {
	revolt = { type = nationalist_rebels size = 0 }
	controller = REB
} #The Return of Christian II
1532.7.15 = {
	revolt = {  }
	controller = DAN
}
1536.1.1  = { religion = protestant} #Unknown date
1641.1.1  = {
	
}
1814.1.14  = {
	owner = SWE
	revolt = { type = nationalist_rebels size = 2 } controller = REB
	remove_core = DAN
} # Norway is ceded to Sweden following the Treaty of Kiel
1814.5.17 = { revolt = {  } owner = NOR controller = NOR }
