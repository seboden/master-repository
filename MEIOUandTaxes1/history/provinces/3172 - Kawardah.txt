# No previous file for Kawardah

owner = BST
controller = BST
culture = sambalpuri
religion = adi_dharam
capital = "Kawardah"
trade_goods = rice
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = indian
discovered_by = muslim 
discovered_by = steppestech

1133.1.1 = { mill = yes }
1356.1.1  = { add_core = BST }
#1424.1.1 Kingdom of Bastar
1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
1741.1.1  = { controller = MAR }	#Maratha expansion
1743.1.1  = {
	owner = BHO
	controller = BHO
	add_core = BHO
} # Marathas (Bhonsle)
