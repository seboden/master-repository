# 3988 - Borovsk

owner = RYA
controller = RYA
culture = russian
religion = orthodox
capital = "Borovsk"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = wheat
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim
hre = no 

#1133.1.1 = { mill = yes }
1356.1.1  = {
	add_core = RYA
	add_core = MOS     
}
1362.1.1  = {
	owner = MOS
	controller = MOS
} # absorbed by the principality of Moscow
1523.8.16 = { mill = yes }
1530.1.4  = {
	bailiff = yes	
	farm_estate = yes
}
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1598.1.1  = {
	unrest = 5
} # "Time of troubles", peasantry brought into serfdom
1613.1.1  = {
	unrest = 0
} # Order returned, Romanov dynasty
1656.1.1  = {
	unrest = 4
} # Tatar revolt
1657.1.1  = {
	unrest = 0
}
1670.1.1  = {
	unrest = 8
} # Stepan Razin
1671.1.1  = {
	unrest = 0
} # Razin is captured
1773.1.1  = {
	unrest = 5
} # Emelian Pugachev, Cossack insurrection
1774.9.14 = {
	unrest = 0
} # Pugachev is captured
