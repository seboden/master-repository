# 587 - Hsipaw
# TM-Smiles indochina-mod for meiou

owner = SST
controller = SST
culture = karen
religion = buddhism
capital = "Yawnghwe"

base_tax = 3
base_production = 3
#base_manpower = 1.0
base_manpower = 2.0
citysize = 6700
trade_goods = lumber


discovered_by = chinese
discovered_by = indian


hre = no

1250.1.1 = { temple = yes }
1356.1.1 = {
	add_core = AVA
	add_core = SST
	add_core = PEG
}
1500.1.1 = { citysize = 8500 }
1530.1.1 = {
	add_core = TAU
	remove_core = AVA
	#remove_core = SST
	remove_core = PEG
	#unrest = 50
}
1550.1.1 = { citysize = 9670 }
1555.1.1 = {
	owner = TAU
	controller = TAU
	add_core = TAU
	remove_core = AVA
	remove_core = PEG
} # The Shan dynasty is overthrown
#1581.1.1 = {
#	owner = SST
#	controller = SST
#  	remove_core = TAU
#} # Very loosely controlled
1599.1.1 = { controller = REB }	#Shan states revolt after burmese dinasty's crisis
1605.1.1 = { controller = TAU }
1650.1.1 = { citysize = 11385 }
1700.1.1 = { citysize = 12890 }
1739.1.1 = { unrest = 5 } # Rebellion
1740.1.1 = { unrest = 0 }
1750.1.1 = { citysize = 13450 }
1752.2.28 = {
	owner = BRM
	controller = BRM
	add_core = BRM
	remove_core = TAU
}
1800.1.1 = { citysize = 14887 }
1885.1.1 = {		
	owner = GBR
	controller = GBR
}
