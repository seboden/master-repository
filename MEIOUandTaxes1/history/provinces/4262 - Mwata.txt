# No previous file for Mwata

owner = LND
controller = LND
add_core = LND
culture = lunda
religion = animism
capital = "Mwata"
trade_goods = livestock
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
hre = no
discovered_by = central_african
