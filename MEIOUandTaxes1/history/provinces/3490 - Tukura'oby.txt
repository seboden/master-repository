# No previous file for Tukura'oby

culture = ge
religion = pantheism
capital = "Tukura'oby"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 15
native_ferocity = 1
native_hostileness = 8

1500.1.1   = { discovered_by = CAS discovered_by = POR } # Pinz�n, Pedro �lvares Cabral 
1516.1.23  = { discovered_by = SPA }
1616.1.12  = {
	owner = POR
	controller = POR
	citysize = 1060
	culture = portugese
	religion = catholic
		trade_goods = cacao
	change_province_name = "Tucurui"
	rename_capital = "Tucurui"
}
1700.1.1   = {
	citysize = 1030
}
1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
