# 2625 - Bolzano

owner = TIR
controller = TIR
culture = austrian
religion = catholic
capital = "Bozen"
base_tax = 4
base_production = 4
base_manpower = 3
is_city = yes
trade_goods = wheat

discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = yes

#1111.1.1 = { post_system = yes }
1133.1.1 = { mill = yes }
1200.1.1 = { road_network = yes }
1250.1.1 = { temple = yes }
1356.1.1   = {
	add_core = TIR
	add_core = BAY
	add_permanent_province_modifier = {
		name = "bishopric_of_brixen"
		duration = -1
	}
}
1363.1.13  = {
	controller = HAB
	owner = HAB
	add_core = HAB
	remove_core = BAY
}
1379.1.1   = {
	controller = TIR
	owner = TIR
	add_core = TIR
}
1490.1.1   = {
	controller = HAB
	owner = HAB
	remove_core = TIR
	culture = austrian
}
1500.1.1 = { road_network = yes }
1504.1.1   = {
	remove_core = BAV
}
1511.7.1   = {
	
} # With the Landslibell came the duty to be prepared for defense -> Sch�tzen
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1525.3.1   = {
	unrest = 6
} # Peasant Revolts
1525.9.1   = {
	unrest = 0
}
1540.1.1   = {
	
}

1570.1.1   = {
	fine_arts_academy = yes
}
1627.1.1   = {
	
} # Kramsacher Glash�tte

1646.1.1   = {
	fort_14th = yes
}
1805.12.26 = {
	owner = BAV
	controller = BAV
	add_core = BAV
     	remove_core = HAB
} # Treaty of Pressburg
1806.7.12  = {
	hre = no
} # The Holy Roman Empire is dissolved
1809.4.9   = {
	controller = REB
} # Tyrolean rebellion
1810.1.19  = {
	controller = BAV
}
1814.5.30  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = BAV
} # Treaty of Paris, ceded to the Habsburgs
