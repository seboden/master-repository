# 476 - Otrat

owner = MGH
controller = MGH
culture = kirgiz
religion = sunni
capital = "Andijan"
trade_goods = wool
hre = no
base_tax = 6
base_production = 6
base_manpower = 2
is_city = yes
discovered_by = muslim
discovered_by = mongol_tech
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1 = {
	add_core = MGH
	add_core = KAS
}
1444.1.1 = {
	owner = TIM
	controller = TIM
	add_core = TIM
   	remove_core = MGH
}
1469.8.27 = {
	owner = CHG
	controller = CHG
	remove_core = TIM
}
1504.1.1 = {
	owner = SHY
	controller = SHY
	add_core = SHY
}
1515.1.1 = { training_fields = yes }
1520.1.1 = {
	owner = BUK
	controller = BUK
	add_core = BUK
   	remove_core = SHY
}
1709.1.1 = {
	owner = KOK
	controller = KOK
	add_core = KOK
   	remove_core = BUK
}
