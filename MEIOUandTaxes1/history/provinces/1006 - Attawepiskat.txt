# No previous file for Attawepiskat

culture = cree
religion = totemism
capital = "Attawepiskat"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 15
native_ferocity = 2
native_hostileness = 5

1000.1.1 = {
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
}
1611.1.1 = { discovered_by = ENG } # Henry Hudson
1689.1.1 = {
	discovered_by = GBR
	owner = ENG
	controller = ENG
	citysize = 100
	culture = english
	religion = protestant
} # Construction of Fort Severn
1699.1.1 = { discovered_by = FRA } # Pierre le Moyne
1707.5.12  = {
	discovered_by = GBR
	owner = GBR
	controller = GBR
}
1714.1.1 = { add_core = GBR }
1750.1.1 = { citysize = 880 trade_goods = fur }
1800.1.1 = { citysize = 1150 }
