# 2619 - Vogtland

owner = BOH
controller = BOH
culture = high_saxon
religion = catholic
capital = "Plauen"
trade_goods = cloth
hre = yes
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
add_core = BOH
add_core = MEI
add_core = REU

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1357.1.1   = { 
	owner = MEI
	controller = MEI
	add_core = MEI
	remove_core = BOH
}#Margraviate of Meissen receives Vogtland
1423.6.1   = { 
	owner = SAX
	controller = SAX 
	add_core = SAX
	remove_core = MEI
}#Margraviate of Meissen inherits Saxony and becomes the Elector.
1500.1.1 = { road_network = yes }
1520.12.10 = {
	religion = protestant
	#reformation_center = protestant
}
1530.1.4  = {
	bailiff = yes	
}
1547.5.19   = { 
	owner = BOH
	controller = BOH
	add_core = BOH
	remove_core = SAX
} #Treaty of Wittenberg
1559.1.1   = { 
	owner = SAX
	controller = SAX
	add_core = SAX
	remove_core = BOH
} #Vogtland is given back again.
1560.1.1  = { fort_16th = yes }
1790.8.1  = { unrest = 5 } # Peasant revolt
1791.1.1  = { unrest = 0 }
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
