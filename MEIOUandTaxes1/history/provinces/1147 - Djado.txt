# 1147 - Djado

culture = tuareg
religion = sunni
capital = "Djado"
native_size = 60
native_ferocity = 4.5
native_hostileness = 9
trade_goods = salt
discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1590.1.1 = {
	owner = KBO
	controller = KBO
	add_core = KBO
} #Idris Katakarmabe retakes Kanem
