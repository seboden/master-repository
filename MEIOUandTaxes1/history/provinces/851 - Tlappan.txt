# 851 - Tlap�n

owner = TLP
controller = TLP
add_core = TLP
culture = tlapanec
religion = nahuatl
capital = "Tinda'i"

base_tax = 5
base_production = 5
#base_manpower = 1.5
base_manpower = 3.0
citysize = 15000
trade_goods = fish 


hre = no

discovered_by = mesoamerican

1487.1.1   = {
	owner = AZT
	controller = AZT
	capital = "Tlappan"
	citysize = 10000
	base_tax = 6
base_production = 6
#	base_manpower = 1.0
	base_manpower = 2.0
	trade_goods = maize
} # Conquest by Ahuitzotl, eighth tlatoani of Tenochtitlan
1506.1.1   = {
	add_core = AZT
}
1519.3.13  = {
	discovered_by = SPA
} # Hern�n Cort�s
1521.8.13  = {
	owner = SPA
	controller = SPA
	citysize = 2000
	naval_arsenal = yes
	marketplace = yes
	bailiff = yes
} #Fall of Tenochtitlan
1546.1.1   = {
	add_core = SPA
}
1571.1.1   = {
	culture = castillian
	religion = catholic
}
1600.1.1   = {
	citysize = 5000
}
1650.1.1   = {
	citysize = 10000
}
1700.1.1   = {
	citysize = 15000
}
1750.1.1   = {
	add_core = MEX
	culture = mexican
	citysize = 25000
}
1800.1.1   = {
	citysize = 30000
}
1810.9.16  = {
	owner = MEX
	controller = MEX
} # Declaration of Independence
1821.8.24  = {
	remove_core = SPA
} # Treaty of Cord�ba
