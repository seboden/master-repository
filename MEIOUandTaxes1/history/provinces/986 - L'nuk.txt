# No previous file for L'nuk

add_core = ABE
owner = ABE
controller = ABE
is_city = yes
culture = abenaki
religion = totemism
capital = "L'nuk"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 1 
native_hostileness = 6
