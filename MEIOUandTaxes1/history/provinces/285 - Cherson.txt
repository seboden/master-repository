# 285 - Pryazovia

owner = WHI
controller = WHI
culture = crimean
religion = sunni
capital = "Kyzyl-Yar"

base_tax = 4
base_production = 4
#base_manpower = 1.5
base_manpower = 3.0
citysize = 10900
trade_goods = wheat



discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech


hre = no

1356.1.1 = {
	unrest = 3
	add_core = WHI
	add_core = CRI
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = WHI
	unrest = 0
}
1441.1.1 = {
	owner = CRI
	controller = CRI
}
1444.1.1 = {
	remove_core = GOL
	remove_core = BLU
	remove_core = WHI
}
#1475.1.1 = {
#	add_core = TUR
#}
1500.1.1 = {
	citysize = 10000
}
1502.1.1 = {
	remove_core = GOL
} # Final destruction of the Golden Horde
1515.1.1 = { training_fields = yes }
1550.1.1  = { citysize = 12823 }
1600.1.1  = { citysize = 15086 }
#1648.1.1  = { culture = ukrainian add_core = UKR }
1650.1.1  = { citysize = 17748 }
1700.1.1  = { citysize = 20880 }
1750.1.1  = {
	citysize = 24565
	fort_14th = yes
} # Estimated, Kherson fortress
1774.7.21 = {
	add_core = RUS
	remove_core = CRI
} # Treaty of Kuchuk-Kainarji
1783.1.1 = {
	owner = RUS
	controller = RUS
} # Annexed by Catherine II
