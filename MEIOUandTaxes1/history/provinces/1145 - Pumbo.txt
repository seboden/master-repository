# 1145 - Pumbo

owner = KON
controller = KON
add_core = KON
culture = kongolese
religion = animism
capital = "Mbanza Kongo"
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = cloth
hre = no
discovered_by = central_african
