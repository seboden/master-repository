# 2766 - Faranah

owner = BKE
controller = BKE
culture = mali
religion = west_african_pagan_reformed
capital = "Faranah"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = ivory
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1   = {
	add_core = BKE
}
