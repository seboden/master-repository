# 462 - Mangystau
# MEIOU-GG - Turko-Mongol mod

owner = KHO
controller = KHO
culture = tartar
religion = sunni
capital = "Aktau"
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
trade_goods = salt
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim
hre = no

1356.1.1   = {
	add_core = KHO
}
1419.1.1 = {
	owner = NOG
	controller = NOG
	add_core = NOG
}
1511.1.1 = {
	owner = KHI 
	controller = KHI
	add_core = KHI
	remove_core = NOG
	remove_core = KZH
	culture = turkmeni
} # Khiva Independent
1515.1.1 = { training_fields = yes }
1677.1.1 = { discovered_by = FRA }
