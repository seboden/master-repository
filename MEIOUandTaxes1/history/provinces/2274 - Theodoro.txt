# 2274 - Theodoro

owner = THD
controller = THD
culture = pontic
religion = orthodox
capital = "Doros"
trade_goods = lumber
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech
add_permanent_claim = BYZ

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1356.1.1  = {
	add_core = THD
}
1444.1.1 = {
	add_core = TUR
}
1475.5.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = THD
	change_province_name = "Mangup"
	rename_capital = "Mangup"
}
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = {
	controller = TUR
	culture = turkish
	religion = sunni
}
1509.1.1  = { controller = REB } # Civil war
1513.1.1  = { controller = TUR }
1519.1.1  = { bailiff = yes }
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR } # Murad tries to quell the corruption
1658.1.1  = { controller = REB } # Revolt of Abaza Hasan Pasha, centered on Aleppo, extending into Anatolia
1659.1.1  = { controller = TUR }
1774.7.21 = {
	owner = CRI
	controller = CRI
	add_core = RUS
	remove_core = CRI
} # Treaty of Kuchuk-Kainarji
1783.1.1 = {
	owner = RUS
	controller = RUS
} # Annexed by Catherine II
