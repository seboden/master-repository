#2378 - Ialomita

owner = WAL
controller = WAL  
add_core = WAL
culture = vlach
religion = orthodox
capital = "Braila"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = wheat
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech
hre = no

#1133.1.1 = { mill = yes }
#1462.1.1  = {
#	add_core = TUR
#} # Vlad III Dracula accepts to pay tribute to Mehmed II

1523.8.16 = { mill = yes }
1530.1.4  = {
	bailiff = yes	
}
1550.1.1  = { fort_14th = yes }
1593.1.1  = { unrest = 7 } # Wallachian rising, the Turks & Tatars are expelled
1595.1.1  = { unrest = 0  } # The Turks take control of most of Wallachia again

1650.1.1  = { controller = REB } # Boyar rebellions
1658.1.1  = { controller = WAL }
1663.1.1  = { unrest = 7 } # Cantacuzino is murdered, struggle for power between the boyars
1688.1.1  = { unrest = 0 } # Period of stability, Constantine Brancoveanu reigns
1689.1.1  = { capital = "Bucuresti" }

1700.1.1  = {  }
1718.1.1  = {
	owner = HAB 
	controller = HAB
	add_core = HAB
} # Ceded to Austria
1739.1.1  = {
	owner = WAL 
	controller = WAL
} # The Ottomans regained Wallachia
1789.1.1  = { controller = HAB } # Occupied by Austrian troops
1791.8.4  = { controller = WAL } # Treaty of Sistova
1806.1.1  = { controller = RUS } # Occupied by Russian troops
1812.5.28 = { controller = WAL } # Treaty of Bucharest
