#1263 - Ugandi - Dorpat - Tartu

owner = LVO
controller = LVO
add_core = LVO
add_core = EST
culture = estonian
religion = catholic
capital = "Dorpat"
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
trade_goods = wheat


discovered_by = western
discovered_by = eastern
discovered_by = steppestech
hre = no

#1133.1.1 = { mill = yes }
1523.8.16 = { mill = yes }
1530.1.4  = {
	bailiff = yes	
	farm_estate = yes
}
1542.1.1   = { religion = protestant} #Unknown date
1561.11.18 = {
	owner = LIT
	controller = LIT
	add_core = LIT
	add_core = LIV
	remove_core = LVO
} # Wilno Pact
1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1600.12.31 = { controller = SWE } #2nd Polish War-Captured by Duke Karl
1603.4.5   = { controller = PLC } #2nd Polish War-Captured by Chodkiewicz
1625.8.21  = { controller = SWE } #2nd Polish War-Captured by Jakob de la Gardie
1629.9.16  = {
	owner = SWE
	add_core = SWE
	remove_core = POL
	remove_core = PLC
} #The Armistice of Altmark

1721.8.30  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = SWE
} #The Peace of Nystad
