# No previous file for Hetu Ala

owner = YUA
controller = YUA
culture = jurchen
religion = tengri_pagan_reformed
capital = "Hetu Ala"
trade_goods = iron
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = steppestech

1356.1.1 = {
	add_core = YUA
	add_core = MXI
}
1392.1.1 = {
	owner = MXI
	controller = MXI
	remove_core = YUA
}
1411.5.1 = {
	owner = MJZ
	controller = MJZ
	add_core = MJZ
	remove_core = MXI
} #Menggtemu settles Odoli tribe
1515.1.1 = { training_fields = yes }
1530.1.1 = { 
	marketplace = yes
	bailiff = yes
	temple = yes
	weapons = yes
}
1542.1.1 = {
	owner = MCH
	controller = MCH
	add_core = MCH
	remove_core = MJZ
} #Aisin Gioro enclipse Odoli
1616.2.17  = {
	owner = JIN
	controller = JIN
	add_core = JIN
	remove_core = MCH
}
1635.1.1 = {
	culture = manchu
}
1636.5.15 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = JIN
} # The Qing Dynasty
1709.1.1 = { discovered_by = SPA }
