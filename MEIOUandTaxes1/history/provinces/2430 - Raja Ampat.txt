# 2430 - Raja Ampat
# MEIOU-FB Indonesia mod
# MEIOU-FB IN updates

culture = moluccan
religion = polynesian_religion
capital = "Misool"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 5
native_ferocity = 2
native_hostileness = 2
discovered_by = MKS
discovered_by = MPH
discovered_by = MTR
discovered_by = chinese
discovered_by = austranesian

1000.1.1   = {
	set_province_flag = moluccan_natives
}
1500.1.1 = { religion = sunni }
1658.1.1 = {
	discovered_by = NED
	owner = NED
	controller = NED
   	citysize = 600
	base_tax = 2
base_production = 2
	trade_goods = clove
	set_province_flag = trade_good_set
} # Dutch control
1683.1.1 = { add_core = NED }
1700.1.1  = {
	citysize = 1100
	culture = batavian
}
1750.1.1 = { citysize = 1465 }
1800.1.1 = { citysize = 1900 }
