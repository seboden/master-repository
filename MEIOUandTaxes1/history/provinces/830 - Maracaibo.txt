culture = guajiro
religion = pantheism
capital = "Maracaibo"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 20
native_ferocity = 1
native_hostileness = 7

1577.1.1  = {
	discovered_by = SPA
	owner = SPA
	controller = SPA
	citysize = 355
	culture = castillian
	religion = catholic
	trade_goods = coffee
	capital = "Altamira de Cáceres"
	set_province_flag = trade_good_set
} 
1596.1.1  = { add_core = SPA }
1650.1.1  = { citysize = 570 }
1669.1.1  = { citysize = 900 } # Sacked by Henry Morgan
1700.1.1  = { citysize = 1270 }
1750.1.1   = {
	add_core = VNZ
	culture = colombian
}
1811.7.5   = {
	owner = VNZ
	controller = VNZ
} # Venezuela declared its independence
1812.7.25  = {
	owner = SPA
	controller = SPA
} # The revolutionary army is defeated
1813.8.7   = {
	owner = VNZ
	controller = VNZ 
} # The Second Republic of Venezuela is established
1814.1.1   = {
	owner = SPA
	controller = SPA
} # The end of the second republic
1819.12.17 = {
	owner = COL
	controller = COL
	add_core = COL
	remove_core = SPA
} # The establishment of Gran Colombia
1830.1.13  = {
	owner = VNZ
	controller = VNZ
	add_core = VNZ
}
1831.11.19 = {
	remove_core = COL
} #Gran Colombia dismantled
