#692 - Wenzhou

owner = CMN
controller = CMN
culture = wuhan
religion = confucianism
capital = "Yongjia"
trade_goods = tea
hre = no
base_tax = 8
base_production = 8
base_manpower = 3
is_city = yes
add_core = CMN
discovered_by = steppestech
discovered_by = chinese

0985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
	bailiff = yes constable = yes
}
1200.1.1 = { paved_road_network = yes }
1276.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1320.1.1 = {
	remove_core = SNG
}
1351.1.1 = {
	owner = CMN
	controller = CMN
	add_core = CMN
	add_core = MNG
}
#1356.1.1 = {	remove_core = YUA } # Red Turbans
1367.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = CMN
}
1368.1.1 = {
	remove_core = SNG
}
1529.3.17 = { 
	marketplace = yes
	constable = no
	courthouse = yes
	plantations = yes
}

1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # The Qing Dynasty
#1662.1.1 = {	owner = QNG
#		controller = QNG
#		add_core = QNG
#		remove_core = MNG
#	   } # The Qing Dynasty
1662.1.1 = { remove_core = MNG }
