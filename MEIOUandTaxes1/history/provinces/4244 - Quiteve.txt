# No previous file for Quiteve

owner = QTV
controller = QTV
capital = "Quiteve"
culture = shona
religion = animism
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = ivory
hre = no
discovered_by = central_african
discovered_by = east_african

1356.1.1 = {
	add_core = QTV
}
