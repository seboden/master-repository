# 1355 - Guriza

owner = GOR
controller = GOR
add_core = GOR
culture = friulian
religion = catholic
base_tax = 4
base_production = 4
trade_goods = lead
base_manpower = 2
is_city = yes
fort_14th = yes
capital = "Guriza"
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech

1100.1.1 = { add_core = AQU }
1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
#1499.1.1 = { road_network = yes }
1500.1.1   = {
	owner = HAB
	controller = HAB
	add_core = HAB
}
1508.1.1   = {
	owner = VEN
	controller = VEN
}
1510.1.1   = {
	owner = HAB
	controller = HAB
}
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1530.1.2 = {
	road_network = no paved_road_network = yes 
}
1647.1.1    = { early_modern_university = yes } # (existed until 1803)
1803.1.1    = { early_modern_university = no }
1805.12.26 = {
	owner = FRA
	controller = FRA
	add_core = FRA
	add_core = STY
} # Treaty of Pressburg
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1813.9.20 = { controller = HAB } # Occupied by Austrian forces
1814.4.6  = {
	owner = HAB
	remove_core = FRA
} # Napoleon abdicates
