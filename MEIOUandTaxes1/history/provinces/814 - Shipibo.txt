# 814- Manubo

culture = quechuan
religion = inti
capital = "Kampa"
trade_goods = unknown
hre = no
native_size = 20
native_ferocity = 1
native_hostileness = 8
discovered_by = south_american

1700.1.1   = {
	discovered_by = SPA
	add_core = SPA
	owner = SPA
	controller = SPA
	is_city = yes
	trade_goods = lumber
	culture = castillian
	religion = catholic
} # Last stronghold found and captured
1750.1.1  = {
	add_core = PEU
	culture = peruvian
}
1810.9.18  = {
	owner = PEU
	controller = PEU
}
1818.2.12  = {
	remove_core = SPA
}
