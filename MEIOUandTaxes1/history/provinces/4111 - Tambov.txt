# 4111 - Tambov

owner = WHI
controller = WHI
culture = kazani
religion = sunni
capital = "Tambov"
base_tax = 3
base_production = 3
base_manpower = 2
citysize = 1250
trade_goods = wool
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim
hre = no

1356.1.1  = {
	add_core = KAZ
	add_core = WHI
	unrest = 3
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = WHI
	unrest = 0
}
1438.1.1  = {
	owner = KAZ
	controller = KAZ
}
1444.1.1 = {
	remove_core = GOL
	remove_core = BLU
	remove_core = WHI
}
1502.6.1   = {
	owner = MOS
	controller = MOS
	remove_core = GOL
	add_core = MOS
}
1530.1.4  = {
	bailiff = yes	
}
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1552.1.1   = {
	owner = RUS
	controller = RUS
	add_core = RUS
	add_core = MOS
} # Conquest of the Khanante by Ivan Grozny
1590.7.1  = {
	} # Construction of the Saratow fortress 
1598.1.1  = {
	unrest = 5
} # "Time of troubles"
1600.1.1   = {
	citysize = 6264
		culture = russian
	religion = orthodox
	remove_core = KAZ
}
1650.1.1   = { citysize = 4770 }
1663.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = KAZ
}
1700.1.1   = { citysize = 5670 }
1750.1.1   = { citysize = 6200 }
1800.1.1   = { citysize = 7000 }
