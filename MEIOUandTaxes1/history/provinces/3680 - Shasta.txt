# No previous file for Shasta

culture = sahaptian
religion = totemism
capital = "Shata"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 3
native_hostileness = 6
