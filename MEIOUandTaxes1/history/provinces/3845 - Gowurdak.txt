# 3845 - Gowurdak

owner = CHG
controller = CHG
culture = uzbehk
religion = sunni
capital = "Gowurdak"
trade_goods = subsistence  #########
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes
discovered_by = muslim
discovered_by = mongol_tech
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1   = {
	add_core = CHG
	add_core = KTD
}
1370.4.1   = {
	owner = TIM
	controller = TIM
	add_core = TIM
	remove_core = CHG
}
1444.1.1 = {
	remove_core = KTD
}
1500.6.1    = {
	owner = SHY
	controller = SHY
	add_core = SHY
	remove_core = TIM
} # Shaybanids break free from the Timurids
1515.1.1 = { training_fields = yes }
1520.1.1    = {
	owner = BUK
	controller = BUK
	add_core = BUK
	remove_core = SHY
} # Emirate of Bukhara established
1785.1.1    = {
	owner = BUK
	controller = BUK
	add_core = BUK
	remove_core = SHY
} # Emirate of Bukhara established
