
owner = BRE
controller = BRE  
culture = old_saxon
religion = catholic
capital = "Verden"
trade_goods = fish
hre = yes
base_tax = 4
base_production = 4
base_manpower = 4
is_city = yes
add_core = BRE

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1200.1.1 = { road_network = yes }
1500.1.1 = { road_network = yes }
1510.1.1   = {
	fort_14th = yes
}
1529.1.1   = {
	religion = protestant
}
1530.1.4  = {
	bailiff = yes	
}
1540.1.1   = {
	
}
1550.1.1   = {
	
}
1600.1.1   = {
	fort_14th = no fort_15th = yes
}
1620.1.1   = {
	
}
1648.10.24 = {
	owner = SWE
	controller = SWE
	add_core = SWE
} # Swedish dominion, Peace of Westphalia
1650.1.1   = {
	fort_15th = no fort_16th = yes
}
1700.1.1   = {
	
}
1719.9.30  = {
	owner = HAN
	controller = HAN
	add_core = HAN
	remove_core = SWE
} # The Treaty of Stockholm
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1807.7.9   = {
	owner = WES
	controller = WES
	add_core = WES
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1813.10.14 = {
	owner = HAN
	controller = HAN
	remove_core = WES
} # Westfalia is dissolved after the Battle of Leipsig
1866.1.1   = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = HAN
}
