# 2914 - Douentza

owner = MAL
controller = MAL
culture = bozo
religion = west_african_pagan_reformed
capital = "Douentza"
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
trade_goods = millet
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1   = {
	add_core = MAL
	add_core = SON
}
1430.1.1   = {
	owner = SON
	controller = SON
	remove_core = MAL
}
1591.4.12  = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = SON
} # Battle of Tondibi
1612.1.1   = {
	owner = TBK
	controller = TBK
	remove_core = MOR
} # Moroccans establish the Arma as new independent ruling class
1618.1.1   = {
	owner = JNN
	controller = JNN
}
1660.1.1  = {
	owner = SEG
	controller = SEG
	add_core = SEG
}

1819.1.1   = {
	owner = MSN
	controller = MSN
	add_core = MSN
	remove_core = SEG
} # The Massina Empire
