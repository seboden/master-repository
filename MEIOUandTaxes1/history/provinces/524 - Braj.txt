# 524 - Braj

owner = DLH
controller = DLH
culture = kanauji
religion = hinduism
capital = "Mathura"
trade_goods = silk
hre = no
base_tax = 14
base_production = 14
base_manpower = 5
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1120.1.1 = { textile = yes }
#1180.1.1 = { post_system = yes }
1200.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = DLH
	add_core = GWA
	fort_14th = yes
}
1399.1.1   = {
	owner = GWA
	controller = GWA
} #Timur sacks Delhi
1415.1.1   = {
	controller = DLH
} # Conquered by Delhi
1416.1.1   = {
	owner = DLH
	add_core = DLH
} # Conquered by Delhi
1444.1.1 = {
	add_core = PTA
	remove_core = GWA
}
1451.4.20 = {
	remove_core = PTA
}
1504.1.1 = {
	capital = "Agra"	
	add_permanent_province_modifier = {
		name = major_city
		duration = -1
	}
	training_fields = yes
}
1515.1.1 = { bailiff = yes }
1520.1.1 = { marketplace = yes }
1526.2.1 = { controller = TIM } # Babur's invasion
1526.4.21  = {
	owner = MUG
	controller = MUG
	add_core = MUG
	remove_core = DLH
} # Battle of Panipat
1530.1.1 = { add_core = TRT }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1540.1.1   = {
	owner = BNG
	controller = BNG
	add_core = BNG
} # Sher Shah Conquers Delhi
1553.1.1   = {
	owner = DLH
	 controller = DLH
	 remove_core = BNG
} #Death of Islam Shah Sur, Suri empire split
1555.7.23  = {
	owner = MUG
	controller = MUG
} # Humayun Returns
1556.10.7  = { controller = DLH }	# Hemu
1556.11.5  = { controller = MUG }	#Aftermath to second battle of Panipat
1571.1.1   = { capital = "Fatehpur Sikri" temple = yes } #Jama Masjid Fatehpur Sikri
1573.1.1 = { 
	fort_14th = no
	fort_15th = yes }
1585.1.1   = { capital = "Akbarabad" }
1622.6.1   = {
	revolt = {
		type = pretender_rebels
		size = 1
		name = "Khurrams Faction"
		leader = "Shah Jahan Timurid"
	}
} #Should be named Khurram at this point
1669.1.1   = { unrest = 6 } # The Jats revolted against Aurangzeb's rule
1670.1.1   = { unrest = 0 }
1685.1.1   = { unrest = 6 }	#Jat raids and inroads. The road south from Agra entirely cut of for much of the time. Imperial trade disrupted
1690.1.1   = { discovered_by = ENG }
1691.1.1   = { unrest = 0 }
1707.3.1   = {
	controller = REB
	revolt = {
		type = pretender_rebels
		size = 1
		leader = "Muhammad Azam Shah Timurid"
	}
} #Pretender
1707.5.12  = { discovered_by = GBR }
1707.6.13  = {
	controller = MUG
	revolt = {  }
} #Pretender beaten
1714.1.1   = {
	owner = ALR
	controller = ALR
	add_core = ALR
	remove_core = MUG
} # Jagir granted to the Jats
1784.1.1   = {
	owner = GWA
	controller = GWA
	capital = "Agra"
	remove_core = MUG
} # The Marathas
1803.1.1   = {
	owner = GBR
	controller = GBR
}
