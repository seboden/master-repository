# 3991 - Tarusa

owner = TSS
controller = TSS        
culture = russian
religion = orthodox 
hre = no
base_tax = 4
base_production = 4
trade_goods = wheat
base_manpower = 2
capital = "Vorotynsk"
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = steppestech
discovered_by = muslim

#1133.1.1 = { mill = yes }
1356.1.1  = {
	add_core = TSS
}
1394.3.21 = {
	owner = MOS
	controller = MOS
	add_core = MOS
} # Turns to Muscowy to counter the advances of Lithuania
1420.1.1  = {
	fort_14th = yes
} # Set as a stronhold on southern approach to Muscowy
1523.8.16 = { mill = yes }
1530.1.4  = {
	bailiff = yes	
}
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1567.1.1  = {
	fort_14th = no
	fort_15th = yes
} # Defend the southern borders of Muscovy.
1610.9.27 = {
	fort_15th = no
	fort_16th = yes
}
1670.1.1  = { unrest = 8 } # Stepan Razin
1671.1.1  = { unrest = 0 } # Razin is captured
1773.1.1  = { unrest = 5 } # Emelian Pugachev, Cossack insurrection.
1774.9.14 = { unrest = 0 } # Pugachev is captured.
