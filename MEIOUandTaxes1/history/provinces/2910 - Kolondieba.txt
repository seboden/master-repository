# 2910 - Kolondieba

owner = MAL
controller = MAL
culture = mali
religion = west_african_pagan_reformed
capital = "Kolondieba"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = ivory
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1   = {
	add_core = MAL
}
1600.1.1  = {
	owner = ZAF
	controller = ZAF
	add_core = ZAF
	remove_core = MAL
}
1620.1.1  = {
	owner = SEG
	controller = SEG
	add_core = SEG
}
