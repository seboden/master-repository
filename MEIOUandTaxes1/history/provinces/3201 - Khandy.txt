# No previous file for Khandy

owner = KTH
controller = KTH
culture = sinhala
religion = buddhism
capital = "Kandy"
trade_goods = cinnamon
hre = no

base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = muslim
discovered_by = indian

1250.1.1 = { temple = yes }
1356.1.1   = {
	add_core = KTH
	fort_14th = yes
}
1505.1.1   = { discovered_by = POR
	fort_14th = no
	fort_15th = yes
} # Francisco de Almeida
1515.1.1 = { training_fields = yes }
1530.3.17 = {
	bailiff = yes
	marketplace = yes
}

1815.3.1   = {
	owner = GBR
	controller = GBR
	add_core = GBR
} # Treaty of Amiens
