# 2953 - Al Obaid

native_size = 50
native_ferocity = 4.5
native_hostileness = 9
culture = nuba
religion = animism
capital = "Al Obaid"
trade_goods = unknown # salt
hre = no
discovered_by = ALW
discovered_by = YAO
discovered_by = ETH
discovered_by = ADA
discovered_by = DAR
discovered_by = KIT
discovered_by = MKU
discovered_by = DSL
discovered_by = SLL

1530.1.1 = { religion = sunni } #Spread of Islam leads to shift in religion affiliation of region
1550.1.1 = { discovered_by = TUR }
1600.1.1 = {
	owner = KDF
	controller = KDF
	add_core = KDF
	discovered_by = KDF
	discovered_by = SEN
	discovered_by = ETH
	is_city = yes
	trade_goods = wool
}
1820.1.1 = {
	owner = TUR
	controller = TUR
}
