# 1425 - Achaea

owner = ACH
controller = ACH
culture = greek
religion = orthodox
capital = "Patras"
trade_goods = wool
hre = no
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
discovered_by = CIR
discovered_by = muslim
discovered_by = eastern
discovered_by = western
discovered_by = turkishtech
add_permanent_claim = BYZ

1088.1.1 = { dock = yes }
1356.1.1  = {
	add_core = ACH
	add_core = MOE
}
1430.1.1   = {
	owner = MOE
	controller = MOE
}
1444.1.1 = {
	add_core = TUR
}
1453.5.29  = {
	add_core = TUR
}
1460.1.1   = {
	owner = TUR
	controller = TUR
	remove_core = MOE
	capital = "Baliabadra"
}
1481.6.1   = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26  = { controller = TUR } # Jem escapes to Rhodes
1515.2.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1522.3.20 = { dock = no naval_arsenal = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes
}
1555.1.1   = { unrest = 5 } # General discontent against the Janissaries' dominance
1556.1.1   = { unrest = 0 }

1611.1.1   = { unrest = 3 } # Revolutionary movement of Dionysios, Christians driven away
1612.1.1  = { unrest = 0 } # Estimated

1700.1.1  = {  }
1750.1.1  = { add_core = GRE }
1788.1.1  = { fort_14th = yes } # Ali Pasha made it a stronghold
1797.1.1  = { controller = REB } # Suliot uprising
1821.3.1  = {
	controller = REB
}
1829.1.1  = {
	owner = GRE
	controller = GRE
}
1832.5.7  = {
	remove_core = TUR
} # Treaty of Constantinople
