# 582 - Mong Kawng

owner = MYA
controller = MYA
culture = shan
religion = buddhism
capital = "Mong Kawng"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = lumber
discovered_by = chinese
discovered_by = indian
hre = no

1356.1.1 = {
	add_core = MYA
	#add_core = SST
	add_core = DLI
}
#1502.1.1 = {
#	controller = SST
#	owner = SST
#	add_core = SST
#	remove_core = MYA
#}
1530.1.1 = { remove_core = AVA remove_core = PEG add_core = TAU }
1557.1.1 = {
	owner = TAU
	controller = TAU
	add_core = TAU
	remove_core = SST
	remove_core = PEG
	remove_core = DLI
}
1599.1.1 = {
		owner = SST
		controller = SST
}
1605.1.1 = {
		owner = TAU
		controller = TAU
}
1752.1.1 = {
		owner = SST
		controller = SST
}
1754.1.1 = {
		owner = TAU
		controller = TAU
}
1769.1.1 = {
	owner = BRM
	controller = BRM
}
1852.1.1 = { unrest = 7 }	# out of control after burmese defeated by british
1885.1.1 = {
	owner = GBR
	controller = REB
}
1890.1.1 = { controller = GBR }
