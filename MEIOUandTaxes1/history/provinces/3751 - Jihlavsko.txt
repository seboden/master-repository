# No previous file for Jihlavsko

owner = MRV
controller = MRV
capital = "Gihlawa"
culture = moravian
religion = catholic
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
trade_goods = wine

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = yes

1356.1.1 = {
	add_core    = MRV
	add_permanent_province_modifier = {
		name = bohemian_estates
		duration = -1
	}
}
1411.1.18  = {
	add_core = BOH
	owner = BOH
	controller = BOH
}
1457.1.1  = { unrest = 5  } # George of Podiebrand had to secure recognition from the German and Catholic towns
1464.1.1  = { unrest = 1 } # The Catholic nobility is still undermines the crown.
1471.1.1  = { unrest = 0 }
1500.1.1 = { road_network = yes }
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1526.8.30 = {
	owner = HAB
	controller = HAB
	add_core = HAB
} #Battle of Mohac where Lajos II dies -> Habsburg succession
1530.1.1  = {
	culture = czech
	remove_core = MRV
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}

1600.1.1  = { fort_14th = yes }
1618.4.23 = { revolt = { type = religious_rebels size = 2 } controller = REB } # Defenstration of Prague
1619.3.1  = {
	revolt = { }
	owner = PAL
	controller = PAL
	add_core = PAL
} #Friedrich V (Pawesternate) accepts to become King of Bohemia
1620.11.8 = {
	owner = HAB
	controller = HAB
	remove_core = PAL
}# Tilly beats the Winterking at White Mountain. Deus Vult!
1627.1.1  = { religion = catholic } # Order from Ferdinand II to reconvert to Catholism, many protestant leave the country

 # Transfered from Olmutz
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
