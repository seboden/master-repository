# 1233 - Wake

culture = polynesian
religion = polynesian_religion
capital = "Wake"
trade_goods = unknown # fish
hre = no
base_tax = 1
base_production = 1
#base_manpower = 0.5
base_manpower = 1.0
native_size = 5
native_ferocity = 0.5
native_hostileness = 2

1356.1.1  = {
	add_permanent_province_modifier = { name = remote_island duration = -1 }
}
1568.1.1 = {
	discovered_by = SPA
} # Discovered by �lvaro de Menda�a de Neyra
1796.1.1 = {
	discovered_by = GBR
} # Visited by William Wake
