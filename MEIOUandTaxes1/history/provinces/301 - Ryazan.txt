# 301 - Ryazan

owner = RYA
controller = RYA
culture = russian
religion = orthodox
capital = "Ryazan"
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
trade_goods = wool
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim
hre = no
add_core = RYA

#1111.1.1 = { post_system = yes }
1444.1.1 = {
	remove_core = GOL
	remove_core = BLU
	remove_core = WHI
}
1452.1.1 = {
}
1480.1.1 = {
	remove_core = GOL
} # Final destruction of the Golden Horde
1516.1.1  = {
	owner = MOS
	controller = MOS
	add_core = MOS
	add_permanent_province_modifier = {
		name = "qasim_khanate"
		duration = -1
	}
} # absorbed by the principality of Moscow
1530.1.4  = {
	bailiff = yes	
}
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1550.1.1  = {
	fort_14th = yes
} 
1598.1.1  = {
	unrest = 5
} # "Time of troubles", peasantry brought into serfdom
1613.1.1  = {
	unrest = 0
} # Order returned, Romanov dynasty
1656.1.1  = {
	unrest = 4
} # Tatar revolt
1657.1.1  = {
	unrest = 0
}
1670.1.1  = {
	unrest = 8
} # Stepan Razin
1671.1.1  = {
	unrest = 0
	religion = orthodox
} # Razin is captured
1681.1.1 = {
	remove_province_modifier = qasim_khanate
} #Qasim Khanate Abolished
1700.1.1  = {
	culture = russian
}
1750.1.1  = {  }
1773.1.1  = {
	unrest = 5
} # Emelian Pugachev, Cossack insurrection
1774.9.14 = {
	unrest = 0
} # Pugachev is captured
