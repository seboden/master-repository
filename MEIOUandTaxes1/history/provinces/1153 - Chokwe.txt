#1153 - Sheba

owner = CKW
controller = CKW
add_core = CKW
culture = chokwe
religion = animism
capital = "Kalemia"
trade_goods = millet
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
hre = no
discovered_by = central_african
