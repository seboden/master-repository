#290 - Kostroma

owner = MOS
controller = MOS
culture = russian
religion = orthodox
capital = "Kostroma"
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
trade_goods = livestock
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim
hre = no 

1356.1.1  = {
	add_core = MOS
}
1450.1.1  = {
	
} # Conquered by Valssili II
 # Spasskiy Monastery
1530.1.4  = {
	bailiff = yes	
}
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
1598.1.1  = { unrest = 5 } # "Time of troubles", peasantry brought into serfdom
1613.1.1  = { unrest = 0 } # Order returned, Romanov dynasty
1667.1.1  = { controller = REB } # Peasant uprising, Stenka Razin
1670.1.1  = { controller = RUS } # Crushed by the Tsar's army

 # First large factories appeared in the Yaroslavl region, linen, silk
1711.1.1  = {  } # Governmental reforms and the absolutism
