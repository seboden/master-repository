# 2662 - Mistratah

owner = HAF
controller = HAF
culture = libyan
religion = sunni
capital = "Misratah"
trade_goods = ivory
hre = no
base_tax = 6
base_production = 6
base_manpower = 2
is_city = yes
discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan
discovered_by = eastern
discovered_by = turkishtech
discovered_by = western

1088.1.1 = { dock = yes shipyard = yes }
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1  = {
	add_core = HAF
	add_core = TRP
}
1517.1.1   = {
	owner = FZA
	controller = FZA
	add_core = FZA
	remove_core = HAF
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
	add_claim = TUR
}
1551.1.1  = {
	owner = TRP
	controller = TRP
	add_core = TRP
	remove_core = KNI
} # Under direct Ottoman control until 1629
1609.1.1  = { revolt = { type = revolutionary_rebels size = 1 } controller = REB } # Janissary revolt
1610.1.1  = { revolt = {} controller = TUR }
1711.1.1 = {
	owner = TRP
	controller = TRP
} # The Karamanli pashas gain some autonomy as the Ottoman central power weakens
