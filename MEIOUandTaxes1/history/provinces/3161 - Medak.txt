# No previous file for Medak

owner = TLG
controller = TLG
culture = telegu
religion = hinduism
capital = "Medak"
trade_goods = indigo
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim

1100.1.1 = { plantations = yes }
1356.1.1  = {
	add_core = GOC
	add_core = TLG
	fort_14th = yes
}
1365.1.1  = {
	owner = BAH
	controller = BAH
	add_core = BAH
	add_core = GOC
	add_core = TLG
}
1453.1.1 = {
	controller = REB
	revolt = {
		type = nationalist_rebels
		size = 0
	}
} #Governor of Telingana revolts, invites Kalji intervention
1456.1.1 = { controller = BAH revolt = { } }	#End of Revolt
1498.1.1  = { discovered_by = POR }
1515.12.17 = { training_fields = yes }
1518.1.1  = {
	owner = GOC
	controller = GOC
	add_core = GOC
	remove_core = BAH
} # The Breakup of the Bahmani sultanate
1530.3.17 = {
	bailiff = yes
	marketplace = yes
}
1574.1.1 = {
	controller = GOC
	owner = GOC
	add_core = GOC
} # captured by Ahmednagar
1596.1.1 = {
	controller = MUG
	owner = MUG
	add_core = MUG
} # captured by Mughal Empire
1712.1.1 = { add_core = HYD }	#Viceroyalty of the Deccan
1724.1.1 = {
	owner = HYD
	controller = HYD
} # Asif Jah declared himself Nizam-al-Mulk
