# 1290 - Suzdal
# MEIOU-GG - Turko-Mongol mod

owner = NZH
controller = NZH
culture = russian
religion = orthodox
capital = "Suzdal"
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
trade_goods = wheat
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim
hre = no

#1133.1.1 = { mill = yes }
1356.1.1  = {
	add_core = NZH
}
1382.1.1  = {
	add_core = GOL
}
1392.1.1  = {
	owner = MOS
	controller = MOS
	add_core = MOS
	remove_core = NZH
}
1444.1.1 = {
	remove_core = GOL
	remove_core = BLU
	remove_core = WHI
}
1480.1.1 = {
	remove_core = GOL
} # Final destruction of the Golden Horde
1523.8.16 = { mill = yes }
1530.1.4  = {
	bailiff = yes	
	farm_estate = yes
}
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia
