# 4113 - Bokey

owner = WHI
controller = WHI
culture = tartar 
religion = sunni
capital = "Bokey"
base_tax = 3
base_production = 3 
base_manpower = 2
is_city = yes
trade_goods = wool 
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech
hre = no

1356.1.1 = {
	unrest = 3
	add_core = WHI
	add_core = AST
}
1382.1.1   = {
	owner = GOL
	controller = GOL
	add_core = GOL
	remove_core = WHI
	unrest = 0
}
1441.1.1 = {
	owner = NOG
	controller = NOG
	add_core = NOG
	remove_core = AST
	remove_core = GOL
}
#1520.1.1  = {
#	owner = KZH
#	controller = KZH
#} # Qasim Khan conquers Nogai lands
1600.1.1   = {
	religion = orthodox
}
1613.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
    	remove_core = NOG
} # The break up of the Nogai tribe
