# No previous file for Talimeco

culture = catawba
religion = totemism
capital = "Talimecae"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 35
native_ferocity = 3
native_hostileness = 4

1524.1.1  = { discovered_by = POR } # Diego Gomez
1707.5.12 = { discovered_by = GBR }
1735.1.1  = {	owner = GBR
		controller = GBR
	    	culture = english
	    	religion = protestant
			citysize = 1500
	    } # Settlement of Upcountry South Carolina
1750.1.1  = { citysize = 2970 }
1760.1.1  = { add_core = GBR }
1760.1.19 = { unrest = 5 } # Cherokee war
1761.1.1  = { unrest = 0 } # Peace attempt
1764.7.1  = {	culture = american
		unrest = 6
	    } # Growing unrest
1776.7.4  = {	owner = USA
		controller = USA
		add_core = USA
	    }
1782.11.1 = {	remove_core = GBR 
		unrest = 0
	    } # Preliminary articles of peace, the British recognized Amercian independence
1800.1.1  = { citysize = 3250 }
