# 1105 - Gurma

owner = GUR
controller = GUR
culture = mossi
religion = west_african_pagan_reformed
capital = "Gurma"
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
trade_goods = ivory
hre = no
discovered_by = soudantech
discovered_by = sub_saharan

1356.1.1   = {
	add_core = GUR
}
