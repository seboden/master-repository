# 1274 - Pressburg

owner = HUN
controller = HUN  
culture = slovak
religion = catholic
capital = "Pressburg"
trade_goods = wine
hre = no
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
fort_14th = yes
 # Saint Martin's Cathedral
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

#1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1356.1.1   = {
	add_core = HUN
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1452.1.1 = { temple = yes }
1465.1.1 = { medieval_university = yes }
1515.1.1 = { training_fields = yes }
1526.8.30  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	fort_14th = no
	fort_15th = yes
}
1685.1.1  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = HUN
	remove_core = TUR
} # Conquered/liberated by Charles of Lorraine, the Ottoman Turks are driven out of Hungary
