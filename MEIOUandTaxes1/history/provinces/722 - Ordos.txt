# 722 - Tangut

owner = YUA
controller = YUA
add_core = YUA
culture = tumed
religion = tengri_pagan_reformed
capital = "Ordos"
trade_goods = wheat
hre = no
base_tax = 2
base_production = 2
#base_manpower = 1.0
base_manpower = 2.0
citysize = 4070
discovered_by = chinese
discovered_by = steppestech


1133.1.1 = { mill = yes }
1392.1.1  = {
	remove_core = YUA
	owner = MNG
	controller = MNG
}
1449.9.1 = {
	owner = TMD
	controller = TMD
	add_core = TMD
} # Ordos mongols move into Ordos
1515.1.1 = { training_fields = yes }
1586.1.1 = { religion = vajrayana } # State religion
1634.1.1 = {
	owner = MCH
	controller = MCH
} # Part of the Manchu empire
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
}
1650.1.1 = { citysize = 7465 }
#1691.1.1 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#}
1700.1.1 = { citysize = 8100 }
1750.1.1 = { citysize = 8653 }
1800.1.1 = { citysize = 9240 }
