# 1535 - Figuig

owner = TFL
controller = TFL 
culture = fassi
religion = sunni
capital = "Figuig"
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = palm
discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1 = {
	add_core = TFL
	owner = TLE
	controller = TLE
	add_core = TLE
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1530.1.1 = {
	owner = TFL
	controller = TFL
	add_core = MOR
	remove_core = TLE
}
	
1549.1.1 = {
	owner = FEZ
	controller = FEZ
	add_core = FEZ
}
1554.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
	remove_core = FEZ
}
1603.1.1 = { unrest = 5 } # The death of the Saadita Ahmad I al-Mansur
1604.1.1 = { unrest = 0 }
1631.1.1 = {
	owner = TFL
	controller = TFL
}
1668.8.2 = {
	owner = MOR
	controller = MOR
}
