# 2830 - Selangor

owner = MLC
controller = MLC
culture = malayan
religion = buddhism
capital = "Kelang"
trade_goods = rice
hre = no
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
add_core = MLC
discovered_by = chinese
discovered_by = indian
discovered_by = muslim
discovered_by = austranesian

1133.1.1 = { mill = yes }
1400.1.1  = { religion = sunni }
1509.1.1  = { discovered_by = POR } # Diego Lopez de Sequiera
1511.9.10 = {
	owner = JOH
	controller = JOH
	add_core = JOH
} # Malacca falls to the Portuguese
