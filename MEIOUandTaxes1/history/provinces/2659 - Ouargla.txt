# 2659 - Ouargla

owner = TOG
controller = TOG
culture = berber
religion = ibadi
capital = "Ouargla"
trade_goods = wool
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan

1356.1.1   = {
	add_core = TOG
}
1530.1.1 = {
	add_permanent_claim = ALG
}
