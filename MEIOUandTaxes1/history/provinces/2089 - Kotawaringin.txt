# Province: Kotawaringin
# file name: 2089 - Kotawaringin

owner = BKS
controller = BKS
culture = dayak
religion = vajrayana
capital = "Kotawaringin"
trade_goods = rice			#FB too much clove
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian

1133.1.1 = { mill = yes }
1356.1.1 = { add_core = BKS }
1521.1.1 = { discovered_by = POR }
1550.1.1 = { religion = sunni }
1606.1.1 = { discovered_by = NED }
1680.1.1 = { add_core = NED unrest = 5 } #NED allies with Kotawaringin, weakening BKS control
1712.1.1 = {
	owner = NED
	controller = NED
	unrest = 2
}
1737.1.1 = { add_core = NED }
1800.1.1 = { unrest = 0 }
1811.9.1 = {
	owner = GBR
	controller = GBR
} # British take over
1816.1.1 = { owner = NED controller = NED } # Returned to the Dutch
