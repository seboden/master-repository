#1189 - Mahenge

owner = KIT
controller = KIT
culture = makua
religion = animism
capital = "Maghele"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = ivory
hre = no
discovered_by = central_african
discovered_by = east_african

1356.1.1 = {
	add_core = KIT
}
