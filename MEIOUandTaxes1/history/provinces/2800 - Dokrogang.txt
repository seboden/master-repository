# 2800 - Dokrogang

owner = KOR
controller = KOR
culture = korean
religion = mahayana #FB-ASSA become confucianism in 1392
capital = "Ganggye"
base_tax = 8
base_production = 8
base_manpower = 3
is_city = yes
trade_goods = rice
fort_14th = yes
discovered_by = chinese
discovered_by = steppestech
hre = no

1133.1.1 = { mill = yes }
1270.1.1  = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1290.1.1 = {	#Yuan returns Dongnyeong Prefecture
	owner = KOR
	controller = KOR
	add_core = KOR
}
1392.6.5  = {
	religion = confucianism
	owner = JOS
	controller = JOS
	add_core = JOS
	remove_core = KOR
	remove_core = YUA
}
1529.3.17 = { 
	marketplace = yes
	road_network = yes	
	bailiff = yes
}
1593.1.1 = {
	unrest = 5
} # Japanese invasion
1593.5.1 = {
	controller = JOS
	unrest = 0
} # Japanese invasion ends
1637.1.1 = {
	add_core = MNG
} # Tributary of Qing China
1644.1.1 = {
	add_core = QNG
	remove_core = MNG
} # Part of the Manchu empire
1653.1.1 = {
	discovered_by = NED
} # Hendrick Hamel
