# 3869 - Lazio-Frosinone

owner = PAP
controller = PAP
culture = umbrian 
religion = catholic 
capital = "Frosinone" 
base_tax = 7
base_production = 7
base_manpower = 3
is_city = yes
fort_14th = yes
trade_goods = wine
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = no

#1111.1.1 = { post_system = yes }
1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1309.1.1 = { add_core = PA2 owner = PA2 controller = PA2 }
1356.1.1   = {
	add_core = PAP
}
1378.1.1 = { remove_core = PA2 owner = PAP controller = PAP }
1530.1.1 = {
	road_network = no paved_road_network = yes 
}
1580.1.1   = {
	
} 
1750.1.1   = {
	add_core = ITA
} 
1809.4.10  = {
	controller = FRA
} # Occupied by French troops
1809.10.14 = {
	owner = FRA
	add_core = FRA
} # Treaty of Schönbrunn
1814.4.11  = {
	owner = PAP
	controller = PAP
	remove_core = FRA
} # # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1870.1.1 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}

