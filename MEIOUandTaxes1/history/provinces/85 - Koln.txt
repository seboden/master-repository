# 85 - Cologne

owner = FRK
controller = FRK
add_core = FRK
culture = ripuarianfranconian
religion = catholic
trade_goods = iron
capital = "K�ln"
base_tax = 9
base_production = 9
base_manpower = 3
is_city = yes


hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim


 # First Printer opens in K�ln

 1000.1.1 = {
	add_permanent_province_modifier = {
		name = minor_inland_center_of_trade_modifier
		duration = -1
	}
}

1100.1.1 = { marketplace = yes }
#1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1133.1.1 = { mill = yes }
1300.1.1 = { road_network = yes } 
1500.1.1 = { road_network = yes }
1520.1.1  = { fort_14th = yes  }
1530.1.3 = {
	road_network = no paved_road_network = yes 
	weapons = yes
}
1553.1.1  = {  } # Stock Exchange Founded
1638.1.1  = {   } # K�ln manages to stay neutral in the 30 years war and prospers through weapon sales. 
1716.1.1  = { refinery = yes weapons = no } # Farnia begins exporting "Eau de Cologne" 
1801.2.9  = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Treaty of Lun�ville
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
1815.6.9  = {
	owner = PRU
	controller = PRU
	add_core = PRU
   	remove_core = FRA
} # Congress of Vienna
