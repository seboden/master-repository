# 1591 - Pirin

owner = BUL
controller = BUL  
culture = bulgarian
religion = orthodox
capital = "Plovdiv"
trade_goods = lumber
hre = no
base_tax = 8
base_production = 8
base_manpower = 3
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

1300.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = BUL
	add_permanent_claim = BYZ
}
1371.2.17 = {
	owner = TAR
	controller = TAR
	add_core = TAR
	remove_core = BUL
}
1372.1.1  = {
	owner = OTT
	controller = OTT
	add_core = OTT
	capital = "Filibe"
	change_province_name = "Filibe"
}
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = OTT
}
1393.7.17 = {
	add_core = BUL
	remove_core = TAR
}
1400.1.1 = { remove_core = BYZ add_permanent_claim = BYZ }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1598.1.1  = {
	controller = REB
} # First Tarnovo Uprising
1600.1.1  = {
	controller = TUR
}
1686.1.1  = {
	revolt = { type = nationalist_rebels size = 1 }
	controller = REB
} # Second Tarnovo Uprising
1690.1.1  = {
	revolt = { }
	controller = TUR
}
