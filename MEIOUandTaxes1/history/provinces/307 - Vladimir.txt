# 307 - Vladimir
# MEIOU-GG - Turko-Mongol mod

owner = MOS
controller = MOS
culture = russian
religion = orthodox
capital = "Vladimir"
base_tax = 6
base_production = 6
base_manpower = 4
is_city = yes
trade_goods = iron

discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim

1191.1.1 = {
	temple = yes
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}

1356.1.1  = {
	add_core = MOS
	add_core = VLA
}
1382.1.1  = {
	add_core = GOL
}
1444.1.1 = {
	remove_core = GOL
	remove_core = BLU
	remove_core = WHI
}
1480.1.1  = {
	remove_core = GOL
}
 # Assumption cathedral, destroyed in 1185, but rebuilt
1530.1.4  = {
	bailiff = yes	
}
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
	remove_core = VLA
} # Ivan Grozny becomes the first Tsar of Russia
1598.1.1  = { unrest = 5 } # "Time of troubles"

1613.1.1  = { unrest = 0 } # Order returned, Romanov dynasty

1773.1.1  = { unrest = 5 } # Emelian Pugachev, Cossack insurrection
1774.9.14 = { unrest = 0 } # Pugachev is captured
