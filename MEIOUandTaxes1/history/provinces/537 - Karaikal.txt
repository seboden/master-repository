# 537 - Thanjavur

owner = MAD
controller = MAD
capital = "Karaikal"
culture = tamil
religion = hinduism
trade_goods = cinnamon
hre = no
base_tax = 3
base_production = 3
#base_manpower = 1.0
base_manpower = 2.0
citysize = 8857
discovered_by = indian
discovered_by = muslim

1000.1.1 = {
	add_permanent_province_modifier = {
		name = ideal_european_port
		duration = -1
	}
}
1356.1.1   = {
	add_core = MAD
}
1378.1.1   = {
	owner = VIJ
	controller = VIJ
}
1405.1.1 = { discovered_by = chinese }
1428.1.1 = { add_core = VIJ }
1498.1.1 = { discovered_by = POR }
1530.1.1 = {
	#owner = TNJ
	#controller = TNJ
	add_core = TNJ
	#remove_core = VIJ
}
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	dock = yes
	road_network = yes
}
1565.7.1 = {
	owner = TNJ
	controller = TNJ
} # The Vijayanagar empire collapses
1661.1.1 = {
	controller = BIJ
} # Bijapur Expansion
1662.1.1 = {
	owner = BIJ
} # Bijapur Expansion
1678.1.1 = {
	owner = MAR
	controller = MAR
} #Venkaji assumes independence
1694.1.1 = {
	controller = MUG
}
1707.1.1 = {
	controller = MAR
}
1728.1.1 = {
	add_core = MAR
}
1739.2.14  = {
	owner = FRA
	controller = FRA
	base_tax = 7
base_production = 7
}
1761.1.15 = {
	controller = GBR
} #the French commander, the Count de Lally, surrendered the town, ending French hopes of establishing a trading empire in India, and leaving the British East India Company triumphant
1763.1.1 = { controller = FRA } # Treaty of Paris, allows the French to use Puducherry as a trading post, it did nor reinstate french rule
1789.2.14  = {
	add_core = FRA
}
1793.1.1   = { controller = GBR }
1797.10.17 = { controller = FRA } # End of the First Coalition
1797.10.18 = { controller = GBR }
1802.10.1  = { controller = FRA } # End of the Second Coalition
1803.5.18  = { controller = GBR }
# 1805.12.26 = { controller = FRA } # End of the Third Coalition
# 1805.12.26 = { controller = GBR }
1807.10.24 = { controller = FRA } # End of the Fourth Coalition
1809.4.10  = { controller = GBR }
1809.10.14 = { controller = FRA } # End of the Fifth Coalition
1812.6.21  = { controller = GBR }
1814.4.11  = {
	controller = FRA
} # Treaty of Fontainebleau, Napoleon abdicates unconditionally
