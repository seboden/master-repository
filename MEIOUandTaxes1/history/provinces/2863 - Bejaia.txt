# 2863 - Bejaia

owner = TLE
controller = TLE
culture = algerian
religion = sunni
capital = "Bejaia"
base_tax = 6
base_production = 6
base_manpower = 1
is_city = yes
trade_goods = olive
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech
hre = no

900.1.1    = { set_province_flag = barbary_port }
1088.1.1 = { dock = yes shipyard = yes }
1337.1.1 = {
	owner = FEZ
	controller = FEZ
}
1356.1.1 = {
	add_core = TLE
	add_core = ALG
	add_core = KBA
	add_core = FEZ
} 
1358.1.1 = {
	owner = HAF
	controller = HAF
}
1510.1.1 = {
	owner = KBA
	controller = KBA
}
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1530.1.2 = {
	remove_core = FEZ
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1555.1.1 = {
	owner = KBA
	controller = KBA
	remove_core = SPA
}
1720.1.1 = {  }

1830.1.1 = {
	owner = FRA
	controller = FRA
	add_core = FRA
}
