# 2436 - Mino

owner = TKI
controller = TKI
culture = chubu
religion = mahayana #shinbutsu
capital = "Gifu"
trade_goods = rice
hre = no
base_tax = 7
base_production = 7
base_manpower = 7
is_city = yes
discovered_by = chinese

1133.1.1 = { mill = yes }
1356.1.1 = {
	add_core = TKI
}
1542.1.1 = { discovered_by = POR }
1582.1.1 = {
	owner = ODA
	controller = ODA
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
