# No previous file for Habr Tojaala

owner = WAR
controller = WAR
add_core = WAR
culture = somali
religion = sunni
capital = "Togdheer"
citysize = 2500
base_manpower = 1
trade_goods = millet
hre = no
base_tax = 1
base_production = 1
discovered_by = ETH
discovered_by = ADA
discovered_by = ZAN
discovered_by = AJU
discovered_by = MBA
discovered_by = MLI
discovered_by = ZIM
discovered_by = indian
discovered_by = muslim
discovered_by = turkishtech

1450.1.1 = { citysize = 3000 }
1499.1.1 = { discovered_by = POR }
1515.2.1 = { training_fields = yes }
#1526.1.1 = { owner = ADA controller = ADA } #Ahmad Gran secures control over Marehan
1550.1.1 = { discovered_by = TUR }
1555.1.1 = { owner = WAR controller = WAR } #Northern part of province no longer conrolled by ADA
