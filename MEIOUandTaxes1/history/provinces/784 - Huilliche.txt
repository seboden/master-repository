# 784 - Huililiche

culture = mapuche
religion = pantheism
capital = "Huilliche"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 10
native_ferocity = 1
native_hostileness = 5

1520.1.1   = {
	discovered_by = SPA
} # Discovered by Ferdinand Magellan
1900.1.1   = {
	owner = CHL
	controller = CHL
	add_core = CHL
	culture = platean
	religion = catholic
	citysize = 200
	trade_goods = fish
}
