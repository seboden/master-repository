# 1148 - Zaouila

owner = AJJ
controller = AJJ
culture = tuareg
religion = sunni
capital = "Ghat"
base_tax = 2
base_production = 2
base_manpower = 2
trade_goods = ivory
hre = no
discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan

1000.1.1 = {
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}
1200.1.1 = { marketplace = yes }
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1   = {
	add_core = AJJ
}
