# 3928 - Minahasa

culture = minahasa
religion = polynesian_religion
capital = "Menado"
trade_goods = clove
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
native_size = 60
native_ferocity = 2
native_hostileness = 7
discovered_by = MKS
discovered_by = MPH
discovered_by = MTR
discovered_by = MNH
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian

1337.1.1 = {
	discovered_by = MNH
	owner = MNH
	controller = MNH
	add_core = MNH
	is_city = yes
	discovered_by = BLI
}
1511.1.1 = { discovered_by = POR }
1608.1.1 = { religion = sunni }
1658.1.1 = {
	discovered_by = NED
	owner = NED
	controller = NED
	add_core = NED
	trade_goods = rice
	unrest = 2
	set_province_flag = trade_good_set
} # Dutch control
1700.1.1  = {
	unrest = 1
}
1750.1.1 = { culture = batavian base_tax = 3 base_production = 3 }
1811.9.1 = {
	owner = GBR
	controller = GBR
} # British take over
1816.1.1 = { owner = NED controller = NED } # Returned to the Dutch
