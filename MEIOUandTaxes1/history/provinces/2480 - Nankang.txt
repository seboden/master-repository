# 2480 - jiangxi_area Xunyang

owner = YUA
controller = YUA
culture = wuhan
religion = confucianism
capital = "Jiujiang"
trade_goods = rice
hre = no
base_tax = 9
base_production = 9
base_manpower = 2
is_city = yes


discovered_by = chinese
discovered_by = steppestech

0985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1133.1.1 = { mill = yes }
1200.1.1 = { paved_road_network = yes }
1276.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1320.1.1 = {
	remove_core = SNG
}
1353.1.1 = {
	add_core = MNG
}
#1356.1.1 = {	remove_core = YUA } # Red Turbans
1357.1.1 = {
	owner = MNG
	controller = MNG
}

1513.1.1 = { unrest = 5 } # Peasant rebellion, Jiangxi
1514.1.1 = { unrest = 0 }
1529.3.17 = { 
	marketplace = yes
	bailiff = yes
	courthouse = yes
}

1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # The Qing Dynasty
#1662.1.1 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1662.1.1 = { remove_core = MNG }
