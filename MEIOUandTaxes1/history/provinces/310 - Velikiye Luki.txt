# 310 - Velikiye Luki

owner = NOV
controller = NOV   
culture = novgorodian
religion = orthodox
hre = no
base_tax = 5
base_production = 5
trade_goods = fur
base_manpower = 2
is_city = yes
capital = "Velikiye Luki"
add_core = NOV
discovered_by = eastern
discovered_by = western
discovered_by = steppestech

1478.1.14 = {
	owner = MOS
	controller = MOS
	add_core = MOS
	remove_core = NOV
	culture = russian
}
1530.1.4  = {
	bailiff = yes	
}
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia

1598.1.1  = { unrest = 5 } # "Time of troubles"
1613.1.1  = { unrest = 0 } # Order returned, Romanov dynasty
1649.1.1  = { fort_14th = yes } # Built to protect Muscovy against the Swedes
1750.1.1  = { trade_goods = iron  } # Ironworking industries
