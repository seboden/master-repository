# 2436 - Kom�rom

owner = HUN
controller = HUN  
culture = hungarian
religion = catholic
capital = "Kom�rom"
trade_goods = lumber
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1   = {
	add_core = HUN
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1515.1.1 = { training_fields = yes }
1526.8.30  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	fort_14th = no
	fort_15th = yes
}
1529.10.7 = { bailiff = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1685.1.1  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = HUN
	remove_core = TUR
} # Conquered/liberated by Charles of Lorraine, the Ottoman Turks are driven out of Hungary
