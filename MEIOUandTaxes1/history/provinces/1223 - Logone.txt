# 1223 - Mandjia

native_size = 10
native_ferocity = 2
native_hostileness = 2
culture = kanouri
religion = animism
capital = "Bossangoa"
base_manpower = 1
trade_goods = millet
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1505.1.1 = {
	owner = KBO
	controller = KBO
	add_core = KBO
	is_city = yes
		base_tax = 2
base_production = 2
	base_manpower = 2
}
