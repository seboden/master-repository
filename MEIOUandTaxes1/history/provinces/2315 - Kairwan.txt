# 2315 - Kairwan

owner = HAF
controller = HAF
add_core = HAF
culture = tunisian
religion = sunni
capital = "Kairwan"
base_tax = 7
base_production = 7
base_manpower = 2
is_city = yes
trade_goods = wheat

discovered_by = KBO
discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech
hre = no

1250.1.1 = { temple = yes }
1489.1.1  = { unrest = 4 } # Abu Zikriya Yahya overthrown by Abul Mumin
1490.1.1  = { unrest = 0 } # Abu Yahya recaptures the power
1530.1.3 = {
	road_network = no paved_road_network = yes 
	add_claim = TUR
}
1534.1.1  = { unrest = 5 } # Ottoman invasion
1535.1.1  = { unrest = 0  }
1574.9.13 = {
	owner = TUR
	controller = TUR
	add_core = TUR
	add_core = TUN
	remove_core = HAF
} # Directly controlled
1591.1.1  = { unrest = 6 } # Janissary revolt
1592.1.1  = { unrest = 0 }
 # Became the center of French commercial life
1702.1.1  = { unrest = 4 add_core = ALG } # Military coup, Murad III is assassinated
1703.1.1  = { unrest = 0 }
1705.1.1  = {
	
	owner = TUN
	controller = TUN
} # Husayn ibn Ali's rule brings some prosperity
1735.1.1  = { unrest = 6 } # Coup with Algerian support

1756.1.1  = { unrest = 0 } # The Husaynid Dynasty is restored
1881.5.12 = {
	owner = FRA
	controller = REB
}
1881.10.28 = { controller = FRA }
