# 105 - Marche de Ancona

owner = ANC
controller = ANC
culture = umbrian 
religion = catholic 
capital = "Ancona" 
base_tax = 9
base_production = 9   
base_manpower = 3
is_city = yes    
trade_goods = wool  
fort_14th = yes 
medieval_university = yes	# University of Macerata
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = no

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "mediterranean_harbour" 
		duration = -1 
		}
}
1088.1.1 = { dock = yes }
1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }

1356.1.1 = {
	add_core = ANC
}
1503.9.1 = { revolt = { type = anti_tax_rebels size = 0 } controller = REB } # Loss of Papal authority after the death of Alexander III, Venetian influence
1506.1.1 = { revolt = { } controller = ANC }
1522.3.20 = { dock = no naval_arsenal = yes }
1530.1.1 = { add_core = PAP
	road_network = no paved_road_network = yes 
	bailiff = yes }
1532.1.1 = {
	owner = PAP
	controller = PAP
	add_core = PAP
}
 
1805.3.17 = { owner = ITE controller = ITE add_core = ITE } # Treaty of Pressburg
1814.4.11 = { owner = PAP controller = PAP remove_core = ITE } # Treaty of Fontainebleau, Napoleon abdicates unconditionally
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
