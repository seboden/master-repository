# 283 - Brestie

owner = LIT
controller = LIT
capital = "Biala Podlaska"
culture = ruthenian
religion = orthodox
trade_goods = lumber
hre = no
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = steppestech

1356.1.1   = {
	add_core = LIT
	add_permanent_province_modifier = {
		name = lithuanian_estates
		duration = -1
	}
}
1386.1.1 = {
	religion = catholic
}

1508.1.1   = { fort_14th = yes }
1530.1.4  = {
	bailiff = yes	
}
1543.1.1   = { unrest = 3 } # Counter reformation

1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1579.1.1   = { medieval_university = yes unrest = 0 } # Vilnius university

1791.6.1   = { unrest = 3} # 3rd May constitution raised opposition
1794.3.24  = { unrest = 6 } # Kosciuszko uprising
1794.11.16 = { unrest = 0 }
1795.10.24  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = LIT
	add_core = PLC
} # Most of Lithuania became part of the Russian empire
1812.6.28  = { controller = FRA } # Occupied by French troops
1814.4.11  = { controller = POL }
1815.6.9   = {
	owner = RUS
	controller = RUS
	add_core = RUS
} # Congress Poland, under Russian control after the Congress of Vienna
