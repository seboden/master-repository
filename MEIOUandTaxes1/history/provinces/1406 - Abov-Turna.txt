# 1406 - Abov-Turna

owner = HUN
controller = HUN  
culture = slovak
religion = catholic
capital = "Kosice"
trade_goods = wheat
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
fort_14th = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = steppestech

#1133.1.1 = { mill = yes }
1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = HUN
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
}
1506.1.1  = { controller = REB } # Szekely rebellion
1507.1.1  = { controller = HUN } # Estimated
1514.4.1  = { controller = REB } # Peasant rebellion against Hungary's magnates
1515.1.1  = { controller = HUN } # Estimated
1515.2.1 = { training_fields = yes }
1523.8.16 = { mill = yes }
1526.8.30  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = TRA
}
1530.1.4  = {
	bailiff = yes	
}
1685.1.1  = {
	owner = HAB
	controller = HAB
	add_core = HAB
	add_core = HUN
	remove_core = TUR
} # Conquered/liberated by Charles of Lorraine, the Ottoman Turks are driven out of Hungary
1703.8.1   = {
	controller = TRA
} # The town supports Francis II R�k�czi in his rebellion against the Habsburgs
1711.5.1   = {	controller = HAB } # Ceded to Austria The treaty of Szatmar, Austrian governors replaced the prince of Transylvania
