# No previous file for Reggio

owner = MLO
controller = MLO
add_core = MLO
culture = emilian
religion = catholic
capital = "Reggio"
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
trade_goods = cheese
hre = yes
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1356.1.1   = {
	add_core = MLO
	add_core = FER
	add_permanent_province_modifier = {
		name = "county_of_correggio"
		duration = -1
	}
}
1405.1.1   = {
	owner = FER
	controller = FER
} # Michele Attendolo hands the city over to Nicolo III d'Este
1513.1.1   = {
	owner = PAP
	controller = PAP
	bailiff = yes
} # Handed over to Pope Julius II
1523.9.29  = {
	owner = FER
	controller = FER
	remove_core = MLO
} # Returned to the d'Este after the death of Hadrian VI
1530.1.1 = {
	owner = MOD 
	controller = MOD
	add_core = MOD
	remove_core = FER
}
1530.2.27 = {
	hre = no
}
1597.10.28 = {
	owner = MOD 
	controller = MOD
	add_core = MOD
	remove_core = FER
}
1618.1.1   = { hre = no }
1796.11.15 = {
	owner = ITD
	controller = ITD
	add_core = ITD
	remove_core = HAB
} # Cispadane Republic
1797.6.29  = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = ITD
} # Cisalpine Republic
1814.4.11 = { 
	owner = MOD 
	controller = MOD 
	add_core = MOD 
	remove_core = ITE
}
1859.8.20= { controller = REB }
1860.3.20 = {
	owner = SPI
	controller = SPI
	add_core = SPI
}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
