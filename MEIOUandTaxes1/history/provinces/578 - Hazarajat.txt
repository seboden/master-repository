# 578 - Hazarajat

owner = CHG
controller = CHG
culture = pashtun
religion = sunni
capital = "Ghazni"
trade_goods = wool
hre = no
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1120.1.1 = { textile = yes }
1356.1.1   = {
	add_core = KAB
	add_core = KHO
	add_core = CHG
	add_permanent_province_modifier = {
		name = pashtun_tribal_area
		duration = -1
	}
}
1370.4.1   = {
	owner = TIM
	controller = TIM
	add_core = TIM
	remove_core = CHG
}
1444.1.1  = {
	owner = KTD
	controller = KTD
	remove_core = TIM
	remove_core = KAB
	add_core = DUR
} # Shaybanids break free from the Timurids
1461.1.1 = {
	owner = TIM
	controller = TIM
}
1469.8.27 = {
	owner = DUR
	controller = DUR
}
1500.1.1 = { religion = sunni }
1507.7.1 = {
	owner = TIM
	controller = TIM
	add_core = TIM
} # Kabul falls to Babur
1515.1.1 = { training_fields = yes }
1526.4.21 = {
	owner = MUG
	controller = MUG
	add_core = MUG
	remove_core = TIM
	bailiff = yes
} #Battle of Panipat
1566.6.1 = { revolt = { }
	owner = KAB
	controller = KAB
}	#Independent of Mughals for a long while
1585.1.1 = {
	controller = MUG
}	# Man Singh occupies Kabulistan after death of Mirza Hakim
1585.2.1 = {
	owner = MUG
} # Annexed into empire again

1672.1.1 = {
	controller = REB
	revolt = { type = nationalist_rebels }
} # Widespread tribal uprisings
1675.1.1 = {
	controller = MUG
	revolt = { }
} # End of uprisings
1677.1.1 = { discovered_by = FRA }
1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
1737.1.1  = {
	controller = PER
}
1739.5.1  = {
	owner = PER
} # Captured by Persia, Nadir Shah
1747.10.1 = {
	owner = DUR
	controller = DUR
	add_core = DUR
	remove_core = MUG
} # Ahmad Shah established the Durrani empire
