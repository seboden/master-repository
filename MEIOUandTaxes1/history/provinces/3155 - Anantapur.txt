# No previous file for Anantapur

owner = GNG
controller = GNG
culture = kannada
religion = hinduism
capital = "Anantapur"
trade_goods = cotton
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim
add_local_autonomy = 25

1100.1.1 = { plantations = yes }
1336.1.1 = {
	owner = VIJ
	controller = VIJ
}
1356.1.1 = {
	add_core = GNG
	add_core = VIJ
}
1385.1.1 = {
	owner = VIJ
	controller = VIJ
}
#1507.1.1 = { add_core = GOC } # Qutb Shahi dynasty
1530.3.17 = {
	bailiff = yes
	marketplace = yes
	road_network = yes
}
1570.1.1 = { unrest = 0 }
1645.1.1 = {
	controller = GOC
} # Conquered by Golconda
1646.1.1 = {
	owner = GOC
	remove_core = VIJ
} # Conquered by Golconda
1650.1.1 = { discovered_by = turkishtech }
1686.1.1 = {
	controller = MUG
}
1687.1.1 = {
	owner = MUG
}
1700.1.1 = { add_core = MUG }
1707.5.12 = { discovered_by = GBR discovered_by = FRA }
1724.1.1 = {
	owner = HYD
	controller = HYD
	add_core = HYD
	remove_core = MUG
} # Asif Jah declared himself Nizam-al-Mulk of Hyderabad

1801.1.1 = {
	owner = GBR
	controller = GBR
}
1826.1.1 = { add_core = GBR }
