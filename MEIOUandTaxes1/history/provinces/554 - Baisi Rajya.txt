# 554 - Baisi Rajya

owner = NPL
controller = NPL
culture = nepali
religion = hinduism
capital = "Dangarhi"
trade_goods = ebony
hre = no
base_tax = 3
base_production = 3
#base_manpower = 1.0
base_manpower = 2.0
citysize = 5387

discovered_by = indian
discovered_by = muslim
discovered_by = chinese
discovered_by = steppestech
add_local_autonomy = 25

1356.1.1  = { add_core = NPL }
1400.1.1  = { citysize = 6020 }
1500.1.1  = { citysize = 7620 }
1550.1.1  = { citysize = 8544 }
1600.1.1  = { citysize = 9733 }
1650.1.1  = { citysize = 10087 }
1690.1.1  = { discovered_by = ENG }
1700.1.1  = { citysize = 11335 }
1707.5.12 = { discovered_by = GBR }
1750.1.1 = { citysize = 12800 }
1800.1.1 = { citysize = 14400 }
