# No previous file for Yat�

culture = ge
religion = pantheism
capital = "Yat�"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 15
native_ferocity = 1
native_hostileness = 7

1500.4.23 = { discovered_by = POR } # Pedro �lvares Cabral
1540.1.1 = {
	owner = POR
	controller = POR
	add_core = POR
	citysize = 150
	culture = portugese
	religion = catholic
	trade_goods = tobacco
	change_province_name = "Cimbres"
	rename_capital = "Cimbres"
}
1650.1.1   = {
	citysize = 1030
}
1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1800.1.1   = {
}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
