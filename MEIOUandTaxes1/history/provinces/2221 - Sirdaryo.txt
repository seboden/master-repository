# 2221 - Syr Darya

owner = CHG
controller = CHG
culture = uzbehk
religion = sunni
capital = "Kyzylorda"
trade_goods = wool
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = muslim
discovered_by = mongol_tech
discovered_by = steppestech
discovered_by = turkishtech

1356.1.1   = {
	add_core = CHG
	add_core = KZH
}
1370.4.1   = {
	owner = TIM
	controller = TIM
	add_core = TIM
	remove_core = CHG
}
1444.1.1 = {
	add_core = SHY
}
1446.1.1 = {
	owner = SHY
	controller = SHY
	add_core = SHY
	remove_core = TIM
	remove_core = GOL
}
#1465.1.1 = {
#	owner = KZH
#	controller = KZH
#}
1502.1.1 = {
	owner = SHY
	controller = SHY
}
1515.1.1 = { training_fields = yes }
1520.1.1 = {
	owner = BUK
	controller = BUK
	add_core = BUK
	remove_core = KZH
	remove_core = SHY
}
1709.1.1 = {
	owner = KOK
	controller = KOK
	add_core = KOK
   	remove_core = BUK
}