# 205 - Savoue maurienne chablais genevois
# chambery suse annecy thonon

owner = SAV
controller = SAV
culture = arpitan
religion = catholic
capital = "Chanberie"
base_tax = 6
base_production = 6
base_manpower = 6
is_city = yes
trade_goods = hemp

discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = yes


1356.1.1   = {
	add_core = SAV
	bailiff = yes
}
1499.8.10  = {
	controller = FRA
}
1504.1.31 = {
	controller = SAV
}
1508.12.10 = {
	controller = FRA
}
1516.12.1 = {
	controller = SAV
} 
1530.2.27 = {
	hre = no
}
1536.4.1   = {
	controller = FRA
	add_core = FRA
} 
1538.6.17   = {
	owner = FRA
	add_local_autonomy = 75
}
1559.1.1   = {
	owner = SAV
	controller = SAV
	remove_core = FRA
	add_local_autonomy = -80
} # Peace of Cateau-Cambrésis - full autonomy restored
1618.1.1  =  { hre = no }
1650.1.1   = {
	
}
1680.1.1   = {
	
}
1700.1.1   = {
	
}
1713.4.12  = {
	owner = SIC
	controller = SIC
	add_core = SIC
	remove_core = SAV
} # Treaty of Utrecht(2) gave Sicily to the House of Savoy 
1718.8.2   = {
	owner = SPI
	controller = SPI
	add_core = SPI
	remove_core = SIC
} # Kingdom of Piedmont-Sardinia
1792.9.21  = {
	controller = FRA
} # Occupied by French troops
1792.11.27 = {
	owner = FRA
	add_core = FRA
} # Savoie is annexed
1814.4.11  = {
	owner = SPI
	controller = SPI
	remove_core = FRA
} # Napoleon abdicates and Victor Emmanuel is able to return to Turin
1860.1.13   = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = SPI
}
