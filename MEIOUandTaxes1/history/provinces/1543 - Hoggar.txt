# 1543 - Hoggar

owner = TUA
controller = TUA
culture = tuareg
religion = sunni
capital = "Tamanrasset"
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
trade_goods = salt
discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1000.1.1 = {
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}
1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1   = {
	add_core = TUA
}
