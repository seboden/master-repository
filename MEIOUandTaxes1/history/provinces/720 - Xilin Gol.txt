# 720 - Onggirat

owner = YUA
controller = YUA
add_core = YUA
culture = chahar
religion = tengri_pagan_reformed
capital = "Xilinhot"
trade_goods = wheat
hre = no
base_tax = 2
base_production = 2
#base_manpower = 1.0
base_manpower = 2.0
citysize = 4070
discovered_by = chinese
discovered_by = steppestech

1392.1.1  = {
	remove_core = YUA
	owner = MNG
	controller = MNG
}
1440.1.1 = {
	owner = CHH
	controller = CHH
	add_core = CHH
	culture = chahar
}
1515.1.1 = { training_fields = yes }
1550.1.1 = { citysize = 5536 }
1586.1.1 = { religion = vajrayana } # State religion
1600.1.1 = { citysize = 6800 }
1634.1.1 = {
	owner = MCH
	controller = MCH
	add_core = MCH
	remove_core = TMD
} # Part of the Manchu empire
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
}
1650.1.1 = { citysize = 7450 }
#1691.1.1 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#}
1700.1.1 = { citysize = 8100 }
1750.1.1 = { citysize = 8653 }
1800.1.1 = { citysize = 9240 }
