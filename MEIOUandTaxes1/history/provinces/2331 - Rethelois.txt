# 2331 - Rethel

owner = FLA
controller = FLA
add_core = FLA
culture = lorrain
religion = catholic 
capital = "Rethel"
base_tax = 4
base_production = 4
base_manpower = 1
is_city = yes
trade_goods = lumber
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = no
add_local_autonomy = 25
estate = estate_nobles

1384.1.1   = { owner = BUR controller = BUR add_core = BUR remove_core = FLA }
1444.1.1 = { remove_core = FRA }
1477.1.5  = { owner = NEV controller = NEV add_core = NEV add_core = FRA }
1491.1.1 = { add_core = KLE remove_core = BUR  }
1500.1.1 = { road_network = yes }
1530.1.1 = {
   owner = FRA
   controller = FRA
   add_core = FRA
}
1530.1.4  = {
	bailiff = yes	
}
1565.1.1 = { add_core = MAN remove_core = KLE  }
1648.10.24 = {
	hre = no
} # Treaty of Westphalia, ending the Thirty Years' War
1659.1.1 = {
	owner = FRA
	controller = FRA
	remove_core = NEV
	remove_core = MAN
}
1793.3.7   = { controller = REB } # The Royalists revolt against the Republic
1796.12.23 = { controller = FRA } # The last rebels are defeated at the battle of Savenay
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
