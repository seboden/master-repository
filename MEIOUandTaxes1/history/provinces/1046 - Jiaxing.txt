# 1046 - Jiaxing

owner = CMN
controller = CMN
culture = wuhan
religion = confucianism
capital = "Jiaxing"
trade_goods = chinaware
hre = no
base_tax = 5
base_production = 5
#base_manpower = 4.0
base_manpower = 3
citysize = 1271600
add_core = CMN


discovered_by = chinese
discovered_by = steppestech

0985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
	remove_core = CMN
	bailiff = yes constable = yes
}
#1111.1.1 = { post_system = yes }
1200.1.1 = { paved_road_network = yes }
1273.1.1 = {
	owner = YUA
	controller = YUA
	add_core = YUA
}
1355.1.1  = {
	owner = ZOU
	controller = ZOU
	add_core = ZOU
	add_core = MNG
	remove_core = SNG
}
#1356.1.1 = { remove_core = YUA }
1367.1.1  = {
	owner = MNG
	controller = MNG
	remove_core = ZOU
}
1400.1.1  = { citysize = 350000 }
1500.1.1  = { citysize = 300000 }
1529.3.17 = { 
	marketplace = yes
	constable = no
	courthouse = yes
	fine_arts_academy = yes
}
1550.1.1  = { citysize = 250000 }
1600.1.1  = { citysize = 250000 }

1630.1.1  = { unrest = 6 } # Li Zicheng rebellion
1645.5.27 = { unrest = 0 } # The rebellion is defeated
1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # The Qing Dynasty
#1644.1.1 = {
#	controller = MCH
#}
#1644.6.6 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1650.1.1  = { citysize = 250000 }
1662.1.1 = { remove_core = MNG }
1700.1.1  = { citysize = 250000}
1745.1.1  = {  }
1750.1.1  = { citysize = 250000 }
1800.1.1  = { citysize = 250000 }
