# No previous file for Borzya

owner = YUA
controller = YUA
add_core = YUA
culture = khalkas
religion = tengri_pagan_reformed
capital = "Borzya"
trade_goods = fur
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = steppestech

1392.1.1 = {
	owner = KHA
	controller = KHA
	add_core = KHA
	remove_core = YUA
}
1515.1.1 = { training_fields = yes }
1586.1.1 = { religion = vajrayana } # State religion
1656.1.1 = {
	owner = RUS
	controller = RUS
   	religion = orthodox
   	culture = russian
}
1691.1.1 = { add_core = RUS }
