# 807 - Tucume

owner = CBC
controller = CBC
add_core = CBC
culture = chibcha
religion = inti
capital = "Ku�lap"

base_tax = 6
base_production = 6
#base_manpower = 2.0
base_manpower = 4.0
citysize = 5000
trade_goods = cotton


 

discovered_by = south_american

hre = no

1356.1.1 = {
}
1520.1.1   = {
	owner = INC
	controller = INC
	add_core = INC
	paved_road_network = yes
	
}
1524.1.1   = { discovered_by = SPA }	#FB was 1524
1529.1.1 = {
	owner = QUI
	controller = QUI
	add_core = QUI
	add_core = CZC
	remove_core = INC
	bailiff = yes
	constable = yes
	marketplace = yes
}
1533.7.26  = {
	add_core = SPA
	owner = SPA
	controller = SPA
	citysize = 2000
	culture = castillian
	religion = catholic
}
1537.1.1  = { unrest = 8 } # Fighting broke out when Almagro seized Cuzco
1538.1.1  = { unrest = 5 } # Almagro is defeated & executed
1541.1.1  = { unrest = 6 } # Pizzaro is assassinated by supporters of Almagro, his brother assumed control
1548.1.1  = { unrest = 0 } # Gonzalo Pizzaro is also executed & Spain succeeds in reasserting its authority
1650.1.1   = {
	citysize = 3500
}
1700.1.1   = {
	citysize = 5000
}
1750.1.1  = {
	citysize = 7500
	add_core = PEU
	culture = peruvian
}
1800.1.1   = {
	citysize = 10000
}
1810.9.18  = {
	owner = PEU
	controller = PEU
}
1818.2.12  = {
	remove_core = SPA
}
