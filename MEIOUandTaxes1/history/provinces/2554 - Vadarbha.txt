# 2554 - Mandla

owner = GHR
controller = GHR # Mandla
culture = gondi
religion = hinduism
capital = "Mandla"
trade_goods = livestock
hre = no
base_tax = 4
base_production = 4
#base_manpower = 1.5
base_manpower = 3.0
citysize = 8000
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1120.1.1 = { farm_estate = yes }
1356.1.1  = {
	add_core = GHR
}
1450.1.1  = { citysize = 15000 }
1500.1.1  = { citysize = 19000}
1530.1.1 = { 
	add_permanent_claim = MUG
}
1550.1.1  = { citysize = 26000 }
1564.6.24 = { controller = MUG } #Conquered by Mughals
1564.7.1  = { owner = MUG } #Annexed by Mughals
1600.1.1  = { citysize = 14000 }
1610.1.1  = { fort_14th = yes }
1614.7.1  = { add_core = MUG }
1650.1.1  = { citysize = 12000 }
1690.1.1  = { discovered_by = ENG }
1700.1.1  = { citysize = 22000 }
1707.5.12 = { discovered_by = GBR }
1742.1.1  = { controller = MAR }	#Maratha expansion
1743.1.1  = {
	owner = BHO
	controller = BHO
	add_core = BHO
} # Marathas (Bhonsle)
1750.1.1  = { citysize = 24000 }
1783.1.1  = { capital = "Tehri" }
1800.1.1  = { citysize = 18000 }
