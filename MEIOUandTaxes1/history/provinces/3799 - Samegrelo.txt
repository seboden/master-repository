# No previous file for Samegrelo

owner = GEO
controller = GEO
culture = georgian
religion = orthodox
capital = "Zugdidi"
trade_goods = wheat
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = muslim
discovered_by = eastern
discovered_by = western
discovered_by = steppestech
discovered_by = turkishtech

#1133.1.1 = { mill = yes }
1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = GEO
}
1444.1.1  = {
        add_core = IME
}
1458.1.1  = { unrest = 5 } # Safavid campaign against Georgia
1460.1.1  = { unrest = 0 } # Defeated by Shirwan Shah
1466.1.1  = {
	owner = IME
	controller = IME
	add_core = IME
	remove_core = GEO
}
1523.8.16 = { mill = yes }
1555.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	remove_core = IME
}
1614.1.1  = { unrest = 6 } # Overrun several times by Persian troops
1617.1.1  = { unrest = 2 } # Thousands were killed or resettled in Iran
1625.1.1  = { unrest = 8 } # Insurrection, headed by Giorgi Saakadze
1630.1.1  = { unrest = 0 }
1659.1.1  = { unrest = 4 } # Revolts, the garrisons in Kakheti were defeated by the Kakhetians
1665.1.1  = { unrest = 0 }
1735.1.1  = {  }
1810.2.20 = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = TUR
	remove_core = IME
} # Annexed by Russia
