# 181 - Tarragona

owner = ARA		#Alfons V of Aragon
controller = ARA
add_core = ARA
culture = catalan
religion = catholic
capital = "Tarragona" 
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
trade_goods = naval_supplies
estate = estate_church
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = no



1000.1.1 = {
	add_permanent_province_modifier = {
		name = ebro_estuary_modifier
		duration = -1
	}
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1200.1.1 = { road_network = yes }
1250.1.1 = { temple = yes }
1461.9.23  = { controller = REB unrest = 3 } #Catalan nobility revolts against King Joan, later the "Remensa" peasant revolt erupts, which is used by the King to erode the Catalan nobility. 
1472.10.16 = { controller = ARA unrest = 0 } #The Generalitat capitulates to King Joan, the civil war ends
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	road_network = no paved_road_network = yes 
	bailiff = yes
	add_core = CAT
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
 # The construction started in 1536

1640.6.7   = {
	controller = REB
	unrest = 5
	add_core = CAT
} # Guerra dels Segadors 
1641.1.26  = {
	unrest = 0
	owner = CAT
	controller = CAT
} # Proclamation of the Republic of Catalonia
1641.2.27  = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Death of Pau Claris i Casademunt, proclamation of Louis XIII de France as count of Barcelona.
1652.10.11 = {
	owner = SPA
	controller = SPA
	remove_core = FRA
} # Barcelona surrenders

1668.4.4   = { unrest = 5 } # Peasant revolt of the "barretines"
1669.12.2  = { unrest = 5 } # End of the revolt
1689.1.1   = { controller = REB } # Peasant revolt against new taxes to fund the war against France
1689.11.30 = { controller = SPA } # The peasants retreat from Barcelona
 # Recovery of the Catalan trade and flourishment of textil industry thanks to exports to America
1697.8.10  = { controller = FRA	} #Barcelona falls to the French
1697.9.20  = { controller = SPA } #Peace of Ryswick
1700.1.1   = {  }
1705.10.9  = { controller = HAB } # Catalonia recognises Archduke Carlos as their sovereign, joining the Anglo-Austrian cause in the War of Spanish Succession. The loyalist troops in Barcelona are defeated.
1713.4.11  = { controller = REB	} # Catalan authorities don't recognise the Peace of Utrecht, and face war alone against the Franco-Spanish army.
1713.7.13  = { remove_core = ARA }
1714.9.11  = { controller = SPA } # Barcelona surrenders
