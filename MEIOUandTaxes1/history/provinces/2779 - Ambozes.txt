# 2779 - Ambozes

owner = CLB
controller = CLB
culture = bakongo		
religion = animism		 
capital = "Ambozes"
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
trade_goods = slaves
discovered_by = soudantech
discovered_by = sub_saharan

1356.1.1   = {
	add_core = CLB
}
