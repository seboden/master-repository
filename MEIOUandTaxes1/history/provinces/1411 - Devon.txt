# 1411 - Devon

owner = ENG
controller = ENG
culture = english
religion = catholic
hre = no
base_tax = 6
base_production = 6
trade_goods = livestock
base_manpower = 3
is_city = yes
capital = "Exeter"
fort_14th = yes
discovered_by = western
discovered_by = muslim
discovered_by = eastern

#1100.1.1 = { bailiff = yes }
1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = ENG
}
1453.1.1  = { unrest = 5 } #Start of the War of the Roses
1461.6.1  = { unrest = 2 } #Coronation of Edward IV
1467.1.1  = { unrest = 5 } #Rivalry between Edward IV & Warwick
1470.9.1  = { controller = REB }
1470.10.6 = { controller = ENG } #Readeption of Henry VI
1471.1.1  = { unrest = 8 } #Unpopularity of Warwick & War with Burgundy
1471.5.4  = { unrest = 2 } #Murder of Henry VI & Restoration of Edward IV
1483.6.26 = { unrest = 8 } #Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23 = { unrest = 0 road_network = no paved_road_network = yes
	bailiff = yes } #Battle of Bosworth Field & the End of the War of the Roses
1575.1.1  = { religion = protestant } #anglican
1600.1.1  = { religion = reformed fort_14th = yes }
 #marketplace Estimated
 #Estimated
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
