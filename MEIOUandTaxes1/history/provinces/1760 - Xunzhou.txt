# 1760 - guangxi_area Xingbin

owner = YUA
controller = YUA
culture = zhuang
religion = confucianism
capital = "Guiping"
trade_goods = rice
hre = no
base_tax = 5
base_production = 5
base_manpower = 2
is_city = yes


discovered_by = chinese
discovered_by = steppestech

0985.1.1 = {
	owner = SNG
	controller = SNG
	add_core = SNG
}
1133.1.1 = { mill = yes }
1276.1.1 = {
	owner = LNG
	controller = LNG
	add_core = LNG
} #creation of liang viceroy
1320.1.1 = {
	remove_core = SNG
}
1368.1.1 = {
	owner = MNG
	controller = MNG
	add_core = MNG
	remove_core = LNG
}
1513.1.1 = { unrest = 5 } # Peasant rebellion, Jiangxi
1514.1.1 = { unrest = 0 }
1529.3.17 = { 
	marketplace = yes
	courthouse = yes
	road_network = yes
}

1645.6.25 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # The Qing Dynasty
#1662.1.1 = {
#	owner = QNG
#	controller = QNG
#	add_core = QNG
#	remove_core = MNG
#} # The Qing Dynasty
1662.1.1 = { remove_core = MNG }
