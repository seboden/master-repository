# No previous file for Trememb�

culture = ge
religion = pantheism
capital = "Tupinamba"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 25
native_ferocity = 1
native_hostileness = 5

1500.1.1   = {
	discovered_by = CAS
	discovered_by = POR
	add_permanent_claim = POR
} # Pedro �lvares Cabral 
1516.1.23  = {
	discovered_by = SPA
}
1614.1.1   = {
	discovered_by = POR
	owner = POR
	controller = POR
	citysize = 100
	culture = portugese
	religion = catholic
	change_province_name = "Parna�ba"
	rename_capital = "S�o Jo�o"
	trade_goods = livestock
}
1750.1.1   = {
	add_core = BRZ
	culture = brazilian
}
1769.1.1   = {
	citysize = 1000
}
1822.9.7   = {
	owner = BRZ
	controller = BRZ
}
1825.1.1   = {
	remove_core = POR
}
