# 3724 - Minden

owner = MDN
controller = MDN
culture = old_saxon
religion = catholic
capital = "Minden"
trade_goods = iron
hre = yes
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
add_core = MDN
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = { road_network = yes }
1529.1.1   = { religion = protestant }
1530.1.4  = {
	bailiff = yes	
}
1625.1.1   = {
	controller = HAB
} # Occupied by Imperial troops
1634.1.1   = {
	controller = MDN
} # Liberated by Swedish troops
1648.10.24 = {
	owner = BRA
	controller = BRA
	add_core = BRA
} # Treaty of Westphalia, ending the Thirty Years' War
1701.1.18  = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = BRA
} # Friedrich III becomes king in Prussia
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1806.11.13 = {
	controller = FRA
}
1807.7.9   = {
	owner = WES
	controller = WES
	add_core = WES
	remove_core = PRU
} # The Second Treaty of Tilsit
1810.12.13 = {
	owner = FRA
	controller = FRA
     	add_core = FRA
     	remove_core = WES
} # Annexed by France
1813.10.13 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = WES
} # Treaty of Paris
