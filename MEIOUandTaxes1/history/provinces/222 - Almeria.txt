# 222 - Almer�a

owner = GRA		#Mustapha Sa'd King of Granada
controller = GRA
culture = andalucian # culture = eastern_andalucian
religion = sunni
hre = no
base_tax = 6
base_production = 6
trade_goods = iron
base_manpower = 4
is_city = yes
capital = "Al Mariyya"
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "mediterranean_harbour" 
		duration = -1 
	}
}
1088.1.1 = { dock = yes }

1356.1.1  = {
	add_core = GRA
	set_province_flag = granada_emirate
	set_province_flag = arabic_name
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1469.1.1   = {
	add_core = CAS
} # Union of the Crowns of Castile and Aragon
1487.1.1   = { controller = REB } # After King Muhammad X pacts with the Catholic Kings, Muhammad El Zagal flees to Almer�a to proceed the war against the christians from there.
1489.12.26 = {
	owner = CAS
	controller = CAS
	rename_capital = "Almer�a" 
	change_province_name = "Almer�a"
	remove_core = GRA
} # Conquest of Gibraltar by King Enrique of Castilla
1499.12.1  = { unrest = 2 } # The Inquisition forces Spanish muslims to convert back to Christianity. Occasional revolts occur.
1502.2.1   = { unrest = 0 religion = catholic } # New capitulations where all the subjects of Granada are baptised and fully incorporated into the legal system of Castilla
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castille
1522.3.20 = { dock = no naval_arsenal = yes }
1568.12.25 = { controller = REB unrest = 5 } # Sublevation of the morisques in the kingdom of Granada.
1570.10.28 = {
	controller = SPA
	unrest = 0
} # To quell the revolt, the morisques in Granada are forcefully deported to other Spanish territories
1713.4.11  = { remove_core = CAS }
1808.6.6   = { controller = REB }
1811.1.1   = { controller = SPA }
1812.10.1  = { controller = REB }
1813.12.11 = { controller = SPA }
