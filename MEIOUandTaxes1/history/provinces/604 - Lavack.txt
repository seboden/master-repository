# 604 - Chatomukh

owner = KHM
controller = KHM
add_core = KHM
culture = khmer
religion = buddhism
capital = "Phnom Penh"
base_tax = 2
base_production = 2
base_manpower = 2
is_city = yes
trade_goods = rice
discovered_by = chinese
discovered_by = indian
hre = no

1811.1.1 = { controller = REB } # The Siamese-Cambodian Rebellion
1812.1.1 = { controller = KHM }
1867.1.1 = { 			
	owner = FRA
	controller = FRA
}
