# 542 - Madraspatnam

owner = GNG
controller = GNG
culture = tamil
religion = hinduism
capital = "Madras"
trade_goods = pepper
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes


discovered_by = indian
discovered_by = muslim

1000.1.1 = {
	
	add_permanent_province_modifier = {
		name = ideal_european_port
		duration = -1
	}
	add_permanent_province_modifier = {
		name = center_of_trade_modifier
		duration = -1
	}
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1356.1.1 = {
	owner = MAD
	controller = MAD
	add_core = GNG
}
1378.1.1 = {
	owner = VIJ
	controller = VIJ
}
1405.1.1 = { discovered_by = chinese }
1428.1.1 = { add_core = VIJ }
1443.1.1 = {
	controller = ORI
	owner = ORI
} # Conquered by Gajapatis
1462.1.1 = { controller = VIJ } # Reconquered by Vijayanagar
1464.1.1 = { owner = VIJ } # Reconquered by Vijayanagar
1505.1.1 = { discovered_by = POR }
1522.1.1  = {
	owner = POR
	controller = POR
	add_core = POR
	base_tax = 7
	base_production = 7
	fort_14th = yes
	naval_arsenal = yes
	marketplace = yes
	customs_house = yes
	armory = yes
	remove_core = VIJ
	set_province_flag = TP_trading_post
	trading_post = yes
	road_network = yes
}
1526.4.21 = { remove_core = DLH } # Battle of Panipat

1550.1.1  = { add_core = POR }
1600.1.1  = {
	discovered_by = turkishtech
	discovered_by = ENG
	discovered_by = NED
}
1612.1.1  = {
	owner = NED
	controller = NED
	remove_core = POR
} #  The region passed into the hands of the Dutch
1639.8.22 = {
	owner = ENG
	controller = ENG
} # English colony
1644.1.1  = {
	fort_14th = no
	fort_17th = yes
} # Fort St George
1650.1.1  = {  add_core = ENG discovered_by = FRA }
1707.5.12 = {
	discovered_by = GBR
	discovered_by = FRA
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
1747.1.1  = {
	controller = FRA
} # Captured by the French
1749.1.1  = {
	controller = GBR
} # Treaty of Aix-la-Chapelle
