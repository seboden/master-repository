# 237 - Oxfordshire

owner = ENG
controller = ENG
culture = english
religion = catholic
hre = no
base_tax = 5
base_production = 5
trade_goods = wheat
base_manpower = 3
is_city = yes
capital = "Oxford"
medieval_university = yes #Oxford University
set_seat_in_parliament = yes
discovered_by = western
discovered_by = muslim
discovered_by = eastern

1250.1.1   = { temple = yes }
1300.1.1   = { road_network = yes }
1356.1.1   = { add_core = ENG }
1453.1.1   = { unrest = 5 } #Start of the War of the Roses
1461.6.1   = { unrest = 2 } #Coronation of Edward IV
1467.1.1   = { unrest = 5 } #Rivalry between Edward IV & Warwick
1471.1.1   = { unrest = 8 } #Unpopularity of Warwick & War with Burgundy
1471.5.4   = { unrest = 2 } #Murder of Henry VI & Restoration of Edward IV
1483.6.26  = { unrest = 8 } #Revulsion at Supposed Murder of the Princes in the Tower
1485.8.23  = { unrest = 0 } #Battle of Bosworth Field & the End of the War of the Roses
1523.8.16  = { mill = yes }
1529.2.5   = { bailiff = yes road_network = no paved_road_network = yes }
1560.1.1   = { religion = protestant } #anglican
1600.1.1   = { fort_14th = yes }
1642.9.10  = { controller = REB }
1642.10.24 = { controller = ENG }
1707.5.12  = {
	owner = GBR
	controller = GBR
	add_core = GBR
    remove_core = ENG
}
