# 465 - Kathiri

owner = YKA
controller = YKA
culture = hadhrami
religion = sunni
capital = "Al Seiyun"
trade_goods = incense
hre = no
base_tax = 3
base_production = 3
base_manpower = 1
is_city = yes
discovered_by = ADA
discovered_by = muslim
discovered_by = indian
discovered_by = east_african


1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1 = {
	add_core = YKA
	add_core = HAD
}
1480.1.1 = { discovered_by = TUR }
1488.1.1 = { discovered_by = POR } # P�ro da Covilh�
1547.1.1 = { controller = POR } # Occupied by Portugal
1551.1.1 = { owner = HAD controller = HAD }

