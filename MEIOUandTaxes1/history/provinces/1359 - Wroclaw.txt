#1359 - Liegnetz

owner = WRO
controller = WRO
add_core = WRO
capital = "Wroclaw"
culture = silesian
religion = catholic
base_tax = 9
base_production = 9
base_manpower = 4
is_city = yes
trade_goods = cloth
discovered_by = western
discovered_by = eastern
discovered_by = muslim
hre = yes

1000.1.1 = {
	add_permanent_province_modifier = {
		name = minor_inland_center_of_trade_modifier
		duration = -1
	}
}
1100.1.1 = { marketplace = yes }
#1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1311.1.1 = {
	owner = BOH
	controller = BOH
	add_core = BOH
	add_permanent_province_modifier = {
		name = bohemian_estates
		duration = -1
	}
}
1356.1.1 = {
	add_permanent_province_modifier = {
		name = "duchy_of_wroclaw"
		duration = -1
	}
}
1419.8.16 = {
#	owner = HUN
#	controller = HUN
	add_core = HUN
}
1437.12.9 = {
#	owner = BOH
#	controller = BOH
	remove_core = HUN
}
#1500.1.1 = { road_network = yes }
1523.1.1 = { religion = reformed  }
1526.8.30 = {
	owner = HAB
	controller = HAB
	add_core = HAB
} #Battle of Mohac where Lajos II dies -> Habsburg succession
1530.1.3 = {
	road_network = no paved_road_network = yes 
}


1618.1.1 = { unrest = 5 }
1619.3.1  = {
	revolt = { }
#	owner = PAL
#	controller = PAL
	add_core = PAL
}
1620.11.8 = {
#	owner = HAB
#	controller = HAB
	remove_core = PAL
}# Tilly beats the Winterking at White Mountain. Deus Vult!
1648.1.1 = { unrest = 0 }
1653.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
}
1694.1.1 = { unrest = 4 }
1702.1.1 = { unrest = 0 }
1742.1.1 = { owner = PRU controller = PRU add_core = PRU } # Peace of Breslau after the first Silesian War
1750.1.1 = {  }
1763.1.1 = { remove_core = HAB } # End of the third Silesian War
1806.7.12 = { hre = no } # The Holy Roman Empire is dissolved
