# No previous file for Betokuag

culture = beothuk
religion = totemism
capital = "Betokuag"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 2
native_hostileness = 5

1536.1.1  = {
	discovered_by = FRA
	owner = FRA
	controller = FRA
	citysize = 100
	trade_goods = fish
	add_core = FRA
} # Jacques Cartier finds French and Basque fisheries
1670.1.1  = {
	is_city = yes
}
1689.1.1   = {
	} # Pointe aux Canons
1713.4.11 = {
	owner = GBR
	controller = GBR
	add_core = GBR
} # Treaty of Utrecht
1763.2.10  = {
	owner = FRA
	controller = FRA
} # Treaty of Paris, islands returned to France to meet provisions of the Treaty of Utrecht
1794.1.1   = { controller = GBR }
1797.10.17 = { controller = FRA } # End of the First Coalition
1797.10.18 = { controller = GBR }
1802.10.1  = { controller = FRA } # End of the Second Coalition
1803.5.18  = { controller = GBR }
# 1805.12.26 = { controller = FRA } # End of the Third Coalition
# 1805.12.26 = { controller = GBR }
1807.10.24 = { controller = FRA } # End of the Fourth Coalition
1809.4.10  = { controller = GBR }
1809.10.14 = { controller = FRA } # End of the Fifth Coalition
1812.6.21  = { controller = GBR }
1814.4.11 = {
	controller = FRA
} # Treaty of Fontainebleu, Napoleon abdicates unconditionally

