# Province: Baltistan
# file name: 2116 - Gilgit
#MEIOU-FB India 1337+ mod Aug 08
# MEIOU-GG - Turko-Mongol mod

owner = KSH
controller = KSH
add_core = KSH
culture = balti
religion = shiite
capital = "Skardu"
trade_goods = wool
hre = no
base_tax = 2
base_production = 2
#base_manpower = 0.5
base_manpower = 1.0
#native_size = 30
#native_ferocity = 4.5
#native_hostileness = 9
#citysize = 7572
##fort_14th = yes
is_city = yes

discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = skardu_state
		duration = -1
	}
}
1356.1.1  = {
	#add_core = LDK
}
1834.1.1  = {
	owner = PUN
	controller = PUN
}
1849.3.30 = {
	owner = GBR
	controller = GBR
} # End of the Second Anglo-Sikh War
