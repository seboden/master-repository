# 298 - Briansk

owner = BRK
controller = BRK 
culture = russian
religion = orthodox
hre = no
base_tax = 4
base_production = 4
trade_goods = wheat
base_manpower = 2
is_city = yes
capital = "Briansk"
discovered_by = western
discovered_by = eastern
discovered_by = steppestech
discovered_by = muslim

#1133.1.1 = { mill = yes }
1356.1.1  = {
	add_permanent_claim = LIT
	add_core = BRK  
}
1399.1.1 = {
	owner = LIT
	controller = LIT
	add_core = LIT
} # Reverts to Lithuania
1503.3.21  = { owner = MOS controller = MOS add_core = MOS remove_core = LIT }
1523.8.16 = { mill = yes }
1530.1.4  = {
	bailiff = yes	
	farm_estate = yes
}
1547.1.1  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia

1772.8.5   = {
	culture = byelorussian
} # First partition
