#Province: Kalapa (was Daio)
#file name: 2108 - Kalapa
#MEIOU-FB Indonesia mod v3 - for IN+JV
#Note: if MEIOU ever has a start date prior to 1280 then this province should be part of the
#Srivijaya Empire.
#
#Future enhancement possibility:
#Make owner Jayakarta (new country) from c1525 to NED takeover
#(at some point making Jayakarta a vassal of BAN)

owner = SUN
controller = SUN
culture = sundanese
religion = hinduism
capital = "Kalapa"
trade_goods = pepper
hre = no
base_tax = 9
base_production = 9
base_manpower = 4
is_city = yes
discovered_by = chinese
discovered_by = indian
discovered_by = austranesian

1000.1.1 = {
	add_permanent_province_modifier = {
		name = ideal_european_port
		duration = -1
	}
}
1356.1.1  = {
	add_core = SUN
}
1512.1.1 = { discovered_by = POR }	#FB Antonio de Abreu
1515.2.1 = { training_fields = yes }
#Demak conquered Kalapa in 1524/1525 but probably never ruled it directly
#Demak set up the sultanate of BAN and may have done the same for Jayakarta,
#but for simplicity Kalapa is treated as a province of BAN.
#According to MC Ricklefs "A History of Modern Indonesia", by 1618 Jayakarta was
#a troublesome vassal state of BAN
#PJJ assumes the mantle of the last hinduism state in western Java
1527.6.22 = {
	capital = "Jayakarta"
	owner = BAN
	controller = BAN
	add_core = BAN
	religion = sunni
	remove_core = SUN
}  # Banten became an independent sultanate
1599.1.1  = { discovered_by = NED } # The Dutch arrived
1619.5.30 = {
	owner = NED
	controller = NED
	capital = "Batavia"
} # Conquered by the Dutch
1644.1.1  = { add_core = NED fort_14th = yes base_tax = 4
base_production = 4 }
1650.1.1  = {  trade_goods = coffee }
1700.1.1  = {
	culture = batavian
}
1740.10.1 = { unrest = 5 } #FB Chinese revolt following anti-Chinese riots in Batavia
1741.7.1 = { unrest = 1 } #FB Mataram joins forces with Chinese - revolt becomes war
1811.9.1 = {
	owner = GBR
	controller = GBR
} # British take over
1816.1.1 = { owner = NED controller = NED } # Returned to the Dutch
