# No previous file for Noakhali

owner = BNG
controller = BNG
culture = bengali
religion = hinduism
capital = "Noakhali"
trade_goods = rice
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = chinese
discovered_by = steppestech

1120.1.1 = { farm_estate = yes }
1133.1.1 = { mill = yes }
1200.1.1 = { road_network = yes }
1356.1.1  = {
	add_core = BNG
	#add_core = ARK
	add_core = TPR
}
1444.1.1 = {
	add_core = ARK
}
1467.1.1 = {
	owner = ARK
	controller = ARK
} # Conquered by Arakan, soon becomes haven for Portuguese pirates
1473.1.1 = {
	owner = BNG
	controller = BNG
}
1500.1.1 = {
	discovered_by = POR
}
1530.1.1 = { 
	add_permanent_claim = MUG
	bailiff = yes	
	
}
1530.1.2 = { add_core = TRT }
1538.1.1 = {
	owner = ARK
	controller = ARK
} # Fallout of the internal Bengal conflict being won by Sher Shah
1666.1.1 = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # Annexation by the Mughals
1707.3.15 = {
	owner = BNG
	controller = BNG
}
1760.1.1 = {
	owner = GBR
	controller = GBR
	remove_core = MUG
} # Given to GBR by Mir Qasim
1800.1.1 = { religion = sunni }
1810.1.1  = {
	add_core = GBR
}
