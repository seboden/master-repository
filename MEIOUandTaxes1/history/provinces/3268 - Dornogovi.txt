# No previous file for Dornogovi

owner = YUA
controller = YUA
add_core = YUA
culture = tumed
religion = tengri_pagan_reformed
capital = "Hohhot"
trade_goods = wool
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = steppestech

1392.1.1  = {
	remove_core = YUA
	owner = MNG
	controller = MNG
}
1440.1.1 = {
	owner = TMD
	controller = TMD
	add_core = TMD
	culture = tumed
} # Ongguds become an otog of Tumed
1515.1.1 = { training_fields = yes }
1586.1.1 = { religion = vajrayana } # State religion
1634.1.1 = {
	owner = MCH
	controller = MCH
} # Part of the Manchu empire
1644.6.6 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = MCH
} # The Qing Dynasty
