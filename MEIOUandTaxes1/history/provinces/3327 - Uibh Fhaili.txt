# No previous file for U�bh Fhail�

owner = MEA
controller = MEA
culture = irish
religion = catholic
hre = no
base_tax = 2
base_production = 2
trade_goods = wheat
is_city = yes
base_manpower = 2
capital = "Tulach Mh�r" # Tullamore
discovered_by = western
discovered_by = muslim
discovered_by = eastern

700.1.1 = {
	add_permanent_province_modifier = { name = clan_land duration = -1 }
}
1356.1.1  = {
	add_core = MEA
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1537.2.3  = {
	owner = ENG
	controller = ENG
   	add_core = ENG
} # 'Silken' Thomas, last independent Earl of Kildare, executed
1642.1.1  = { controller = REB } #Estimated
1642.6.7  = { owner = IRE controller = IRE } #Confederation of Kilkenny
1650.3.28 = { controller = ENG } #Capture of Kilkenny
1652.4.1  = { owner = ENG } #End of the Irish Confederates
1689.3.12 = { controller = REB } #James II Lands in Ireland
1690.8.1  = { controller = ENG } #Estimated
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = ENG
}
