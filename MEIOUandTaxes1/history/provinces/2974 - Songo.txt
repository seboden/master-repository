# 2974 - Songo

owner = KON
controller = KON 
add_core = KON
culture = kongolese
religion = animism
capital = "Mbanza Kongo"
hre = no
base_tax = 11
base_production = 11
base_manpower = 2
is_city = yes
trade_goods = ivory
discovered_by = central_african

1483.1.1   = { discovered_by = POR } # Diogo C�o
1506.6.2 = {
	religion = catholic
}
