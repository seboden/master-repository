# No previous file for Nunavik

culture = inuit
religion = animism
capital = "Nunavik"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 5
native_ferocity = 2
native_hostileness = 5

1498.1.1 = { discovered_by = POR } # Jo�o Fernandes Lavrador
1576.7.28 = { discovered_by = ENG } # Martin Frobisher
1710.1.1 = {
	owner = FRA
	controller = FRA
	culture = francien
	religion = catholic
	citysize = 100
} # Fort La Forteau
1738.7.13 = { add_core = FRA }
1750.1.1 = { citysize = 680 trade_goods = fish }
1763.2.10 = {
	owner = GBR
	controller = GBR
	remove_core = FRA
	    	culture = french_colonial
} # Treaty of Paris
1788.2.10 = { add_core = GBR }
1800.1.1 = { citysize = 1450 }
