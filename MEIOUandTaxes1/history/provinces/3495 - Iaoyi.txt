# No previous file for Iaoyi

culture = cariban
religion = pantheism
capital = "Iaoyi"
trade_goods = unknown
hre = no
native_size = 40
native_ferocity = 2
native_hostileness = 8

1750.1.1   = {
	owner = NED
	controller = NED
	religion = reformed
	culture = dutch
	trade_goods = sugar
	citysize = 750
	change_province_name = "Suriname"
	rename_capital = "Brokopondo"
} # First Dutch colonies are established
