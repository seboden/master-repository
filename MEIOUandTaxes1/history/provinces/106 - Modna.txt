# 106 - Modena

owner = FER
controller = FER
culture = emilian 
religion = catholic 
hre = yes 
base_tax = 7
base_production = 7        
trade_goods = wax    
base_manpower = 3
is_city = yes
fort_14th = yes 
capital = "M�dna" 
add_core = FER
add_core = MOD
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

#1111.1.1 = { post_system = yes }
1250.1.1 = { temple = yes }
1300.1.1 = { road_network = yes }
1356.1.1   = {
	add_permanent_province_modifier = {
		name = "county_of_mirandola"
		duration = -1
	}
	bailiff = yes
}
1530.1.1 = {
	owner = MOD 
	controller = MOD
	add_core = MOD
	remove_core = FER
}
1530.2.27 = {
	hre = no
}
1597.10.28 = {
	owner = MOD 
	controller = MOD
	add_core = MOD
	remove_core = FER
}
1618.1.1  = { hre = no }
1796.11.15 = {
	owner = ITD
	controller = ITD
	add_core = ITD
	remove_core = MOD
} # Cispadane Republic
1797.6.29  = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = ITD
} # Cisalpine Republic
1814.4.11 = { 
	owner = MOD 
	controller = MOD 
	add_core = MOD 
	remove_core = ITE
}
1859.8.20= { controller = REB }
1860.3.20 = {
	owner = SPI
	controller = SPI
	add_core = SPI
}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
	remove_core = SPI
}
