# 2929 - Itesan

owner = ZIN
controller = ZIN
culture = tuareg  
religion = sunni
capital = "Tahut"
trade_goods = wool
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
is_city = yes
discovered_by = sub_saharan

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}
1356.1.1 = {
	add_core = ZIN
}
1500.1.1  = {
	owner = SON
	controller = SON
	add_core = SON
}
1591.3.15 = {
	owner = AIR
	controller = AIR
	add_core = AIR
}
