#296 - Kaluga

owner = LIT
controller = LIT     
culture = lithuanian
religion = baltic_pagan_reformed
hre = no
base_tax = 4
base_production = 4
trade_goods = wheat
base_manpower = 2
is_city = yes
capital = "Trakai"
add_core = LIT
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = steppestech

#1133.1.1 = { mill = yes }
1356.1.1 = {
	add_permanent_province_modifier = {
		name = lithuanian_estates
		duration = -1
	}
}
1386.1.1   = {
	religion = catholic
}
1523.8.16 = { mill = yes }
1530.1.4  = {
	bailiff = yes	
	farm_estate = yes
}
1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin

1795.10.24  = {
	owner = RUS
	controller = RUS
	add_core = RUS
	culture = byelorussian
} #Third Partition
