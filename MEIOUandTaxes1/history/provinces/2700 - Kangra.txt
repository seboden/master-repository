# 2700 - Kangra

owner = DLH
controller = DLH
culture = pahari
religion = hinduism
capital = "Kangra"
trade_goods = cotton
hre = no
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1000.1.1 = {
	add_permanent_province_modifier = { #now Kangra
		name = spiti_state
		duration = -1
	}
	#add_permanent_province_modifier = {
	#	name = kangra_fort
	#	duration = -1
	#}
}
1356.1.1 = {
	add_core = DLH
	fort_14th = yes
}
1419.1.1 = {
	owner = PTA
	controller = PTA
	add_core = PTA
}
1451.4.20 = {
	controller = DLH
	owner = DLH
	remove_core = PTA
	revolt = {  }
} #Final triumph of the Lodi
1526.2.1 = { controller = TIM } # Babur's invasion
1526.4.21 = {
	owner = MUG
	controller = MUG
	add_core = MUG
	remove_core = DLH
	training_fields = yes
} # Battle of Panipat
1530.1.2 = { add_core = TRT }
1540.1.1  = {
	owner = BNG
	controller = BNG
	add_core = BNG
} # Sher Shah Conquers Delhi
1553.1.1  = {
	owner = DLH
	controller = DLH
	remove_core = BNG
} #Death of Islam Shah Sur, Suri empire split
1555.7.23 = {
	owner = MUG
	controller = MUG
} # Humayun Returns
1620.1.1 = {
	controller = MUG
} # Mughals
1621.1.1 = {
	owner = MUG
} # Mughals
1690.1.1  = { discovered_by = ENG }
1707.3.15 = {
	controller = KGR
	owner = KGR
}
1707.5.12 = { discovered_by = GBR }
1806.1.1  = {
	controller = NPL
}
1809.1.1  = {
	controller = KGR
}
