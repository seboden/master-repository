# 2915 - Barda�

culture = toubous
religion = sunni
capital = "Barda�"
native_size = 60
native_ferocity = 4.5
native_hostileness = 9
trade_goods = unknown
discovered_by = soudantech
discovered_by = sub_saharan
discovered_by = muslim
hre = no

1204.1.1 = {
	add_permanent_province_modifier = {
		name = arab_tribal_area
		duration = -1
	}
}