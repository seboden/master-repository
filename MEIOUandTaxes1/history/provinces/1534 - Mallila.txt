# 1534 - Melilla

owner = FEZ
controller = FEZ
culture = rifain
religion = sunni
capital = "Melilla"
base_tax = 3
base_production = 3
base_manpower = 1
is_city = yes
trade_goods = olive
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = no

900.1.1    = { set_province_flag = barbary_port }
1088.1.1 = { dock = yes shipyard = yes }
1356.1.1   = {
	add_core = FEZ
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
#1465.1.1 = {
#	owner = MOR
#	controller = MOR
#	add_core = MOR
#}
1497.1.1   = {
	owner = CAS
	controller = CAS
	add_core = CAS
} # Conquered by the Duke of Medina Sidonia
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = CAS
	culture = andalucian # eastern_andalucian
	religion = catholic
	rename_capital = "Melilla" 
	change_province_name = "Melilla"
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1530.1.1 = {
	add_core = MOR
}
1530.1.2 = {
	remove_core = FEZ
}
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1556.1.1   = { remove_core = MOR } # Formally annexed Melilla

1713.4.11  = { remove_core = CAS }
1780.12.25 = { add_core = MOR } # Treaty of Aranjuez, Spain ceded some territory to Morocco
