# 3381 - Phineka / Finike

owner = TKE
controller = TKE
culture = yorouk
religion = sunni
capital = "Finike"
trade_goods = naval_supplies
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
add_permanent_claim = BYZ

1356.1.1  = {
	add_core = TKE
	add_claim = HMD
	add_core = MEN
}
1426.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
	culture = turkish
}
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { controller = TUR } # Jem escapes to Rhodes
1492.1.1  = { remove_core = HMD } ###
1509.1.1  = { controller = REB } # Civil war
1513.1.1  = { controller = TUR }
1515.1.1 = { training_fields = yes }
1519.1.1 = { bailiff = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1623.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1625.1.1  = { controller = TUR } # Murad tries to quell the corruption

1690.1.1  = { base_tax = 2 } #The Decentralizing Effect of the Provincial System

