#56 - Osnabr�ck

owner = OSN
controller = OSN
culture = old_saxon
religion = catholic
capital = "Osnabr�ck"
trade_goods = salt
hre = yes
base_tax = 6
base_production = 6
base_manpower = 2
is_city = yes
add_core = OSN
discovered_by = eastern
discovered_by = western
discovered_by = muslim

#1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1200.1.1 = { road_network = yes }

1250.1.1 = { temple = yes }

1500.1.1 = { road_network = yes }
1610.1.1   = { fort_14th = yes }
1648.10.24 = { add_core = LUN } # Peace of Westphalia
1740.1.1   = {  }
1803.1.1   = {
	owner = HAN
	controller = HAN
	add_core = HAN
	remove_core = OSN
} # Taken into possession of Hannover
1803.7.5   = { controller = FRA } # Occupied by French forces
1805.12.15 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = HAN
} # Treaty of Schoenbrunn, ceded to Prussia
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1807.7.9   = {
	owner = WES
	controller = WES
	add_core = WES
	remove_core = PRU
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1810.12.13 = {
	owner = FRA
	controller = FRA
     	add_core = FRA
     	remove_core = WES
} # Annexed by France
1813.10.13 = {
	owner = HAN
	controller = HAN
	add_core = HAN
	remove_core = FRA
} # The kingdom is dissolved
1866.1.1   = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = HAN
}
