# No previous file for Mawarannahr

owner = CHG
controller = CHG
culture = uzbehk
religion = sunni
capital = "Baykand"
trade_goods = salt
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = mongol_tech
discovered_by = steppestech
discovered_by = turkishtech
discovered_by = muslim

1356.1.1   = {
	add_core = CHG
	add_core = KTD
}
1370.4.1   = {
	owner = TIM
	controller = TIM
	add_core = TIM
	remove_core = CHG
}
1444.1.1 = {
	remove_core = KTD
}
1444.1.1 = {
	add_core = SHY
}
#1446.1.1 = {
#	owner = SHY
#	controller = SHY
#	add_core = SHY
#	remove_core = GOL
#}
#1465.1.1 = {
#	owner = KZH
#	controller = KZH
#}
1502.1.1 = {
	owner = SHY
	controller = SHY
}
1511.1.1 = {
	owner = KHI 
	controller = KHI
	add_core = KHI
	remove_core = TIM
	remove_core = SHY
} # Khiva Independent
1515.1.1 = { training_fields = yes }
1723.1.1 = { owner = OIR controller = OIR } # Dzungarian invasion
1728.1.1 = { owner = BUK controller = BUK }
1740.1.1 = { owner = OIR controller = OIR } # Dzungarian invasion
1755.1.1 = { owner = BUK controller = BUK }
