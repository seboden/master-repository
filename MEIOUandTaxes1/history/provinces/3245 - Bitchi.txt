
owner = MWK
controller = MWK
culture = jurchen
religion = tengri_pagan_reformed
capital = "Furdan"
trade_goods = fish
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = chinese
discovered_by = steppestech

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
1200.1.1 = { road_network = yes }
1356.1.1 = {
	add_core = MWK
}
#1388.1.1 = {
#	owner = MHU
#	controller = MHU
#	add_core = MHU
#	remove_core = MJZ
#} #Odoli marches west, Huora splits off Hurha
1515.1.1 = { training_fields = yes }
1530.1.1 = { 
	marketplace = yes
	bailiff = yes
}
1611.1.1 = {
	owner = MCH
	controller = MCH
	add_core = MCH
	remove_core = MWK
} # The Later Jin Khanate
1616.2.17  = {
	owner = JIN
	controller = JIN
	add_core = JIN
	remove_core = MCH
}
1635.1.1 = {
	culture = manchu
}
1636.5.15 = {
	owner = QNG
	controller = QNG
	add_core = QNG
	remove_core = JIN
} # The Qing Dynasty
1858.1.1 = {
   	owner = RUS
   	controller = RUS
   	religion = orthodox
   	culture = russian
} # Treaty of Aigun
