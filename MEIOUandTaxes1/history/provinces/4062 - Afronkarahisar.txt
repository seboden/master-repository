# 4062 - Afronkarahisar

owner = GRN
controller = GRN
culture = turkish
religion = sunni
capital = "Afronkarahisar"
trade_goods = wheat
hre = no
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
discovered_by = CIR
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_core = GRN
	set_province_flag = turkish_name
}
1390.1.1  = {
	owner = TUR
	controller = TUR
	add_core = TUR
}
1402.1.1  = {
	owner = GRN
	controller = GRN
} # Tamerlane defeat the Ottomans in Angora (Ankara)
1403.1.1  = {
	owner = TUR
	controller = TUR
}
#1453.5.29 = {
#	remove_core = BYZ
#} # Fall of Constantinople
1481.6.1  = { controller = REB } # Civil war, Bayezid & Jem
1482.7.26 = { controller = TUR } # Jem escapes to Rhodes
1492.1.1  = { remove_core = GRN } ###
1509.1.1  = { controller = REB } # Civil war
1513.1.1  = { controller = TUR }
1515.1.1 = { training_fields = yes }
1526.1.1  = { controller = REB } # Anti-Ottoman uprising, second Celali uprising
1528.1.1  = { controller = TUR }
1530.1.4  = {
	bailiff = yes	
}
1595.1.1  = { controller = REB } # Celali uprising
1610.1.1  = { controller = TUR }
1622.1.1  = { controller = REB } # The empire fell into anarchy, Janissaries stormed the palace
1623.1.1  = { controller = TUR } # Murad tries to quell the corruption
1654.1.1  = { controller = REB } # Celali uprising
1655.1.1  = { controller = TUR }
1658.1.1  = { controller = REB } # Revolt of Abaza Hasan Pasha, centered on Aleppo, extending into Anatolia
1659.1.1  = { controller = TUR }
1700.1.1  = {  }
1718.1.1  = { unrest = 3 } # Lale Devri (the tulip age), not appreciated by everyone  
1720.1.1  = { unrest = 0 }
1795.1.1  = { unrest = 6 } # Reforms by Sultan Selim III, tried to replace the Janissary corps
