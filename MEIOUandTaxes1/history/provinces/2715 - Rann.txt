# 2715 - Kharan

owner = MIH
controller = MIH
culture = baluchi
religion = sunni
capital = "Kharan"
trade_goods = wool
hre = no
base_tax = 1
base_production = 1
base_manpower = 2.0
is_city = yes
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1356.1.1   = {
	add_core = MIH
	add_core = TIM
	add_core = BAL
}
1384.1.1   = {
	owner = TIM
	controller = TIM
}
1409.1.1  = {
	owner = MIH
	controller = MIH
	remove_core = TIM
} #1409 Timurid Empire of Shah Rukh 1409 split between Shah Rukh and Ulugh Beg
1500.1.1  = { discovered_by = POR }
1510.1.1  = {
	controller = SAM
}
1511.1.1  = {
	owner = SAM
}
1512.1.1  = {
	owner = PER
	controller = PER
	add_core = PER
	#religion = shiite
} # Safawids "form persia"
1666.1.1 = {
	owner = BAL
	controller = BAL
} #Kingdom of Kalat
1747.10.1 = {
	owner = DUR
	controller = DUR
	add_core = DUR
	remove_core = MUG
} # Ahmad Shah established the Durrani empire
1758.1.1 = {
	owner = BAL
	controller = BAL
} #Kingdom of Kalat
