# 2927 - Gomb�

owner = ZZZ
controller = ZZZ
culture = haussa		
religion = west_african_pagan_reformed		 
capital = "Gomb�"
base_tax = 3
base_production = 3
base_manpower = 1
is_city = yes
trade_goods = millet
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1 = {
	add_core = ZZZ
}
1807.1.1 = {
	owner = SOK
	controller = SOK
	add_core = SOK
}
1810.1.1 = {
	remove_core = ZZZ
}
