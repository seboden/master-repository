# No previous file for Rijeka

owner = FIU
controller = FIU
culture = dalmatian
religion = catholic
capital = "Rijeka"
base_tax = 2
base_production = 4
base_manpower = 1
is_city = yes
trade_goods = wine
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
hre = yes

1000.1.1   = {
	add_permanent_province_modifier = { 
		name = "natural_harbour" 
		duration = -1 
		}
}
#1111.1.1 = { post_system = yes }
1250.1.1 = { temple = yes }
1260.1.1 = { dock = yes }
1356.1.1   = { 
	add_core = FIU
	add_claim = CRO
	#add_claim = VEN
	#add_claim = HAB
}
1466.1.1   = {
	owner = STY
	controller = STY
	add_core = STY
	remove_claim = CRO
	bailiff = yes
} # bought by Friedrich III von Habsburg
1490.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = STY
}
1500.1.1 = { road_network = yes }
1530.1.2 = {
	road_network = no paved_road_network = yes 
}
# 1719 - declared a free port

1805.1.1 = { controller = FRA }
1805.12.26 = {
	controller = HAB
} # Treaty of Pressburg
1809.8.1   = { controller = FRA }
1809.10.14 = {
	owner = FRA
	add_core = FRA
}
1813.9.20 = {
	controller = HAB
} # Occupied by Austrian forces
1814.4.6  = {
	owner = HAB
	remove_core = FRA
} # Napoleon abdicates
