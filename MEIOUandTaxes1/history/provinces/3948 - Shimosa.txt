# 3948 - Shimosa

owner = CHB
controller = CHB
culture = kanto
religion = mahayana
capital = "Chiba"
trade_goods = rice
hre = no
base_tax = 2
base_production = 2
base_manpower = 7
is_city = yes
discovered_by = chinese

1133.1.1 = { mill = yes }
1356.1.1 = {
	add_core = CHB #home province
	add_core = YUK #home province
}
1538.1.1 = { #After battle of Konodai
	owner = HJO
	controller = HJO
}
1542.1.1   = { discovered_by = POR }
1590.8.4 = {
	owner = ODA
	controller = ODA
} # Toyotomi Hideyoshi takes Odawara Castle, Hojo Ujimasa commits seppuku
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
