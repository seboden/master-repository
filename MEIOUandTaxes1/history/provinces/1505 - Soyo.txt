#1505 - Soyo

owner = LOA
controller = LOA
add_core = LOA
culture = bakongo
religion = animism
capital = "Soyo"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = fish
hre = no
discovered_by = soudantech
discovered_by = sub_saharan
discovered_by = central_african

1356.1.1   = {
	add_core = LOA
}
1472.1.1 = {
	discovered_by = POR
} # Fren�o do P�, add trade post
