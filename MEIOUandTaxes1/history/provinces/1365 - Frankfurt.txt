#1365 - Frankfurt

owner = FRF
controller = FRF
culture = hessian
religion = catholic
capital = "Frankfurt"
trade_goods = linen
hre = yes
base_tax = 7
base_production = 7
base_manpower = 1
is_city = yes
fort_14th = yes
add_core = FRF
medieval_university = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1000.1.1 = {
	add_permanent_province_modifier = {
		name = inland_center_of_trade_modifier
		duration = -1
	}
}
1100.1.1 = { marketplace = yes }
#1111.1.1 = { post_system = yes }
1119.1.1 = { bailiff = yes }
1200.1.1 = { road_network = yes }
1500.1.1 = { road_network = yes }
1510.1.1 = { fort_14th = yes }
1530.1.3 = {
	road_network = no paved_road_network = yes 
}

1556.1.1 = { religion = protestant }
1710.1.1 = {  }
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1807.7.9 = {
	owner = WES
	controller = WES
	add_core = WES
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1813.10.14 = {
	owner = HES
	controller = HES
	add_core = HES
	remove_core = WES
} # Westfalia is dissolved after the Battle of Leipsig
