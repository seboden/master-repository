# 11 - H�lsingland
# MEIOU - Gigau

add_core = SWE
owner = SWE
controller = SWE
culture = swedish
religion = catholic
hre = no
base_tax = 2
base_production = 2
trade_goods = lumber
base_manpower = 3
is_city = yes
capital = "G�vle"
discovered_by = eastern
discovered_by = western

1522.2.15 = { dock = yes shipyard = yes }
1527.6.1 = { religion = protestant}
1529.10.7 = { bailiff = yes }
1529.12.17 = { merchant_guild = yes }
 #V�sterbottens regemente
1623.1.1 = { fort_14th = yes }
1650.1.1 = { trade_goods = lumber } #Estimated Date
1704.1.1 = { fort_14th = yes }
1809.3.24 = { controller = RUS } # Conquered by Barclay de Tolly
1809.9.17 = { controller = SWE } # Treaty of Fredrikshamn
