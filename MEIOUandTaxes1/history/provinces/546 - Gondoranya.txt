# 546 - Gondoranya

owner = TLG
controller = TLG
culture = telegu
religion = hinduism
capital = "Warangal"
trade_goods = copper
hre = no
base_tax = 7
base_production = 7
base_manpower = 3
is_city = yes
fort_14th = yes
discovered_by = indian
discovered_by = muslim

1100.1.1 = { marketplace = yes }
1115.1.1 = { bailiff = yes }
1250.1.1 = { temple = yes }
1356.1.1 = {
	#add_core = GOC
	add_core = TLG
}
1443.1.1 = {
	controller = BAH
	owner = BAH
	add_core = BAH
	add_core = GOC
} # Conquered by Gajapatis
1470.1.1 = {
	owner = BAH
	controller = BAH
} # Result of lengthy war
1498.1.1 = { discovered_by = POR }
1515.12.17 = { training_fields = yes }
1518.1.1  = {
	owner = GOC
	controller = GOC
	add_core = GOC
	remove_core = BAH
	weapons = yes
} # The Breakup of the Bahmani sultanate
1530.3.17 = {
	bailiff = yes
	marketplace = yes
}
1686.1.1 = {
	controller = MUG
}
1687.1.1 = {
	owner = MUG
}
1712.1.1 = { add_core = HYD }	#Viceroyalty of the Deccan
1724.1.1 = {
	owner = HYD
	controller = HYD
} # Asif Jah declared himself Nizam-al-Mulk
