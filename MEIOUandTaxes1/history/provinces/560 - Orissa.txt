# 560 - Orissa

owner = ORI
controller = ORI
culture = oriya
religion = hinduism
capital = "Katak"		# Cuttack
trade_goods = livestock
hre = no
base_tax = 10
base_production = 10
base_manpower = 4
is_city = yes

discovered_by = indian
discovered_by = muslim
discovered_by = steppestech

1000.1.1 = {
	add_permanent_province_modifier = {
		name = baramba_state
		duration = -1
	}
}
1115.1.1 = { bailiff = yes }
1120.1.1 = { farm_estate = yes }
1200.1.1 = { marketplace = yes }
1250.1.1 = { temple = yes }
1356.1.1  = {
	add_core = ORI
	fort_15th = yes
}
#1361 Feroz of Delhi goes elephant hunting in Orissa
1505.1.1  = { discovered_by = steppestech }
1526.4.21 = { remove_core = DLH } # Battle of Panipat
1530.1.1 = { 
	add_permanent_claim = MUG
}
1550.1.1  = { fort_14th = yes }
1568.1.1 = {
	owner = BNG
	controller = BNG
	add_core = BNG
} # Annexed to Bengal
1576.1.1 = {
	owner = ORI
	controller = ORI
} # Orissa resurgent after Akbar's defeat of Bengal
1592.1.1  = {
	owner = MUG
	controller = MUG
	add_core = MUG
} # The Mughal Empire
1623.5.1 = {
	revolt = {
		type = pretender_rebels
		size = 0
		name = "Khurrams Faction"
		leader = "Shah Jahan Timurid"
	}
} #Defeated, retreats into Bengal
1624.1.1 = {
	controller = MUG
	revolt = { }
} #Khurram is defeated and is chased away by Mughal forces and rebelious Zamindars. Takes refuge in Ahmednagar and eventually surrenders on terms dictated by Nur Jahan

1657.9.14 = {
	controller = REB
	revolt = {
		type = pretender_rebels
		size = 0
		name = "Muhammed Shuja's Faction"
		leader = "Muhammed Shuja Timurid"
	}
} # Shah Jahan falls ill, his sons starts civil war
1658.12.28 = {
	controller = MUG
	revolt = { }
} # Muhammed Shuja defeated by Aurangzeb

1707.3.15 = {
	owner = BNG
	controller = BNG
}
1707.5.12 = { discovered_by = GBR }
1752.1.1 = {
	owner = BHO
	controller = BHO
	add_core = BHO
} # Marathas
1803.1.1 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = BHO
} # Treaty of Deogaon
