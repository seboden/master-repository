# No previous file for Burundi

owner = BUU
controller = BUU
add_core = BUU
culture = rwandan
religion = animism
capital = "Nkoma"
trade_goods = fish
hre = no
base_tax = 3
base_production = 3
base_manpower = 3
is_city = yes
discovered_by = central_african
