# 2557 - Jarvi-Suomi

owner = AHM
controller = AHM
culture = avadhi
religion = hinduism
capital = "Prayaga" #Allahabad
trade_goods = cotton
hre = no
base_tax = 12
base_production = 12
#base_manpower = 4.0
base_manpower = 8.0
citysize = 25000

discovered_by = indian
discovered_by = muslim
discovered_by = steppestech 
discovered_by = turkishtech

1115.1.1 = { bailiff = yes }
1120.1.1 = { plantations = yes }
#1180.1.1 = { post_system = yes }
1200.1.1 = { road_network = yes }
1356.1.1 = {
	#add_core = DLH
	add_core = AHM
	unrest = 6
}
1399.1.1 = {
	owner = AHM
	controller = AHM
	unrest = 0
} #guessed date for independance from Delhi Sultanate
1444.1.1 = {
	add_core = DLH
}
1483.1.1  = {
	owner = DLH
	controller = DLH
	add_core = DLH
}
1486.1.1  = {
	remove_core = AHM
} # Bahlul Lodi places his eldest surviving son Barbak Shah Lodi on the throne of Jaunpur
1500.1.1  = { citysize = 32000 }
1515.12.17 = { training_fields = yes }
1526.4.1  = {
	controller = TIM
} #Conquered by Babur
1526.4.21 = {
	owner = MUG
	controller = MUG
	#add_core = MUG
	remove_core = DLH
} # Battle of Panipat
1530.1.1   = {
	owner = MUG
	controller = MUG 
	add_core = MUG
} #Lodi Pretender
1530.1.2 = { add_core = TRT }
1531.1.1  = {
	revolt = { }
	controller = MUG
} #Lodi pretender beaten
1540.1.1 = {
	owner = BNG
	controller = BNG
	add_core = BNG
} # Sher Shah Conquers Delhi
1550.1.1  = { citysize = 35000 }
1553.1.1  = {
	owner = AHM
	controller = AHM
	remove_core = BNG
} #Death of Islam Shah Sur, Suri empire split
1558.11.1 = {
	controller = MUG
}	#Ibrahim defeated
1558.12.1 = {
	owner = MUG
}	#Jaunpur annexed
1565.1.1  = {
	controller = REB
	revolt = { type = noble_rebels }
}	#Revolt of Uzbek commanders
1566.6.1  = {
	controller = MUG
	revolt = { }
}
1575.1.1 = { capital = "Allahabad" }
1583.1.1  = { fort_14th = yes } # Allahabad fort
1600.1.1  = { citysize = 40000 }
1602.1.1  = { unrest = 4 } # Salim revolted against Akbar
1603.1.1  = { unrest = 0 }
1618.12.1 = {
	add_core = MUG
}
1650.1.1  = { citysize = 58000 }
1690.1.1  = {
	discovered_by = ENG
}
1700.1.1  = { citysize = 55000 }
1707.5.12 = {
	discovered_by = GBR
}
1724.1.1  = {
	owner = ODH
	controller = ODH
	add_core = ODH
	remove_core = MUG
} # Foundation of the Oudh dynasty
1750.1.1  = { citysize = 17000 }
1758.1.1  = {
	owner = GWA
	controller = GWA
} # Marathas
1794.1.1  = {
	owner = GBR
	controller = GBR
	remove_core = ODH
}
1800.1.1  = { citysize = 16000 }
