# 2237 - Cetinje

owner = SER
controller = SER
culture = serbian
religion = orthodox
capital = "Cetinje"
trade_goods = wool
hre = no
base_tax = 6
base_production = 6
base_manpower = 3
is_city = yes
add_core = SER
add_core = MON
discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1300.1.1 = { road_network = yes }
1353.1.1   = {
	owner = MON
	controller = MON
	remove_core = SER
	add_permanent_claim = SER
}
1421.1.1   = {
	owner = SER
	controller = SER
	add_core = SER
} # Inherited by despot Stefan
1444.1.1 = {
	add_core = TUR
}
1451.1.1   = {
	owner = MON
	controller = MON
	remove_core = SER
}
1478.1.1   = {
	add_core = TUR
} # Conquered Zabljak, but never fully conquered Zeta
1499.1.1   = {
	owner = TUR
	controller = TUR
}
1515.1.1   = {
	owner = MON
	controller = MON
	remove_core = TUR
} # Prince-bishops of Cetinje
1530.1.3 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1540.1.1   = {
	
}
1600.1.1   = {
	fort_14th = yes
}
# 1684.1.1 - End of the Sanjak of Montenegro
1709.1.1   = {
	unrest = 5
	
} # Montenegro's Muslim converts were massacred at Vladika Danilo's order
1712.1.1   = {
	unrest = 2
} # Cetinje sacked again as a response
1750.1.1   = {
	
}
