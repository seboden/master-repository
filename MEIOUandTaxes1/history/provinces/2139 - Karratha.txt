# 2139 - Karratha

culture = aboriginal
religion = polynesian_religion
trade_goods = unknown #grain
capital = "Karratha"
hre = no
native_size = 10
native_ferocity = 0.5
native_hostileness = 1

1620.1.1 = {
	discovered_by = NED
} # Dutch navigator Willem Janszoon
