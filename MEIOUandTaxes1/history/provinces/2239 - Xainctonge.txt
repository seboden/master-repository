# 2239 - Saintonghe cognac royan
# MEIOU-GG
# Anarchovampire - corrections

owner = FRA
controller = FRA
culture = poitevin
religion = catholic
capital = "Saintes"
base_tax = 4
base_production = 4
base_manpower = 3
is_city = yes
trade_goods = salt
fort_14th = yes 
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = no

1258.1.1   = {
	owner = GUY
	controller = GUY
	add_core = FRA
	#add_core = ENG
	add_core = GUY
}
1300.1.1 = { road_network = yes }
1372.8.23  = {
	owner = FRA
	controller = FRA
	unrest = 0
} # Conquered back by Bertrand du Guesclin
1422.10.21 = {
	owner = DAU
	controller = DAU
	add_core = DAU
	remove_core = FRA
}
1429.7.17  = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = DAU
}
1475.8.29  = {
	remove_core = GUY
} # Treaty of Picquigny, ending the Hundred Year War
1530.1.2 = {
	road_network = no paved_road_network = yes 
	bailiff = yes
}

1535.1.1   = { fort_14th = yes }
1560.1.1   = { religion = reformed }
1565.1.1   = { unrest = 8 } # France is restless once again as ultra-catholic intentions become clear
1568.9.1   = { unrest = 15 } # Catherine de Medici and Charles IX side with the Guise faction, religious intolerance peaks
1570.8.8   = { unrest = 10 } # Edict of Saint-Germain: temporary pacification
1573.9.1   = { unrest = 15 } # Saint Barthelew's Day Massacre: the consequences in the land
1574.5.1   = { unrest = 7 } # Charles IX dies, situation cools a bit   
1584.1.1   = { unrest = 12 } # Situation heats up again
1588.12.1  = { unrest = 15 } # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1   = { unrest = 10 } # 'Paris vaut bien une messe!', Henri converts to Catholicism
1598.4.13  = { unrest = 3 } # Edict of Nantes, alot more freedom to the protestants
1598.5.2   = { unrest = 0 } # Peace of Vervins, formal end to the Wars of Religion
 # Henri IV's quest to eliminate corruption and establish state control
1627.8.1   = { revolt = { type = religious_rebels size = 0 } controller = REB } # Huguenot rebels take the city of La Rochelle
1628.10.10 = { revolt = { } controller = FRA } # France wastes La Rochelle

1640.1.1   = { fort_14th = no fort_15th = yes }
1650.1.14  = { unrest = 7 } # Mazarin arrests the Princes Cond�, Conti & Longueville, the beginning of the Second Fronde
1651.4.1   = { unrest = 4 } # An unstable peace is concluded
1651.12.1  = { unrest = 7 } # Mazarin returns from exile, Cond� sides with Spain, situation heats up again
1652.2.15  = { revolt = { type = noble_rebels size = 0 } controller = REB } # Fronde rebels make a foothold in the Guyenne area, under Cond�
1652.10.21 = { revolt = { } controller = FRA unrest = 0 } # The King is allowed to enter Paris again, Mazarin leaves France for good. Second Fronde over.
1670.1.1   = { fort_15th = no fort_16th = yes }
1680.1.1   = { fort_16th = no fort_17th = yes } # Vauban's 'pointed' forts in La Rochelle and on the Isle-de-R�
1685.10.18 = { unrest = 8 } # Edict of Nantes revoked by Louis XIV
1686.1.17  = { religion = catholic } # Dragonnard campaign succesful: region reverts back to catholicism
1689.1.1   = { unrest = 0 } # War of the Grand Alliance erupts: Louis XIV can't persue his religious policy anymore


