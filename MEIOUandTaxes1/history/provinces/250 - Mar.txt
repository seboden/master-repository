# 250 - Fife

owner = SCO
controller = SCO
culture = lowland_scottish #?
religion = catholic
hre = no
base_tax = 5
base_production = 5
trade_goods = wheat
base_manpower = 1
is_city = yes
capital = "Aiberdeen"
add_core = SCO
discovered_by = western
discovered_by = muslim
discovered_by = eastern
#1133.1.1 = { mill = yes }
1250.1.1 = { 
	temple = yes 
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1495.1.1 = { medieval_university = yes }
1523.8.16 = { mill = yes }
1530.1.1 = { bailiff = yes }
1560.8.1  = { religion = reformed }
#1600.1.1  = {  fort_14th = yes } #marketplace Estimated
1644.9.1  = { controller = REB }
1645.9.13 = { controller = SCO } #Battle of Philiphaugh
1651.8.2  = { controller = ENG }
1652.4.21 = { controller = SCO } #Union of Scotland and the COmmonwealth
1707.5.12 = {
	owner = GBR
	controller = GBR
	add_core = GBR
}
