#2351 - Chiletico

culture = huarpe
religion = pantheism
capital = "Chiletico"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 10
native_ferocity = 1
native_hostileness = 5
discovered_by = south_american

1356.1.1 = {
}
1525.1.1   = {
	owner = INC
	controller = INC
	add_core = INC
	is_city = yes
	paved_road_network = yes
}
1529.1.1 = {
	owner = CZC
	controller = CZC
	add_core = QUI
	add_core = CZC
	remove_core = INC
	bailiff = yes
	constable = yes
	marketplace = yes
}
1535.1.1  = {
	discovered_by = SPA
	add_core = SPA
	owner = SPA
	controller = SPA
	religion = catholic
	culture = castillian
	citysize = 2500
	unrest = 8
}
1558.1.1   = {
	capital = "San Felipe"
}
1650.1.1   = {
	citysize = 3500
}
1700.1.1   = {
	 citysize = 5000
}
1750.1.1   = {
	unrest = 2
	citysize = 5000
   	add_core = LAP
	culture = platean
} # Spanish administration required all trade to pass via Lima, extracted taxes
1780.1.1   = {
	unrest = 4
} # The desire for independence grew
1790.1.1   = {
	unrest = 6
}
1800.1.1   = {
	citysize = 10000
}
1810.5.25  = {
	owner = LAP
	controller = LAP
	unrest = 0
}
1816.7.9   = {
	remove_core = SPA
}
