# 452 - Kandahar

owner = CHG
controller = CHG
culture = pashtun			#FB was: tajihk
religion = sunni
capital = "Kandahar"
trade_goods = cloth
hre = no
base_tax = 8
base_production = 8
base_manpower = 2
is_city = yes
add_core = MIH
discovered_by = indian
discovered_by = muslim
discovered_by = steppestech
discovered_by = turkishtech

1100.1.1 = { marketplace = yes }
#1111.1.1 = { post_system = yes }
1115.1.1 = { bailiff = yes }
1356.1.1  = {
	add_core = CHG
	add_core = KTD
	add_core = KAB
	add_core = KHO
	fort_14th = yes
	add_permanent_province_modifier = {
		name = pashtun_tribal_area
		duration = -1
	}
}
1370.4.1   = {
	owner = TIM
	controller = TIM
	add_core = TIM
	remove_core = CHG
}
1444.1.1  = {
	owner = KTD
	controller = KTD
	remove_core = TIM
	add_core = KAB
} # Shaybanids break free from the Timurids
1461.1.1 = {
	owner = TIM
	controller = TIM
}
1469.8.27 = {
	owner = KAB
	controller = KAB
}
1507.7.1 = {
	owner = TIM
	controller = TIM
	add_core = TIM
	remove_core = KTD
} # Kabul falls to Babur
1515.1.1 = { training_fields = yes }
1526.4.21 = {
	owner = MUG
	controller = MUG
	add_core = MUG
	remove_core = TIM
} #Battle of Panipat
1529.12.11 = { add_core = DUR remove_core = KAB }
1537.1.1 = {
	owner = PER
	controller = PER
} # Persia
1545.1.1 = {
	owner = MUG
	controller = MUG
} # Back to Mughals
1566.6.1 = { revolt = { }
	owner = KAB
	controller = KAB
}	#Independent of Mughals for a long while
1585.1.1 = {
	controller = MUG
}	# Man Singh occupies Kabulistan after death of Mirza Hakim
1585.2.1 = {
	owner = MUG
} # Annexed into empire again
1618.1.1  = {
	controller = PER
} # Persia again
1622.1.1  = {
	owner = PER
} # Persia again
1637.1.1  = {
	controller = MUG
} # Back to Mughals
1638.1.1  = {
	owner = MUG
} # Back to Mughals
1647.1.1  = {
	controller = PER
} # Back to Persia
1648.1.1  = {
	owner = PER
} # Back to Persia
1677.1.1  = { discovered_by = FRA }
1690.1.1  = { discovered_by = ENG }
1707.5.12 = { discovered_by = GBR }
1708.1.1  = { controller = REB unrest = 8 } # Mir Wais rebelled against Persian rule
1709.1.1  = {
	controller = REB
	revolt = { type = nationalist_rebels size = 3 }
} # Widespread tribal uprisings
1711.1.1  = {
	owner = KAB
	controller = KAB
	revolt = { }
}
1738.1.1  = {
	owner = PER
	controller = PER
} # Nader Shah absorbs Afghanistan
1747.10.1 = {
	owner = DUR
	controller = DUR
	add_core = DUR
	remove_core = MUG
} # Ahmad Shah established the Durrani empire
