# 3023 - Serae

owner = MED
controller = MED
culture = tigrean 
religion = coptic
capital = "Debarwa"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = millet
hre = no
discovered_by = ALW
discovered_by = MKU
discovered_by = MED
discovered_by = ADA
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech
discovered_by = indian
discovered_by = east_african

1356.1.1 = {
	add_core = MED
}
#1486.1.1 = { unrest = 5 add_core = ADA } #Raids by Mahfuz Of Zayla
1495.1.1 = { unrest = 5 } #Raids by Mahfuz Of Zayla
1499.1.1 = { unrest = 5 } #Raids by Mahfuz Of Zayla
1510.1.1 = { unrest = 9 } #Raids by Mahfuz Of Zayla
1515.2.1 = { training_fields = yes }
1588.1.1 = { unrest = 7 } # Raids by Sarsa Dengel
1589.1.1 = { unrest = 0 }
