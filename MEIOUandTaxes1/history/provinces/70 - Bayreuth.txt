# 70 - Bayreuth

owner = NUS
controller = NUS
culture = eastfranconian
religion = catholic
base_tax = 3
base_production = 3
trade_goods = wheat
base_manpower = 1
is_city = yes
capital = "Bayreuth" 
hre = yes
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1119.1.1 = { bailiff = yes }
1133.1.1 = { mill = yes }
1356.1.1   = {
	add_core = NUS
	add_core = BYR
}
1398.1.1   = {
	rename_capital = "Kulmbach" 
	change_province_name = "Kulmbach"
	owner = BYR
	controller = BYR
	remove_core = NUS
}
1500.1.1 = { road_network = yes }
1528.1.1   = { religion = protestant }
1604.1.1   = {
	rename_capital = "Bayreuth" 
	change_province_name = "Bayreuth"
}
1791.1.18  = {
	owner = PRU
	controller = PRU
	add_core = PRU
}
1806.7.12  = {
	hre = no
}
1806.10.14 = {
	controller = FRA
} # Battle of Iena
1807.7.7   = {
	add_core = FRA
	owner = FRA
} # Treaty of Tislit
1810.6.30  = {
	owner = BAV
	controller = BAV
	add_core = BAV
	remove_core = PRU
	remove_core = FRA
} # Bought for 15 million francs
