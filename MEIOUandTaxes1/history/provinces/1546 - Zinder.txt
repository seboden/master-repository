# 1546 - Daura

owner = GOB
controller = GOB
culture = haussa		
religion = sunni		 
capital = "Daura"
base_tax = 4
base_production = 4
base_manpower = 2
is_city = yes
trade_goods = cotton
discovered_by = soudantech
discovered_by = sub_saharan
hre = no

1356.1.1 = {
	add_core = GOB
}
1806.1.1 = {
	owner = SOK
	controller = SOK
	add_core = SOK
}
1810.1.1   = {
	remove_core = GOB
}
