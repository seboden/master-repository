# No previous file for Tequesta

culture = calusa
religion = totemism
capital = "Tequesta"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1
native_size = 80
native_ferocity = 5
native_hostileness = 8

1716.1.1   = {
	culture = seminole
} # Pushed by the Yamasee Wars, some Muskogeans migrate, driving out the Calusas.
