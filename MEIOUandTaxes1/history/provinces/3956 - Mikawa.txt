# 3956 - Mikawa

owner = NIK
controller = NIK
culture = chubu
religion = mahayana
capital = "Yoshida"
trade_goods = fish
hre = no
base_tax = 4
base_production = 4
base_manpower = 6
is_city = yes
discovered_by = chinese

1356.1.1 = {
	add_core = NIK
	add_core = TGW #home province
	add_core = ISK #home province
}
1379.1.1 = {
	add_core = ISK
	owner = ISK
	controller = ISK
}
1440.1.1 = {
	add_core = HKW
	owner = HKW
	controller = HKW
}
1479.1.1 = {
	owner = TGW
	controller = TGW
}
1542.1.1 = { discovered_by = POR }
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
