# 206 - Santiago

owner = CAS		#Juan II of Castille
controller = CAS
culture = galician
religion = catholic
hre = no
base_tax = 6
base_production = 6
trade_goods = fish
is_city = yes # citysize = 1500
fort_14th = yes
base_manpower = 4
capital = "Santiago de Compostela" 
 # Santiago de Compostela pilgrimage
estate = estate_church
discovered_by = eastern
discovered_by = western
discovered_by = muslim


1100.1.1 = { marketplace = yes }
1250.1.1 = { temple = yes }
#1300.1.1 = { road_network = yes }
1356.1.1   = {
	owner = ENR	
	controller = ENR
	add_core = ENR
	add_core = CAS
	add_core = GAL
	add_permanent_province_modifier = {
		name = "kingdom_of_leon"
		duration = -1
	}
}
1369.3.23  = { 
	remove_core = ENR
	owner = CAS
	controller = CAS
}
1467.1.1   = { unrest = 4 } # Second war of the "irmandiņos"
1470.1.1   = { unrest = 0 } # End of the Second war of the "irmandiņos"
1475.6.2   = { controller = POR }
1476.3.2   = { controller = CAS }
1516.1.23  = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = CAS
	road_network = no paved_road_network = yes 
	bailiff = yes
	remove_core = GAL
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castilla


1713.4.11  = { remove_core = GAL }
1808.6.6   = { controller = REB }
1809.1.1   = { controller = SPA }
1812.10.1  = { controller = REB }
1813.12.11 = { controller = SPA }
