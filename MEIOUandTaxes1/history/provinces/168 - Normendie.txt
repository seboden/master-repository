# 168 - Normandie (Principal cities: Caen)

owner = FRA
controller = FRA 
culture = normand
religion = catholic
capital = "Caen"
base_tax = 9
base_production = 9
base_manpower = 4
is_city = yes
trade_goods = cheese
fort_14th = yes # A way to represent the important Mont Saint-Michel & the Palais Ducal
shipyard = yes # Clos aux gal�es
discovered_by = eastern
discovered_by = western
discovered_by = muslim
hre = no


1088.1.1 = { dock = yes }
#1111.1.1 = { post_system = yes }
#1119.1.1 = { bailiff = yes }
1250.1.1 = { temple = yes }
1343.1.1 = {
	add_core = NAV
	add_core = FRA
	add_core = ENG
}
1378.4.20  = {
	owner = FRA
	controller = FRA
	remove_core = NAV
} # Public trial of Jacques de Rue for regicide and treason. Charles V moves against Charles le Mauvais
# Edward III captures Caen, but the city refuses english rule
1419.1.19  = {
	owner = ENG
	controller = ENG
}
1429.7.17  = {
	owner = ENG
	controller = ENG
}
1432.1.1   = {
	medieval_university = yes
} # L'Universit� de Caen
1434.12.1  = {
	owner = FRA
	controller = FRA
	unrest = 0
} # Caen revolts back to France
1475.8.29  = {
	remove_core = ENG
} # Treaty of Picquigny, ending the Hundred Year War
1515.1.1 = { training_fields = yes }
1529.2.5 = { bailiff = yes }
1588.12.1  = { unrest = 5 } # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1594.1.1   = { unrest = 0 } # 'Paris vaut bien une messe!', Henri converts to Catholicism
1631.1.1   = { unrest = 3 } # Region is restless
1633.1.1   = { unrest = 0 } 
1639.1.1   = { unrest = 3 }
1641.1.1   = { unrest = 0 }
1650.1.1   = { fort_14th = no fort_15th = yes }
1760.1.1   = { fort_15th = no fort_16th = yes }
1786.1.1   = { base_tax = 5
base_production = 5 } # The Eden Agreement
