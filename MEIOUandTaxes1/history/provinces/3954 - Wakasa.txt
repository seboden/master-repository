# 3350 - Wakasa

owner = HKW
controller = HKW
culture = chubu
religion = mahayana
capital = "Obama"
trade_goods = fish
hre = no
base_tax = 7
base_production = 7
base_manpower = 2
is_city = yes # Wakasahime Shirine (Obama) and Kehi Shirine (Tsuruga). 
discovered_by = chinese

1356.1.1 = {
	add_core = SBA
	add_core = HKW
	add_core = ISK #home province
	add_core = TKD #home province
}
1361.1.1 = { #owned briefly by Ishibashi Kazuyoshi, son of Ashikaga Yoshihiro
	controller = JAP
}
1363.1.1 = {
	owner = SBA
	controller = SBA
}
1366.1.1 = {
	owner = ISK
	controller = ISK
}
1440.1.1 = { #owned by Wakasa-Takeda (separate faction?)
	owner = TKD
	controller = TKD
}
1598.1.1 = {
	owner = TGW
	controller = TGW
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
