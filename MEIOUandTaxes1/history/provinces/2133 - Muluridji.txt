# 2133 - Muluridji

culture = aboriginal
religion = polynesian_religion
trade_goods = unknown #grain
capital = "Mareeba"
hre = no
native_size = 10
native_ferocity = 0.5
native_hostileness = 1

1770.7.1 = {
	discovered_by = GBR
} # Cook's 1st voyage
