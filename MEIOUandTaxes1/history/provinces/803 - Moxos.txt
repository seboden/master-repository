# 803 - Moxos

culture = chacoan
religion = inti
capital = "Moxos"
trade_goods = unknown
hre = no
native_size = 60
native_ferocity = 1
native_hostileness = 7
discovered_by = south_american

1565.1.1  = {
	discovered_by = SPA
	owner = SPA
	controller = SPA
	capital = "Santa Ana del Yacuma"
	citysize = 1567
	culture = castillian
	religion = catholic
	trade_goods = cacao
	set_province_flag = trade_good_set
} # Founded by Padre Cipriano Barace
1700.1.1  = { citysize = 1955 }
1711.1.1  = { add_core = SPA }
1750.1.1  = { citysize = 7030
	add_core = BOL
	culture = peruvian
} # Decline as the mining began to wane
1800.1.1  = { citysize = 12798 }
1809.7.16 = {
	owner = BOL
	controller = BOL
} # Bolivian War of Independence
1825.8.6  = {
	remove_core = SPA
}
