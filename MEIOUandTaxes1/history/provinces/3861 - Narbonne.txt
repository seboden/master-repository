# 3861 - Narbona

owner = FRA
controller = FRA
capital = "Narbona"
base_tax = 5
base_production = 5
base_manpower = 3
is_city = yes
culture = occitain
religion = catholic
trade_goods = wine
hre = no
estate = estate_church
discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech


1204.6.15  = {
	owner = ARA
	controller = ARA
	add_core = ARA
}
1220.1.1   = {
	medieval_university = yes
}
1249.1.1 = {
	add_permanent_province_modifier = {
		name = medieval_castle
		duration = -1
	}
}
1276.1.1   = {
	owner = BLE
	controller = BLE
	add_core = BLE
}
1300.1.1 = { road_network = yes }
1349.1.1   = {
	owner = FRA
	controller = FRA
	remove_core = ARA
	add_claim = ARA
}
1356.1.1   = {
	add_core = FRA
	add_core = TOU
}
1422.1.1  = { remove_core = BLE }
1422.10.21 = {
	owner = DAU
	controller = DAU
	add_core = DAU
}
1429.7.17  = {
	owner = FRA
	controller = FRA
	remove_core = DAU
}
1494.1.1  = { remove_claim = ARA }
1530.1.2 = {
	road_network = no paved_road_network = yes 
}
1530.1.4  = {
	bailiff = yes	
}
1545.1.1   = { fort_14th = yes }
1560.1.1   = { religion = reformed  }
1622.1.17  = { religion = catholic  }
