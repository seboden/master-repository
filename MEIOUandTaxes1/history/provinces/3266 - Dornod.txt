# No previous file for Dornod

owner = YUA
controller = YUA
add_core = YUA
culture = chahar
religion = tengri_pagan_reformed
capital = "Bayan Tu'men"
trade_goods = wheat
hre = no
base_tax = 2
base_production = 2
base_manpower = 1
is_city = yes
discovered_by = steppestech

1392.1.1 = {
	owner = CHH
	controller = CHH
	add_core = CHH
	remove_core = YUA
}
1440.1.1 = {
	owner = KHA
	controller = KHA
	add_core = KHA
	remove_core = CHH
	culture = khalkas
}
1515.1.1 = { training_fields = yes }
1586.1.1 = { religion = vajrayana } # State religion
1688.1.1 = {
	owner = ZUN
	controller = ZUN
}
1696.1.1 = {
	owner = QNG
	controller = QNG
	add_core = QNG
} # Kangxi leads Qing army pushing Zunghars back
