# 3725 - Ravensberg

owner = RAV
controller = RAV
culture = old_saxon
religion = catholic
capital = "Bielefeld"
trade_goods = wheat
hre = yes
base_tax = 3
base_production = 3
base_manpower = 2
is_city = yes
add_core = RAV
discovered_by = eastern
discovered_by = western
discovered_by = muslim

1423.1.1   = {
	owner = JUL
	controller = JUL
	add_core = JUL
}
1500.1.1 = { road_network = yes }
1521.3.15 = {
	owner = JBC
	controller = JBC
	add_core = JBC
	remove_core = JUL
}
1529.1.1   = { religion = protestant }
1530.1.4  = {
	bailiff = yes	
}
1614.11.12 = {
	owner = BRA
	controller = BRA
	add_core = BRA
	remove_core = JUL
} # Treaty of Xanten, ending the war of the J�lich succession
1701.1.18  = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = BRA
} # Friedrich III becomes king in Prussia
1806.7.12  = { hre = no } # The Holy Roman Empire is dissolved
1807.7.9   = {
	owner = WES
	controller = WES
	add_core = WES
	remove_core = PRU
} # The Second Treaty of Tilsit
1810.12.13 = {
	owner = FRA
	controller = FRA
     	add_core = FRA
     	remove_core = WES
} # Annexed by France
1813.10.13 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = WES
} # Treaty of Paris
