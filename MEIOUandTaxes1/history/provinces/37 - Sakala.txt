# 37 - Sakala 
# Parnawskie

owner = LVO
controller = LVO
culture = estonian
religion = catholic
capital = "Pernau"
base_tax = 3
base_production = 3
base_manpower = 3
is_city = yes
trade_goods = wheat
discovered_by = western
discovered_by = eastern
discovered_by = steppestech
hre = no

#1133.1.1 = { mill = yes }
1356.1.1 ={
	add_core = LVO
	add_core = EST
}
1523.8.16 = { mill = yes }
1530.1.4  = {
	bailiff = yes	
	farm_estate = yes
}
1542.1.1   = {
	religion = protestant
} #Unknown date
1561.11.18 = {
	owner = LIT
	controller = LIT
	add_core = LIT
	remove_core = LVO
} # Wilno Pact
1569.7.1  = {
	owner = PLC
	controller = PLC
	add_core = PLC
} # Union of Lublin
1600.12.31 = {
	controller = SWE
} #2nd Polish War-Captured by Duke Karl
1603.4.5   = {
	controller = PLC
} #2nd Polish War-Captured by Chodkiewicz
1625.8.21  = {
	controller = SWE
} #2nd Polish War-Captured by Jakob de la Gardie
1629.9.16  = {
	owner = SWE
	add_core = SWE
	remove_core = POL
	remove_core = PLC
} #The Armistice of Altmark
1656.11.3  = {
	controller = RUS
} #Karl X Gustavs Russian War-Captured by Russians
1661.6.21 = {
	controller = SWE
} #The Peace of Kardis
1704.7.14  = {
	controller = RUS
} #The Great Nordic War-Captured Dorpat
1721.8.30  = {
	owner = RUS
	add_core = RUS
	remove_core = SWE
} #The Peace of Nystad
