# 1494 - Otoe

culture = osagee
religion = totemism
capital = "Otoe"
trade_goods = unknown
hre = no
base_tax = 1
base_production = 1
base_manpower = 1.0
native_size = 20
native_ferocity = 3
native_hostileness = 5

1541.1.1 = { discovered_by = SPA } # Francisco V�squez de Coronado
