country_decisions = {

	found_hall_of_worthies = {
		potential = {
			primary_culture = korean
			owns = 735
			NOT = { has_country_modifier = hall_of_worthies }
			religion = confucianism
		}
		allow = {
			OR = {
				advisor = philosopher
				advisor = natural_scientist
				advisor = theologian
				adm = 4
			}
			treasury = 50
			adm_power = 30
		}
		effect = {
			add_country_modifier = {
				name = "hall_of_worthies"
				duration = -1
			}
			add_treasury = -50
			add_adm_power = -30
		}
		ai_will_do = {
			factor = 1
		}
	}
	build_gyeongbok = {
		major = yes
		potential = {
			tag = JOS
			NOT = {
				has_country_flag = gyeongbok_build
			}
		}
		allow = {
			is_at_war = no
			is_subject = no
			treasury = 300
			adm_power = 30
			OR = {
				advisor = architect
				adm = 4
			}
		}
		effect = {
			capital_scope = {
				add_building = royal_palace
				add_base_tax = 1
			}
			add_treasury = -300
			add_prestige = 10
			set_country_flag = gyeongbok_build
		}
		ai_will_do = {
			factor = 1
		}
	}
	reform_government_gor = {
		major = yes
		potential = {
			has_country_modifier = goryeo_gov_mod
			NOT = { has_country_flag = reforming_goryeo_government }
		}
		allow = {
			is_at_war = no
			OR = {
				ADM = 5
				OR = {
					advisor = alderman
					advisor = treasurer
					advisor = statesman
				}
			}
			adm_power = 300
			stability = 1
			NOT = {
				has_country_modifier = korea_under_reform
			}
			OR = {
				has_regency = no
				check_variable = { which = "goryeo_buddhist_confucian" value = 10 }
				AND = {
					check_variable = { which = "goryeo_buddhist_confucian" value = -10 }
					NOT = { check_variable = { which = "goryeo_buddhist_confucian" value = -9 } }
				}
			}
			NOT = { has_country_modifier = korea_no_reform }
			NOT = { has_country_flag = reforming_goryeo_foreign }
			NOT = { has_country_flag = reforming_goryeo_military }
			NOT = { num_of_revolts = 1 }
		}
		effect = {
			add_country_modifier = {
				name = "korea_under_reform"
				duration = 1825
			}
			subtract_stability_1 = yes
			add_adm_power = -300
			set_country_flag = reforming_goryeo_government
			if = {
				limit = {
					NOT = { check_variable = { which = "goryeo_buddhist_confucian" value = 10 } }
					check_variable = { which = "goryeo_buddhist_confucian" value = -9 }
				}
				country_event = {
					id = fall_of_goryeo.2
				}
			}
		}
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 0
				NOT = { manpower = 4 }
			}
			modifier = {
				factor = 0
				NOT = { army_size_percentage = 0.3 }
			}
		}
		ai_importance = 400
	}
	reform_foreign_affairs_gor = {
		major = yes
		potential = {
			has_country_modifier = goryeo_dip_mod
			NOT = { has_country_flag = reforming_goryeo_foreign }
		}
		allow = {
			is_at_war = no
			OR = {
				DIP = 5
				OR = {
					advisor = diplomat
					advisor = statesman
				}
			}
			dip_power = 300
			stability = 1
			NOT = {
				has_country_modifier = korea_under_reform
			}
			OR = {
				has_regency = no
				check_variable = { which = "goryeo_buddhist_confucian" value = 10 }
				AND = {
					check_variable = { which = "goryeo_buddhist_confucian" value = -10 }
					NOT = { check_variable = { which = "goryeo_buddhist_confucian" value = -9 } }
				}
			}
			NOT = { has_country_modifier = korea_no_reform }
			NOT = { has_country_flag = reforming_goryeo_government }
			NOT = { has_country_flag = reforming_goryeo_military }
			NOT = { num_of_revolts = 1 }
		}
		effect = {
			add_country_modifier = {
				name = "korea_under_reform"
				duration = 1825
			}
			subtract_stability_1 = yes
			add_dip_power = -300
			set_country_flag = reforming_goryeo_foreign
			if = {
				limit = {
					NOT = { check_variable = { which = "goryeo_buddhist_confucian" value = 10 } }
					check_variable = { which = "goryeo_buddhist_confucian" value = -9 }
				}
				country_event = {
					id = fall_of_goryeo.3
				}
			}
		}
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 0
				NOT = { manpower = 4 }
			}
			modifier = {
				factor = 0
				NOT = { army_size_percentage = 0.3 }
			}
		}
		ai_importance = 400
	}
	reform_military_gor = {
		major = yes
		potential = {
			has_country_modifier = goryeo_mil_mod
			NOT = { has_country_flag = reforming_goryeo_military }
		}
		allow = {
			is_at_war = no
			OR = {
				MIL = 5
				OR = {
					advisor = army_reformer
					advisor = army_organiser
				}
			}
			mil_power = 300
			stability = 1
			NOT = {
				has_country_modifier = korea_under_reform
			}
			OR = {
				has_regency = no
				check_variable = { which = "goryeo_buddhist_confucian" value = 10 }
				AND = {
					check_variable = { which = "goryeo_buddhist_confucian" value = -10 }
					NOT = { check_variable = { which = "goryeo_buddhist_confucian" value = -9 } }
				}
			}
			NOT = { has_country_modifier = korea_no_reform }
			NOT = { has_country_flag = reforming_goryeo_foreign }
			NOT = { has_country_flag = reforming_goryeo_government }
			NOT = { num_of_revolts = 1 }
		}
		effect = {
			add_country_modifier = {
				name = "korea_under_reform"
				duration = 1825
			}
			subtract_stability_1 = yes
			add_mil_power = -300
			set_country_flag = reforming_goryeo_military
			if = {
				limit = {
					NOT = { check_variable = { which = "goryeo_buddhist_confucian" value = 10 } }
					check_variable = { which = "goryeo_buddhist_confucian" value = -9 }
				}
				country_event = {
					id = fall_of_goryeo.4
				}
			}
		}
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 0
				NOT = { manpower = 4 }
			}
			modifier = {
				factor = 0
				NOT = { army_size_percentage = 0.3 }
			}
		}
		ai_importance = 400
	}
	proclaim_korean_empire = {
		major = yes
		potential = {
			primary_culture = korean
			government = monarchy
			NOT = { government_rank = 6 }
			NOT = { has_country_flag = jos_china_relations }
			NOT = { chinese_imperial_gov_trigger = yes }
		}
		allow = {
			is_at_war = no
			is_subject = no
			has_regency = no
			num_of_cities = 40
			prestige = 60
		}
		effect = {
			set_government_rank = 6
			add_prestige = 10
	        increase_centralisation = yes
			every_country = {
				limit = {
					capital_scope = {
						chinese_region_trigger = yes
					}
					NOT = { is_subject_of = ROOT }
					OR = {
						NOT = { alliance_with = ROOT }
						AND = {
							alliance_with = ROOT
							OR = {
								chinese_imperial_gov_trigger = yes
								check_variable = { which = "Demesne_in_the_Mandate_of_Heaven" value = 20 }									
							}
						}
					}
				}
				add_opinion = { who = KOR modifier = kor_empire }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	centralized_government_gor = {
		potential = {
			has_country_modifier = goryeo_ref_bud_gov_mod
			NOT = { has_country_flag = goryeo_centralizing_government }
		}
		allow = {
			NOT = { check_variable = { which = "centralization_decentralization" value = -1 } }
			is_at_war = no
			OR = {
				ADM = 5
				OR = {
					advisor = alderman
					advisor = treasurer
					advisor = statesman
				}
			}
			adm_power = 300
			stability = 1
			NOT = {
				has_country_modifier = korea_under_reform
			}
			has_regency = no
			NOT = { has_country_modifier = korea_no_reform }
			NOT = { num_of_revolts = 1 }
			NOT = { has_country_flag = goryeo_centralizing_military }
		}
		effect = {
			add_country_modifier = {
				name = "korea_under_reform"
				duration = 1825
			}
			subtract_stability_1 = yes
			add_adm_power = -300
			set_country_flag = goryeo_centralizing_government
		}
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 0
				NOT = { manpower = 4 }
			}
			modifier = {
				factor = 0
				NOT = { army_size_percentage = 0.5 }
			}
		}
		ai_importance = 400
	}
	centralized_military_gor = {
		potential = {
			has_country_modifier = goryeo_ref_bud_mil_mod
			NOT = { has_country_flag = goryeo_centralizing_military }
		}
		allow = {
			NOT = { check_variable = { which = "centralization_decentralization" value = -1 } }
			is_at_war = no
			OR = {
				MIL = 5
				OR = {
					advisor = army_reformer
					advisor = army_organiser
				}
			}
			mil_power = 300
			stability = 1
			NOT = {
				has_country_modifier = korea_under_reform
			}
			has_regency = no
			NOT = { has_country_modifier = korea_no_reform }
			NOT = { num_of_revolts = 1 }
			NOT = { has_country_flag = goryeo_centralizing_military }
		}
		effect = {
			add_country_modifier = {
				name = "korea_under_reform"
				duration = 1825
			}
			subtract_stability_1 = yes
			add_mil_power = -300
			set_country_flag = goryeo_centralizing_military
		}
		ai_will_do = {
			factor = 1
			
			modifier = {
				factor = 0
				NOT = { manpower = 4 }
			}
			modifier = {
				factor = 0
				NOT = { army_size_percentage = 0.5 }
			}
		}
		ai_importance = 400
	}
	
	form_goguryeo = {	#Goguryeo changed into Balhae - Warial
		potential = {
			OR = {
				primary_culture = jurchen
				primary_culture = korean
			}
			NOT = { tag = QNG }
			NOT = { tag = MCH }
			NOT = { exists = GGR }
			NOT = { has_country_flag = goguryeo_formed }
		}
		allow = {
			num_of_owned_provinces_with = {
				region = korea_region
				is_core = ROOT
				value = 8
			}
			num_of_owned_provinces_with = {
				region = manchuria_region
				is_core = ROOT
				value = 15
			}
			is_at_war = no
			is_subject = no
		}
		effect = {
			korea_region = { limit = { owned_by = ROOT } remove_core = GGR add_core = GGR }
			korea_region = { limit = { NOT = { owned_by = ROOT } } add_claim = GGR }
			manchuria_region = { limit = { owned_by = ROOT } remove_core = GGR add_core = GGR }
			manchuria_region = { limit = { NOT = { owned_by = ROOT } } add_claim = GGR }
			add_prestige = 20
			#set_government_rank = 6
			set_capital = 730 # Pyongan
			change_tag = GGR
			add_accepted_culture = jurchen
			add_accepted_culture = korean
			increase_centralisation = yes
		}
		ai_will_do = {
			factor = 0
		}
	}
}