# Tianxia Historical Decisions
# 1. The Unified China

country_decisions = {

	the_unified_china = {
		major = yes
		potential = {
			culture_group = chinese_group
			NOT = { dynasty = "Borjigin" }
			NOT = {
				primary_culture = lussong
				has_country_flag = request_defect
			}
			any_neighbor_country = {
				has_country_flag = mandate_of_heaven_claimed
				NOT = { overlord_of = ROOT }
				culture_group = chinese_group
			}
		NOT = { is_year = 1400 }
		}
		allow = {
			any_neighbor_country = {
				has_country_flag = mandate_of_heaven_claimed
				NOT = { war_with = ROOT }
				culture_group = chinese_group
			}
			NOT = { overlord = { NOT = { culture_group = chinese_group } } }
			NOT = {
				has_ruler_modifier = unified_china
			}
		}
		effect = {
			every_neighbor_country = {
				limit = {
					has_country_flag = mandate_of_heaven_claimed
					NOT = { war_with = ROOT }
					culture_group = chinese_group
				}
				country_event = { id = "tianxia.15" }
			}
			set_country_flag = request_defect
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}

}