country_decisions = {
	curtail_estate_noble = {
		major = yes
		potential = {
			OR = {
				government = noble_republic
				has_country_flag = noble_estate_in_power
			}
		}
		allow = {
			stability = 1
			NOT = { num_of_rebel_armies = 1 }
			NOT = { num_of_rebel_controlled_provinces = 1 }			
			NOT = {
				estate_influence = {
					estate = estate_nobles
					influence = 60
				}				
			}
		}
		effect = {
			subtract_stability_3 = yes
			add_prestige = -25
			if = {
				limit = { government = noble_republic }
				change_government = despotic_monarchy
			}
			clr_country_flag = noble_estate_in_power
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	curtail_estate_church = {
		major = yes
		potential = {
			has_country_flag = church_estate_in_power
		}
		allow = {
			stability = 1
			NOT = { num_of_rebel_armies = 1 }
			NOT = { num_of_rebel_controlled_provinces = 1 }			
			NOT = {
				estate_influence = {
					estate = estate_church
					influence = 60
				}				
			}
		}
		effect = {
			subtract_stability_3 = yes
			add_piety = -0.5
			add_karma = -25
			add_patriarch_authority = -25
			add_fervor = -25
			add_church_power = -100
			clr_country_flag = church_estate_in_power
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	curtail_estate_burghers = {
		major = yes
		potential = {
			has_country_flag = burghers_estate_in_power
		}
		allow = {
			stability = 1
			NOT = { num_of_rebel_armies = 1 }
			NOT = { num_of_rebel_controlled_provinces = 1 }			
			NOT = {
				estate_influence = {
					estate = estate_burghers
					influence = 60
				}				
			}
		}
		effect = {
			subtract_stability_3 = yes
			capital_scope = {
				add_base_tax = -1
				add_base_production = -1
			}
			clr_country_flag = burghers_estate_in_power
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	curtail_estate_nomadic_tribes = {
		major = yes
		potential = {
			has_country_flag = nomadic_tribes_estate_in_power
		}
		allow = {
			stability = 1
			NOT = { num_of_rebel_armies = 1 }
			NOT = { num_of_rebel_controlled_provinces = 1 }			
			NOT = {
				estate_influence = {
					estate = estate_nomadic_tribes
					influence = 60
				}				
			}
		}
		effect = {
			subtract_stability_3 = yes
			capital_scope = {
				add_base_tax = -1
				add_base_production = -1
			}
			clr_country_flag = nomadic_tribes_estate_in_power
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	curtail_estate_dhimmi = {
		major = yes
		potential = {
			has_country_flag = dhimmi_estate_in_power
		}
		allow = {
			stability = 1
			NOT = { num_of_rebel_armies = 1 }
			NOT = { num_of_rebel_controlled_provinces = 1 }			
			NOT = {
				estate_influence = {
					estate = estate_dhimmi
					influence = 60
				}				
			}
		}
		effect = {
			subtract_stability_3 = yes
			add_piety = 0.5
			clr_country_flag = dhimmi_estate_in_power
		}
		ai_will_do = {
			factor = 1
		}
	}	
}