country_decisions = {
	
	select_policy_vassals = {
		potential = {
			num_of_vassals = 1
			OR = {
				ai = no
				NOT = { has_ruler_modifier = policy_vassal }
			}
		}
		allow = {
		}  
		effect = {
			country_event = { id = subject_integration.1000 }
			hidden_effect = {
				if = {
					limit = {
						ai = yes
					}
					add_ruler_modifier = {
						name = policy_vassal
						hidden = yes
					}
				}
				every_subject_country = {
					clr_country_flag = vassal_01
					clr_country_flag = vassal_02
					clr_country_flag = vassal_03
					clr_country_flag = vassal_04
					clr_country_flag = vassal_05
					clr_country_flag = vassal_06
					clr_country_flag = vassal_07
					clr_country_flag = vassal_08
					clr_country_flag = vassal_09
					clr_country_flag = vassal_10
				}
				clr_country_flag = vassal_01
				clr_country_flag = vassal_02
				clr_country_flag = vassal_03
				clr_country_flag = vassal_04
				clr_country_flag = vassal_05
				clr_country_flag = vassal_06
				clr_country_flag = vassal_07
				clr_country_flag = vassal_08
				clr_country_flag = vassal_09
				clr_country_flag = vassal_10
			}
		}
		ai_will_do = {
			factor = 0.5
		}
	}
	
	select_policy_partners = {
		potential = {
			num_of_unions = 1
			is_lesser_in_union = no
			OR = {
				ai = no
				NOT = { has_ruler_modifier = policy_partner }
			}
		}
		allow = {
		}  
		effect = {
			country_event = { id = subject_integration.2000 }
			hidden_effect = {
				if = {
					limit = {
						ai = yes
					}
					add_ruler_modifier = {
						name = policy_partner
						hidden = yes
					}
				}
				every_subject_country = {
					clr_country_flag = partner_01
					clr_country_flag = partner_02
					clr_country_flag = partner_03
					clr_country_flag = partner_04
					clr_country_flag = partner_05
					clr_country_flag = partner_06
					clr_country_flag = partner_07
					clr_country_flag = partner_08
					clr_country_flag = partner_09
					clr_country_flag = partner_10
				}
				clr_country_flag = partner_01
				clr_country_flag = partner_02
				clr_country_flag = partner_03
				clr_country_flag = partner_04
				clr_country_flag = partner_05
				clr_country_flag = partner_06
				clr_country_flag = partner_07
				clr_country_flag = partner_08
				clr_country_flag = partner_09
				clr_country_flag = partner_10
			}
		}
		ai_will_do = {
			factor = 0.5
		}
	}

}
