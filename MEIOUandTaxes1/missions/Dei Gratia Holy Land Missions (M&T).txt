holy_land_rome = { #Vanilla 118, M&T 2530
	
	type = country

	category = MIL
	
	allow = {
		religion = catholic
		piety = 0.20
		is_subject = no
		NOT = { owns_or_vassal_of = 2530 }
		2530 = { owner = { 
			NOT = { religion = ROOT }
			NOT = { overlord = { religion = ROOT } }
			NOT = { is_subject_of = ROOT }
			OR = { 
				is_neighbor_of = ROOT
				is_rival = ROOT
				is_enemy = ROOT
				truce_with = ROOT
				war_with = ROOT
				} 
			} }
		}
	abort = {
		OR = {
			is_subject = yes
			NOT = { piety = -0.20 }
			2530 = {
				OR = { 
					owner = { religion = ROOT }
					owner = { overlord = { religion = ROOT } }
					}
				NOT = { owned_by = ROOT }
				NOT = { owner = { overlord = { tag = ROOT } } }
				}
			}
		}
	success = {
		owns_or_vassal_of = 2530
		}
	chance = {
		factor = 1500
		modifier = { 
			2530 = { owner = { army_size = ROOT } }
			factor = 0.7
			}
		modifier = { 
			any_neighbor_country = { owns = 2530 }
			factor = 1.4
			}
		modifier = {
			factor = 1.4
			piety = 0.60
			}
		modifier = {
			factor = 2
			is_defender_of_faith = yes
			}
		modifier = {
			factor = 2
			2530 = { is_core = ROOT }
			}
		modifier = { 
			factor = 2
			2530 = { owner = { is_rival = ROOT } }
			}
		modifier = { 
			factor = 2
			2530 = { owner = { has_country_flag = religious_turmoil_catholic } }
			}
		modifier = { 
			NOT = { religious_unity = 0.9 }
			factor = 0.7
			}
		modifier = { 
			is_at_war = yes
			factor = 0.7
			}
		}
	immediate = {
		add_claim = 2530
		}
	abort_effect = {
		remove_claim = 2530
		}
	effect = {
		if = { 
			limit = { piety = 0.60 }
			add_prestige = 30
			}
		if = { 
			limit = { piety = 0.20 NOT = { piety = 0.60 } }
			add_prestige = 20
			}
		if = { 
			limit = { piety = -0.20 NOT = { piety = 0.20 } }
			add_prestige = 10
			}
		if = {
			limit = {
				2530 = { NOT = { is_core = ROOT } }
				}
			2530 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
					}
				}
			}
		}
	}

holy_land_al_najaf = { #Vanilla 409, M&T 1330
	
	type = country

	category = MIL
	
	allow = {
		religion = shiite
		piety = 0.20
		is_subject = no
		NOT = { owns_or_vassal_of = 1330 }
		1330 = { owner = { 
			NOT = { religion = ROOT }
			NOT = { overlord = { religion = ROOT } }
			NOT = { is_subject_of = ROOT }
			OR = { 
				is_neighbor_of = ROOT
				is_rival = ROOT
				is_enemy = ROOT
				truce_with = ROOT
				war_with = ROOT
				} 
			} }
		}
	abort = {
		OR = {
			is_subject = yes
			NOT = { piety = -0.20 }
			1330 = {
				OR = { 
					owner = { religion = ROOT }
					owner = { overlord = { religion = ROOT } }
					}
				NOT = { owned_by = ROOT }
				NOT = { owner = { overlord = { tag = ROOT } } }
				}
			}
		}
	success = {
		owns_or_vassal_of = 1330
		}
	chance = {
		factor = 1500
		modifier = { 
			1330 = { owner = { army_size = ROOT } }
			factor = 0.7
			}
		modifier = { 
			any_neighbor_country = { owns = 1330 }
			factor = 1.4
			}
		modifier = {
			factor = 1.4
			piety = 0.60
			}
		modifier = {
			factor = 2
			is_defender_of_faith = yes
			}
		modifier = {
			factor = 2
			1330 = { is_core = ROOT }
			}
		modifier = { 
			factor = 2
			1330 = { owner = { is_rival = ROOT } }
			}
		modifier = { 
			factor = 2
			1330 = { owner = { has_country_flag = religious_turmoil_shiite } }
			}
		modifier = { 
			NOT = { religious_unity = 0.9 }
			factor = 0.7
			}
		modifier = { 
			is_at_war = yes
			factor = 0.7
			}
		}
	immediate = {
		add_claim = 1330
		}
	abort_effect = {
		remove_claim = 1330
		}
	effect = {
		if = { 
			limit = { piety = 0.60 }
			add_prestige = 30
			}
		if = { 
			limit = { piety = 0.20 NOT = { piety = 0.60 } }
			add_prestige = 20
			}
		if = { 
			limit = { piety = -0.20 NOT = { piety = 0.20 } }
			add_prestige = 10
			}
		if = {
			limit = {
				1330 = { NOT = { is_core = ROOT } }
				}
			1330 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
					}
				}
			}
		}
	}

holy_land_constantinople = { #Vanilla 151, M&T 1402
	
	type = country

	category = MIL
	
	allow = {
		OR = { 
			religion = orthodox
			religion = coptic
			}
		piety = 0.20
		is_subject = no
		NOT = { owns_or_vassal_of = 1402 }
		1402 = { owner = { 
			NOT = { religion = ROOT }
			NOT = { overlord = { religion = ROOT } }
			NOT = { is_subject_of = ROOT }
			OR = { 
				is_neighbor_of = ROOT
				is_rival = ROOT
				is_enemy = ROOT
				truce_with = ROOT
				war_with = ROOT
				} 
			} }
		}
	abort = {
		OR = {
			is_subject = yes
			NOT = { piety = -0.20 }
			1402 = {
				OR = { 
					owner = { religion = ROOT }
					owner = { overlord = { religion = ROOT } }
					}
				NOT = { owned_by = ROOT }
				NOT = { owner = { overlord = { tag = ROOT } } }
				}
			}
		}
	success = {
		owns_or_vassal_of = 1402
		}
	chance = {
		factor = 1500
		modifier = { 
			1402 = { owner = { army_size = ROOT } }
			factor = 0.7
			}
		modifier = { 
			any_neighbor_country = { owns = 1402 }
			factor = 1.4
			}
		modifier = {
			factor = 1.4
			piety = 0.60
			}
		modifier = {
			factor = 2
			is_defender_of_faith = yes
			}
		modifier = {
			factor = 2
			1402 = { is_core = ROOT }
			}
		modifier = { 
			factor = 2
			1402 = { owner = { is_rival = ROOT } }
			}
		modifier = { 
			factor = 2
			1402 = { owner = { has_country_flag = religious_turmoil_orthodox } }
			}
		modifier = { 
			OR = { 
				NOT = { capital_scope = { continent = europe } }
				capital_scope = { near_east_region_trigger = yes }
				}
			factor = 0.5
			}
		modifier = { 
			NOT = { religious_unity = 0.9 }
			factor = 0.7
			}
		modifier = { 
			is_at_war = yes
			factor = 0.7
			}
		}
	immediate = {
		add_claim = 1402
		}
	abort_effect = {
		remove_claim = 1402
		}
	effect = {
		if = { 
			limit = { 
				piety = 0.60 
				capital_scope = { continent = europe NOT = { near_east_region_trigger = yes } }
				}
			add_prestige = 30
			}
		if = { 
			limit = { 
				piety = 0.20 
				NOT = { piety = 0.60 } 
				capital_scope = { continent = europe NOT = { near_east_region_trigger = yes } }
				}
			add_prestige = 20
			}
		if = { 
			limit = { 
				piety = -0.20 
				NOT = { piety = 0.20 } 
				capital_scope = { continent = europe NOT = { near_east_region_trigger = yes } }
				}
			add_prestige = 10
			}
		if = { 
			limit = { 
				piety = 0.60 
				NOT = { capital_scope = { continent = europe NOT = { near_east_region_trigger = yes } } }
				}
			add_prestige = 15
			}
		if = { 
			limit = { 
				piety = 0.20 
				NOT = { piety = 0.60 } 
				NOT = { capital_scope = { continent = europe NOT = { near_east_region_trigger = yes } } }
				}
			add_prestige = 10
			}
		if = { 
			limit = { 
				piety = -0.20 
				NOT = { piety = 0.20 } 
				NOT = { capital_scope = { continent = europe NOT = { near_east_region_trigger = yes } } }
				}
			add_prestige = 5
			}
		if = {
			limit = {
				1402 = { NOT = { is_core = ROOT } }
				}
			1402 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
					}
				}
			}
		}
	}

holy_land_alexandria = { #358
	
	type = country

	category = MIL
	
	allow = {
		OR = { 
			religion = orthodox
			religion = coptic
			}
		piety = 0.20
		is_subject = no
		NOT = { owns_or_vassal_of = 358 }
		358 = { owner = { 
			NOT = { religion = ROOT }
			NOT = { overlord = { religion = ROOT } }
			NOT = { is_subject_of = ROOT }
			OR = { 
				is_neighbor_of = ROOT
				is_rival = ROOT
				is_enemy = ROOT
				truce_with = ROOT
				war_with = ROOT
				} 
			} }
		}
	abort = {
		OR = {
			is_subject = yes
			NOT = { piety = -0.20 }
			358 = {
				OR = { 
					owner = { religion = ROOT }
					owner = { overlord = { religion = ROOT } }
					}
				NOT = { owned_by = ROOT }
				NOT = { owner = { overlord = { tag = ROOT } } }
				}
			}
		}
	success = {
		owns_or_vassal_of = 358
		}
	chance = {
		factor = 1500
		modifier = { 
			358 = { owner = { army_size = ROOT } }
			factor = 0.7
			}
		modifier = { 
			any_neighbor_country = { owns = 358 }
			factor = 1.4
			}
		modifier = {
			factor = 1.4
			piety = 0.60
			}
		modifier = {
			factor = 2
			is_defender_of_faith = yes
			}
		modifier = {
			factor = 2
			358 = { is_core = ROOT }
			}
		modifier = { 
			factor = 2
			358 = { owner = { is_rival = ROOT } }
			}
		modifier = { 
			factor = 2
			358 = { owner = { has_country_flag = religious_turmoil_orthodox } }
			religion = orthodox
			}
		modifier = { 
			factor = 2
			358 = { owner = { has_country_flag = religious_turmoil_coptic } }
			religion = coptic
			}
		modifier = { 
			NOT = { capital_scope = { continent = africa } }
			factor = 0.5
			}
		modifier = { 
			NOT = { religious_unity = 0.9 }
			factor = 0.7
			}
		modifier = { 
			is_at_war = yes
			factor = 0.7
			}
		}
	immediate = {
		add_claim = 358
		}
	abort_effect = {
		remove_claim = 358
		}
	effect = {
		if = { 
			limit = { 
				piety = 0.60 
				capital_scope = { continent = africa }
				}
			add_prestige = 30
			}
		if = { 
			limit = { 
				piety = 0.20 
				NOT = { piety = 0.60 } 
				capital_scope = { continent = africa }
				}
			add_prestige = 20
			}
		if = { 
			limit = { 
				piety = -0.20 
				NOT = { piety = 0.20 } 
				capital_scope = { continent = africa }
				}
			add_prestige = 10
			}
		if = { 
			limit = { 
				piety = 0.60 
				NOT = { capital_scope = { continent = africa } }
				}
			add_prestige = 15
			}
		if = { 
			limit = { 
				piety = 0.20 
				NOT = { piety = 0.60 } 
				NOT = { capital_scope = { continent = africa } }
				}
			add_prestige = 10
			}
		if = { 
			limit = { 
				piety = -0.20 
				NOT = { piety = 0.20 } 
				NOT = { capital_scope = { continent = africa } }
				}
			add_prestige = 5
			}
		if = {
			limit = {
				358 = { NOT = { is_core = ROOT } }
				}
			358 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
					}
				}
			}
		}
	}

holy_land_antioch = { #382 - Damascus
	
	type = country

	category = MIL
	
	allow = {
		OR = { 
			religion = orthodox
			religion = coptic
			}
		piety = 0.20
		is_subject = no
		NOT = { owns_or_vassal_of = 382 }
		382 = { owner = { 
			NOT = { religion = ROOT }
			NOT = { overlord = { religion = ROOT } }
			NOT = { is_subject_of = ROOT }
			OR = { 
				is_neighbor_of = ROOT
				is_rival = ROOT
				is_enemy = ROOT
				truce_with = ROOT
				war_with = ROOT
				} 
			} }
		}
	abort = {
		OR = {
			is_subject = yes
			NOT = { piety = -0.20 }
			382 = {
				OR = { 
					owner = { religion = ROOT }
					owner = { overlord = { religion = ROOT } }
					}
				NOT = { owned_by = ROOT }
				NOT = { owner = { overlord = { tag = ROOT } } }
				}
			}
		}
	success = {
		owns_or_vassal_of = 382
		}
	chance = {
		factor = 1500
		modifier = { 
			382 = { owner = { army_size = ROOT } }
			factor = 0.7
			}
		modifier = { 
			any_neighbor_country = { owns = 382 }
			factor = 1.4
			}
		modifier = {
			factor = 1.4
			piety = 0.60
			}
		modifier = {
			factor = 2
			is_defender_of_faith = yes
			}
		modifier = {
			factor = 2
			382 = { is_core = ROOT }
			}
		modifier = { 
			factor = 2
			382 = { owner = { is_rival = ROOT } }
			}
		modifier = { 
			factor = 2
			382 = { owner = { has_country_flag = religious_turmoil_orthodox } }
			religion = orthodox
			}
		modifier = { 
			factor = 2
			382 = { owner = { has_country_flag = religious_turmoil_coptic } }
			religion = coptic
			}
		modifier = { 
			NOT = { capital_scope = { continent = asia } }
			NOT = { capital_scope = { near_east_region_trigger = yes } }
			factor = 0.5
			}
		modifier = { 
			NOT = { religious_unity = 0.9 }
			factor = 0.7
			}
		modifier = { 
			is_at_war = yes
			factor = 0.7
			}
		}
	immediate = {
		add_claim = 382
		}
	abort_effect = {
		remove_claim = 382
		}
	effect = {
		if = { 
			limit = { 
				piety = 0.60 
				OR = { 
					capital_scope = { continent = asia }
					capital_scope = { near_east_region_trigger = yes }
					}
				}
			add_prestige = 30
			}
		if = { 
			limit = { 
				piety = 0.20 
				NOT = { piety = 0.60 } 
				OR = { 
					capital_scope = { continent = asia }
					capital_scope = { near_east_region_trigger = yes }
					}
				}
			add_prestige = 20
			}
		if = { 
			limit = { 
				piety = -0.20 
				NOT = { piety = 0.20 } 
				OR = { 
					capital_scope = { continent = asia }
					capital_scope = { near_east_region_trigger = yes }
					}
				}
			add_prestige = 10
			}
		if = { 
			limit = { 
				piety = 0.60 
				NOT = { capital_scope = { continent = asia } }
				NOT = { capital_scope = { near_east_region_trigger = yes } }
				}
			add_prestige = 15
			}
		if = { 
			limit = { 
				piety = 0.20 
				NOT = { piety = 0.60 } 
				NOT = { capital_scope = { continent = asia } }
				NOT = { capital_scope = { near_east_region_trigger = yes } }
				}
			add_prestige = 10
			}
		if = { 
			limit = { 
				piety = -0.20 
				NOT = { piety = 0.20 } 
				NOT = { capital_scope = { continent = asia } }
				NOT = { capital_scope = { near_east_region_trigger = yes } }
				}
			add_prestige = 5
			}
		if = {
			limit = {
				382 = { NOT = { is_core = ROOT } }
				}
			382 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
					}
				}
			}
		}
	}

holy_land_jerusalem = { #379
	
	type = country

	category = MIL
	
	allow = {
		OR = { 
			religion = catholic
			religion = coptic
			religion = chaldean
			religion = sunni
			religion = shiite
			religion = jewish
			}
		piety = 0.20
		is_subject = no
		NOT = { owns_or_vassal_of = 379 }
		379 = { owner = { 
			NOT = { religion = ROOT }
			NOT = { overlord = { religion = ROOT } }
			NOT = { is_subject_of = ROOT }
			OR = { 
				is_neighbor_of = ROOT
				is_rival = ROOT
				is_enemy = ROOT
				truce_with = ROOT
				war_with = ROOT
				} 
			} }
		}
	abort = {
		OR = {
			is_subject = yes
			NOT = { piety = -0.20 }
			379 = {
				OR = { 
					owner = { religion = ROOT }
					owner = { overlord = { religion = ROOT } }
					}
				NOT = { owned_by = ROOT }
				NOT = { owner = { overlord = { tag = ROOT } } }
				}
			}
		}
	success = {
		owns_or_vassal_of = 379
		}
	chance = {
		factor = 1500
		modifier = { 
			379 = { owner = { army_size = ROOT } }
			factor = 0.7
			}
		modifier = { 
			any_neighbor_country = { owns = 379 }
			factor = 1.4
			}
		modifier = {
			factor = 1.4
			piety = 0.60
			}
		modifier = {
			factor = 2
			is_defender_of_faith = yes
			}
		modifier = {
			factor = 2
			379 = { is_core = ROOT }
			}
		modifier = { 
			factor = 2
			379 = { owner = { is_rival = ROOT } }
			}
		modifier = { 
			factor = 2
			379 = { owner = { has_country_flag = religious_turmoil_catholic } }
			religion = catholic
			}
		modifier = { 
			factor = 2
			379 = { owner = { has_country_flag = religious_turmoil_orthodox } }
			religion = orthodox
			}
		modifier = { 
			factor = 2
			379 = { owner = { has_country_flag = religious_turmoil_sunni } }
			religion = sunni
			}
		modifier = { 
			factor = 2
			379 = { owner = { has_country_flag = religious_turmoil_shiite } }
			religion = shiite
			}
		modifier = { 
			factor = 2
			379 = { owner = { has_country_flag = religious_turmoil_jewish } }
			religion = jewish
			}
		modifier = { 
			NOT = { religion = jewish }
			factor = 0.5
			}
		modifier = { 
			NOT = { religious_unity = 0.9 }
			factor = 0.7
			}
		modifier = { 
			is_at_war = yes
			factor = 0.7
			}
		}
	immediate = {
		add_claim = 379
		}
	abort_effect = {
		remove_claim = 379
		}
	effect = {
		if = { 
			limit = { 
				piety = 0.60 
				NOT = { religion = jewish }
				}
			add_prestige = 15
			}
		if = { 
			limit = { 
				piety = 0.20 
				NOT = { piety = 0.60 } 
				NOT = { religion = jewish }
				}
			add_prestige = 10
			}
		if = { 
			limit = { 
				piety = -0.20 
				NOT = { piety = 0.20 } 
				NOT = { religion = jewish }
				}
			add_prestige = 5
			}
		if = { 
			limit = { 
				piety = 0.60 
				religion = jewish
				}
			add_prestige = 30
			}
		if = { 
			limit = { 
				piety = 0.20 
				NOT = { piety = 0.60 } 
				religion = jewish
				}
			add_prestige = 20
			}
		if = { 
			limit = { 
				piety = -0.20 
				NOT = { piety = 0.20 } 
				religion = jewish
				}
			add_prestige = 10
			}
		if = {
			limit = {
				379 = { NOT = { is_core = ROOT } }
				}
			379 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
					}
				}
			}
		}
	}

holy_land_mecca = { #385
	
	type = country

	category = MIL
	
	allow = {
		OR = { 
			religion = sunni
			religion = shiite
			religion = wahhabi
			}
		piety = 0.20
		is_subject = no
		NOT = { owns_or_vassal_of = 385 }
		385 = { owner = { 
			NOT = { religion = ROOT }
			NOT = { overlord = { religion = ROOT } }
			NOT = { is_subject_of = ROOT }
			OR = { 
				is_neighbor_of = ROOT
				is_rival = ROOT
				is_enemy = ROOT
				truce_with = ROOT
				war_with = ROOT
				} 
			} }
		}
	abort = {
		OR = {
			is_subject = yes
			NOT = { piety = -0.20 }
			385 = {
				OR = { 
					owner = { religion = ROOT }
					owner = { overlord = { religion = ROOT } }
					}
				NOT = { owned_by = ROOT }
				NOT = { owner = { overlord = { tag = ROOT } } }
				}
			}
		}
	success = {
		owns_or_vassal_of = 385
		}
	chance = {
		factor = 1500
		modifier = { 
			385 = { owner = { army_size = ROOT } }
			factor = 0.7
			}
		modifier = { 
			any_neighbor_country = { owns = 385 }
			factor = 1.4
			}
		modifier = {
			factor = 1.4
			piety = 0.60
			}
		modifier = {
			factor = 2
			is_defender_of_faith = yes
			}
		modifier = {
			factor = 2
			385 = { is_core = ROOT }
			}
		modifier = { 
			factor = 2
			385 = { owner = { is_rival = ROOT } }
			}
		modifier = { 
			factor = 2
			385 = { owner = { has_country_flag = religious_turmoil_sunni } }
			religion = sunni
			}
		modifier = { 
			factor = 2
			385 = { owner = { has_country_flag = religious_turmoil_wahhabi } }
			religion = wahhabi
			}
		modifier = { 
			NOT = { religious_unity = 0.9 }
			factor = 0.7
			}
		modifier = { 
			is_at_war = yes
			factor = 0.7
			}
		}
	immediate = {
		add_claim = 385
		}
	abort_effect = {
		remove_claim = 385
		}
	effect = {
		if = { 
			limit = { 
				piety = 0.60 
				}
			add_prestige = 30
			}
		if = { 
			limit = { 
				piety = 0.20 
				NOT = { piety = 0.60 } 
				}
			add_prestige = 20
			}
		if = { 
			limit = { 
				piety = -0.20 
				NOT = { piety = 0.20 } 
				}
			add_prestige = 10
			}
		if = {
			limit = {
				385 = { NOT = { is_core = ROOT } }
				}
			385 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
					}
				}
			}
		}
	}

holy_land_medina = { #384
	
	type = country

	category = MIL
	
	allow = {
		OR = { 
			religion = sunni
			religion = shiite
			religion = wahhabi
			}
		piety = 0.20
		is_subject = no
		NOT = { owns_or_vassal_of = 384 }
		384 = { owner = { 
			NOT = { religion = ROOT }
			NOT = { overlord = { religion = ROOT } }
			NOT = { is_subject_of = ROOT }
			OR = { 
				is_neighbor_of = ROOT
				is_rival = ROOT
				is_enemy = ROOT
				truce_with = ROOT
				war_with = ROOT
				} 
			} }
		}
	abort = {
		OR = {
			is_subject = yes
			NOT = { piety = -0.20 }
			384 = {
				OR = { 
					owner = { religion = ROOT }
					owner = { overlord = { religion = ROOT } }
					}
				NOT = { owned_by = ROOT }
				NOT = { owner = { overlord = { tag = ROOT } } }
				}
			}
		}
	success = {
		owns_or_vassal_of = 384
		}
	chance = {
		factor = 1500
		modifier = { 
			384 = { owner = { army_size = ROOT } }
			factor = 0.7
			}
		modifier = { 
			any_neighbor_country = { owns = 384 }
			factor = 1.4
			}
		modifier = {
			factor = 1.4
			piety = 0.60
			}
		modifier = {
			factor = 2
			is_defender_of_faith = yes
			}
		modifier = {
			factor = 2
			384 = { is_core = ROOT }
			}
		modifier = { 
			factor = 2
			384 = { owner = { is_rival = ROOT } }
			}
		modifier = { 
			factor = 2
			384 = { owner = { has_country_flag = religious_turmoil_sunni } }
			religion = sunni
			}
		modifier = { 
			factor = 2
			384 = { owner = { has_country_flag = religious_turmoil_wahhabi } }
			religion = wahhabi
			}
		modifier = { 
			NOT = { religious_unity = 0.9 }
			factor = 0.7
			}
		modifier = { 
			is_at_war = yes
			factor = 0.7
			}
		}
	immediate = {
		add_claim = 384
		}
	abort_effect = {
		remove_claim = 384
		}
	effect = {
		if = { 
			limit = { 
				piety = 0.60 
				}
			add_prestige = 30
			}
		if = { 
			limit = { 
				piety = 0.20 
				NOT = { piety = 0.60 } 
				}
			add_prestige = 20
			}
		if = { 
			limit = { 
				piety = -0.20 
				NOT = { piety = 0.20 } 
				}
			add_prestige = 10
			}
		if = {
			limit = {
				384 = { NOT = { is_core = ROOT } }
				}
			384 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
					}
				}
			}
		}
	}

holy_land_ctesiphon = { #410
	
	type = country

	category = MIL
	
	allow = {
		religion = chaldean
		piety = 0.20
		is_subject = no
		NOT = { owns_or_vassal_of = 410 }
		410 = { owner = { 
			NOT = { religion = ROOT }
			NOT = { overlord = { religion = ROOT } }
			NOT = { is_subject_of = ROOT }
			OR = { 
				is_neighbor_of = ROOT
				is_rival = ROOT
				is_enemy = ROOT
				truce_with = ROOT
				war_with = ROOT
				} 
			} }
		}
	abort = {
		OR = {
			is_subject = yes
			NOT = { piety = -0.20 }
			410 = {
				OR = { 
					owner = { religion = ROOT }
					owner = { overlord = { religion = ROOT } }
					}
				NOT = { owned_by = ROOT }
				NOT = { owner = { overlord = { tag = ROOT } } }
				}
			}
		}
	success = {
		owns_or_vassal_of = 410
		}
	chance = {
		factor = 1500
		modifier = { 
			410 = { owner = { army_size = ROOT } }
			factor = 0.7
			}
		modifier = { 
			any_neighbor_country = { owns = 410 }
			factor = 1.4
			}
		modifier = {
			factor = 1.4
			piety = 0.60
			}
		modifier = {
			factor = 2
			is_defender_of_faith = yes
			}
		modifier = {
			factor = 2
			410 = { is_core = ROOT }
			}
		modifier = { 
			factor = 2
			410 = { owner = { is_rival = ROOT } }
			}
		modifier = { 
			factor = 2
			410 = { owner = { has_country_flag = religious_turmoil_chaldean } }
			}
		modifier = { 
			NOT = { religious_unity = 0.9 }
			factor = 0.7
			}
		modifier = { 
			is_at_war = yes
			factor = 0.7
			}
		}
	immediate = {
		add_claim = 410
		}
	abort_effect = {
		remove_claim = 410
		}
	effect = {
		if = { 
			limit = { piety = 0.60 }
			add_prestige = 15
			}
		if = { 
			limit = { 
				piety = 0.20 
				NOT = { piety = 0.60 } 
				}
			add_prestige = 10
			}
		if = { 
			limit = { 
				piety = -0.20 
				NOT = { piety = 0.20 } 
				}
			add_prestige = 5
			}
		if = {
			limit = {
				410 = { NOT = { is_core = ROOT } }
				}
			410 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
					}
				}
			}
		}
	}

holy_land_balga = { #41
	
	type = country

	category = MIL
	
	allow = {
		religion = baltic_pagan_reformed
		piety = 0.20
		is_subject = no
		NOT = { owns_or_vassal_of = 41 }
		41 = { owner = { 
			NOT = { religion = ROOT }
			NOT = { overlord = { religion = ROOT } }
			NOT = { is_subject_of = ROOT }
			OR = { 
				is_neighbor_of = ROOT
				is_rival = ROOT
				is_enemy = ROOT
				truce_with = ROOT
				war_with = ROOT
				} 
			} }
		}
	abort = {
		OR = {
			is_subject = yes
			NOT = { piety = -0.20 }
			41 = {
				OR = { 
					owner = { religion = ROOT }
					owner = { overlord = { religion = ROOT } }
					}
				NOT = { owned_by = ROOT }
				NOT = { owner = { overlord = { tag = ROOT } } }
				}
			}
		}
	success = {
		owns_or_vassal_of = 41
		}
	chance = {
		factor = 1500
		modifier = { 
			41 = { owner = { army_size = ROOT } }
			factor = 0.7
			}
		modifier = { 
			any_neighbor_country = { owns = 41 }
			factor = 1.4
			}
		modifier = {
			factor = 1.4
			piety = 0.60
			}
		modifier = {
			factor = 2
			is_defender_of_faith = yes
			}
		modifier = {
			factor = 2
			41 = { is_core = ROOT }
			}
		modifier = { 
			factor = 2
			41 = { owner = { is_rival = ROOT } }
			}
		modifier = { 
			factor = 2
			41 = { owner = { has_country_flag = religious_turmoil_baltic_pagan_reformed } }
			}
		modifier = { 
			NOT = { religious_unity = 0.9 }
			factor = 0.7
			}
		modifier = { 
			is_at_war = yes
			factor = 0.7
			}
		}
	immediate = {
		add_claim = 41
		}
	abort_effect = {
		remove_claim = 41
		}
	effect = {
		if = { 
			limit = { piety = 0.60 }
			add_prestige = 15
			}
		if = { 
			limit = { 
				piety = 0.20 
				NOT = { piety = 0.60 } 
				}
			add_prestige = 10
			}
		if = { 
			limit = { 
				piety = -0.20 
				NOT = { piety = 0.20 } 
				}
			add_prestige = 5
			}
		if = {
			limit = {
				41 = { NOT = { is_core = ROOT } }
				}
			41 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
					}
				}
			}
		}
	}


