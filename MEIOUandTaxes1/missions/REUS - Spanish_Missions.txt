# Spanish Missions

finish_reconquista = {

	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		iberia_region_trigger = yes
		owner = { religion_group = muslim }
	}
	allow = {
		NOT = { is_year = 1600 }
		OR = {
			tag = CAS
			tag = SPA
		}
		is_subject = no
		mil = 3
		is_at_war = no
		religion = catholic
		NOT = { has_country_modifier = completing_the_reconquista }
		NOT = { has_country_flag = granada_gone }
		iberia_superregion = {
			owned_by = ROOT
		}
		any_target_province = {
			owner = { religion_group = muslim NOT = { vassal_of = ROOT } }
		}
	}
	abort = {
		OR = {
			NOT = { religion = catholic }
			NOT = { iberia_superregion = { owned_by = ROOT } }
		}
	}
	success = {
		all_target_province = {
			OR = {
				owned_by = ROOT
				owner = { NOT = { religion_group = muslim } }
			}
		}
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 10
			exists = GRA
			iberia_superregion = {
				owned_by = GRA
			}
		}
	}
	effect = {
		add_army_tradition = 15
		add_war_exhaustion = -4
		set_country_flag = granada_gone
		add_country_modifier = {
			name = "completing_the_reconquista"
			duration = 3650
		}
		every_target_province = {
			add_gaining_control_effect = yes
		}
	}
}


no_truce_with_the_moors = {

	type = country

	category = MIL
	
	allow = {
		OR = {
			tag = CAS
			tag = SPA
		}
		is_subject = no
		mil = 4
		religion = catholic
		is_at_war = no
		iberia_superregion = {
			owned_by = GRA
		}
		GRA = {
			is_subject = no
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = {	exists = GRA }
			NOT = { religion = catholic }
			GRA = { is_subject = yes }
		}
	}	
	success = {
		war_with = GRA
	}
	chance = {
		factor = 1000
	}
	effect = {
		add_prestige = 10
		add_papal_influence = 5
	}
}


spain_must_be_christian = {

	type = country

	category = ADM
	
	allow = {
		OR = {
			tag = CAS
			tag = SPA
		}
		religion = catholic
		is_at_war = no
		NOT = {	exists = GRA }
		NOT = { has_country_modifier = achieved_religious_unity_mission }
		any_owned_province = {
			muslim_iberia_region_trigger = yes
		}
	}
	abort = {
		NOT = {	religion = catholic  }
	}	
	success = {
		NOT = {
			any_owned_province = {
				muslim_iberia_region_trigger = yes
			}
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 3
		}
	}
	effect = {
		add_prestige = 10
		add_papal_influence = 50
		add_country_modifier = {
			name = "achieved_religious_unity_mission"
			duration = 3650
		}
		if = {
			limit = {
				iberia_superregion = {
					NOT = { continent = africa }
					owned_by =  ROOT
					any_core_country = {
						NOT = { religion_group = christian }
					}
				}
			}
			every_province = {
				limit = {
					iberia_region_trigger = yes
					owned_by =  ROOT
					any_core_country = {
						NOT = { religion_group = christian }
					}
				}
				every_core_country = {
					limit = {
						NOT = { religion_group = christian }
					}
					remove_core = PREV
				}
			}
		}
	}
}


continue_reconquista = {

	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		OR = {
			province_id = 334	# Tangiers
			province_id = 1534	# Melilla
			province_id = 337	# Oran
			province_id = 1533	# Ceuta
		}
		owner = { 
			NOT = { religion_group = christian }
			NOT = { alliance_with = ROOT }
			NOT = { is_subject_of = ROOT }
		}
	}
	allow = {
		OR = {
			tag = CAS
			tag = SPA
		}		
		religion = catholic
		is_at_war = no
		mil = 4
		NOT = { has_country_modifier = completing_the_reconquista }
		NOT = {
			iberia_superregion = {
				NOT = { continent = africa }
				owner = { religion_group = muslim }
			}
		}
		1407 = { owned_by = ROOT }	# Gibraltar.
		OR = {
			334 = { owner = { NOT = { religion_group = christian } } }
			1534 = { owner = { NOT = { religion_group = christian } } }
			337 = { owner  = { NOT = { religion_group = christian } } }
			1533 = { owner = { NOT = { religion_group = christian } } }
		}
	}
	abort = {
		all_target_province = {
			owner = {
				religion_group = christian
				NOT = { tag = ROOT }
			}
		}
	}
	success = {
		any_target_province = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 3
		}
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 5
		}
		modifier = {
			factor = 10
			has_idea = just_war
		}		
	}
	immediate = {
		every_target_province = {
			add_claim = ROOT
		}
	}
	abort_effect = {
		every_target_province = {
			remove_claim = ROOT
		}
	}
	effect = {
		add_adm_power = 200
		add_army_tradition = 10
		add_country_modifier = {
			name = "completing_the_reconquista"
			duration = 3650
		}
		every_target_province = {
			add_gaining_control_effect = yes
		}
	}
}


spain_discover_america = {

	type = country

	category = DIP
	
	allow = {
		OR = {
			tag = CAS
			tag = SPA
		}
		is_subject = no
		has_idea = quest_for_the_new_world
		num_of_ports = 1
		NOT = { has_country_modifier = colonial_enthusiasm }
		NOT = {
			carribeans_region = {
				has_discovered = ROOT
			}
		}
		carribeans_region = { range = ROOT }
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = {	has_idea = quest_for_the_new_world  }
			NOT = { num_of_ports = 1 }
		}
	}
	success = {
		carribeans_region = {
			has_discovered = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 3
		}
		modifier = {
			factor = 2
			adm = 4
		}		
		modifier = {
			factor = 2
			adm = 5
		}
	}
	effect = {
		add_dip_power = 25
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


establish_spanish_carribean = {

	type = country

	category = DIP
	
	allow = {
		OR = {
			tag = CAS
			tag = SPA
		}
		is_subject = no
		has_idea = quest_for_the_new_world
		num_of_ports = 1
		NOT = { has_country_modifier = colonial_enthusiasm }
		NOT = {
			carribeans_region = {
				country_or_vassal_holds =  ROOT
			}
		}
		carribeans_region = {
			has_discovered = ROOT
			is_empty = yes
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = {	has_idea = quest_for_the_new_world  }
			NOT = { num_of_ports = 1 }
			AND = {
				NOT = { carribeans_region = { country_or_vassal_holds = ROOT } }
				NOT = { carribeans_region = { is_empty = yes } }
			}
		}
	}
	success = {
		carribeans_region = {
			country_or_vassal_holds = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 4
		}
		modifier = {
			factor = 0.1
			is_at_war = yes
		}		
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 3650
		}
	}
}


discover_spanish_main = {

	type = country

	category = DIP
	
	allow = {
		OR = {
			tag = CAS
			tag = SPA
		}
		is_subject = no
		has_idea = quest_for_the_new_world
		num_of_ports = 1
		carribeans_region = {
			has_discovered = ROOT
		}	
		NOT = {
			the_spanish_main_group = {
				has_discovered = ROOT
			}
		}
		the_spanish_main_group = { range = ROOT }
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = {	has_idea = quest_for_the_new_world  }
			NOT = { num_of_ports = 1 }
		}
	}
	success = {
		the_spanish_main_group = {
			has_discovered = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 3
		}
		modifier = {
			factor = 2
			adm = 4
		}		
		modifier = {
			factor = 2
			adm = 5
		}
	}
	effect = {
		add_dip_power = 25
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


establish_spanish_main = {

	type = country
	
	category = DIP
	
	allow = {
		OR = {
			tag = CAS
			tag = SPA
		}
		is_subject = no
		has_idea = quest_for_the_new_world
		num_of_ports = 1
		NOT = { has_country_modifier = colonial_enthusiasm }
		NOT = {
			the_spanish_main_group = {
				owned_by =  ROOT
			}
		}
		the_spanish_main_group = {
			has_discovered = ROOT
			is_empty = yes
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = {	has_idea = quest_for_the_new_world  }
			NOT = { num_of_ports = 1 }
			AND = {
				NOT = { the_spanish_main_group = { owned_by = ROOT } }
				NOT = { the_spanish_main_group = { is_empty = yes } }
			}
		}
	}
	success = {
		the_spanish_main_group = {
			owned_by = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 4
		}
		modifier = {
			factor = 0.1
			is_at_war = yes
		}		
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 3650
		}
	}
}


discover_la_plata = {

	type = country

	category = MIL
	
	allow = {
		OR = {
			tag = CAS
			tag = SPA
		}
		is_subject = no
		has_idea = quest_for_the_new_world
		num_of_ports = 1
		NOT = {
			la_plata_region = {
				has_discovered = ROOT
			}
		}
		la_plata_region = { range = ROOT }
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = {	has_idea = quest_for_the_new_world  }
			NOT = { num_of_ports = 1 }
		}
	}
	success = {
		la_plata_region = {
			has_discovered = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 3
		}
		modifier = {
			factor = 2
			adm = 4
		}		
		modifier = {
			factor = 2
			adm = 5
		}
	}
	effect = {
		add_dip_power = 25
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 1875
		}
	}
}


establish_spanish_la_plata = {

	type = country
	
	category = DIP
	
	allow = {
		OR = {
			tag = CAS
			tag = SPA
		}
		is_subject = no
		has_idea = quest_for_the_new_world
		num_of_ports = 1
		NOT = { has_country_modifier = colonial_enthusiasm }
		NOT = {
			la_plata_region = {
				country_or_vassal_holds =  ROOT
			}
		}
		la_plata_region = {
			has_discovered = ROOT
			is_empty = yes
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = {	has_idea = quest_for_the_new_world  }
			NOT = { num_of_ports = 1 }
			AND = {
				NOT = { la_plata_region = { owned_by = ROOT } }
				NOT = { la_plata_region = { is_empty = yes } }
			}
		}
	}
	success = {
		la_plata_region = {
			country_or_vassal_holds = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			adm = 4
		}
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
			name = "colonial_enthusiasm"
			duration = 3650
		}
	}
}


conquer_incas = {

	type = country
	
	category = MIL
	ai_mission = yes

	target_provinces = {
		owned_by = INC
	}
	allow = {
		OR = {
			tag = CAS
			tag = SPA
		}
		is_subject = no
		is_at_war = no
		is_neighbor_of = INC
		NOT = { alliance_with = INC }
		INC = { religion_group = new_world_pagan }
		NOT = { has_country_flag = conquered_incas }
	}
	abort = {
		NOT = { exists = INC }
	}
	success = {
		NOT = { exists = INC }
		all_target_province = {
			country_or_vassal_holds = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}		
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	effect = {
		add_treasury = 2000
		set_country_flag = conquered_incas
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
	}
}


conquer_aztecs = {

	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = AZT
	}
	allow = {
		OR = {
			tag = CAS
			tag = SPA
		}
		is_subject = no
		is_at_war = no
		is_neighbor_of = AZT
		NOT = { alliance_with = AZT }
		NOT = { has_country_flag = conquered_aztecs }
		AZT = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = AZT }
	}
	success = {
		NOT = { exists = AZT }
		all_target_province = {
			country_or_vassal_holds = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}		
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	effect = {
		add_treasury = 1500
		set_country_flag = conquered_aztecs
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
	}
}


conquer_zapotecs = {

	type = country

	category = MIL
	ai_mission = yes
	
	target_provinces = {
		owned_by = ZAP
	}
	allow = {
		OR = {
			tag = CAS
			tag = SPA
		}
		is_subject = no
		is_at_war = no
		is_neighbor_of = ZAP
		NOT = { alliance_with = ZAP }
		NOT = { has_country_flag = conquered_zapotecs }
		ZAP = { religion_group = new_world_pagan }
	}
	abort = {
		NOT = { exists = ZAP }
	}
	success = {
		NOT = { exists = ZAP }
		all_target_province = {
			country_or_vassal_holds = ROOT
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
		modifier = {
			factor = 2
			adm = 4
		}	
	}
	effect = {
		add_treasury = 1000
		set_country_flag = conquered_zapotecs
		add_country_modifier = {
			name = "military_victory"
			duration = 3650
		}
	}
}


no_surrender_to_the_dutch = {

	type = country
	
	category = MIL
	
	allow = {
		NOT = { is_year = 1648 }
		tag = SPA
		NOT = { has_country_flag = freed_low_countries }
		is_subject = no
		is_neighbor_of = NED
		NOT = { alliance_with = NED }
		NOT = { war_with = NED }
		NED = {
			is_subject = no
		}
		low_countries_superregion = {
			owned_by = ROOT
		}
		low_countries_superregion = {
			owned_by = NED
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { war_with = NED }
			NED = {
				is_subject = yes
			}
			AND = {
				NOT = { low_countries_superregion = { owned_by = NED } }
				NOT = { low_countries_superregion = { owned_by = SPA } }
			}
		}
	}
	success = {
		NOT = { war_with = NED }
		NOT = {
			low_countries_superregion = {
				owned_by = NED
			}
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			mil = 6
		}
	}
	effect = {
		add_war_exhaustion = -5
		add_adm_power = 100
		add_prestige = 10
		set_country_flag = freed_low_countries
	}
}


bring_the_dutch_back_to_catholisism = {

	type = country
	
	category = MIL
	
	allow = {
		NOT = { is_year = 1648 }
		tag = SPA
		is_subject = no
		religion = catholic
		is_neighbor_of = NED
		NED = {
			is_subject = no
			religion_group = ROOT
			NOT = { religion = ROOT }
		}
		low_countries_superregion = {
			owned_by = ROOT
		}
	}
	abort = {
		OR = {
			is_subject = yes
			NOT = { religion = catholic }
			NOT = {
				low_countries_superregion = {
					owned_by = ROOT
				}
			}
		}
	}
	success = {
		NED = { religion = ROOT }
	}
	chance = {
		factor = 1000
	}
	effect = {
		add_prestige = 50
		add_stability_2 = yes
	}	
}


spanish_netherlands_should_be_catholic = {

	type = country
	
	category = ADM
	
	allow = {
		tag = SPA
		is_at_war = no
		religion = catholic
		low_countries_superregion = {
			owned_by = ROOT
			religion_group = christian
			NOT = { religion = catholic }
		}
	}
	abort = {
		OR = {
			NOT = { religion = catholic }
			NOT = {
				low_countries_superregion = {
					owned_by = ROOT
				}
			}
		}
	}
	success = {
		NOT = {
			low_countries_superregion = {
				owned_by = ROOT
				NOT = { religion = catholic }
			}
		}	
	}
	chance = {
		factor = 1000
	}
	effect = {
		add_stability_1 = yes
		add_prestige = 5
	}
}


spanish_portugese_relations = {

	type = country
	
	category = DIP
	
	allow = {
		OR = {
			tag = CAS
			tag = SPA
		}
		exists = POR
		government = monarchy
		NOT = { has_opinion = { who = POR value = 50 } }
		NOT = { has_country_modifier = foreign_contacts }
		POR = {
			is_subject = no
			government = monarchy
		}
		NOT = { marriage_with = POR }
		NOT = { war_with = POR }
		NOT = { FROM = { is_rival = ROOT } }
		NOT = { ROOT = { is_rival = FROM } }
	}
	abort = {
		OR = {
			NOT = { government = monarchy }
			war_with = POR
			NOT = { exists = POR }
			POR = {
				is_subject = yes
				NOT = { government = monarchy }
			}
		}
	}
	success = {
		POR = { has_opinion = { who = ROOT value = 100 } }
		marriage_with = POR
	}
	chance = {
		factor = 1000
	}
	effect = {
		add_prestige = 10
		add_country_modifier = {
				name = "foreign_contacts"
				duration = 3650
			}
	}
}


mediterranean_christian_fleet = {

	type = country
	
	category = ADM
	
	allow = {
		tag = SPA
		NOT = { is_year = 1700 }
		exists = TUR
		126 = {
			OR = {
				owned_by = ROOT
				owned_by = KNI
			}
		}
		TUR = {
			num_of_galley = 10
			num_of_ports = 10
			num_of_galley = ROOT
		}		
	}
	abort = {
		NOT = { exists = TUR }
	}
	success = {
		TUR = {
			NOT = { num_of_galley = ROOT }
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}	
	}
	effect = {
		add_navy_tradition = 10
		add_country_modifier = {
			name = "mediterranean_ambition"
			duration = 3650
		}
	}
}


no_territory_to_france = {

	type = country
	
	category = MIL
	
	allow = {
		tag = SPA
		iberia_superregion = { owned_by = FRA }
		NOT = { alliance_with = FRA }
		FRA = {
			is_neighbor_of = ROOT
			num_of_infantry = ROOT
			num_of_cavalry = ROOT
		}
	}
	abort = {
		NOT = { iberia_superregion = { owned_by = FRA } }
	}
	success = {
		NOT = { war_with = FRA }
		NOT = { iberia_superregion = { owned_by = FRA } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}	
	}
	effect = {
		add_prestige = 10
		add_war_exhaustion = -5
	}
}
