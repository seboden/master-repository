# Papal States Missions

conquer_ferrara = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = PAP
		is_subject = no
		1378 = {	# Ferrara
			NOT = { owned_by = ROOT }
			owner = { is_papal_controller = no }
		}
	}
	abort = {
		OR = {
			is_subject = yes
			1378 = {
				owner = { is_papal_controller = yes }
				NOT = { owned_by = ROOT }
			}
		}
	}
	success = {
		owns = 1378
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			adm = 4
		}
	}
	immediate = {
		add_claim = 1378
	}
	abort_effect = {
		remove_claim = 1378
	}
	effect = {
		add_prestige = 5
		1378 = { 
			add_province_modifier = {
			name = "faster_integration"
			duration = 3650
			}
		}
		if = {
			limit = {
				1378 = { NOT = { is_core = ROOT } }
			}
			1378 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}


conquer_ancona = {
	
	type = country

	category = MIL
	ai_mission = yes
	
	allow = {
		tag = PAP
		is_subject = no
		owns = 1378		# Ferrara
		105 = { # Ancona
			NOT = { owned_by = ROOT }
			owner = { is_papal_controller = no }
		}
	}
	abort = {
		OR = {
			is_subject = yes
			105 = {
				owner = { is_papal_controller = yes }
				NOT = { owned_by = ROOT }
			}
		}
	}
	success = {
		owns = 105
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			adm = 4
		}
	}
	immediate = {
		add_claim = 105
	}
	abort_effect = {
		remove_claim = 105
	}
	effect = {
		add_prestige = 5
		105 = { 
			add_province_modifier = {
			name = "faster_integration"
			duration = 3650
			}
		}
		if = {
			limit = {
				105 = { NOT = { is_core = ROOT } }
			}
			105 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}


annex_avignon = {
	
	type = country

	category = MIL
	
	allow = {
		tag = PAP
		is_subject = no
		AVI = {
			owns = 202		# Avignon
			religion_group = ROOT
			is_papal_controller = no
		}
	}
	abort = {
		OR = {
			is_subject = yes
			AVI = {
				OR = {
					NOT = { religion_group = ROOT }
					is_papal_controller = yes
				}
			}
		}
	}
	success = {
		owns = 202
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			mil = 4
		}
		modifier = {
			factor = 2
			adm = 4
		}
	}
	immediate = {
		add_claim = 202
	}
	abort_effect = {
		remove_claim = 202
	}
	effect = {
		add_prestige = 10
		add_dip_power = 30
		202 = { 
			add_province_modifier = {
			name = "faster_integration"
			duration = 3650
			}
		}
		if = {
			limit = {
				202 = { NOT = { is_core = ROOT } }
			}
			202 = {
				add_province_modifier = {
					name = "gaining_control"
					duration = -1
				}
			}
		}
	}
}


catholic_italy = {

	type = country

	category = ADM
	
	allow = {
		tag = PAP
		is_subject = no
		is_at_war = no
		NOT = { has_country_flag = catholic_italy_done }
		italy_region = { NOT = { religion = catholic } }
	}
	abort = {
		is_subject = yes
	}
	success = {
		NOT = { italy_region = { NOT = { religion = catholic } } }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 10
			dip = 4
		}
	}
	effect = {
		add_prestige = 25
		add_army_tradition = 50
		set_country_flag = catholic_italy_done
		add_country_modifier = {
			name = "catholic_italy_modifier"
			duration = 3650
		}
	}
}


academy_in_rome = {
	
	type = country

	category = ADM
	
	allow = {
		tag = PAP
		owns = 2530	# Rome
		adm_tech = 19
		2530 = {
			NOT = { has_building = fine_arts_academy }
			NOT = { has_building = refinery }
			NOT = { has_building = wharf }
			NOT = { has_building = weapons }
			NOT = { has_building = textile }
			}
	}
	abort = {
		OR = {
			2530 = { has_building = refinery }
			2530 = { has_building = weapons }
			2530 = { has_building = wharf }
			2530 = { has_building = textile }
			NOT = { owns = 2530 }
		}
	}
	success = {
		2530 = { has_building = fine_arts_academy }
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2
			advisor = statesman
		}
		modifier = {
			factor = 2
			statesman = 3
		}
	}
	effect = {
		add_prestige = 10
		define_advisor = { type = artist skill = 2 discount = yes }
	}
}
