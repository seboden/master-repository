# A region can only belong to one super region.

france_superregion = {
	france_region
	provence_region
	languedoc_region
	brittany_region
	high_countries_region
}

iberia_superregion = {
	aragon_region
	castille_region
	leon_region
	andalucia_region
	extremadura_region
	navarra_region
	portugal_region
	macaronesia_region
}

italy_superregion = {
	italy_region
	sicily_region
}

british_isles_superregion = {
	england_region
	scotland_region
	ireland_region
}

scandinavian_superregion = {
	swedish_region
	danish_region
	norwegian_region
	finland_region
}

low_countries_superregion = {
	belgii_region
	low_countries_region
}

germany_superregion = {
	lower_saxon_circle_region
	upper_saxon_circle_region
	westphalian_circle_region
	upper_rhenish_circle_region
	swabian_circle_region
	bavarian_circle_region
	franconian_circle_region
}

commonwealth_superregion = {
	baltic_region
	poland_region
	ruthenia_region
}

balkan_superregion = {
	west_balkan_region
	east_balkan_region
	greece_region
	magyar_region
	carpathia_region
}

##########################

north_india_superregion = {
	bengal_region
	hindusthan_region
	rajputana_region
	west_india_region
	central_india_region
}

south_india_superregion = {
	deccan_region
	coromandel_region
	western_ghats_region
}

east_indies_superregion = {
	burma_region
	vietnam_region
	nam_tien_region
	lan_xang_region
	angkor_region
	siam_region
	malaya_region
}

central_asia_superregion = {
	kazakh_region
	uzbek_region
	central_asia_region
}

china_superregion = {
	north_china_region
	xinan_region
	south_china_region
}

fareast_superregion = {
	japan_region
	korea_region
}

east_africa_superregion = {
	horn_of_africa_region
	east_africa_region
	monomotapa_region
	madagascar_region
	sudan_region
	ethiopia_region
}

west_africa_superregion = {
	west_africa_region
	central_africa_region
	kongo_region
	middle_africa_region
	mali_region
}

north_africa_superregion = {
	egypt_region
	maghreb_region
}

siberia_superregion = {
	west_siberia_region
	east_siberia_region
}

south_america_superregion = {
	brazil_region
	la_plata_region
	chile_region
	colombia_region
	peru_region
}

north_america_superregion = {
	hudson_bay_region
	prairies_region
	canada_region
	maritimes_region
}

east_america_superregion = {
	great_plains_region
	great_lakes_region
	mississippi_region
	southeast_america_region
	northeast_america_region
}

west_america_superregion = {
	cascadia_region
	california_region
}

central_america_superregion = {
	central_america_region
	mexico_region
	carribeans_region
}

near_east_superregion = {
	coastal_anatolia_region
	inland_anatolia_region
	mashriq_region
	caucasia_region
	arabia_region
}

persia_superregion = {
	khorasan_region
	baluchistan_region
	persia_region
	afghanistan_region
}

southeast_asia_superregion = {
	sumatra_region
	java_region
	borneo_region
	indonesia_region
	philippines_region
}

new_world_superregion ={
}
