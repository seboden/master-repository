### The Yixing Revolution ###

### For China ###
# Currently
# Chen, Han, Shun, Xi Jin, Liang, #Liao Min, Ming, Ning, Qi, Qin, #Qing, Shen, Shu, Song, Wei, Wu, Yan, Yu, Yue, Zhou, Xia, Tang, Chu
# CEN DAA DSH DXI JIN LNG #CLI CMN MNG CNG QII #QNG CSE SHU SNG XNG WUU CYN CYU YUE ZOU XIA CTA ZHE 22(+2 unused)
country_event = {
	id = "tianxia.35"
	title = "tianxia.35.t"
	desc = "tianxia.35.d"
	picture = COURT_eventPicture
	
	is_triggered_only = yes

	trigger = {
		culture_group = chinese_group
		NOT = { primary_culture = lussong }
		has_new_dynasty = yes
		government_rank = 5
		NOT = { 
			has_ruler_flag = yixing_revolution
			has_country_flag = jingnan_revolter
		}
	}

	option = {
		name = "tianxia.35a"
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
		ai_chance = { factor = 0 }
	}
	option = {
		name = "tianxia.35b"
		trigger = {
			NOT = { exists = CEN }
		}
		change_tag = CEN
		every_owned_province = {
			limit = { is_core = owner }
			add_core = CEN
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = CEN
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35c"
		trigger = {
			NOT = { exists = DAA }
		}
		change_tag = DAA
		every_owned_province = {
			limit = { is_core = owner }
			add_core = DAA
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = DAA
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35d"
		trigger = {
			NOT = { exists = DSH }
		}
		change_tag = DSH
		every_owned_province = {
			limit = { is_core = owner }
			add_core = DSH
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = DSH
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35e"
		trigger = {
			NOT = { exists = DXI }
		}
		change_tag = DXI
		every_owned_province = {
			limit = { is_core = owner }
			add_core = DXI
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = DXI
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35f"
		trigger = {
			NOT = { exists = JIN }
		}
		change_tag = JIN
		every_owned_province = {
			limit = { is_core = owner }
			add_core = JIN
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = JIN
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35g"
		trigger = {
			NOT = { exists = LNG }
		}
		change_tag = LNG
		every_owned_province = {
			limit = { is_core = owner }
			add_core = LNG
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = LNG
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35h"
		trigger = {
			NOT = { exists = CMN }
		}
		change_tag = CMN
		every_owned_province = {
			limit = { is_core = owner }
			add_core = CMN
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = CMN
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35i"
		trigger = {
			NOT = { exists = MNG }
		}
		change_tag = MNG
		every_owned_province = {
			limit = { is_core = owner }
			add_core = MNG
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = MNG
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35j"
		trigger = {
			NOT = { exists = CNG }
		}
		change_tag = CNG
		every_owned_province = {
			limit = { is_core = owner }
			add_core = CNG
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = CNG
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35k"
		trigger = {
			NOT = { exists = QII }
		}
		change_tag = QII
		every_owned_province = {
			limit = { is_core = owner }
			add_core = QII
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = QII
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35l"
		trigger = {
			NOT = { exists = QIN }
		}
		change_tag = QIN
		every_owned_province = {
			limit = { is_core = owner }
			add_core = QIN
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = QIN
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35m"
		trigger = {
			NOT = { exists = CSE }
		}
		change_tag = CSE
		every_owned_province = {
			limit = { is_core = owner }
			add_core = CSE
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = CSE
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35n"
		trigger = {
			NOT = { exists = SHU }
		}
		change_tag = SHU
		every_owned_province = {
			limit = { is_core = owner }
			add_core = SHU
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = SHU
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35o"
		trigger = {
			NOT = { exists = SNG }
		}
		change_tag = SNG
		every_owned_province = {
			limit = { is_core = owner }
			add_core = SNG
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = SNG
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35p"
		trigger = {
			NOT = { exists = WUU }
		}
		change_tag = WUU
		every_owned_province = {
			limit = { is_core = owner }
			add_core = WUU
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = WUU
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35q"
		trigger = {
			NOT = { exists = XNG }
		}
		change_tag = XNG
		every_owned_province = {
			limit = { is_core = owner }
			add_core = XNG
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = XNG
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35r"
		trigger = {
			NOT = { exists = CYN }
		}
		change_tag = CYN
		every_owned_province = {
			limit = { is_core = owner }
			add_core = CYN
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = CYN
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35s"
		trigger = {
			NOT = { exists = CYU }
		}
		change_tag = CYU
		every_owned_province = {
			limit = { is_core = owner }
			add_core = CYU
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = CYU
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35t"
		trigger = {
			NOT = { exists = YUE }
		}
		change_tag = YUE
		every_owned_province = {
			limit = { is_core = owner }
			add_core = YUE
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = YUE
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35u"
		trigger = {
			NOT = { exists = ZOU }
		}
		change_tag = ZOU
		every_owned_province = {
			limit = { is_core = owner }
			add_core = ZOU
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = ZOU
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35v"
		trigger = {
			NOT = { exists = XIA }
		}
		change_tag = XIA
		every_owned_province = {
			limit = { is_core = owner }
			add_core = XIA
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = XIA
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35w"
		trigger = {
			NOT = { exists = CTA }
		}
		change_tag = CTA
		every_owned_province = {
			limit = { is_core = owner }
			add_core = CTA
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = CTA
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35x"
		trigger = {
			NOT = { exists = ZHE }
		}
		change_tag = ZHE
		every_owned_province = {
			limit = { is_core = owner }
			add_core = ZHE
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = ZHE
		}
		if = {
			limit = {
				has_new_dynasty = yes
			}
			set_ruler_flag = yixing_revolution
		}
	}
	option = {
		name = "tianxia.35y"
		trigger = {
			NOT = { exists = CLI }
		}
		change_tag = CLI
		every_owned_province = {
			limit = { is_core = owner }
			add_core = CLI
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = CLI
		}
		set_ruler_flag = yixing_revolution
	}
}
### Barbarian dynasties ###
#Tibetans
#country_event = {
#	id = "tianxia.41"
#	title = "tianxia.41.t"
#	desc = "tianxia.41.d"
#	picture = COURT_eventPicture
#	
#	is_triggered_only = yes
#
#	option = {
#		name = "tianxia.41a"
#		#change_tag = TIB #What tag for Tibet?
#		ai_chance = { factor = 1 }
#	}
#	option = {
#		name = "tianxia.35a"
#		ai_chance = { factor = 0 }
#	}
#	option = {
#		name = "tianxia.35zz"
#		country_event = { id = tianxia.35 days = 0 }
#		ai_chance = { 
#			factor = 1
#			modifier = {
#				factor = 0
#				NOT = { exists = TIB }
#			}
#		}
#	}
#}
#Qiang
#country_event = {
#	id = "tianxia.42"
#	title = "tianxia.41.t"
#	desc = "tianxia.42.d"
#	picture = COURT_eventPicture
#	
#	is_triggered_only = yes
#
#	option = {
#		name = "tianxia.35v"
#		trigger = {
#			NOT = { exists = XIA }
#		}
#		change_tag = XIA
#		ai_chance = { factor = 1 }
#	}
#	option = {
#		name = "tianxia.35l"
#		trigger = {
#			NOT = { exists = QIN }
#		}
#		change_tag = QIN
#		ai_chance = { factor = 1 }
#	}
#	option = {
#		name = "tianxia.35a"
#		ai_chance = { factor = 0 }
#	}
#	option = {
#		name = "tianxia.35zz"
#		country_event = { id = tianxia.35 days = 0 }
#		ai_chance = { 
#			factor = 1
#			modifier = {
#				factor = 0
#				OR = {
#					NOT = {	exists = XIA }
#					NOT = { exists = QIN }
#				}
#			}
#		}
#	}
#}
#Koreans
country_event = {
	id = "tianxia.43"
	title = "tianxia.41.t"
	desc = "tianxia.43.d"
	picture = COURT_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "tianxia.35m"
		trigger = {
			NOT = { exists = CSE }
		}
		change_tag = CSE
		ai_chance = { factor = 1 }
	}
	option = {
		name = "tianxia.35a"
		ai_chance = { factor = 0 }
	}
	option = {
		name = "tianxia.35zz"
		country_event = { id = tianxia.35 days = 0 }
		ai_chance = { 
			factor = 1
			modifier = {
				factor = 0
				NOT = { exists = CSE }
			}
		}
	}
}
#Jurchens
#ountry_event = {
#	id = "tianxia.44"
#	title = "tianxia.41.t"
#	desc = "tianxia.44.d"
#	picture = COURT_eventPicture
#	
#	is_triggered_only = yes
#
#	option = {
#		name = "tianxia.35f"
#		trigger = {
#			NOT = { exists = JIN }
#		}
#		change_tag = JIN
#		ai_chance = { factor = 1 }
#	}
#	option = {
#		name = "tianxia.35a"
#		ai_chance = { factor = 0 }
#	}
#	option = {
#		name = "tianxia.35zz"
#		country_event = { id = tianxia.35 days = 0 }
#		ai_chance = { 
#			factor = 1
#			modifier = {
#				factor = 0
#				NOT = { exists = JIN }
#			}
#		}
#	}
#}
#Ongguds (Shatuo Turks)
#country_event = {
#	id = "tianxia.45"
#	title = "tianxia.41.t"
#	desc = "tianxia.45.d"
#	picture = COURT_eventPicture
#	
#	is_triggered_only = yes
#
#	option = {
#		name = "tianxia.35w"
#		trigger = {
#			NOT = { exists = CTA }
#		}
#		change_tag = CTA
#		ai_chance = { factor = 1 }
#	}
#	option = {
#		name = "tianxia.35f"
#		trigger = {
#			NOT = { exists = JIN }
#		}
#		change_tag = JIN
#		ai_chance = { factor = 1 }
#	}
#	option = {
#		name = "tianxia.35d"
#		trigger = {
#			NOT = { exists = DAA }
#		}
#		change_tag = DAA
#		ai_chance = { factor = 1 }
#	}
#	option = {
#		name = "tianxia.35a"
#		ai_chance = { factor = 0 }
#	}
#	option = {
#		name = "tianxia.35zz"
#		country_event = { id = tianxia.35 days = 0 }
#		ai_chance = { 
#			factor = 1
#			modifier = {
#				factor = 0
#				OR = {
#					NOT = {	exists = CTA }
#					NOT = { exists = JIN }
#					NOT = { exists = DAA }
#				}
#			}
#		}
#	}
#}
#Vietnamese and Zuqun
country_event = {
	id = "tianxia.46"
	title = "tianxia.41.t"
	desc = "tianxia.46.d"
	picture = COURT_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "tianxia.35t"
		trigger = {
			NOT = { exists = YUE }
		}
		change_tag = YUE
		ai_chance = { factor = 1 }
	}
	option = {
		name = "tianxia.35a"
		ai_chance = { factor = 0 }
	}
	option = {
		name = "tianxia.35zz"
		country_event = { id = tianxia.35 days = 0 }
		ai_chance = { 
			factor = 1
			modifier = {
				factor = 0
				NOT = { exists = YUE }
			}
		}
	}
}
#Daurs
country_event = {
	id = "tianxia.47"
	title = "tianxia.41.t"
	desc = "tianxia.47.d"
	picture = COURT_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "tianxia.35t"
		trigger = {
			NOT = { exists = CLI }
		}
		change_tag = CLI
		ai_chance = { factor = 1 }
	}
	option = {
		name = "tianxia.35a"
		ai_chance = { factor = 0 }
	}
	option = {
		name = "tianxia.35zz"
		country_event = { id = tianxia.35 days = 0 }
		ai_chance = { 
			factor = 1
			modifier = {
				factor = 0
				NOT = { exists = CLI }
			}
		}
	}
}
#Japanese - for now Wu, new tag needed
country_event = {
	id = "tianxia.48"
	title = "tianxia.41.t"
	desc = "tianxia.48.d"
	picture = COURT_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "tianxia.35p"
		trigger = {
			NOT = { exists = WUU }
		}
		change_tag = WUU
		ai_chance = { factor = 1 }
	}
	option = {
		name = "tianxia.35a"
		ai_chance = { factor = 0 }
	}
	option = {
		name = "tianxia.35zz"
		country_event = { id = tianxia.35 days = 0 }
		ai_chance = { 
			factor = 1
			modifier = {
				factor = 0
				NOT = { exists = WUU }
			}
		}
	}
}
#Timurids
#country_event = {
#	id = "tianxia.49"
#	title = "tianxia.41.t"
#	desc = "tianxia.49.d"
#	picture = COURT_eventPicture
#	
#	is_triggered_only = yes
#
#	#option = {
#	#	name = "tianxia.49a"
#	#	trigger = {
#	#		NOT = { exists = HUI }
#	#	}
#	#	change_tag = HUI
#	#	ai_chance = { factor = 1 }
#	#}
#	option = {
#		name = "tianxia.35a"
#		ai_chance = { factor = 0 }
#	}
#	option = {
#		name = "tianxia.35zz"
#		country_event = { id = tianxia.35 days = 0 }
#	#	ai_chance = { 
#	#		factor = 1
#	#		modifier = {
#	#			factor = 0
#	#			NOT = { exists = HUI }
#	#		}
#	#	}
#	}
#}
country_event = {
	id = "tianxia.50"
	title = "tianxia.35.t"
	desc = "tianxia.35.d"
	picture = COURT_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "tianxia.35a"
		ai_chance = { factor = 0 }
	}
	option = {
		name = "tianxia.35b"
		trigger = {
			NOT = { exists = CEN }
		}
		change_tag = CEN
		every_owned_province = {
			limit = { is_core = owner }
			add_core = CEN
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = CEN
		}
	}
	option = {
		name = "tianxia.35c"
		trigger = {
			NOT = { exists = DAA }
		}
		change_tag = DAA
		every_owned_province = {
			limit = { is_core = owner }
			add_core = DAA
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = DAA
		}
	}
	option = {
		name = "tianxia.35d"
		trigger = {
			NOT = { exists = DSH }
		}
		change_tag = DSH
		every_owned_province = {
			limit = { is_core = owner }
			add_core = DSH
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = DSH
		}
	}
	option = {
		name = "tianxia.35e"
		trigger = {
			NOT = { exists = DXI }
		}
		change_tag = DXI
		every_owned_province = {
			limit = { is_core = owner }
			add_core = DXI
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = DXI
		}
	}
	option = {
		name = "tianxia.35zz"
		set_country_flag = yixing_option
		country_event = { id = tianxia.51 days = 0 }
	}
}
country_event = {
	id = "tianxia.51"
	title = "tianxia.35.t"
	desc = "tianxia.35.d"
	picture = COURT_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "tianxia.35f"
		trigger = {
			NOT = { exists = JIN }
		}
		change_tag = JIN
		every_owned_province = {
			limit = { is_core = owner }
			add_core = JIN
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = JIN
		}
	}
	option = {
		name = "tianxia.35g"
		trigger = {
			NOT = { exists = LNG }
		}
		change_tag = LNG
		every_owned_province = {
			limit = { is_core = owner }
			add_core = LNG
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = LNG
		}
	}
	option = {
		name = "tianxia.35h"
		trigger = {
			NOT = { exists = CMN }
		}
		change_tag = CMN
		every_owned_province = {
			limit = { is_core = owner }
			add_core = CMN
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = CMN
		}
	}
	option = {
		name = "tianxia.35i"
		trigger = {
			NOT = { exists = MNG }
		}
		change_tag = MNG
		every_owned_province = {
			limit = { is_core = owner }
			add_core = MNG
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = MNG
		}
	}
	option = {
		name = "tianxia.35j"
		trigger = {
			NOT = { exists = CNG }
		}
		change_tag = CNG
		every_owned_province = {
			limit = { is_core = owner }
			add_core = CNG
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = CNG
		}
	}
	option = {
		name = "tianxia.35zz"
		country_event = { id = tianxia.52 days = 0 }
	}
}
country_event = {
	id = "tianxia.52"
	title = "tianxia.35.t"
	desc = "tianxia.35.d"
	picture = COURT_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "tianxia.35k"
		trigger = {
			NOT = { exists = QII }
		}
		change_tag = QII
		every_owned_province = {
			limit = { is_core = owner }
			add_core = QII
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = QII
		}
	}
	option = {
		name = "tianxia.35l"
		trigger = {
			NOT = { exists = QIN }
		}
		change_tag = QIN
		every_owned_province = {
			limit = { is_core = owner }
			add_core = QIN
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = QIN
		}
	}
	option = {
		name = "tianxia.35m"
		trigger = {
			NOT = { exists = CSE }
		}
		change_tag = CSE
		every_owned_province = {
			limit = { is_core = owner }
			add_core = CSE
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = CSE
		}
	}
	option = {
		name = "tianxia.35n"
		trigger = {
			NOT = { exists = SHU }
		}
		change_tag = SHU
		every_owned_province = {
			limit = { is_core = owner }
			add_core = SHU
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = SHU
		}
	}
	option = {
		name = "tianxia.35o"
		trigger = {
			NOT = { exists = SNG }
		}
		change_tag = SNG
		every_owned_province = {
			limit = { is_core = owner }
			add_core = SNG
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = SNG
		}
	}
	option = {
		name = "tianxia.35zz"
		 country_event = { id = tianxia.53 days = 0 }
	}
}
country_event = {
	id = "tianxia.53"
	title = "tianxia.35.t"
	desc = "tianxia.35.d"
	picture = COURT_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "tianxia.35p"
		trigger = {
			NOT = { exists = WUU }
		}
		change_tag = WUU
		every_owned_province = {
			limit = { is_core = owner }
			add_core = WUU
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = WUU
		}
	}
	option = {
		name = "tianxia.35q"
		trigger = {
			NOT = { exists = XNG }
		}
		change_tag = XNG
		every_owned_province = {
			limit = { is_core = owner }
			add_core = XNG
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = XNG
		}
	}
	option = {
		name = "tianxia.35r"
		trigger = {
			NOT = { exists = CYN }
		}
		change_tag = CYN
		every_owned_province = {
			limit = { is_core = owner }
			add_core = CYN
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = CYN
		}
	}
	option = {
		name = "tianxia.35s"
		trigger = {
			NOT = { exists = CYU }
		}
		change_tag = CYU
		every_owned_province = {
			limit = { is_core = owner }
			add_core = CYU
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = CYU
		}
	}
	option = {
		name = "tianxia.35t"
		trigger = {
			NOT = { exists = YUE }
		}
		change_tag = YUE
		every_owned_province = {
			limit = { is_core = owner }
			add_core = YUE
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = YUE
		}
	}
	option = {
		name = "tianxia.35zz"
		country_event = { id = tianxia.54 days = 0 }
	}
}
country_event = {
	id = "tianxia.54"
	title = "tianxia.35.t"
	desc = "tianxia.35.d"
	picture = COURT_eventPicture
	
	is_triggered_only = yes

	option = {
		name = "tianxia.35u"
		trigger = {
			NOT = { exists = ZOU }
		}
		change_tag = ZOU
		every_owned_province = {
			limit = { is_core = owner }
			add_core = ZOU
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = ZOU
		}
	}
	option = {
		name = "tianxia.35v"
		trigger = {
			NOT = { exists = XIA }
		}
		change_tag = XIA
		every_owned_province = {
			limit = { is_core = owner }
			add_core = XIA
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = XIA
		}
	}
	option = {
		name = "tianxia.35w"
		trigger = {
			NOT = { exists = CTA }
		}
		change_tag = CTA
		every_owned_province = {
			limit = { is_core = owner }
			add_core = CTA
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = CTA
		}
	}
	option = {
		name = "tianxia.35x"
		trigger = {
			NOT = { exists = ZHE }
		}
		change_tag = ZHE
		every_owned_province = {
			limit = { is_core = owner }
			add_core = ZHE
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = ZHE
		}
	}
	option = {
		name = "tianxia.35y"
		trigger = {
			NOT = { exists = CLI }
		}
		change_tag = CLI
		every_owned_province = {
			limit = { is_core = owner }
			add_core = CLI
		}
		every_owned_province = {
			limit = { owned_by = ROOT }
			add_core = CLI
		}
	}
	option = {
		name = "tianxia.35zz"
		if = {
			limit = {
				has_country_flag = yixing_option
			}
			clr_country_flag = yixing_option
		}
		country_event = { id = tianxia.50 days = 0 }
	}
}
