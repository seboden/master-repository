
country_event = {
	id = rank_reduction.1
	title = rank_reduction.1t
	desc = rank_reduction.1d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		government_rank = 2
		NOT = { government_rank = 3 }
		NOT = { num_of_cities = 2 }
		NOT = { culture_group = japanese }
	}
	
	option = {
		name = rank_reduction.1a 
		set_government_rank = 1
		add_prestige = -5
	}
}

country_event = {
	id = rank_reduction.2
	title = rank_reduction.1t
	desc = rank_reduction.1d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		government_rank = 3
		NOT = { government_rank = 4 }
		NOT = { num_of_cities = 3 }
		NOT = { culture_group = japanese }
	}
	
	option = {
		name = rank_reduction.1a 
		set_government_rank = 2
		add_prestige = -5
	}
}

country_event = {
	id = rank_reduction.3
	title = rank_reduction.1t
	desc = rank_reduction.1d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		government_rank = 4
		NOT = { government_rank = 5 }
		NOT = { num_of_cities = 8 }
		NOT = { culture_group = japanese }
	}
	
	option = {
		name = rank_reduction.1a 
		set_government_rank = 3
		add_prestige = -5
	}
}

country_event = {
	id = rank_reduction.4
	title = rank_reduction.1t
	desc = rank_reduction.1d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		government_rank = 5
		NOT = { tag = DEN }
		NOT = { tag = DAN }
		NOT = { tag = SWE }
		NOT = { tag = NOR }
		NOT = { tag = SCO }
		NOT = { tag = ENG }
		NOT = { tag = FRA }
		NOT = { tag = CAS }
		NOT = { tag = LEO }
		NOT = { tag = ARA }
		NOT = { tag = SIC }
		NOT = { tag = KNP }
		NOT = { tag = NAP }
		NOT = { tag = HUN }
		NOT = { tag = BOH }
		NOT = { tag = POL }
		NOT = { tag = POR }
		NOT = { government_rank = 6 }
		NOT = { num_of_cities = 12 }
		NOT = { culture_group = japanese }
	}
	
	option = {
		name = rank_reduction.1a 
		set_government_rank = 4
		add_prestige = -5
	}
}

country_event = {
	id = rank_reduction.5
	title = rank_reduction.1t
	desc = rank_reduction.1d
	picture = COUNTRY_COLLAPSE_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		government_rank = 6
		NOT = { num_of_cities = 75 }
		NOT = { culture_group = japanese }
	}
	
	option = {
		name = rank_reduction.1a 
		set_government_rank = 5
		add_prestige = -5
	}
}


