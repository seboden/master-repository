#By 1530 the Hohenzollerns of Brandenburg had secured the rights to succced the Gryfs in Pommerania, and the families other holdings within the empire recognized them as the senion branch of the family. This ensures that, should the Pommeranians or the other Hohenzollerns die out and Brandenburg is still ruled by Hohenzollerns, they will recieve the land

country_event = {
	id = hohenzollerns_1530.1
	
	title = "hohenzollerns_1530.1.t"
	desc = "hohenzollerns_1530.1.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	trigger = {
		has_country_flag = hohenzollern_succession
		tag = POM
		OR = {
			NOT = { dynasty = "Gryf" }
			is_female = yes
		}
		BRA = {
			dynasty = "von Hohenzollern"
		}
		PRU = {
			OR = {
				exists = no
				has_country_flag = hohenzollern_succession
			}
		}
	}
	
	option = {
		name = "hindustani_unification.2a"
		clr_country_flag = hohenzollern_succession
		BRA = {
			inherit = ROOT
		}
	}
}
country_event = {
	id = hohenzollerns_1530.2
	
	title = "hohenzollerns_1530.1.t"
	desc = "hohenzollerns_1530.1.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	trigger = {
		has_country_flag = hohenzollern_succession
		tag = POM
		OR = {
			NOT = { dynasty = "Gryf" }
			is_female = yes
		}
		PRU = {
			dynasty = "von Hohenzollern"
			has_country_flag = hohenzollern_succession_secured
		}
	}
	
	option = {
		name = "hindustani_unification.2a"
		clr_country_flag = hohenzollern_succession
		PRU = {
			inherit = ROOT
		}
	}
}
country_event = {
	id = hohenzollerns_1530.3
	
	title = "hohenzollerns_1530.1.t"
	desc = "hohenzollerns_1530.1.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	trigger = {
		has_country_flag = hohenzollern_succession
		OR = {
			tag = BYR
			tag = ANS
			tag = HOH
		}
		OR = {
			NOT = { dynasty = "von Hohenzollern" }
			is_female = yes
		}
		BRA = {
			dynasty = "von Hohenzollern"
			has_country_flag = hohenzollern_succession_secured
		}
		PRU = {
			OR = {
				exists = no
				has_country_flag = hohenzollern_succession
			}
		}
	}
	
	option = {
		name = "hindustani_unification.2a"
		clr_country_flag = hohenzollern_succession
		BRA = {
			inherit = ROOT
		}
	}
}
country_event = {
	id = hohenzollerns_1530.4
	
	title = "hohenzollerns_1530.1.t"
	desc = "hohenzollerns_1530.1.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	trigger = {
		has_country_flag = hohenzollern_succession
		OR = {
			tag = BYR
			tag = ANS
			tag = HOH
		}
		OR = {
			NOT = { dynasty = "von Hohenzollern" }
			is_female = yes
		}
		OR = {
			AND = {
				PRU = {
					dynasty = "von Hohenzollern"
				}
				not = { exists = BRA }
			}
			PRU = {
				dynasty = "von Hohenzollern"
				has_country_flag = hohenzollern_succession_secured
			}
		}	
	}
	
	option = {
		name = "hindustani_unification.2a"
		clr_country_flag = hohenzollern_succession
		PRU = {
			inherit = ROOT
		}
	}
}
country_event = {
	id = hohenzollerns_1530.5
	
	title = "hohenzollerns_1530.1.t"
	desc = "hohenzollerns_1530.1.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	trigger = {
		has_country_flag = hohenzollern_succession
		tag = PRU
		OR = {
			NOT = { dynasty = "von Hohenzollern" }
			is_female = yes
		}
		BRA = {
			dynasty = "von Hohenzollern"
			has_country_flag = hohenzollern_succession_secured
		}
	}
	
	option = {
		name = "hindustani_unification.2a"
		clr_country_flag = hohenzollern_succession
		every_owned_province = {
			limit = {
				region = prussia_region
			}
			remove_core = PLC
			remove_claim = PLC
			remove_core = POL
			remove_claim = POL
			remove_core = LIT
			remove_claim = LIT
		}
		BRA = {
			inherit = ROOT
		}
	}
}
country_event = {
	id = hohenzollerns_1530.6
	
	title = "hohenzollerns_1530.1.t"
	desc = "hohenzollerns_1530.1.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	trigger = {
		has_country_flag = hohenzollern_succession
		tag = BRA
		OR = {
			NOT = { dynasty = "von Hohenzollern" }
			is_female = yes
		}
		PRU = {
			dynasty = "von Hohenzollern"
		}
	}
	
	option = {
		name = "hindustani_unification.2a"
		clr_country_flag = hohenzollern_succession
		PRU = {
			inherit = ROOT
		}
	}
}
country_event = {
	id = hohenzollerns_1530.7
	
	title = "hohenzollerns_1530.1.t"
	desc = "hohenzollerns_1530.1.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	trigger = {
		has_country_flag = hohenzollern_succession
		tag = BRA
		OR = {
			NOT = { dynasty = "von Hohenzollern" }
			is_female = yes
		}
		HOH = {
			dynasty = "von Hohenzollern"
		}
	}
	
	option = {
		name = "hindustani_unification.2a"
		clr_country_flag = hohenzollern_succession
		HOH = {
			inherit = ROOT
		}
	}
}
country_event = {
	id = hohenzollerns_1530.8
	
	title = "hohenzollerns_1530.1.t"
	desc = "hohenzollerns_1530.1.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	trigger = {
		has_country_flag = hohenzollern_succession
		tag = BRA
		OR = {
			NOT = { dynasty = "von Hohenzollern" }
			is_female = yes
		}
		ANS = {
			dynasty = "von Hohenzollern"
		}
	}
	
	option = {
		name = "hindustani_unification.2a"
		clr_country_flag = hohenzollern_succession
		ANS = {
			inherit = ROOT
		}
	}
}
country_event = {
	id = hohenzollerns_1530.9
	
	title = "hohenzollerns_1530.1.t"
	desc = "hohenzollerns_1530.1.d"
	
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	#hidden = yes
	
	trigger = {
		has_country_flag = hohenzollern_succession
		tag = BRA
		OR = {
			NOT = { dynasty = "von Hohenzollern" }
			is_female = yes
		}
		BYR = {
			dynasty = "von Hohenzollern"
		}
	}
	
	option = {
		name = "hindustani_unification.2a"
		clr_country_flag = hohenzollern_succession
		BYR = {
			inherit = ROOT
		}
	}
}

### Second Peace of Thorn by Vorondil ###


country_event = {
	id = secondpeaceofthorn.1
	title = "secondpeaceofthorn.1.t"
	desc = "secondpeaceofthorn.1.d"
	picture = SIEGE_eventPicture
	
	fire_only_once = yes
	
	trigger = {
		tag = POL
		OR = {
			owns = 2355 #Danzig
			owns = 1844 #Marienburg
			owns = 41 #Ostpreussen
		}
		TEU = {
			NOT = { war_with = POL }
			is_subject = no
			any_owned_province = {
				region = prussia_region
			}
		}
	}
	
	option = {
		name = "secondpeaceofthorn.1.a"
		every_owned_province = {
			limit = {
				region = prussia_region
				owned_by = ROOT
				NOT = { province_id = 2398 } #Chelmno
			}
			cede_province = TEU
			add_core = TEU
		}
		if = {
			limit = {
				2398 = {
					owned_by = TEU
				}
			}
			2398 = {
				cede_province = POL
				add_core = POL
			}
		}
		
		LIT = {
			if = {
				limit = {
					junior_union_with = POL
				}
				every_owned_province = {
					limit = {
						region = prussia_region
						owned_by = THIS
						NOT = { province_id = 2398 } #Chelmno
					}
					remove_core = THIS
					cede_province = TEU
					add_core = TEU
				}
			}
		}
		country_event = { 
			id = secondpeaceofthorn.2
		}
	}
}

country_event = {
	id = secondpeaceofthorn.2	
	title = "secondpeaceofthorn.2.t"
	desc = "secondpeaceofthorn.2.d"
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "secondpeaceofthorn.2.a"
		ai_chance = { factor = 100 }
		TEU = {
			country_event = {
				id = secondpeaceofthorn.3
			}
		}
		add_prestige = 25
		hidden_effect = { vassalize = TEU }
		create_march = TEU	
	}
	
	option = {
		name = "secondpeaceofthorn.2.b"
		ai_chance = { factor = 0 }
		add_prestige = -25
		add_country_modifier = {
			name = "claimed_whole_prussia"
			duration = 3650
		}
		inherit = TEU
		country_event = {
			id = secondpeaceofthorn.5
		}
	}
}

country_event = {
	id = secondpeaceofthorn.3
	title = "secondpeaceofthorn.3.t"
	desc = "secondpeaceofthorn.3.d"
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "secondpeaceofthorn.3.a"
		ai_chance = { factor = 100 }
		set_capital = 41 # Ostpreussen
		free_vassal = LVO
		every_owned_province = {
			limit = {
				area = west_prussia_area
				owned_by = ROOT
			}
			cede_province = POL
		}
		POL = {
			country_event = {
				id = secondpeaceofthorn.4
			}
		}
	}
}

country_event = {
	id = secondpeaceofthorn.4
	title = "secondpeaceofthorn.4.t"
	desc = "secondpeaceofthorn.4.d"
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "secondpeaceofthorn.4.a"
		every_owned_province = {
			limit = {
				region = prussia_region
				owned_by = ROOT
				NOT = { is_core = ROOT }
			}
			add_nationalism = -15
			add_claim = POL
			add_province_modifier = {
				name = "gaining_control"
				duration = -1
			}
		}
	}
}

country_event = {
	id = secondpeaceofthorn.5
	title = "secondpeaceofthorn.5.t"
	desc = "secondpeaceofthorn.5.d"
	picture = SIEGE_eventPicture
	
	is_triggered_only = yes
	
	option = {
		name = "secondpeaceofthorn.5.a"
		every_owned_province = {
			limit = {
				region = prussia_region
				owned_by = ROOT
				is_core = ROOT
			}
			add_nationalism = 15
			remove_core = ROOT
			add_claim = ROOT
			add_core = TEU
		}
		free_vassal = LVO
	}
}
