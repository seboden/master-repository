namespace = DO_culture_transfer

country_event = {
	id = DO_culture_transfer.001
	title = DO_culture_transfer.001.t
	desc = DO_culture_transfer.001.d
	picture = CORRUPTION_eventPicture
	hidden = yes
	
	trigger = {	 OR = {
					has_country_flag = cultural_exploitation1
					has_country_flag = cultural_exploitation2
					has_country_flag = cultural_exploitation3
					has_country_flag = cultural_exploitation4
					has_country_flag = cultural_exploitation5
					has_country_flag = cultural_exploitation6
					has_country_flag = cultural_exploitation7
					has_country_flag = cultural_exploitation8
				}	
				NOT = { has_country_flag = show_development }
	}
	
	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = DO_culture_transfer.001.a
				if = { 
					limit = { has_country_flag = cultural_exploitation1 }	
					random_owned_province = { 
						limit = {
							is_overseas = no 
							OR = { 
								has_owner_accepted_culture = yes 
							    has_owner_culture = yes
							}
						}
						random_list = {
								3 = { add_base_manpower = 1 }
								3 = { add_base_production = 1 }
								3 = { add_base_tax = 1 }
								91 = {}
						}	
					}
				}
				if = { 
					limit = { has_country_flag = cultural_exploitation2 }	
					random_owned_province = { 
						limit = {
							is_overseas = no 
							OR = { 
								has_owner_accepted_culture = yes 
							    has_owner_culture = yes
							}
						}
						random_list = {
								5 = { add_base_manpower = 1 }
								5 = { add_base_production = 1 }
								5 = { add_base_tax = 1 }
								85 = {}
						}	
					}
				}
				if = { 
					limit = { has_country_flag = cultural_exploitation3 }	
					random_owned_province = { 
						limit = {
							is_overseas = no 
							OR = { 
								has_owner_accepted_culture = yes 
							    has_owner_culture = yes
							}
						}
						random_list = {
								7 = { add_base_manpower = 1 }
								7 = { add_base_production = 1 }
								7 = { add_base_tax = 1 }
								79 = {}
						}	
					}
				}
				if = { 
					limit = { has_country_flag = cultural_exploitation4 }	
					random_owned_province = { 
						limit = {
							is_overseas = no 
							OR = { 
								has_owner_accepted_culture = yes 
							    has_owner_culture = yes
							}
						}
						random_list = {
								9 = { add_base_manpower = 1 }
								9 = { add_base_production = 1 }
								9 = { add_base_tax = 1 }
								73 = {}
						}	
					}
				}
				if = { 
					limit = { has_country_flag = cultural_exploitation5 }	
					random_owned_province = { 
						limit = {
							is_overseas = no 
							OR = { 
								has_owner_accepted_culture = yes 
							    has_owner_culture = yes
							}
						}
						random_list = {
								11 = { add_base_manpower = 1 }
								11 = { add_base_production = 1 }
								11 = { add_base_tax = 1 }
								67 = {}
						}	
					}
				}
				if = { 
					limit = { has_country_flag = cultural_exploitation6 }	
					random_owned_province = { 
						limit = {
							is_overseas = no 
							OR = { 
								has_owner_accepted_culture = yes 
							    has_owner_culture = yes
							}
						}
						random_list = {
								13 = { add_base_manpower = 1 }
								13 = { add_base_production = 1 }
								13 = { add_base_tax = 1 }
								61 = {}
						}	
					}
				}
				if = { 
					limit = { has_country_flag = cultural_exploitation7 }	
					random_owned_province = { 
						limit = {
							is_overseas = no 
							OR = { 
								has_owner_accepted_culture = yes 
							    has_owner_culture = yes
							}
						}
						random_list = {
								15 = { add_base_manpower = 1 }
								15 = { add_base_production = 1 }
								15 = { add_base_tax = 1 }
								55 = {}
						}	
					}
				}
				if = { 
					limit = { has_country_flag = cultural_exploitation7 }	
					random_owned_province = { 
						limit = {
							is_overseas = no 
							OR = { 
								has_owner_accepted_culture = yes 
							    has_owner_culture = yes
							}
						}
						random_list = {
								17 = { add_base_manpower = 1 }
								17 = { add_base_production = 1 }
								17 = { add_base_tax = 1 }
								49 = {}
						}	
					}
				}
						
				hidden_effect = { clr_country_flag =cultural_exploitation1		
									clr_country_flag =cultural_exploitation2
									clr_country_flag =cultural_exploitation3
									clr_country_flag =cultural_exploitation4
									clr_country_flag =cultural_exploitation5
									clr_country_flag =cultural_exploitation6
									clr_country_flag =cultural_exploitation7
									clr_country_flag =cultural_exploitation8
				}

	}
}

country_event = {
	id = DO_culture_transfer.002
	title = DO_culture_transfer.002.t
	desc = DO_culture_transfer.002.d
	picture = CORRUPTION_eventPicture
	hidden = no
	
	trigger = {	 OR = {
					has_country_flag = cultural_exploitation1
					has_country_flag = cultural_exploitation2
					has_country_flag = cultural_exploitation3
					has_country_flag = cultural_exploitation4
					has_country_flag = cultural_exploitation5
					has_country_flag = cultural_exploitation6
					has_country_flag = cultural_exploitation7
					has_country_flag = cultural_exploitation8
				}	
					has_country_flag = show_development
	}
	
	mean_time_to_happen = {
		days = 1
	}

	option = {
		name = DO_culture_transfer.002.a
				if = { 
					limit = { has_country_flag = cultural_exploitation1 }	
					random_owned_province = { 
						limit = {
							is_overseas = no 
							OR = { 
								has_owner_accepted_culture = yes 
							    has_owner_culture = yes
							}
						}
						random_list = {
								3 = { add_base_manpower = 1 }
								3 = { add_base_production = 1 }
								3 = { add_base_tax = 1 }
								91 = {}
						}	
					}
				}
				if = { 
					limit = { has_country_flag = cultural_exploitation2 }	
					random_owned_province = { 
						limit = {
							is_overseas = no 
							OR = { 
								has_owner_accepted_culture = yes 
							    has_owner_culture = yes
							}
						}
						random_list = {
								5 = { add_base_manpower = 1 }
								5 = { add_base_production = 1 }
								5 = { add_base_tax = 1 }
								85 = {}
						}	
					}
				}
				if = { 
					limit = { has_country_flag = cultural_exploitation3 }	
					random_owned_province = { 
						limit = {
							is_overseas = no 
							OR = { 
								has_owner_accepted_culture = yes 
							    has_owner_culture = yes
							}
						}
						random_list = {
								7 = { add_base_manpower = 1 }
								7 = { add_base_production = 1 }
								7 = { add_base_tax = 1 }
								79 = {}
						}	
					}
				}
				if = { 
					limit = { has_country_flag = cultural_exploitation4 }	
					random_owned_province = { 
						limit = {
							is_overseas = no 
							OR = { 
								has_owner_accepted_culture = yes 
							    has_owner_culture = yes
							}
						}
						random_list = {
								9 = { add_base_manpower = 1 }
								9 = { add_base_production = 1 }
								9 = { add_base_tax = 1 }
								73 = {}
						}	
					}
				}
				if = { 
					limit = { has_country_flag = cultural_exploitation5 }	
					random_owned_province = { 
						limit = {
							is_overseas = no 
							OR = { 
								has_owner_accepted_culture = yes 
							    has_owner_culture = yes
							}
						}
						random_list = {
								11 = { add_base_manpower = 1 }
								11 = { add_base_production = 1 }
								11 = { add_base_tax = 1 }
								67 = {}
						}	
					}
				}
				if = { 
					limit = { has_country_flag = cultural_exploitation6 }	
					random_owned_province = { 
						limit = {
							is_overseas = no 
							OR = { 
								has_owner_accepted_culture = yes 
							    has_owner_culture = yes
							}
						}
						random_list = {
								13 = { add_base_manpower = 1 }
								13 = { add_base_production = 1 }
								13 = { add_base_tax = 1 }
								61 = {}
						}	
					}
				}
				if = { 
					limit = { has_country_flag = cultural_exploitation7 }	
					random_owned_province = { 
						limit = {
							is_overseas = no 
							OR = { 
								has_owner_accepted_culture = yes 
							    has_owner_culture = yes
							}
						}
						random_list = {
								15 = { add_base_manpower = 1 }
								15 = { add_base_production = 1 }
								15 = { add_base_tax = 1 }
								55 = {}
						}	
					}
				}
				if = { 
					limit = { has_country_flag = cultural_exploitation7 }	
					random_owned_province = { 
						limit = {
							is_overseas = no 
							OR = { 
								has_owner_accepted_culture = yes 
							    has_owner_culture = yes
							}
						}
						random_list = {
								17 = { add_base_manpower = 1 }
								17 = { add_base_production = 1 }
								17 = { add_base_tax = 1 }
								49 = {}
						}	
					}
				}
						
				hidden_effect = { clr_country_flag =cultural_exploitation1		
									clr_country_flag =cultural_exploitation2
									clr_country_flag =cultural_exploitation3
									clr_country_flag =cultural_exploitation4
									clr_country_flag =cultural_exploitation5
									clr_country_flag =cultural_exploitation6
									clr_country_flag =cultural_exploitation7
									clr_country_flag =cultural_exploitation8
				}

	}
}
