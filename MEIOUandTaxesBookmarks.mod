name="MEIOU and Taxes Bookmarks Module"
path="mod/MEIOUandTaxesBookmarks"
dependencies={
	"MEIOU and Taxes 1.27"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesBM.jpg"
supported_version="1.19.*.*"
