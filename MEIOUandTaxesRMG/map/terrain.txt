##################################################################
### Terrain Categories
###
### Terrain types: plains, mountains, hills, desert, artic, forest, jungle, marsh, pti
### Types are used by the game to apply certain bonuses/maluses on movement/combat etc.
###
### Sound types: plains, forest, desert, sea, jungle, mountains

categories =  {
	pti = {
		type = pti
	}

	ocean = {
		movement_cost = 1.0
		is_water = yes
		sound_type = sea
		color = { 255 255 255 }
	}

	inland_ocean = {
		movement_cost = 1.0
		is_water = yes
		inland_sea = yes
		sound_type = sea
		color = { 0 0 200 }
		
		terrain_override = {
			1851 1853 1856 2782 2784 
		}
	}

	glacier = {
		color = { 235 235 235 }

		sound_type = desert

		defence = 1
		movement_cost = 1.25
		supply_limit = 4		
		local_development_cost = 0.5
		nation_designer_cost_multiplier = 0.75
		
		terrain_override = {
			2194 2193 978 979 2192 2191 977 2190 2189 
		}
	}

		movement_cost = 2.0
		attrition = 2
		supply_limit = 4
		type = arctic
		sound_type = desert
		color = { 235 235 235 }
		
		local_development_cost = 0.5
		nation_designer_cost_multiplier = 0.75
		terrain_override = {
			2194 2193 978 979 2192 2191 977 2190 2189 
		}
	}

	farmlands = {
		movement_cost = 1.00
		
		supply_limit = 8
		type = plains
		sound_type = plains
		color = { 137 104 165  }
		
		allowed_num_of_buildings = 1
		nation_designer_cost_multiplier = 1.1
		terrain_override = {
			105 114 262 398 3084 3085 415 3086 410 1322 3087 3900 3089 1329 1320 3279 440 118 1347
			445 3278 2222 456 457 3374 2330 167 1293 1310 442 3285 2239 174 109 3697 2562 196 363 
			3372 178 168 2362 184 2356 183 1335 88 1428 6 63 2307 87 90 2363 95 98 92 2367 2364 104 2372 3700
			704 705 707 708 1041 2248 2249 2801 733 735 737 2233 730 2607 2149 2150 2125 695 1068
			1172 1337 699 698 2491 703 688 701 706 724 2472 2485 2560 2605 684 700 1046 1089 2477
			712 670 2480 4035 229 561 563 3190 2696 1588 3203 2693 558 3134 3135 3136 3176 3182 3183 3184 3187 
			3188 3189 3191 567 2699 2690 3126 3128 3129 3131 2555 555 524 2705 3123 3127 3115
			3104 3103 3102 3109 510 564 2113 2163 521 511 3111 3199 2112 2360 2370 540 532
			2212 2681 3163 3161 3162 546 545 3174 507 2627 3166 3164 2243 543 2569 3145 542 533 534 2570 3156 2220 
			398 392 3826 1322 410 3087 3085 415 3086 
			358 2498 361 1338 2543 3003 1213 360 1214 4094 4093 
		}
	}
	
	forest = {
		movement_cost = 1.50

		combat_width = -0.35
		supply_limit = 6
		type = forest
		sound_type = forest
		color = { 91 123 45 }
		
		local_development_cost = 0.2
		nation_designer_cost_multiplier = 0.9
		terrain_override = {
			972 2186 4 7 1857 3992 2240 264 3274 1048 1050 727 3247 
			3650 3648 987 3647 3583 845 3582 920 1462 947 3579 3580 
			957 944 1489 1459 960 3637 2198 274 
			1030 1032 2286 2293 2435 3962 3966 3974 
		}
	}
	
	hills = {
		movement_cost = 1.5
		defence = 1
		local_defensiveness = 0.15
		color = { 135 70 0 }

		combat_width = -0.45
		supply_limit = 6
		type = hills
		sound_type = mountain
		
		local_development_cost = 0.25
		nation_designer_cost_multiplier = 0.85
		terrain_override = {
			122 3377 3373 102 3862 1421 2514 1247 127 2852 2242 2241 891 3446 376 791 2534 3872 
			1405 3463 3465 763 3468 3471 3469 760 3774 758 490 487 2188 3672 786 115 116 117 368 
			764 3702 782 1065 3474 1518 1528 3899 3942 1527 1226 3860 1512 1382 207 2385 1414 
			2366 193 2812 1323 194 370 3703 1513 2564 101 1390 3382 130 204 1420 2563 3699 3869 
			2854 2357 1413 3863 212 3368 2308 2471 2251 709 2800 2805 2803 687 2170 736 2804 
			2797 685 2793 1143 1043 2493 2142 692 2490 680 2606 2488 1053 679 681 682 2478 683 
			2802 2731 473 428 120 123 1383 128 3747 129 2383 2473 2313 16 242 3323 1412 1422 
			1508 3263 2716 3106 2532 642 643 644 4045 3928 2432 2433 4044 2858 3701 405 1306 281
			1318 416 2377
		}
	}

	woods = {
		movement_cost = 1.25

		color = { 165 205 108 }

		combat_width = -0.35
		supply_limit = 6
		type = forest
		sound_type = forest
		
		local_development_cost = 0.15
		nation_designer_cost_multiplier = 0.9
		
		terrain_override = {
			428 2536 2332 191 2365 2234 1388 171 170 2329 4008 169 8 26 18 2 1258 
			27 28 32 3401 1262 31 29 30 1256 271 283 128 3245 3987 3984 3787 161 2378
		}
	}
	
	mountain = {
		movement_cost = 2.0
		defence = 2
		attrition = 2
		local_defensiveness = 0.45
		color = { 117 108 119 }
		
		combat_width = -0.60
		supply_limit = 5
		type = mountain
		sound_type = mountain
		
		local_development_cost = 0.5
		nation_designer_cost_multiplier = 0.75
		terrain_override = {
			121 205 210 2550 1082 2438 2548 1343 3444 2348 3442 2344 3439 3438 1420 1375 199 
			3434 3435 3437 3436 3427 3428 3426 3423 3424 2340 3418 2067 3420 3421 
			820 158 3612 936 3614 954 3621 3620 3624 889 1823 2199 2167 3570 3669 119 2855 
			1464 1465 3675 3676 875 876 3681 1822 2469 3668 2470 2441 1376 23 371 68 185 166 2625 110 
			2246 2467 2468 3257 3261 2806 2328 665 686 734 2484 693 2250 669 2259 3208 2748 
			3300 3302 3848 1036 3065 1407 1255 2376 1398 3788 1404 3791 2236 2379
			3076 385 384 2665 3445 2347 3441 2740 
		}
	}
	
	impassable_mountains = {
		movement_cost = 8.0
		defence = 6
		sound_type = desert
		
		color = { 128 128 128 }
		
		local_development_cost = 10
	}

	grasslands = {
		movement_cost = 1.0

		supply_limit = 7
		type = plains
		sound_type = plains
		color = { 211 201 184 }
		
		allowed_num_of_buildings = 1
		nation_designer_cost_multiplier = 1
		terrain_override = {
			3028 3888 2666 2544 2660 2951 426 
			779 778 3454 2451 781 3452 789 790 1346 3866 1057 3265 3267 4042 
			713 2266 3258 3259 3262 2276 2248 4038 2253 3268 720 622 623 641 
			4093 2649 
			1391 1429 2634 2837 2839 3397 2776 2877 2875 2873 1110 2870 3553 3382 134 1510 83 386 2361 
			1663 1419 893 897 884 97 1345 108 146 1285 3870 2856 2853 3861 3342 
		}
	}

	jungle = {
		movement_cost = 2.0
		defence = 1
		attrition = 2

		combat_width = -0.35
		supply_limit = 5
		type = jungle
		sound_type = jungle
		color = { 40 180 149 }
		
		local_development_cost = 0.35
		nation_designer_cost_multiplier = 0.8
		terrain_override = {
			2890 2885 2787 1128 1129 2768 607 2397 2419 
			3455 3477 3482 758 3473 3461 2075 2071 801 775 774 3488 3491 
			3456 3457 766 1126 1142 1146 1154 1156 2980 2303 674 3234 2424 
			3915 2094 2095 3916 620 3921 
		}
	}	
	
	marsh = {
		movement_cost = 2.0
		defence = 1
		attrition = 1

		combat_width = -0.4
		supply_limit = 5
		type = marsh
		sound_type = forest
		color = { 76 112 105 }
		
		local_development_cost = 0.25
		nation_designer_cost_multiplier = 0.85
		terrain_override = {
			315 362 2891 3597 289 2840 2839 1394 200 201 3721 3722 3084 4092 
			
		}
	}
	
	desert = {
		movement_cost = 2
		attrition = 2
		local_defensiveness = -0.1
		
		supply_limit = 4
		type = desert
		sound_type = desert
		
		color = { 220 210 0 }
		
		local_development_cost = 0.35
		nation_designer_cost_multiplier = 0.8
		terrain_override = {
			2743 2119 2268 3274 3305 2747 2746 
			3306 461 477 3309 3311 2223 471 3299 3295 3298 3283 455 438 443 3286 
			462 439 441 386 3083 3092 3849 446 390 3839 432 3095 3096 3094 3097 
			2717 3101 2718 2721 3284 3846 3297 3296 2221 2979 1157 1159 1184 2254 
			3210 3211 739 2257 3275 

			11 1260 3997 19 891 2327 1261 35 315 3994 314 279 278 4023 1072 1075 
			1529 1074 2445 2154 2155 2157 1040 1039 1038 1037 2744 2301 3248 1049 
			3249 1047 1044 1045 1052 3255 1051 1054 1059 2156 1063 1060 1064 1065 
			2153 1066 1078 981 980 3634 890 985 991 1009 2197 997 4202 4203 4204 
			2481 1000 1003 994 3638 3639 3641 3651 3652 992 2196 3657 1001 4205 
			3656 1002 3655 3654 3653 1007 3661 1006 1005 3662 1010 1008 3578 910 
			908 1011 3663 2168 909 2169 2189 2190 3660 1004 3640 3644 3250 3251 
			3270 2161 2159 999 1517 995 996 4206 4208 4209 4200 4194 1055 4195 1062 
			1056 4197 

			33 277 2441 2152 2158  1036 1035 1034 1530 979 978 2194 2193 3670 2475 
			2160 1104 998 
		}
	}
	
	coastline = {
		movement_cost = 1.0

		supply_limit = 5
		local_defensiveness = 0.10
		
		sound_type = sea
		
		color = { 200 128 0 }

		local_development_cost = 0.1
		nation_designer_cost_multiplier = 0.95
		terrain_override = {
			112 3500 741 3499 1481 743 744 1490 745 746 747 1483 3492 742 2752 
			3491 748 749 3489 3486 753 3485 754 3480 1485 3479 755 3476 756 3470 
			757 3467 3466 761 762 764 3464 765 767 768 770 772 776 751 1484 1482 
			2185 3677 2184 3679 872 870 869 1824 3690 871 3689 878 1735 3894 3893 
			1228 1521 1519 1227 3898 3897 3896 3895 172 67 16 2515 9 1 25 1257 
			20 1250 1251 977 2191 2192 96 4008 2452 2453 3868 1415 126 1533 228 
			334 2896 2869 2870 2962 676 677 1332 1582 1163 2881 
			1164 1581 1165 1166 2982 2986 2983 1177 2985 1180 1151 2994 1181 1564 
			1190 1191 1186 1187 1188 1565 2352 2995 1195 3000 2996 2998 1198 1566 
			3032 3033 1199 3034 3035 3036 3037 3054 3039 1200 3050 1201 3047 3044 
			1574 3006 1202 1506 2981 2879 2975 1140 1505 1504 2775 1141 3816 
			1138 1139 2779 1549 2776 2790 2786 2885 2890 2877 1126 2878 2875 2572 
			2758 2874 2892 1125 1123 2894 1119 2768 1120 2767 2872 2773 2883 2218 
			4040 4041 1110 1114 2871 1499 2882 1514 3719 1417 369 3875 1435 164 320 
			3383 1436 142 2434 
		}
	}
	
	coastal_desert = {
		color = { 255 211 110 }

		type = desert
		sound_type = desert

		movement_cost = 1.0
		local_development_cost = 0.25
		nation_designer_cost_multiplier = 0.9		
		supply_limit = 4

		terrain_override = {
			380 3077 474 388 4095 1211 3005 2650 1571 3043 
		}	
	}

	#drylands
	drylands = {
		movement_cost = 1.00
		
		supply_limit = 9
		type = plains
		sound_type = plains
		color = { 208 158 108  }

		local_development_cost = 0.05
		nation_designer_cost_multiplier = 0.95		
		terrain_override = {
			866 865 1733 1466 877 879 3543 862 3542 880 881 2638 2639 882 3540 
			1467 3541 1734 860 885 887 3549 886 780 3453 868 867 2166 848 
			853 1463 3539 858 3547 863 3548 1461 888 3551 1418 3552 722 2267 718
		}
	}

	#highlands
	highlands = {
		movement_cost = 1.50
		defence = 1
		local_defensiveness = 0.2
		color = { 135 70 0 }

		combat_width = -0.45
		supply_limit = 6
		type = hills
		sound_type = mountain
		
		local_development_cost = 0.2
		nation_designer_cost_multiplier = 0.9
		terrain_override = {
			702 2252 2256 2170 2162 2260 2258 3207 2263 2262 2261 252 253 3338 2512
			2225 3079 414 429 1313 3083 3092 1314 463
		} 
	}

	savannah = {
		movement_cost = 1.00
			
		supply_limit = 7
		type = plains
		sound_type = plains
		color = { 201 214 148  }

		local_development_cost = 0.15
		nation_designer_cost_multiplier = 0.95			
		terrain_override = {
			1487 3451 781 1170 2982 2986 2983 1177 2989 2988 2991 1586 2970 1179 1507 
			2992 2968 1178 1183 1174 2993 2990 2969 1150 1579 1578 1193 1192 1492 1189 
			1153 
		}
	}
	
	#steppe
	steppe = {
		movement_cost = 1.00
		
		supply_limit = 7
		type = plains
		sound_type = plains
		color = { 185 106 210  }
		
		local_development_cost = 0.10
		nation_designer_cost_multiplier = 0.95
		terrain_override = {
			1303 287 303 1309 1304 1297 302 4020 1296 1308 4016 1307 1311 3310 
			1083 1312 3287 1081 1073 475 1080 2442 472 1076 3304 479 3308 1071 
			480 3276 1069 2270 1067 2271 3272 1061 3271 3270 3269 719 3266 3264 
			721 3260 726 3256 723 1488 3448 3449 3450 3277 3312 2444 4199 4198 
			2443 286 2437 2439 1287 2545 4018 4021 4019 300 285 1431 1302 
			3796 2549 284 3794 160 1269 1400 2626 3793 1433 2838 261 2848 
			280 299 785 3447 276 1077 1070 2656 282 3273 1066 2446 1079 4201 4110
		}
	}	
}
	
##################################################################
### Graphical terrain
###		type	=	refers to the terrain defined above, "terrain category"'s 
### 	color 	= 	index in bitmap color table (see terrain.bmp)
###

terrain
{
	grasslands			= { type = grasslands		color = { 	0	 } }
	hills				= { type = hills			color = { 	1	 } }
	desert_mountain		= { type = mountain			color = { 	2	 } }
	desert				= { type = desert			color = { 	3	 } }

	plains				= { type = grasslands		color = { 	4	 } }
	terrain_5			= { type = grasslands		color = { 	5	 } }
	mountain			= { type = mountain			color = { 	6	 } }
	desert_mountain_low	= { type = desert			color = { 	7	 } }

	terrain_8			= { type = hills			color = { 	8	 } }
	marsh				= { type = marsh			color = { 	9	 } }
	terrain_10			= { type = farmlands		color = { 	10	 } }
	terrain_11			= { type = farmlands		color = { 	11	 } }

	forest_12			= { type = forest			color = { 	12	 } }
	forest_13			= { type = forest			color = { 	13	 } }
	forest_14			= { type = forest			color = { 	14	 } }
	ocean				= { type = ocean			color = { 	15	 } }

	snow				= { type = mountain 		color = { 	16	 } } # (SPECIAL CASE) Used to identify permanent snow
	inland_ocean_17 	= { type = inland_ocean		color = {	17	 } }

	coastal_desert_18	= { type = coastal_desert	color = { 	19	 } }
	coastline			= { type = coastline		color = { 	35	 } }
	
	savannah			= { type = savannah 		color = {	20	 } }
	drylands			= { type = drylands			color = {	22	 } }
	highlands			= { type = highlands		color = {	23	 } }
	dry_highlands		= { type = highlands		color = {	24	 } }
	
	woods				= { type = woods			color = { 	255	 } }
	jungle				= { type = jungle			color = { 	254	 } }
	
	terrain_21			= { type = farmlands		color = { 	21	 } }	
}

##################################################################
### Tree terrain
###		terrain	=	refers to the terrain tag defined above
### 	color 	= 	index in bitmap color table (see tree.bmp)
###

tree
{
	forest				= { terrain = forest 			color = { 	3 4 6 7 19 20	} }
	woods				= { terrain = woods 			color = { 	2 5 8 18	} }
	jungle				= { terrain = jungle 			color = { 	13 14 15	} }
	palms				= { terrain = desert 			color = { 	12	} }
	savana				= { terrain = grasslands 		color = { 	27 28 29 30	} }
}