name="MEIOU and Taxes RMG-Compatibility Module v2.0"
path="mod/MEIOUandTaxesRMG"
dependencies=
{
	"MEIOU and Taxes 1.26"
}
tags=
{
	"Map" 
	"MEIOU and Taxes"
}
supported_version="1.18"
picture="MEIOUandTaxesRMG.jpg"
