name="M&T Cossacks Content DLC Support"
path="mod/MEIOUandTaxes_cossacks_content_DLC_support"
dependencies={
	"MEIOU and Taxes 1.27"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesCC.jpg"
supported_version="1.19.*.*"
