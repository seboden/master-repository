# 2183 - Maliseet

culture = miqmaq
religion = totemism
capital = "Maliseet"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1.0
native_size = 10
native_ferocity = 1
native_hostileness = 3

1609.1.1  = {  } # Samuel de Champlain
1629.1.1  = {  }
1650.1.1 = {
	owner = ABE
	controller = ABE
	add_core = ABE
	is_city = yes
	trade_goods = fur
} #Extent of the Abenaki/Wabanaki c. the Beaver Wars
1707.5.12 = {  }
1707.5.12 = {  }
