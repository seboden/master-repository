# No previous file for Wahnapitee

culture = anishinabe
religion = totemism
capital = "Wahnapitee"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 25
native_ferocity = 1 
native_hostileness = 6

1650.1.1 = {
	owner = WCR
	controller = WCR
	add_core = WCR 
	trade_goods = fur
	is_city = yes
} #Before the Beaver Wars
