# No previous file for Tawehash

culture = caddo
religion = totemism
capital = "Tawehash"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 20
native_ferocity = 3
native_hostileness = 6

1760.1.1  = {	owner = OSA
		controller = OSA
		add_core = OSA
		is_city = yes } #Arkansas Osage migrate southward
