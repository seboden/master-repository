# No previous file for Towakoni

culture = apache
religion = totemism
capital = "Towakomi"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 5
native_ferocity = 4
native_hostileness = 8

1710.1.1 = { 	owner = APA
		controller = APA
		is_city = yes
		trade_goods = wool
		add_core = APA } #Plains tribe expand

