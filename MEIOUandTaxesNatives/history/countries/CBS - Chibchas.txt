# CBS - Chibchas

government = tribal_monarchy
mercantilism = 0.1
technology_group = mesoamerican
primary_culture = muisca
religion = aztec_reformed
capital = 832

1000.1.1 = {
	set_variable = { which = "centralization_decentralization" value = 5 }
}

1349.1.1 = {
	monarch = {
		name = "Monarch"
		#dynasty = "Xiu"
		ADM = 5
		DIP = 3
		MIL = 5
	}
}
