Events/EventChain/DHE/Decisions (POL/PLC):
Date	Name
1364	Congress of Krak�w (Wierzynek)
1364	University of Krak�w
1385	Union of Krewo
1412	Treaty of Lubowla
1413	Treaty of Horodlo
1466	13YW
1520	Sigismund Bell
1525	Secularisation of TO
1561	Secularisation of LVO
1569	Union of Lublin
1569	No heir -> Elective monarchy (introduced with union of Lublin, but date mostly irrelevant)
1573	Warszawa Confederation (date only approximate, basically after reformation) -> mostly covered by awesome dharper in DG's edict_of_tolerance.
1595	Union of Brest
1596	Move royal residence to Warszawa (Wawel fire event), instead of decision.
1604	Dymitriads (Fight for influence in Russia/Muscovy) (?)
1648	Zaporizhia Sich/Cossack uprisings
?   	Crimean Tatar raids(16-17th century?)
1658	Commonwealth of Three Nations - Treaty of Hadiach (Polish-Lithuanian-Ruthenian Commonwealth)
1791	3rd May constitution + Targowica
1772	Partitions

Events/EventChain/DHE/Decisions (Other):
	Danzig rebelions/priviledges

przyjecie Jadwigi spowoduje ze w p�xniejszym czasie b�dize mo�liwe obscure documents na wegry

�ancuch przywilej�w
- przywilej co� daje, co� zabiera, je�li pierwszy przywilej nie zostal przyj�ty to pojawi si� za X lat, (my�la�em tutaj o  triggerze podczas wojny, tak aby nie by�o za �atwo go odrzuci�)
 a)je�li przyjmiemy pierwszym, to znowu pojawi si� kolejny, i np po przyjeciu 4, pojawia sie event odnosnie EM,
 b) mo�na usuwac przywileje, poprzez decyzje. Przyj��e� przywilej, pojawia si� w decyzji cofni�cie go w oparciu o jakie� kary.

 tolerancja religijna 
 - Polska s�yne�a ze swojej tolerancji, my�la�em o zasumulowaniu tego. 
 
 Jagiella obejmuje tron i rozszerza prawa �yd�w na Litw�, dodanie paru gmin �ydowskich
Missions


	POL
		Red Ruthenia (Kazimierz III)
		Danzig/Pommeleren/Mazovia/Plock (overhaul of vanilla missions)
		Silesia (with massive AE/DIP relation hit as Kazimierz previously renounced all claims to Silesia in exchange for John of Luxembourg renouncing claims on the Crown of Poland)
		
	LIT
		Smole�sk
		Kiev
		Protect against the hordes/Get access to Black Sea
		
	TEU/LVO
		Pagan crusade ?
		
	DNZ/RIG
		Hanseatic league ?
	
	PLC
		Wielki Go�ciniec Litewski
		
Other:
	elective_monarchy improvements
	
	revise events for Univ of Vilnus and Zamoyski Academy
	
	Foreign section decision/event for PLC giving a timed discount of MIL tech?
	
NOT RELATED:
	generic event for university foreign students
	
	province event with a 5000MTTH for universities giving an advisor (one event for each non-military type)
